#ifndef AMR_CS5463
#define AMR_CS5463




#define nreset		PORTEbits.RE0
#define INT1_ADC	PORTEbits.RE5


#define	PCB5	0xf0						
#define	PCB1	0xf1

#define	VRN	240.00												// DEFINE CALIBRATION VOLTAGE FOR RPHASE 

#define	IR	3.000								    			// DEFINE CALIBRATION CURRENT FOR RPHASE

//#define PULSES	0.1											// DEFINE CALIBRATION FOR PULSES FOR 3P4W
#define PULSES	0.03											// DEFINE CALIBRATION FOR PULSES FOR 3P4W
#define PULSES_120 0.015

//#define CREEP_LIMIT	0.030										// DEFINE MINIMUM CURRENT LIMIT
#define CREEP_LIMIT	0.050										// DEFINE MINIMUM CURRENT LIMIT

#define MIN_CREEP_VALUE 0.006
#define	CT_PRIMARY		0x00									// DEFINE EEPROM ADDRESS FOR CT_PRIMARY VARIALBE STORAGE
#define	CT_SECONDARY	0X03									// DEFINE EEPROM ADDRESS FOR CT_SECONDARY  DATA STORAGE
#define	PT_RATIO		0X06									// DEFINE EEPROM ADDRESS FOR PT_RATIO VARIALBE STORAGE

#define	VR_MF			0X30									// DEFINE EEPROM ADDRESS FOR STATION ID VARIALBE STORAGE
#define	IR_MF			0X3f									// DEFINE EEPROM ADDRESS FOR STATION ID VARIALBE STORAGE
#define PULSES_R		0x4e									// DEFINE EEPROM ADDRESS FOR STATION ID VARIALBE STORAGE
#define KWHCOUNT_R		0x5d

#define PHASE_ANGLE_MF  (180/3.1428571428571428571428571428571)

extern uint8_t data[]; //data buffer for read and write
extern uint8_t EmRxcount;
extern unsigned long int pulses_r;
extern float newkwh;
extern float diff;
extern unsigned int address;

extern unsigned char datalow, datahigh, datamid;

extern unsigned char zempbyte;

extern unsigned long int zemplong;
extern unsigned long int reg_data_r[5];
extern unsigned char tempbyte;
extern unsigned char intesec;

extern float base_val;
extern float zempdoub;

extern float kbase;

extern float irdoub;
extern float vry;
extern float pf;
extern float va1;
extern float w1;
extern float var1;
extern float kw1;
extern float kwh;

extern float mf1;
extern float mf2;
extern float mf3;

extern float vrdoub;

extern BYTE CAL_page;
extern unsigned char power_on_flag;
extern unsigned int emt_int_count, Kwh_count;
extern unsigned char check_day_burnar_flag; //SAN 
extern unsigned int em_reset_counter;
extern unsigned long int Em_status_reg;
extern char temperature;
extern uint8_t ReadEM;
void init_cs5463(void);
unsigned char receive_byte(void);
void transfer_byte(unsigned char);
void read_a_reg(unsigned char, unsigned char);
void write_to_reg(unsigned char, unsigned char, unsigned char, unsigned char, unsigned char);
void write_to_all_regs(unsigned char, unsigned char, unsigned char, unsigned char);
void calculate_3p4w(void);
float get_ieee(unsigned long int);
void calculate_kwh(void);
void Read_EnergyMeter(void);
void set_mf_energy(void);
void calibration_Energy(void);
void delay(unsigned int);
float Get_Value_from_Raw_Data(unsigned long int Rawdata, char Signed_num, float Weight);
void calculate_kwh_LampOFF(void);
#endif


#include <xc.h>
#include <stdbool.h>
#define FCY 32000000UL
#include <libpic30.h>

#define PCF85363_ADDR 0x51
#define PCF85363_DATETIME 1


/* Default values */
#define baudRate_default  600
#define baudRate_9600     9600
#define baudRate_115200   115200

// all comands templates
#define readByte        0b00000000
#define writeByte       0b01000000
#define pageByte        0b10000000
#define instructionByte 0b11000000

typedef enum AfeGainI_e {
    I_GAIN_10x = 0x00,
    I_GAIN_50x = 0x01

} AfeGainI_t;

void WRITE_CS5480(int page, int address, uint32_t value);
void READ_CS5480(int page, int address);
void instruct(int instruction);
void begin(int baudRate);
void clearSerialBuffer();
uint32_t concatData();
uint32_t getRegChk(void);


/*** Instructions ***/
void reset();
void wakeUp();
void contConv();
void hardwareReset(void);


/*** Configuration ***/
long getBaudRate();
void setBaudRate(uint16_t value);
void delay(uint16_t i);
uint32_t readReg(int page, int address);

//void init_CS5480(void);
char CS_wait_and_clr_DRDY(void);
void CS_clear_DRDY(void);
char CS_wait_for_DRDY(void);

