
#include "SC16IS740.h"

int peek_buf[2] = {-1, -1};
uint8_t peek_flag[2] = {0, 0};
uint8_t fifo_available[2] = {0, 0};
uint8_t device_address_sspin;
uint8_t protocol;
uint8_t initialized = 0;
//void begin(uint32_t baud_A, uint32_t baud_B)
//{
//  Initialize(); // Force initialize, since we're initializing both channels at once
//  beginA(baud_A);
//  beginB(baud_B);
//}

void beginA(uint32_t baud_A)
    {
    if (!initialized)
        {
        Initialize();
        }
    FIFOEnable(SC16IS740_CHANNEL_A, 1);
    SetBaudrate(SC16IS740_CHANNEL_A, baud_A);
    SetLine(SC16IS740_CHANNEL_A, 8, 0, 1);
    }

void beginB(uint32_t baud_B)
    {
    if (!initialized)
        {
        Initialize();
        }
    FIFOEnable(SC16IS740_CHANNEL_B, 1);
    SetBaudrate(SC16IS740_CHANNEL_B, baud_B);
    SetLine(SC16IS740_CHANNEL_B, 8, 0, 1);
    }

int available(uint8_t channel)
    {
    return FIFOAvailableData(channel);
    }

int read(uint8_t channel)
    {
    if (peek_flag[channel] == 0)
        {
        return ReadByte(channel);
        }
    peek_flag[channel] = 0;
    return peek_buf[channel];
    }

uint8_t write(uint8_t channel, uint8_t val)
    {
    WriteByte(channel, val);
    return 1;
    }

//void pinMode(uint8_t pin, uint8_t i_o)
//{
//  GPIOSetPinMode(pin, i_o);
//}
//
//void digitalWrite(uint8_t pin, uint8_t value)
//{
//  GPIOSetPinState(pin, value);
//}
//
//uint8_t digitalRead(uint8_t pin)
//{
//  return GPIOGetPinState(pin);
//}

/*    UART_I2C_start();
    UART_I2C_write(IC_ADDRESS);
    UART_I2C_write(SUB_ADDRESS);
    UART_I2C_write(value);
    UART_I2C_stop();*/

uint8_t ReadRegister(uint8_t channel, uint8_t reg_addr)
    {
    uint8_t result = 0;
    UART_I2C_start();
    UART_I2C_write(SC16IS740_ADDRESS_AA); /* write slave address + write */
    UART_I2C_write((reg_addr << 3 | channel << 1)); /* write register address, 1st clock register */
    UART_I2C_stop();
    UART_I2C_start();
    UART_I2C_write(SC16IS740_ADDRESS_AA | 1); /* write slave address + read */
    result = UART_I2C_read(ACK); /* starts w/last address stored in register pointer */



#ifdef  SC16IS740_DEBUG_PRINT
    Serial.print("ReadRegister channel=");
    Serial.print(channel, HEX);
    Serial.print(" reg_addr=");
    Serial.print((reg_addr << 3 | channel << 1), HEX);
    Serial.print(" result=");
    Serial.println(result, HEX);
#endif // ifdef  SC16IS740_DEBUG_PRINT
    return result;
    }

void WriteRegister(uint8_t channel, uint8_t reg_addr, uint8_t val)
    {
#ifdef  SC16IS740_DEBUG_PRINT
    Serial.print("WriteRegister channel=");
    Serial.print(channel, HEX);
    Serial.print(" reg_addr=");
    Serial.print((reg_addr << 3 | channel << 1), HEX);
    Serial.print(" val=");
    Serial.println(val, HEX);
#endif // ifdef  SC16IS740_DEBUG_PRINT

    UART_I2C_start();
    UART_I2C_write(SC16IS740_ADDRESS_AA); /* write slave address + write */
    UART_I2C_write(reg_addr << 3); /* write register address, 1st clock register */
    UART_I2C_write(val);
    UART_I2C_stop();
    }

void Initialize()
    {

    ResetDevice();
    initialized = 1;
    }

int16_t SetBaudrate(uint8_t channel, uint32_t baudrate) // return error of baudrate parts per thousand
    {
    uint16_t divisor;
    uint8_t prescaler;
    uint32_t actual_baudrate;
    int16_t error;
    uint8_t temp_lcr;

    if ((ReadRegister(channel, SC16IS740_REG_MCR) & 0x80) == 0)
        { // if prescaler==1
        prescaler = 1;
        }
    else
        {
        prescaler = 4;
        }

    divisor = (SC16IS740_CRYSTCAL_FREQ / prescaler) / (baudrate * 16);

    temp_lcr = ReadRegister(channel, SC16IS740_REG_LCR);
    temp_lcr |= 0x80;
    WriteRegister(channel, SC16IS740_REG_LCR, temp_lcr);

    // write to DLL
    WriteRegister(channel, SC16IS740_REG_DLL, (uint8_t) divisor);

    // write to DLH
    WriteRegister(channel, SC16IS740_REG_DLH, (uint8_t) (divisor >> 8));
    temp_lcr &= 0x7F;
    WriteRegister(channel, SC16IS740_REG_LCR, temp_lcr);


    actual_baudrate = (SC16IS740_CRYSTCAL_FREQ / prescaler) / (16 * divisor);
    error = ((float) actual_baudrate - baudrate) * 1000 / baudrate;
#ifdef  SC16IS740_DEBUG_PRINT
    Serial.print("Desired baudrate: ");
    Serial.println(baudrate, DEC);
    Serial.print("Calculated divisor: ");
    Serial.println(divisor, DEC);
    Serial.print("Actual baudrate: ");
    Serial.println(actual_baudrate, DEC);
    Serial.print("Baudrate error: ");
    Serial.println(error, DEC);
#endif // ifdef  SC16IS740_DEBUG_PRINT

    return error;
    }

void SetLine(uint8_t channel, uint8_t data_length, uint8_t parity_select, uint8_t stop_length)
    {
    uint8_t temp_lcr;

    temp_lcr = ReadRegister(channel, SC16IS740_REG_LCR);
    temp_lcr &= 0xC0; // Clear the lower six bit of LCR (LCR[0] to LCR[5]
#ifdef  SC16IS740_DEBUG_PRINT
    Serial.print("LCR Register:0x");
    Serial.println(temp_lcr, DEC);
#endif // ifdef  SC16IS740_DEBUG_PRINT

    switch (data_length)
        { // data length settings
        case 5:
            break;
        case 6:
            temp_lcr |= 0x01;
            break;
        case 7:
            temp_lcr |= 0x02;
            break;
        case 8:
            temp_lcr |= 0x03;
            break;
        default:
            temp_lcr |= 0x03;
            break;
        }

    if (stop_length == 2)
        {
        temp_lcr |= 0x04;
        }

    switch (parity_select)
        { // parity selection length settings
        case 0: // no parity
            break;
        case 1: // odd parity
            temp_lcr |= 0x08;
            break;
        case 2: // even parity
            temp_lcr |= 0x18;
            break;
        case 3: // force '1' parity
            temp_lcr |= 0x03;
            break;
        case 4: // force '0' parity
            break;
        default:
            break;
        }

    WriteRegister(channel, SC16IS740_REG_LCR, temp_lcr);
    }

//void GPIOSetPinMode(uint8_t pin_number, uint8_t i_o)
//{
//  uint8_t temp_iodir;
//
//  temp_iodir = ReadRegister(SC16IS740_CHANNEL_BOTH, SC16IS740_REG_IODIR);
//
//  if (i_o == OUTPUT) {
//    temp_iodir |= (0x01 << pin_number);
//  } else {
//    temp_iodir &= (uint8_t) ~(0x01 << pin_number);
//  }
//
//  WriteRegister(SC16IS740_CHANNEL_BOTH, SC16IS740_REG_IODIR, temp_iodir);
//}
//
//void GPIOSetPinState(uint8_t pin_number, uint8_t pin_state)
//{
//  uint8_t temp_iostate;
//
//  temp_iostate = ReadRegister(SC16IS740_CHANNEL_BOTH, SC16IS740_REG_IOSTATE);
//
//  if (pin_state == 1) {
//    temp_iostate |= (0x01 << pin_number);
//  } else {
//    temp_iostate &= (uint8_t) ~(0x01 << pin_number);
//  }
//
//  WriteRegister(SC16IS740_CHANNEL_BOTH, SC16IS740_REG_IOSTATE, temp_iostate);
//}
//
//uint8_t GPIOGetPinState(uint8_t pin_number)
//{
//  uint8_t temp_iostate;
//
//  temp_iostate = ReadRegister(SC16IS740_CHANNEL_BOTH, SC16IS740_REG_IOSTATE);
//
//  if ((temp_iostate & (0x01 << pin_number)) == 0) {
//    return 0;
//  }
//  return 1;
//}
//
//uint8_t GPIOGetPortState(void)
//{
//  return ReadRegister(SC16IS740_CHANNEL_BOTH, SC16IS740_REG_IOSTATE);
//}
//
//void GPIOSetPortMode(uint8_t port_io)
//{
//  WriteRegister(SC16IS740_CHANNEL_BOTH, SC16IS740_REG_IODIR, port_io);
//}
//
//void GPIOSetPortState(uint8_t port_state)
//{
//  WriteRegister(SC16IS740_CHANNEL_BOTH, SC16IS740_REG_IOSTATE, port_state);
//}
//
//void SetPinInterrupt(uint8_t io_int_ena)
//{
//  WriteRegister(SC16IS740_CHANNEL_BOTH, SC16IS740_REG_IOINTENA, io_int_ena);
//}

void ResetDevice()
    {
    uint8_t reg;

    reg = ReadRegister(SC16IS740_CHANNEL_BOTH, SC16IS740_REG_IOCONTROL);
    reg |= 0x08;
    WriteRegister(SC16IS740_CHANNEL_BOTH, SC16IS740_REG_IOCONTROL, reg);
    }

void ModemPin(uint8_t gpio) // gpio == 0, gpio[7:4] are modem pins, gpio == 1 gpio[7:4] are gpios
    {
    uint8_t temp_iocontrol;

    temp_iocontrol = ReadRegister(SC16IS740_CHANNEL_BOTH, SC16IS740_REG_IOCONTROL);

    if (gpio == 0)
        {
        temp_iocontrol |= 0x02;
        }
    else
        {
        temp_iocontrol &= 0xFD;
        }
    WriteRegister(SC16IS740_CHANNEL_BOTH, SC16IS740_REG_IOCONTROL, temp_iocontrol);
    }

void GPIOLatch(uint8_t latch)
    {
    uint8_t temp_iocontrol;

    temp_iocontrol = ReadRegister(SC16IS740_CHANNEL_BOTH, SC16IS740_REG_IOCONTROL);

    if (latch == 0)
        {
        temp_iocontrol &= 0xFE;
        }
    else
        {
        temp_iocontrol |= 0x01;
        }
    WriteRegister(SC16IS740_CHANNEL_BOTH, SC16IS740_REG_IOCONTROL, temp_iocontrol);
    }

void InterruptControl(uint8_t channel, uint8_t int_ena)
    {
    WriteRegister(channel, SC16IS740_REG_IER, int_ena);
    }

uint8_t InterruptPendingTest(uint8_t channel)
    {
    return ReadRegister(channel, SC16IS740_REG_IIR) & 0x01;
    }

void __isr(uint8_t channel)
    {
    uint8_t irq_src;

    irq_src = ReadRegister(channel, SC16IS740_REG_IIR);
    irq_src = (irq_src >> 1);
    irq_src &= 0x3F;

    switch (irq_src)
        {
        case 0x06: // Receiver Line Status Error
            break;
        case 0x0c: // Receiver time-out interrupt
            break;
        case 0x04: // RHR interrupt
            break;
        case 0x02: // THR interrupt
            break;
        case 0x00: // modem interrupt;
            break;
        case 0x30: // input pin change of state
            break;
        case 0x10: // XOFF
            break;
        case 0x20: // CTS,RTS
            break;
        default:
            break;
        }
    }

void FIFOEnable(uint8_t channel, uint8_t fifo_enable)
    {
    uint8_t temp_fcr;

    temp_fcr = ReadRegister(channel, SC16IS740_REG_FCR);

    if (fifo_enable == 0)
        {
        temp_fcr &= 0xFE;
        }
    else
        {
        temp_fcr |= 0x01;
        }
    WriteRegister(channel, SC16IS740_REG_FCR, temp_fcr);
    }

void FIFOReset(uint8_t channel, uint8_t rx_fifo)
    {
    uint8_t temp_fcr;

    temp_fcr = ReadRegister(channel, SC16IS740_REG_FCR);

    if (rx_fifo == 0)
        {
        temp_fcr |= 0x04;
        }
    else
        {
        temp_fcr |= 0x02;
        }
    WriteRegister(channel, SC16IS740_REG_FCR, temp_fcr);
    }

void FIFOSetTriggerLevel(uint8_t channel, uint8_t rx_fifo, uint8_t length)
    {
    uint8_t temp_reg;

    temp_reg = ReadRegister(channel, SC16IS740_REG_MCR);
    temp_reg |= 0x04;
    WriteRegister(channel, SC16IS740_REG_MCR, temp_reg); // SET MCR[2] to '1' to use TLR register or trigger level control in FCR
    // register

    temp_reg = ReadRegister(channel, SC16IS740_REG_EFR);
    WriteRegister(channel, SC16IS740_REG_EFR, temp_reg | 0x10); // set ERF[4] to '1' to use the  enhanced features

    if (rx_fifo == 0)
        {
        WriteRegister(channel, SC16IS740_REG_TLR, length << 4); // Tx FIFO trigger level setting
        }
    else
        {
        WriteRegister(channel, SC16IS740_REG_TLR, length); // Rx FIFO Trigger level setting
        }
    WriteRegister(channel, SC16IS740_REG_EFR, temp_reg); // restore EFR register
    }

uint8_t FIFOAvailableData(uint8_t channel)
    {
#ifdef  SC16IS740_DEBUG_PRINT
    Serial.print("=====Available data:");
    Serial.println(ReadRegister(channel, SC16IS740_REG_RXLVL), DEC);
#endif // ifdef  SC16IS740_DEBUG_PRINT
    if (fifo_available[channel] == 0)
        {
        fifo_available[channel] = ReadRegister(channel, SC16IS740_REG_RXLVL);
        }
    return fifo_available[channel];
    //    return ReadRegister(channel, SC16IS740_REG_LSR) & 0x01;
    }

uint8_t FIFOAvailableSpace(uint8_t channel)
    {
    return ReadRegister(channel, SC16IS740_REG_TXLVL);
    }

void WriteByte(uint8_t channel, uint8_t val)
    {
    uint8_t tmp_lsr;

    /*   while ( FIFOAvailableSpace(channel) == 0 ){
     #ifdef  SC16IS740_DEBUG_PRINT
                   Serial.println("No available space");
     #endif
           };
     #ifdef  SC16IS740_DEBUG_PRINT
       Serial.println("++++++++++++Data sent");
     #endif
       WriteRegister(SC16IS740_REG_THR,val);
     */
    do
        {
        tmp_lsr = ReadRegister(channel, SC16IS740_REG_LSR);
        }
    while ((tmp_lsr & 0x20) == 0);

    WriteRegister(channel, SC16IS740_REG_THR, val);
    }

int ReadByte(uint8_t channel)
    {
    volatile uint8_t val;

    if (FIFOAvailableData(channel) == 0)
        {
#ifdef  SC16IS740_DEBUG_PRINT
        Serial.println("No data available");
#endif // ifdef  SC16IS740_DEBUG_PRINT
        return -1;
        }
    else
        {
#ifdef  SC16IS740_DEBUG_PRINT
        Serial.println("***********Data available***********");
#endif // ifdef  SC16IS740_DEBUG_PRINT
        if (fifo_available[channel] > 0)
            {
            --fifo_available[channel];
            }
        val = ReadRegister(channel, SC16IS740_REG_RHR);
        return val;
        }
    }

void EnableTransmit(uint8_t channel, uint8_t tx_enable)
    {
    uint8_t temp_efcr;

    temp_efcr = ReadRegister(channel, SC16IS740_REG_EFCR);

    if (tx_enable == 0)
        {
        temp_efcr |= 0x04;
        }
    else
        {
        temp_efcr &= 0xFB;
        }
    WriteRegister(channel, SC16IS740_REG_EFCR, temp_efcr);
    }

uint8_t ping()
    {
    WriteRegister(SC16IS740_CHANNEL_A, SC16IS740_REG_SPR, 0x55);

    if (ReadRegister(SC16IS740_CHANNEL_A, SC16IS740_REG_SPR) != 0x55)
        {
        return 0;
        }

    WriteRegister(SC16IS740_CHANNEL_A, SC16IS740_REG_SPR, 0xAA);

    if (ReadRegister(SC16IS740_CHANNEL_A, SC16IS740_REG_SPR) != 0xAA)
        {
        return 0;
        }

    WriteRegister(SC16IS740_CHANNEL_B, SC16IS740_REG_SPR, 0x55);

    if (ReadRegister(SC16IS740_CHANNEL_B, SC16IS740_REG_SPR) != 0x55)
        {
        return 0;
        }

    WriteRegister(SC16IS740_CHANNEL_B, SC16IS740_REG_SPR, 0xAA);

    if (ReadRegister(SC16IS740_CHANNEL_B, SC16IS740_REG_SPR) != 0xAA)
        {
        return 0;
        }

    return 1;
    }

void flush(uint8_t channel)
    {
    uint8_t tmp_lsr;

    do
        {
        tmp_lsr = ReadRegister(channel, SC16IS740_REG_LSR);
        }
    while ((tmp_lsr & 0x20) == 0);
    }

int peek(uint8_t channel)
    {
    if (peek_flag[channel] == 0)
        {
        peek_buf[channel] = ReadByte(channel);

        if (peek_buf[channel] >= 0)
            {
            peek_flag[channel] = 1;
            }
        }

    return peek_buf[channel];
    }

/*
void I2C_SendData(unsigned char *dataPtr, unsigned int len)
    {
    unsigned int datalength=0;
    unsigned char temp_buf2[20]={0};
     WriteData_ModbusUART2("\r\nTx len:", strlen("\r\nTx: len:"));
            sprintf(temp_buf2, "%d ",len);
           WriteData_ModbusUART2(temp_buf2, strlen(temp_buf2));
    for(datalength=0;datalength<=len;datalength++)
        {
        WriteByte(SC16IS740_CHANNEL_A,dataPtr[datalength]);

        
//                    sprintf(temp_buf2, "%02x ",dataPtr[datalength]);
//            WriteData_ModbusUART2(temp_buf2, strlen(temp_buf2));
       }
    }*/
void WriteBust(uint8_t channel,unsigned char *dataPtr, unsigned int len)
    {
    uint8_t tmp_lsr;
    uint8_t datalength=0;
    do
        {
        tmp_lsr = ReadRegister(channel, SC16IS740_REG_LSR);
        }
    while ((tmp_lsr & 0x20) == 0);
    UART_I2C_start();
    UART_I2C_write(SC16IS740_ADDRESS_AA); /* write slave address + write */
    UART_I2C_write(SC16IS740_REG_THR << 3); /* write register address, 1st clock register */
    for(datalength=0;datalength<=len;datalength++)
    {
           UART_I2C_write(dataPtr[datalength]);
    }
    UART_I2C_stop();
    }