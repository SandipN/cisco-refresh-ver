
#include "Compiler.h"
#include "generic.h"
#include "AMR_RTC.h"
#include "Application_logic.h"

//////////////////// DS1339.c ///////////////
/* global variables */

RTCTime Time, PTime, GPS_Time, SSN_GPS_TIME, Local_Time;
RTCDate Date, PDate, GPS_Date, SSN_GPS_DATE, Local_Date, EMDate;

unsigned char day;
volatile unsigned char I2C_ACK = 0;
unsigned char I2C_data_rec_flag = 0, Rec_count = 0;
unsigned char I2C_data_Buff[70] = {0};
volatile unsigned char I2C_Timeout = 0;
unsigned char I2C_datarec_flag = 0;
////////////////////// AMR_RTC.c /////////////////////////

void RTC_delay(unsigned int i)
    {
    volatile unsigned int j, k;
    for (j = 0; j < i; j++)
        {
        for (k = 0; k < 23; k++)//53 RTC n EM
            {
            }
        ClrWdt();
        }
    }

void I2C_start() /* ----------------------------------------------- */
    {
    TRISGbits.TRISG3 = 0;
    sda = 1;
    RTC_delay(10);
    scl = 1; /* Initiate start condition */
    sda = 0;
    RTC_delay(10);
    }

void I2C_stop() /* ----------------------------------------------- */
    {
    TRISGbits.TRISG3 = 0;
    //	sda = 0; 
    //	sda = 0; 
    sda = 0;
    sda = 0; /* Initiate stop condition */
    RTC_delay(5);
    scl = 1;
    scl = 1;
    //	sda = 1;//#rht
    RTC_delay(5);
    }

void I2C_write(uchar d) /* ----------------------------- */
    {
    uchar i, local;
    TRISGbits.TRISG3 = 0;
    scl = 0;
    for (i = 1; i <= 8; i++)
        {
        local = (d >> 7);
        if (local)
            sda = 1;
        else
            sda = 0;

        RTC_delay(5);
        scl = 1;
        d = d << 1; /* increase scl high time */
        RTC_delay(5);
        scl = 0;
        }
    //TRISGbits.TRISG3 = 0;
    //RTC_delay(1); 
    //sda = 1; /* Release the sda line */
    TRISGbits.TRISG3 = 1;
    scl = 0;
    RTC_delay(5);
    scl = 1;

    RTC_delay(1);
    if (PORTGbits.RG3)
        {
        //		printf("Ack bit missing");
        }
    else
        {
        //		printf("Ack bit OK");	
        }
    scl = 0;
    }

uchar I2C_read(uchar b) /* ----------------------------------- */
    {
    uchar d = 0, i, TmpByte = 0;
    TRISGbits.TRISG3 = 1;
    //	sda = 1; /* Let go of sda line */
    scl = 0;
    for (i = 1; i <= 8; i++) /* read the msb first */
        {
        RTC_delay(5);
        scl = 1;
        d = d << 1;
        TmpByte = (unsigned char) PORTGbits.RG3;
        d = d | TmpByte;
        RTC_delay(5);
        scl = 0;

        }
    TRISGbits.TRISG3 = 0;
    //	sda = b; /* Hold sda low for acknowledge */
    sda = 0; /* Hold sda low for acknowledge */
    //scl = 0;//#rht
    RTC_delay(5);
    scl = 1;
    RTC_delay(5);
    if (b == NACK) sda = 1; /* sda = 1 if next cycle is reset */
    scl = 1;
    sda = 1; //#rht
    return d;
    }

void init_PCF85363()//PCF85363
    {
    TRISGbits.TRISG2 = 0; //I2C clock pin, will always remain in output mode.
    TRISGbits.TRISG3 = 0; // No Open Drain config, used as IO change mode.

    LATGbits.LATG2 = 0;
    LATGbits.LATG3 = 0;

    I2C_start();
    I2C_write(ADDRTC); /* write slave address + write */
    I2C_write(0x2E); /* write register address, control register */
    I2C_write(0x00); /* enable osc, bbsqi *///0x20   as per hitesh original 20  //V1.0.18.2
#if defined(SUPER_CAP)
    //I2C_write(0xA9);          /* Trickle charge enable with one diode and 250 ohm resistor  */
#endif
    I2C_stop();
    RTC_delay(40);

    I2C_start();
    I2C_write(ADDRTC);
    I2C_write(0x25);
    I2C_write(0x00); //25       
    I2C_write(0x00); //26       
    I2C_write(0x20); //27       
    I2C_write(0x00); //28       
    I2C_write(0x00); //29       
    I2C_write(0x00); //2A       
    I2C_write(0x00); //2B       
    I2C_write(0x00); //2C       
    I2C_write(0x00); //2D       
    I2C_write(0x00); //2E

    I2C_write(0x00); //2F     
    I2C_stop();
    RTC_delay(40);


    }

void PCF_Force_SW_Reset(void)
    {
    I2C_start();
    I2C_write(ADDRTC);
    I2C_write(0x2E);
    I2C_write(0x01); //2E       
    I2C_write(0x2C); //2F   
    I2C_stop();
    RTC_delay(40);
    I2C_start();
    I2C_write(ADDRTC);
    I2C_write(0x2E);
    I2C_write(0x00); //2E       
    I2C_stop();
    }

void RTC_SET_TIME(RTCTime RTC_Time)
    {

    unsigned char sdHr, sdMin, sdSs, i;

    sdHr = RTC_Time.Hour;
    i = (sdHr / 10);
    sdHr = (sdHr + (i * 6));

    sdMin = RTC_Time.Min;
    i = (sdMin / 10);
    sdMin = (sdMin + (i * 6));

    sdSs = RTC_Time.Sec;
    i = (sdSs / 10);
    sdSs = (sdSs + (i * 6));

    sdHr = sdHr & 0x3f;

    I2C_start();
    I2C_write(ADDRTC); /* write slave address + write */
    I2C_write(0x01); /* write register address, 1st clock register */
    I2C_write(sdSs);
    //RTC_delay(40);

    I2C_write(sdMin);
    //RTC_delay(40);

    I2C_write(sdHr);
    //RTC_delay(40);
    I2C_stop();
    }

void RTC_SET_DATE(RTCDate RTC_Date)
    {

    unsigned char sdDt, sdMn, sdYr, i;

    sdDt = RTC_Date.Date;
    i = (sdDt / 10);
    sdDt = (sdDt + (i * 6));

    sdMn = RTC_Date.Month;
    i = (sdMn / 10);
    sdMn = (sdMn + (i * 6));

    sdYr = RTC_Date.Year;
    i = (sdYr / 10);
    sdYr = (sdYr + (i * 6));

    I2C_start();
    I2C_write(ADDRTC); /* write slave address + write */
    I2C_write(0x04); /* write register address, 1st clock register */
    I2C_write(sdDt);
    I2C_stop();
    I2C_start();
    I2C_write(ADDRTC); /* write slave address + write */
    I2C_write(0x06); /* write register address, 1st clock register */
    I2C_write(sdMn);
    //	delay(40);

    I2C_write(sdYr);
    //	delay(40);
    I2C_stop();
    }

void read_PCF85363_bytes(void)//PCF85363
    {
    //    char PrintStr[80];
    unsigned char temp_sec1, temp_sec2, temp_min1, temp_min2, temp_hour1, temp_hour2;
    unsigned char temp_date1, temp_date2, temp_month1, temp_month2, temp_year1, temp_year2;
    unsigned char SLC_Mix_R_weekday = 0;

    temp_sec1 = ReadRTCReg(0x01);
    temp_min1 = ReadRTCReg(0x02);
    temp_hour1 = ReadRTCReg(0x03);
    temp_date1 = ReadRTCReg(0x04);
    temp_month1 = ReadRTCReg(0x06);
    day = ReadRTCReg(0x05);
    temp_year1 = ReadRTCReg(0x07);

    RTC_delay(40);
    temp_sec2 = ReadRTCReg(0x01);
    temp_min2 = ReadRTCReg(0x02);
    temp_hour2 = ReadRTCReg(0x03);
    temp_date2 = ReadRTCReg(0x04);
    temp_month2 = ReadRTCReg(0x06);
    day = ReadRTCReg(0x05);
    temp_year2 = ReadRTCReg(0x07);

    //    	sprintf(PrintStr,"\r\nT1: %02d/%02d/%02d %02d:%02d:%02d",temp_date1,temp_month1,temp_year1,temp_hour1,temp_min1,temp_sec1);
    //    	UART3_Write(PrintStr,strlen(PrintStr));
    //		sprintf(PrintStr,"\r\nT2: %02d/%02d/%02d %02d:%02d:%02d",temp_date2,temp_month2,temp_year2,temp_hour2,temp_min2,temp_sec2);
    //	UART3_Write(PrintStr,strlen(PrintStr));
    if ((temp_sec1 == temp_sec2) && (temp_min1 == temp_min2) && (temp_hour1 == temp_hour2))
        {
        if ((temp_date1 == temp_date2) && (temp_month1 == temp_month2) && (temp_year1 == temp_year2))
            {

            if ((temp_hour1 > 23) || (temp_min1 > 59) || (temp_sec1 > 59) || (temp_date1 > 31) || (temp_date1 < 1) || (temp_month1 > 12) || (temp_month1 < 1) || (temp_year1 < 1) || (temp_year1 > 99)) // Check RTC time
                {

                }
            else
                {
                Time.Sec = temp_sec1;
                Time.Min = temp_min1;
                Time.Hour = temp_hour1;
                Date.Date = temp_date1;
                Date.Month = temp_month1;
                Date.Year = temp_year1;
                Date.Week = week_day(Date.Year, Date.Date, Date.Month); // V6.1.10

                if (Date.Week == 0) // sunday
                    {
                    SLC_Mix_R_weekday = 1;
                    }
                else if (Date.Week == 1) // monday
                    {
                    SLC_Mix_R_weekday = 64;
                    }
                else if (Date.Week == 2) // tuesday
                    {
                    SLC_Mix_R_weekday = 32;
                    }
                else if (Date.Week == 3) // wenesday
                    {
                    SLC_Mix_R_weekday = 16;
                    }
                else if (Date.Week == 4) // thrusday
                    {
                    SLC_Mix_R_weekday = 8;
                    }
                else if (Date.Week == 5) // friday
                    {
                    SLC_Mix_R_weekday = 4;
                    }
                else if (Date.Week == 6) // seturday
                    {
                    SLC_Mix_R_weekday = 2;
                    }
                else
                    {
                    }


                //				SLC_Mix_R_weekday = 2 ^ Date.Week;
                //				SLC_Mix_R_weekday = 1;
                //				for(i=0;i<Date.Week;i++)
                //				{
                //					SLC_Mix_R_weekday = SLC_Mix_R_weekday * 2;	
                //				}
                //				SLC_Mix_R_weekday = 2 * 2;
                day_sch_R.Val = SLC_Mix_R_weekday;
                }
            }
        }
    }

unsigned char week_day(unsigned int year, unsigned char date, unsigned char month) // V6.1.10
    {
    unsigned char MonDay[12] = {0, 3, 3, 6, 1, 4, 6, 2, 5, 0, 3, 5};
    unsigned char LMonDay[12] = {6, 2, 3, 6, 1, 4, 6, 2, 5, 0, 3, 5};
    unsigned int century = 0;
    unsigned int WYear = 0;
    unsigned char Weekday = 0;
    unsigned char MDay = 0;

    century = 2 * (3 - (20 % 4));
    WYear = (short int) ((year / 4));

    if (year % 4 == 0)
        {
        MDay = LMonDay[month - 1];
        }
    else
        {
        MDay = MonDay[month - 1];
        }

    Weekday = (century + year + WYear + MDay + date) % 7; //  0 = SUNDAY
    return Weekday;
    }

/*************************************************************************
Function Name: Find_Previous_date_Month
input: None.
Output: None.
Discription: This function is use to find previous date from the current date.
 *************************************************************************/
void Find_Previous_date_Month(void)
    {
    unsigned char temp_date, temp_month;
    unsigned int temp_year;

    temp_date = Date.Date - 1; // decrese date by 1.
    if (temp_date == 0) // if result is 0 then find last date of previous month.
        {
        temp_month = Date.Month - 1;
        if (temp_month == 0) // if previous month result is 0 then subtract year and make moth to december.
            {
            temp_month = 12;
            temp_year = Date.Year - 1;
            }
        else
            {
            temp_year = Date.Year;
            }

        /*      check last date in previous month.        */
        if ((temp_month == 1) || (temp_month == 3) || (temp_month == 5) || (temp_month == 7) || (temp_month == 8) || (temp_month == 10) || (temp_month == 12))
            {
            temp_date = 31;
            }
        else if ((temp_month == 4) || (temp_month == 6) || (temp_month == 9) || (temp_month == 11))
            {
            temp_date = 30;
            }

        if (temp_month == 2)
            {
            if ((temp_year % 4) == 0)
                temp_date = 29;
            else
                temp_date = 28;
            }

        }
    else
        {
        temp_month = Date.Month;
        }

    /* copy result of date and month in to final variables. */
    PDate.Date = temp_date;
    PDate.Month = temp_month;
    }

unsigned char ReadRTCReg(unsigned char add)
    {
    unsigned char RTC_ACK = 0, dummy = 0;
    I2C_start();
    I2C_write(ADDRTC); /* write slave address + write */
    I2C_write(add); /* write register address, 1st clock register */
    I2C_stop();
    I2C_start();
    I2C_write(ADDRTC | 1); /* write slave address + read */
    RTC_ACK = I2C_read(ACK); /* starts w/last address stored in register pointer */
    dummy = RTC_ACK & 0x7F;
    RTC_ACK = (dummy - ((dummy / 16) * 6));
    return RTC_ACK;
    }
//**************************************************//

void UART_I2C_start() /* ----------------------------------------------- */
    {
    TRISFbits.TRISF4 = 0;
    I2C_SDA = 1;
    RTC_delay(10);
    I2C_SCL = 1; /* Initiate start condition */
    I2C_SDA = 0;
    RTC_delay(10);
    }

void UART_I2C_stop() /* ----------------------------------------------- */
    {
    TRISFbits.TRISF4 = 0;
    //	sda = 0; 
    //	sda = 0; 
    I2C_SDA = 0;
    I2C_SDA = 0; /* Initiate stop condition */
    RTC_delay(5);
    I2C_SCL = 1;
    I2C_SCL = 1;
    //	sda = 1;//#rht
    RTC_delay(5);
    }

void UART_I2C_write(uchar d) /* ----------------------------- */
    {
    uchar i, local;
    TRISFbits.TRISF4 = 0;
    I2C_SCL = 0;
    for (i = 1; i <= 8; i++)
        {
        local = (d >> 7);
        if (local)
            I2C_SDA = 1;
        else
            I2C_SDA = 0;

        RTC_delay(5);
        I2C_SCL = 1;
        d = d << 1; /* increase scl high time */
        RTC_delay(5);
        I2C_SCL = 0;
        }
    //TRISGbits.TRISG3 = 0;
    //RTC_delay(1); 
    //sda = 1; /* Release the sda line */
    TRISFbits.TRISF4 = 1;
    I2C_SCL = 0;
    RTC_delay(5);
    I2C_SCL = 1;

    RTC_delay(1);
    if (PORTFbits.RF4)
        {
        //		printf("Ack bit missing");
        //WriteData_UART2("\r\nAck miss", strlen("\r\nAck miss"));
        I2C_datarec_flag = 0;
        }
    else
        {
        //		printf("Ack bit OK");	
        // WriteData_UART2("\r\nAck OK", strlen("\r\nAck OK"));
        I2C_datarec_flag = 1;
        }
    I2C_SCL = 0;
    }

uchar UART_I2C_read(uchar b) /* ----------------------------------- */
    {
    uchar d = 0, i, TmpByte = 0;
    TRISFbits.TRISF4 = 1;
    //	sda = 1; /* Let go of sda line */
    I2C_SCL = 0;
    for (i = 1; i <= 8; i++) /* read the msb first */
        {
        RTC_delay(5);
        I2C_SCL = 1;
        d = d << 1;
        TmpByte = (unsigned char) PORTFbits.RF4;
        d = d | TmpByte;
        RTC_delay(5);
        I2C_SCL = 0;

        }
    TRISFbits.TRISF4 = 0;
    //	sda = b; /* Hold sda low for acknowledge */
    I2C_SDA = 0; /* Hold sda low for acknowledge */
    //scl = 0;//#rhtI2C_read
    RTC_delay(5);
    I2C_SCL = 1;
    RTC_delay(5);
    if (b == NACK) I2C_SDA = 1; /* sda = 1 if next cycle is reset */
    I2C_SCL = 1;
    I2C_SDA = 1; //#rht
    return d;
    }

void I2CRxData(void)
    {
    UART_I2C_start();
    UART_I2C_write(I2C_ADD | 1);
    if (I2C_datarec_flag)
        {
        I2C_ACK = UART_I2C_read(ACK); /* starts w/last address stored in register pointer */
        }
    else
        {
        UART_I2C_stop();
        }
    if (I2C_ACK != 0xff)
        {
        I2C_data_rec_flag = 1;
        I2C_data_Buff[Rec_count] = I2C_ACK;
        Rec_count++;
        I2C_ACK = 0xff;
        I2C_Timeout = 0;
        if (Rec_count >= 70)
            {
            Rec_count = 0;
            }
        }

    }

