
///***********************************************************************
//	PC COMMUNICATION PROTOCOL FILE FOR GETTING AMR DATA
//	AUTHOR : RAMANDEEP SINGH
//***********************************************************************/



#include <stdio.h>
#include <ctype.h>
#include <math.h>
#include <string.h>
#include <stdlib.h>

#include "serial.h"
#include "protocol.h"
#include "AMR_CS5463.h"

volatile unsigned char GPS_Read_stop = 0;
unsigned char Serial_RF_Buff[B_SIZE_SERIAL_RF];
unsigned char Serial_GPS_Buff[B_SIZE_SERIAL_GPS];
unsigned int temp_rx_RF = 0;
unsigned char data_rec_RF = 0;

unsigned int temp_rx_GPS = 0;
volatile unsigned char data_rec_GPS = 0;
volatile unsigned char GPS_serial_timeout = 0;
extern unsigned char Time_Out;

uint8_t U2RxByte = 0;

void Configure_UART1(void) // UART RF 
    {
    unsigned int i = 0;

    U1MODE = (0x8008 & ~(1 << 15)); // disabling UARTEN bit
    // UTXISEL0 TX_ONE_CHAR; UTXINV disabled; OERR NO_ERROR_cleared; URXISEL RX_ONE_CHAR; UTXBRK COMPLETED; UTXEN disabled; ADDEN disabled; 
    U1STA = 0x00;
    // BaudRate = 115200;  BRG 34; 
    U1BRG = 0x22;

    IFS0bits.U1RXIF = 0;
    IFS0bits.U1TXIF = 0;
    IEC0bits.U1RXIE = 1;
    IEC0bits.U1TXIE = 0;
    U1MODEbits.UARTEN = 1; // enabling UART ON bit
    U1STAbits.UTXEN = 1;

    for (i = 0; i < B_SIZE_SERIAL_RF; i++) Serial_RF_Buff[i] = 0;
    }

void Configure_UART2(void) //RS485
    {

    /**    
         Set the UART3 module to the options selected in the user interface.
         Make sure to set LAT bit corresponding to TxPin as high before UART initialization
     */
    // STSEL 1; IREN disabled; PDSEL 8N; UARTEN enabled; RTSMD disabled; USIDL disabled; WAKE disabled; ABAUD disabled; LPBACK disabled; BRGH enabled; RXINV disabled; UEN TX_RX; 
    // Data Bits = 8; Parity = None; Stop Bits = 1;
    U2MODE = (0x8008 & ~(1 << 15)); // disabling UARTEN bit
    // UTXISEL0 TX_ONE_CHAR; UTXINV disabled; OERR NO_ERROR_cleared; URXISEL RX_ONE_CHAR; UTXBRK COMPLETED; UTXEN disabled; ADDEN disabled; 
    U2STA = 0x00;
    // BaudRate = 115200;  BRG 34; 
    U2BRG = 0x22;

    IFS1bits.U2RXIF = 0;
    IFS1bits.U2TXIF = 0;
    IEC1bits.U2RXIE = 1;
    IEC1bits.U2TXIE = 0;
    U2MODEbits.UARTEN = 1; // enabling UART ON bit
    U2STAbits.UTXEN = 1;

    }

void Configure_UART3(void)//GPS
    {

    unsigned int i = 0;
    U3MODE = (0x8008 & ~(1 << 15)); // disabling UARTEN bit
    // UTXISEL0 TX_ONE_CHAR; UTXINV disabled; OERR NO_ERROR_cleared; URXISEL RX_ONE_CHAR; UTXBRK COMPLETED; UTXEN disabled; ADDEN disabled; 
    U3STA = 0x00;
    // BaudRate = 9600;  BRG 416; 
    U3BRG = 0x1A0; /* TX & RX interrupt modes */

    IFS5bits.U3RXIF = 0;
    IFS5bits.U3TXIF = 0;
    IEC5bits.U3RXIE = 1;
    IEC5bits.U3TXIE = 0;
    U3MODEbits.UARTEN = 1; // enabling UART ON bit
    U3STAbits.UTXEN = 1;
    for (i = 0; i < B_SIZE_SERIAL_GPS; i++) Serial_GPS_Buff[i] = 0;
    }

void Configure_UART4(uint16_t value)//EM
    {
    U4MODE = (0x8008 & ~(1 << 15)); // disabling UARTEN bit
    // UTXISEL0 TX_ONE_CHAR; UTXINV disabled; OERR NO_ERROR_cleared; URXISEL RX_ONE_CHAR; UTXBRK COMPLETED; UTXEN disabled; ADDEN disabled; 
    U4STA = 0x00;

    if (value == baudRate_default)
        {
        // BaudRate = 600; Frequency = 4000000 Hz; BRG 1666;
        U4BRG = 0x1A0A;
        }
    else if (value == baudRate_9600)
        {
        // BaudRate = 9600; Frequency = 4000000 Hz; BRG 1666; 
        U4BRG = 0x1A0;
        }


    IFS5bits.U4RXIF = 0;
    IFS5bits.U4TXIF = 0;
    IEC5bits.U4RXIE = 1;
    IEC5bits.U4TXIE = 0;
    U4MODEbits.UARTEN = 1; // enabling UART ON bit
    U4STAbits.UTXEN = 1;
    }
/*****************************************************************************/
/*		WriteData_UART2(): Writes data to the serial port for transmitting   */
/*		This function writes one by one data byte to the serial port.
                The data buffer and length of buffer is passed as parameters.		 */

/*****************************************************************************/
void WriteData_UART1(unsigned char *dataPtr, unsigned int len)
    {
    int i = 0;
    for (i = 0; i < len; i++)
        {
        Time_Out = 0;
        while (!U1STAbits.TRMT)
            {
            if (Time_Out > 5)
                {
                break;
                }
            }
        U1TXREG = dataPtr[i];
        }
    }

void UART1_Write(uint8_t txData)
    {
    Time_Out = 0;
    while (U1STAbits.UTXBF == 1)
        {
          if (Time_Out > 5)
                {
                break;
                }
        }

    U1TXREG = txData; // Write the data byte to the USART.
    }

void WriteData_UART2(unsigned char *dataPtr, unsigned int len)
    {
    int i = 0;
    for (i = 0; i < len; i++)
        {
        Time_Out = 0;
        while (U2STAbits.UTXBF == 1)
            {
            if (Time_Out > 5)
                {
                break;
                }
            }

        U2TXREG = dataPtr[i];

        }
    }

void WriteData_UART3(unsigned char *dataPtr, unsigned int len)
    {
    int i = 0;
    for (i = 0; i < len; i++)
        {
        Time_Out = 0;
        while (U3STAbits.UTXBF == 1)
            {
            if (Time_Out > 5)
                {
                break;
                }
            }

        U3TXREG = dataPtr[i];

        }
    }

void UART4_Write(unsigned char txData)
    {
    Time_Out = 0;
    while (U4STAbits.UTXBF == 1)
        {
            if (Time_Out > 5)
                {
                break;
                }
        }

    U4TXREG = txData; // Write the data byte to the USART.
    }

void UART2_Write(uint8_t txData)
    {
    Time_Out = 0;
    while (U2STAbits.UTXBF == 1)
        {
          if (Time_Out > 5)
                {
                break;
                }
        }

    U2TXREG = txData; // Write the data byte to the USART.
    }

void __attribute__((interrupt, auto_psv)) _U1RXInterrupt(void)
    {
    IFS0bits.U1RXIF = 0;

    while (U1STAbits.URXDA)
        {

        data_rec_RF = 1; //query received
        Serial_RF_Buff[temp_rx_RF] = U1RXREG;
        //UART2_Write(Serial_RF_Buff[temp_rx_RF]);
        temp_rx_RF++;
        if (temp_rx_RF >= B_SIZE_SERIAL_RF)
            {
            temp_rx_RF = 0;
            }

        //////////////

        if ((U1STA & 0x000E) != 0x0000)
            {
            U1STAbits.OERR = 0; //clear OERR to keep receiving
            U1STAbits.FERR = 0;
            U1STAbits.URXDA = 0;
            }
          if(RF_timeout > 5)
		{ 
			break;
		}
        ///////////////
        }
    }

void __attribute__((interrupt, auto_psv)) _U2RXInterrupt(void)
    {

    IFS1bits.U2RXIF = 0;

    while (U2STAbits.URXDA)
        {
        U2RxByte = U2RXREG;
        Nop();
        UART1_Write(U2RxByte);

        if ((U2STA & 0x000E) != 0x0000)
            {
            U2STAbits.OERR = 0; //clear OERR to keep receiving
            U2STAbits.FERR = 0;
            U2STAbits.URXDA = 0;
            }

        ///////////////
        }



    }

void __attribute__((interrupt, auto_psv)) _U3RXInterrupt(void)
    {
    IFS5bits.U3RXIF = 0;

    while (U3STAbits.URXDA)
        {
        data_rec_GPS = 1; //query received
        //	Serial_GPS_Buff[temp_rx_GPS] = U2RXREG;
        GPS_serial_timeout = 0;
        //	temp_rx_GPS++;
        //////////////
        if (GPS_Read_stop == 0)
            {
            Serial_GPS_Buff[temp_rx_GPS] = U3RXREG;
            temp_rx_GPS++;
            }

        /////////////
        if (temp_rx_GPS >= B_SIZE_SERIAL_GPS)
            {
            temp_rx_GPS = 0;
            }
                if(GPS_timeout > 5)
		{ 
			break;
		}
            if ((U3STAbits.OERR == 1))
        {
        U3STAbits.OERR = 0;
        }
        }


    }

void __attribute__((interrupt, auto_psv)) _U4RXInterrupt(void)
    {
    IFS5bits.U4RXIF = 0;

    while (U4STAbits.URXDA)
        {
        data[EmRxcount] = U4RXREG;
        EmRxcount = EmRxcount + 1;
        if (EmRxcount > 9)
            {
            EmRxcount = 0;
            }
                        if(EM_timeout > 5)
		{ 
			break;
		}
            if ((U4STAbits.OERR == 1))
        {
        U4STAbits.OERR = 0;
        }
        }

    

    }
