#ifndef _SC16IS740_H_
#define _SC16IS740_H_
#include "generic.h"
#include "AMR_RTC.h"
#include <xc.h>

// Device Address

// A:VDD
// B:GND
// C:SCL
// D:SDA
#define     SC16IS740_ADDRESS_AA     (0x90)


// General Registers
#define     SC16IS740_REG_RHR        (0x00)
#define     SC16IS740_REG_THR        (0X00)
#define     SC16IS740_REG_IER        (0X01)
#define     SC16IS740_REG_FCR        (0X02)
#define     SC16IS740_REG_IIR        (0X02)
#define     SC16IS740_REG_LCR        (0X03)
#define     SC16IS740_REG_MCR        (0X04)
#define     SC16IS740_REG_LSR        (0X05)
#define     SC16IS740_REG_MSR        (0X06)
#define     SC16IS740_REG_SPR        (0X07)
#define     SC16IS740_REG_TCR        (0X06)
#define     SC16IS740_REG_TLR        (0X07)
#define     SC16IS740_REG_TXLVL      (0X08)
#define     SC16IS740_REG_RXLVL      (0X09)
#define     SC16IS740_REG_IODIR      (0X0A)
#define     SC16IS740_REG_IOSTATE    (0X0B)
#define     SC16IS740_REG_IOINTENA   (0X0C)
#define     SC16IS740_REG_IOCONTROL  (0X0E)
#define     SC16IS740_REG_EFCR       (0X0F)

// Special Registers
#define     SC16IS740_REG_DLL        (0x00)
#define     SC16IS740_REG_DLH        (0X01)

// Enhanced Registers
#define     SC16IS740_REG_EFR        (0X02)
#define     SC16IS740_REG_XON1       (0X04)
#define     SC16IS740_REG_XON2       (0X05)
#define     SC16IS740_REG_XOFF1      (0X06)
#define     SC16IS740_REG_XOFF2      (0X07)

//
#define     SC16IS740_INT_CTS        (0X80)
#define     SC16IS740_INT_RTS        (0X40)
#define     SC16IS740_INT_XOFF       (0X20)
#define     SC16IS740_INT_SLEEP      (0X10)
#define     SC16IS740_INT_MODEM      (0X08)
#define     SC16IS740_INT_LINE       (0X04)
#define     SC16IS740_INT_THR        (0X02)
#define     SC16IS740_INT_RHR        (0X01)

// Application Related

// #define     SC16IS740_CRYSTCAL_FREQ (14745600UL)
#define         SC16IS740_CRYSTCAL_FREQ (4096000UL)//1843200UL
//#define         SC16IS740_CRYSTCAL_FREQ (8000000UL)//1843200UL

// #define     SC16IS740_CRYSTCAL_FREQ (16000000UL)
//#define     SC16IS740_DEBUG_PRINT   (1)
#define     SC16IS740_PROTOCOL_I2C  (0)
#define     SC16IS740_PROTOCOL_SPI  (1)

#define     SC16IS740_CHANNEL_A                 0x00
#define     SC16IS740_CHANNEL_B                 0x01
#define     SC16IS740_CHANNEL_BOTH              0x00

#define     SC16IS740_DEFAULT_SPEED             9600




void beginA(uint32_t baud_A);
void beginB(uint32_t baud_B);
int read(uint8_t channel);
uint8_t write(uint8_t channel,
        uint8_t val);
int available(uint8_t channel);
void pinMode(uint8_t pin,
        uint8_t io);
void digitalWrite(uint8_t pin,
        uint8_t value);
uint8_t digitalRead(uint8_t pin);
uint8_t ping();

//	void setTimeout(uint32_t);
//	size_t readBytes(char *buffer, size_t length);
int peek(uint8_t channel);
void flush(uint8_t channel);
uint8_t GPIOGetPortState(void);
uint8_t InterruptPendingTest(uint8_t channel);
void SetPinInterrupt(uint8_t io_int_ena);
void InterruptControl(uint8_t channel,
        uint8_t int_ena);
void ModemPin(uint8_t gpio); // gpio == 0, gpio[7:4] are modem pins, gpio == 1 gpio[7:4] are gpios
void GPIOLatch(uint8_t latch);





//	uint32_t timeout;
void Initialize();
int16_t SetBaudrate(uint8_t channel,
        uint32_t baudrate);
uint8_t ReadRegister(uint8_t channel,
        uint8_t reg_addr);
void WriteRegister(uint8_t channel,
        uint8_t reg_addr,
        uint8_t val);
void SetLine(uint8_t channel,
        uint8_t data_length,
        uint8_t parity_select,
        uint8_t stop_length);
void GPIOSetPinMode(uint8_t pin_number,
        uint8_t i_o);
void GPIOSetPinState(uint8_t pin_number,
        uint8_t pin_state);

uint8_t GPIOGetPinState(uint8_t pin_number);
void GPIOSetPortMode(uint8_t port_io);
void GPIOSetPortState(uint8_t port_state);
void ResetDevice();


void __isr(uint8_t channel);
void FIFOEnable(uint8_t channel,
        uint8_t fifo_enable);
void FIFOReset(uint8_t channel,
        uint8_t rx_fifo);
void FIFOSetTriggerLevel(uint8_t channel,
        uint8_t rx_fifo,
        uint8_t length);
uint8_t FIFOAvailableData(uint8_t channel);
uint8_t FIFOAvailableSpace(uint8_t channel);
void WriteByte(uint8_t channel,
        uint8_t val);
int ReadByte(uint8_t channel);
void EnableTransmit(uint8_t channel,
        uint8_t tx_enable);

//void I2C_SendData(unsigned char *dataPtr, unsigned int len);
void WriteBust(uint8_t channel,unsigned char *dataPtr, unsigned int len);

#endif //