#ifndef APPLICATION_LOGIC
#define APPLICATION_LOGIC



#define DO_1 LATBbits.LATB14
#define DO_2 LATBbits.LATB15
#define DRIVER_AO

#ifdef DRIVER_AO
#define PWM_CONSTANT        (3199.0)
#else
#define PWM_CONSTANT        (15999.0)
#endif

#define DIM_DRIVER_MUX_AO                       (0)
#define DIM_DRIVER_MUX_DALI                     (1)

#define DIM_DRIVER_SELECT_AUTO_ON_POWER_CYCLE	(0)
#define DIM_DRIVER_SELECT_AUTO_ON_REQUEST		(1)
#define DIM_DRIVER_SELECT_DALI					(2)
#define DIM_DRIVER_SELECT_AO					(3)

#define DIM_DRIVER_STATUS_AO_SET				(0)
#define DIM_DRIVER_STATUS_DALI_SET				(1)
#define DIM_DRIVER_STATUS_DETECTION				(2)
#define DIM_DRIVER_STATUS_ERR_AO_SET			(3)
#define Photo_Pin   PORTBbits.RB5		
#define PDO			PORTDbits.RD0
#define TILT_DI1    PORTBbits.RB8 
#define TP1        PORTBbits.RB10 
#define MOTION_DI1  PORTBbits.RB9  
#define TP7        PORTBbits.RB2


#define HI_PIN_OUT_IN1                  LATEbits.LATE6
#define HI_PIN_OUT_IN2                  LATEbits.LATE7

#define SUNRISE 0
#define SUNSET  1
//#define PI 3.14
#define PI 3.1415926535897932384626433


extern uint8_t PWM_FREQ;

//#define AUTO_ENENT                    // define if want auto data send after any event occurs.

struct Schedule // V6.1.10
{
    unsigned char bSchFlag;
    unsigned char cStartHour;
    unsigned char cStartMin;
    unsigned char cStopHour;
    unsigned char cStopMin;
};

//SAN

struct DimSchedule {
    unsigned char bSchFlag;
    unsigned char cStartHour;
    unsigned char cStartMin;
    unsigned char cStopHour;
    unsigned char cStopMin;
    unsigned char dimvalue;
};

struct Mix_DimSch {
    unsigned char bSchFlag;
    unsigned char cStartHour;
    unsigned char cStartMin;
    unsigned char cStopHour;
    unsigned char cStopMin;
    unsigned char dimvalue;
    unsigned char Dim_Mode;
};

struct M_Schedule {
    unsigned char bSchFlag;
    unsigned char cStartHour;
    unsigned char cStartMin;
    char cStartOff;
    unsigned char cStopHour;
    unsigned char cStopMin;
    char cStopOff;
    unsigned char dimvalue;
    unsigned char Dim_Mode;
};

struct M_Compress_Schedule {
    unsigned char compress1;
    unsigned char compress2;
    unsigned char compress3;
    unsigned char compress4;
    unsigned char compress5;
    unsigned char compress6;
};

struct Stree_light_Lat {
    char deg;
    char min;
    char sec;
};

struct Stree_light_Log {
    char deg;
    char min;
    char sec;
};

struct Stree_light_UTC {
    char hour;
    char min;
    char ScheduleConfig;
};

struct Slc_M_Sch {
    unsigned char SLC_Mix_En;
    unsigned char SLC_Mix_Sch_Start_Date;
    unsigned char SLC_Mix_Sch_Start_Month;
    unsigned char SLC_Mix_Sch_Stop_Date;
    unsigned char SLC_Mix_Sch_Stop_Month;
    unsigned char SLC_Mix_Sch_WeekDay;
    unsigned char SLC_Mix_Sch_Photocell_Override;
    struct M_Schedule Slc_Mix_Sch[9];
};

struct Slc_Compress_M_Sch {
    unsigned char SLC_Mix_En;
    unsigned char SLC_Mix_Sch_Start_Date;
    unsigned char SLC_Mix_Sch_Start_Month;
    unsigned char SLC_Mix_Sch_Stop_Date;
    unsigned char SLC_Mix_Sch_Stop_Month;
    unsigned char SLC_Mix_Sch_WeekDay;
    unsigned char SLC_Mix_Sch_Photocell_Override;
    struct M_Compress_Schedule Slc_Mix_Sch[9];
};

struct push_sch // V6.1.10
{
    unsigned char PushHour;
    unsigned char PushMin;

};



extern struct push_sch push[];
extern unsigned char set_do_flag;
extern struct Stree_light_Lat Lat;
extern struct Stree_light_Log Log;
extern struct Stree_light_UTC UTC;
extern float Latitude, longitude;

extern long sunriseTime, sunsetTime;
extern BYTE_VAL error_condition, error_condition1, test_result, day_sch, day_sch_R;
extern struct Schedule sSch[1][10], Astro_sSch[2], DLH_sSch[2]; // V6.1.10
extern unsigned char RTC_Faulty_Shift_to_Local_Timer;
extern struct DimSchedule Master_sSch[1][10];
extern struct Mix_DimSch Mix_sSch[];

extern volatile unsigned char photo_cell_timer;
extern volatile unsigned char photo_cell_toggel_timer;
extern unsigned char cycling_lamp_fault;
//extern unsigned char check_current_counter;
extern volatile unsigned int check_current_counter; //As BUG of lamp fault parameter
extern volatile unsigned int check_current_firsttime_counter;
extern unsigned char check_cycling_current_counter;
extern unsigned char check_half_current_counter;
extern volatile unsigned int lamp_off_on_timer;
extern volatile unsigned int  Dimming_start_timer;
extern unsigned char detect_lamp_current;
extern float lamp_current;
extern unsigned char chk_interlock;
extern volatile unsigned char check_counter;
extern unsigned char lamp_lock;
extern unsigned char photo_cell_toggel_counter;
extern unsigned char low_current_counter;
extern unsigned int Vol_hi;
extern unsigned int Vol_low;
extern unsigned int Curr_Steady_Time;
extern unsigned int Per_Val_Current;
extern unsigned int Lamp_Fault_Time;
extern unsigned int Lamp_off_on_Time;
extern unsigned int Lamp_faulty_retrieve_Count;
extern volatile unsigned int Sec_Counter;
extern unsigned char automode;
extern unsigned char Photo_Cell_Ok;
extern unsigned char Set_Do_On;
extern unsigned char Cloudy_Flag;
extern unsigned char Master_event_generation;
extern unsigned char event_counter;

extern volatile unsigned char energy_time;
extern unsigned char energy_meter_ok;
extern float current_creep_limit;
extern unsigned int Lamp_lock_condition;

extern unsigned char protec_flag;
extern unsigned char old_dim_val;
extern volatile unsigned char dim_inc_timer;
extern unsigned char dim_inc_val;
extern unsigned int dim_applay_time;

extern unsigned char Master_Sch_Match; // Master sch 15/04/11
//extern unsigned char Photo_feedback;				//SAN

extern unsigned char ballast_type;
extern unsigned char adaptive_light_dimming;
extern unsigned int analog_input_scaling_high_Value;
extern unsigned int analog_input_scaling_low_Value;
extern unsigned int desir_lamp_lumen;
extern unsigned char lumen_tollarence;
extern unsigned int desir_lamp_lumen;

//extern unsigned char dispStr[];


extern unsigned char Motion_pre_val;
extern unsigned char Motion_countr;
extern unsigned char Motion_detected;
extern unsigned int motion_dimming_timer;
extern unsigned int motion_intersection_dimming_timer;
extern unsigned int Motion_dimming_time;
extern unsigned char Motion_dimming_percentage;
extern unsigned char motion_dimming_Broadcast_send;
extern unsigned char motion_dimming_send;
extern unsigned char Motion_detected_broadcast;
extern unsigned char Motion_normal_dimming_percentage;
extern unsigned char Motion_group_id;
extern unsigned char Motion_pulse_rate;
extern volatile unsigned char motion_broadcast_timer;
extern unsigned char Motion_half_sec;
extern unsigned char Motion_Continiue;
extern volatile unsigned char Motion_detect_sec;
extern unsigned char Motion_Detect_Timeout;
extern unsigned char Motion_Broadcast_Timeout;
extern unsigned char Motion_Sensor_Type;
extern unsigned char Motion_continue_timer;
extern unsigned char Motion_Received_broadcast;
extern unsigned char Motion_Received_broadcast_counter;
extern unsigned char Motion_Rebroadcast_timeout;
extern volatile unsigned char Relay_weld_timer;
extern unsigned char Motion_intersection_detected;

extern struct Slc_M_Sch Slc_Mix_Mode_schedule[];
extern struct Slc_Compress_M_Sch Slc_Comp_Mix_Mode_schedule[];
extern unsigned char Civil_Twilight;
extern unsigned char Schedule_offset;

extern unsigned char Photocell_steady_timeout_Val;
extern unsigned char photo_cell_toggel_counter_Val;
extern unsigned char photo_cell_toggel_timer_Val;
extern unsigned char Photo_cell_Frequency;
extern unsigned int Photocell_unsteady_timeout_Val;
extern unsigned char RTC_faulty_detected;
extern unsigned char previous_mode;
extern unsigned char RTC_New_fault_logic_Enable;
extern unsigned int RTC_drift;
extern volatile unsigned char f_dalirx;
extern unsigned short int forward;
extern unsigned char answer;
extern unsigned char one_sec_cyclic, Commissioning_flag;

void EEWriteAstroSch(void);
void check_dimming_protection(void); // SAN
extern unsigned char DaliHwSelect_u8;
extern unsigned char DimmerDriverSelectionProcess;
extern unsigned char DimDriverStatus;
extern unsigned char DimmerDriver_SELECTION;
extern unsigned char lamp_on_first;
extern unsigned char default_dali;
void Increment_Local_Timer(void);
extern volatile unsigned char DaliDelayTimeout_u8;
extern volatile unsigned char DaliRx_Status;
void CheckSchedule(void);
void Set_Do(unsigned char i);
void check_logic(void);
double DegreesToAngle(double degrees, double minutes, double seconds);
double FixValue(double value, double min, double max);
double Rad2Deg(double angle);
double Deg2Rad(double angle);
long int Calculate(unsigned char direction);
void GetSCHTimeFrom_LAT_LOG(void);
unsigned char CheckSchedule_sun(void);
void check_interlock(void);
void check_photocell_interlock(void);
void Check_MASTER_Schedule(void); // Master sch 15/04/11
void InitTimer1(); // SAN
void InitTimer3(void);
void dimming_applied(unsigned char dim_val);
void Check_day_burning_fault(void);
void adaptive_dimming(void);
void Motion_Based_dimming(void);
void Day_light_harves_time(void);
//void Motion_dimming_applied(unsigned char dim_val);
void idimmer_dimming_applied(unsigned char dim_val);
void check_Mix_Mode_schedule(void);
void Run_Mix_Mode_Sch(void);
void check_Day_Light_Saving(void);
void DALI_DIMMING_CMD(unsigned char dim);
void set_Linear_Logarithmic_dimming(unsigned char Curv_type);
void DALI_ON(unsigned char);
void DALI_Send(void);
void make_default(void);
void unicast_shortaddress(void);

unsigned char find_dst_date_from_rule(unsigned char occurance, unsigned char occurance_month);
unsigned char check_mix_mode_week_day(unsigned char sch_weekday);
void Mix_MOD_Motion_Based_dimming(void);
void Increment_Date(void);
void Decrement_Date(void);
void Fill_Decrement_Date(void);
void Fill_RTC_BY_Local_Timer(void);
void Delay(long K);
extern void DimDriverMuxSelect(unsigned char);
extern unsigned char detectDimmerDriver(void);
extern void AppDimmerAutoDetectInit(void);
extern unsigned char sendDaliCommand_ToCheck_DimmerDriver(void);
void Save_BH_kWh(void);
#endif

