/*********************************************************************
 *
 *                  Master SPI routintes
 *
 *********************************************************************
 * FileName:        MSPI.c
 * Dependencies:
 * Processor:       PIC18 / PIC24 / dsPIC33
 * Complier:        MCC18 v1.00.50 or higher
 *                  MCC30 v2.05 or higher
 * Company:         Microchip Technology, Inc.
 *
 * Software License Agreement
 *
 * Copyright (c) 2004-2008 Microchip Technology Inc.  All rights reserved.
 *
 * Microchip licenses to you the right to use, copy and distribute Software 
 * only when embedded on a Microchip microcontroller or digital signal 
 * controller and used with a Microchip radio frequency transceiver, which 
 * are integrated into your product or third party product (pursuant to the 
 * sublicense terms in the accompanying license agreement).  You may NOT 
 * modify or create derivative works of the Software.  
 *
 * If you intend to use this Software in the development of a product for 
 * sale, you must be a member of the ZigBee Alliance.  For more information, 
 * go to www.zigbee.org.
 *
 * You should refer to the license agreement accompanying this Software for 
 * additional information regarding your rights and obligations.
 *
 * SOFTWARE AND DOCUMENTATION ARE PROVIDED �AS IS� WITHOUT WARRANTY OF ANY 
 * KIND, EITHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION, ANY WARRANTY 
 * OF MERCHANTABILITY, TITLE, NON-INFRINGEMENT AND FITNESS FOR A PARTICULAR 
 * PURPOSE. IN NO EVENT SHALL MICROCHIP OR ITS LICENSORS BE LIABLE OR OBLIGATED 
 * UNDER CONTRACT, NEGLIGENCE, STRICT LIABILITY, CONTRIBUTION, BREACH OF 
 * WARRANTY, OR OTHER LEGAL EQUITABLE THEORY ANY DIRECT OR INDIRECT DAMAGES OR 
 * EXPENSES INCLUDING BUT NOT LIMITED TO ANY INCIDENTAL, SPECIAL, INDIRECT, 
 * PUNITIVE OR CONSEQUENTIAL DAMAGES, LOST PROFITS OR LOST DATA, COST OF 
 * PROCUREMENT OF SUBSTITUTE GOODS, TECHNOLOGY, SERVICES, OR ANY CLAIMS BY 
 * THIRD PARTIES (INCLUDING BUT NOT LIMITED TO ANY DEFENSE THEREOF), OR OTHER 
 * SIMILAR COSTS.
 *
 *
 * Author               Date    Comment
 *~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Nilesh Rajbharti     7/12/04 Original
 * Nilesh Rajbharti     11/1/04 Pre-release version
 * DF/KO                04/29/05 Microchip ZigBee Stack v1.0-2.0
 * DF/KO                07/18/05 Microchip ZigBee Stack v1.0-3.0
 * DF/KO                07/27/05 Microchip ZigBee Stack v1.0-3.1
 * DF/KO                01/09/06 Microchip ZigBee Stack v1.0-3.5
 * DF/KO                08/31/06 Microchip ZigBee Stack v1.0-3.6
 * DF/KO/YY             11/27/06 Microchip ZigBee Stack v1.0-3.7
 * DF/KO/YY				01/12/07 Microchip ZigBee Stack v1.0-3.8
 * DF/KO/YY             02/26/07 Microchip ZigBee Stack v1.0-3.8.1
 ********************************************************************/
#include "MSPI.h"
#include "Zigbee.def"
#include "Compiler.h"
#include "DEE Emulation 16-bit.h"

unsigned char DEEdata = 10;
//unsigned int DEEaddr1 = 4, DEEaddr2 = 261, DEEaddr3 = 302;
//unsigned int DEEaddr1 = 0x8000, DEEaddr2 = 261, DEEaddr3 = 302;

unsigned int DEEaddr1 = 0x1, DEEaddr2 = 261, DEEaddr3 = 302;

#if defined(__dsPIC33F__) || defined(__PIC24F__) || defined(__PIC24H__)

ALLTYPES ReadEEPROM(unsigned int add)
    {
    ALLTYPES readData;

    readData.byte[3] = ReadByte_EEPROM(add);
    readData.byte[2] = ReadByte_EEPROM(add + 1);
    readData.byte[1] = ReadByte_EEPROM(add + 2);
    readData.byte[0] = ReadByte_EEPROM(add + 3);

    return (readData);
    }

void WriteEEPROM(unsigned int add, ALLTYPES writeData)
    {

    WriteByte_EEPROM(add, (unsigned char) (writeData.byte[3]));
    WriteByte_EEPROM(add + 1, (unsigned char) (writeData.byte[2]));
    WriteByte_EEPROM(add + 2, (unsigned char) (writeData.byte[1]));
    WriteByte_EEPROM(add + 3, (unsigned char) (writeData.byte[0]));
    }

unsigned char ReadByte_EEPROM(unsigned int add)
    {

    unsigned char dest = 0;

#ifdef USE_INTERNAL_FLASH

    add = DEEaddr1 + 10 + add;
    dest = (unsigned char) (DataEERead(add)&0xff); //#Rht

#else	 
    add = EXTERNAL_NVM_BYTES + 10 + add;

    SPISelectEEPROM();
    EE_SPIPut(SPIREAD);
    EE_SPIPut((BYTE) (((WORD) add >> 8) & 0xFF));
    EE_SPIPut((BYTE) ((WORD) add & 0xFF));
    dest = EE_SPIGet();
    SPIUnselectEEPROM();
#endif    

    return dest;

    }

void WriteByte_EEPROM(unsigned int add, unsigned char data)
    {
    unsigned char status;
#ifdef USE_INTERNAL_FLASH	

    add = DEEaddr1 + 10 + add;
    status = DataEEWrite((unsigned int) data, add); //#Rht

#else
    add = EXTERNAL_NVM_BYTES + 10 + add;
    // Make sure the chip is unlocked.
    SPISelectEEPROM(); // Enable chip select
    EE_SPIPut(SPIWRSR); // Send WRSR - Write Status Register opcode
    EE_SPIPut(0x00); // Write the status register
    SPIUnselectEEPROM(); // Disable Chip Select

    SPISelectEEPROM(); // Enable chip select
    EE_SPIPut(SPIWREN); // Transmit the write enable instruction
    SPIUnselectEEPROM(); // Disable Chip Select to enable Write Enable Latch

    SPISelectEEPROM(); // Enable chip select
    EE_SPIPut(SPIWRITE); // Transmit write instruction
    EE_SPIPut((BYTE) ((add >> 8) & 0xFF)); // Transmit address high byte
    EE_SPIPut((BYTE) ((add) & 0xFF)); // Trabsmit address low byte

    EE_SPIPut(data);
    SPIUnselectEEPROM();
    Time_Out = 0;
    do
        {
        SPISelectEEPROM(); // Enable chip select
        EE_SPIPut(SPIRDSR); // Send RDSR - Read Status Register opcode
        status = EE_SPIGet();
        SPIUnselectEEPROM(); // Disable Chip Select
        CLRWDT()
        if (Time_Out > 5)
            {
            break;
            }
        }
    while (status & WIP_MASK);

#endif     

    }



#else
#error Unknown processor.  See Compiler.h
#endif

