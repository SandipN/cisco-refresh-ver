/****************************************protocol.c************************************/

#include <stdio.h>
#include <ctype.h>
#include <math.h>
#include <string.h>
#include <stdlib.h>

#include "Compiler.h"
#include "generic.h"
#include "protocol.h"
#include "AMR_RTC.h"
#include "Application_logic.h"
#include "AMR_cs5463.h"
#include "serial.h"
#include "ADC_VI.h"
#include "Gps.h"
#include "MSPI.h"
#include "SC16IS740.h"


#define NEW_LAMP_FAULT_LOGIC
extern BYTE_VAL GPS_error_condition;
unsigned char need_to_send_lat_long = 0;
#ifdef NEW_LAMP_FAULT_LOGIC
extern float KW_Threshold;

#endif
unsigned char Cmd_Rcv_To_Change_Curv_Ype = 0;
unsigned char _SendDaliConfig_u8 = 0;
unsigned char KW_Calibration_120 = 0;
unsigned char dimm_cmd_rec = 0;
unsigned char Rtc_set_Replay, Get_rtc_Replay, Send_Data = 0, Send_Data_push = 0;
unsigned int Frm_no = 1;
//unsigned int amr_id =0;
unsigned long int amr_id = 0; //SCAL 
unsigned char packetlength = 0;
//unsigned char temp_received[127];
//unsigned char temp_arr[220];
unsigned char temp_arr[160], heartbit_buff[4], temp_arr_event[64]; // SAN
unsigned char Eep_Ers = 0;
unsigned char Send_COUNT = 0;
unsigned char send_id_frame = 0;
unsigned char send_get_Gps_Config = 0;
unsigned char Id_frame_received = 0;
volatile float lamp_burn_hour = 0.0;
unsigned char connection_ok = 0;
extern unsigned char rf_wake_flag;
extern unsigned char chek_connection, automode;
unsigned char Sch_receive = 0, DO_receive = 0, auto_receive = 0;
ALLTYPES pulseCounter;
//unsigned char base_buff[230];
unsigned char base_buff[150];
unsigned char track_res[15], Geuid[16];
void Filldata(void);
extern void get_data(unsigned char *data, unsigned int fr_no);

//#################################################//
volatile unsigned long int read_dim_val_timer1 = 0;
extern unsigned int timer_arm;
extern BYTE macLongAddrArray[];
extern float Dimvalue; // SAN
extern unsigned char CURV_TYPE; 
unsigned char Send_curv_type =0;
unsigned long int Rf_Track_Id = 0;
unsigned long int ack_Rf_Track_Id = 0;

//unsigned char temp_data_buff[20][44];
unsigned char temp_data_retrive_counter[20];

unsigned char send_rf_event = 0;
extern float power_on_count;

extern unsigned char Pro_count; // SAN

unsigned char no_of_pending_event = 0;
unsigned char broadcast_read_req = 0;
unsigned char stop_event = 0;
unsigned char read_event_flag = 0;
unsigned char send_get_mode_replay = 0;
unsigned char send_get_mode_replay_auto = 0;
unsigned char Send_Fault_Para_Replay = 0;
unsigned char Send_Slc_Diagnosis = 0;


unsigned char Diagnosis_DI = 0;
unsigned char Send_Network_Para_Replay = 0;
unsigned char Send_Get_Sch_Replay = 0, event_no;

int Sunset_delay = 0;
int Sunrise_delay = 0;

volatile unsigned int RF_network_time_out = 0;
unsigned char RF_network_faulty = 0;

unsigned int Broadcast_time_delay = 0;
unsigned int Unicast_time_delay = 0;

unsigned char delet_no = 0;

unsigned char send_SLC_test_query_1 = 0;
unsigned char send_SLC_test_query_2 = 0;
volatile unsigned char test_query_1_send_timer = 0;

float test_voltage;
float test_current;
unsigned int test_emt_int_count;
unsigned int test_Kwh_count;

unsigned char photocell_test_dicision = 0;
unsigned char SLC_Test_Query_2_received = 0;

unsigned char Send_Get_Master_Sch_Replay = 0; // V6.1.10	// Master sch 15/04/11
unsigned char Sch_week_day = 0; // V6.1.10
unsigned char get_sch_day = 0; // V6.1.10
unsigned char get_MASTER_sch_day = 0; // V6.1.10
volatile unsigned char Get_Sch_timer = 0; // V6.1.10
volatile unsigned char Get_Master_Sch_timer = 0; // V6.1.10

unsigned char Send_Get_Version_info_Replay = 0;

unsigned char sch_day_send[10];
unsigned char sch_day[10];

unsigned char sch_Master_day[10];
unsigned char sch_Master_day_send[10];

unsigned char Send_Get_Adaptive_dimming_para_Replay = 0;
//unsigned char send_get_motion_dimming_para = 0;

unsigned char send_data_direct = 0, send_data_direct_tilt = 0;

unsigned char Motion_broadcast_miss_counter = 0;
unsigned long int Motion_broadcast_Receive_counter = 0, Temp_Motion_broadcast_Receive_counter = 0;

int Day_light_harvesting_start_offset = 0;
int Day_light_harvesting_stop_offset = 0;

long Day_light_harvesting_start_time = 0;
long Day_light_harvesting_stop_time = 0;

////////// idimmer ///////
unsigned char idimmer_en = 0;
unsigned int idimmer_id = 0, time_slice = 0;
//unsigned char idimmer_longaddress[10];
unsigned char send_idimmer_id_frame = 0;
volatile unsigned int Send_dimming_cmd = 0;
unsigned char idimmer_Id_frame_received = 0;
unsigned char send_idimmer_Val_frame = 0;
unsigned char dimming_cmd_ack = 0;
unsigned char idimmer_trip_erase = 0;
unsigned char idimmer_dimval = 0;
unsigned char current_dimval = 0;
unsigned char dimming_cmd_cnt = 0;
unsigned char  read_dim_val_timer = 0;
volatile unsigned char  send_id_frame_time = 0;
unsigned char read_dim_val_idimmer = 0;
unsigned char Send_dimming_short_ack = 0, Test_get_cisco_config_send = 0;
////////// idimmer ///////


////////// free joining ///////
unsigned char ID_Frame_Send_counter = 0;
unsigned char change_SLC_channel = 0;
unsigned char Extended_pan[8];
unsigned char SLC_channel = 11;
unsigned char Test_application_flag = 0; // 1 = communicate with test application 0 = communication with DCU.
unsigned char Valid_DCU = 0;
unsigned char joining_time = 0;
unsigned char Network_found = 0;
unsigned char SLC_joinig_retry_counter = 0;
unsigned char Tilt_Enable = 0;
unsigned char Send_Get_Slc_Config_para_Replay = 0;
unsigned char broadcast_received = 0;
volatile unsigned char broadcsat_received_timer = 0;
unsigned char Network_serch_sec = 0;
unsigned char channel_shift = 0, Dimming_test_dicision = 0;
////////// free joining ///////

unsigned char Read_db_val = 0;
unsigned char Slc_db = 0;

unsigned char SLC_DST_En = 0;
unsigned char SLC_DST_Start_Rule = 0;
unsigned char SLC_DST_Start_Month = 0;
unsigned char SLC_DST_Start_Time = 0;
unsigned char SLC_DST_Stop_Rule = 0;
unsigned char SLC_DST_Stop_Month = 0;
unsigned char SLC_DST_Stop_Time = 0;
float SLC_DST_Time_Zone_Diff = 0;
unsigned char SLC_DST_Rule_Enable = 0;
unsigned char SLC_DST_Start_Date = 0;
unsigned char SLC_DST_Stop_Date = 0;

unsigned char SLC_DST_R_Start_Date;
unsigned char SLC_DST_R_Stop_Date;
unsigned char Time_change_dueto_DST = 0;
unsigned int old_year = 0;
unsigned int Client = 0, temp_client = 0;

unsigned char send_get_slc_dst_config_para = 0;
unsigned char Send_Get_Mix_Mode_Sch = 0;

unsigned char L_No = 0;
unsigned char Get_Mix_mode_Sch_Loc = 0;

unsigned char Mix_photo_override = 0;
unsigned char Auto_event = 1;
unsigned char send_data_direct_counter = 0;

unsigned char SLC_New_Manula_Mode_En = 0;
unsigned char SLC_New_Manula_Mode_Val = 0;

//unsigned char Link_Key[20] = {0x00,0x11,0x22,0x33,0x44,0x55,0x66,0x77,0x88,0x99,0xAA,0xBB,0xCC,0xDD,0xEE,0xFF};
unsigned char Send_Link_key = 0;

volatile unsigned char change_Link_key_timer = 0;
unsigned char change_Link_key = 0;
unsigned int SLC_New_Manual_Mode_Timer = 0;
volatile unsigned long int SLC_New_Manual_Mode_counter = 0;

unsigned char Module_Type = 0; // Ember based Module
unsigned char send_get_photocell_config = 0;
unsigned char send_get_Rs485_DI2_Config = 0;
unsigned char uChar_GET_FOOT_CANDLE = 0;
unsigned char Temp_Slc_Multi_group = 0;
unsigned char send_id_frame_flage = 0;
unsigned char Cali_buff[50]; //80       //SAN
extern unsigned char packet_length, Tilt_send_cnt, Normal_reset_time, ID_Frame_reset_time, idframe_frequency;
extern float Calculated_Frequency_row;
extern unsigned int ID_frame_timeout;

BYTE_VAL Group_val1;
////////////////////////////////////////// Calibration /////////////////


unsigned char Get_calibration_data, send_to_Router;
unsigned char Send_Kw_calibration_120 = 0;
unsigned char Read_KWH_Value;
unsigned char Read_Em_Calibration = 0;
unsigned char Send_Kw_calibration = 0;
unsigned char KW_Calibration = 0;
unsigned char Current_calibration = 0;
unsigned char Send_Current_calibration = 0;
unsigned char Send_Voltage_calibration = 0;
volatile unsigned int RTC_power_on_Logic_Timer = 0;
unsigned char check_RTC_logic_cycle = 0;
unsigned char Voltage_calibration = 0;
unsigned char Interface_down = 0, Multicast_Interface_down = 0, temp_hour;
extern unsigned char nic_reset_cnt2, nic_reset_cnt3, Send_Last_gasp_msg;
extern volatile unsigned int  idframe_frequency_cnt;
extern volatile unsigned int nic_reset_timer;
extern volatile unsigned int uart_timeout;
unsigned char Test_Config_Data_Send = 0;
unsigned char Start_dimming_test = 0, Test_dimming_val = 0;
//unsigned char Time_Out_Flag=0,Time_Out_Check=60;
extern unsigned char RF_Modem_detected, Id_Frame_Cnt, heart_beat_ok, enter_boot_mode, heart_beat_response, heart_beat_done, Energy_data_cnt;
unsigned char send_get_log_conf; //timeslice[2];
//unsigned int timeslice2;
extern volatile unsigned long int slice_milli;
extern unsigned long int push_time_slice_ms;
//extern float cisco_nic_reset;
unsigned char IPack_buff[6], Number_of_En_data = 0, retry_push_data_cnt = 0, retry_send_data_cnt, retry_push_event_cnt, Get_AO_DALI_AUTO_CONFIG = 0, Get_auto_recover = 0;
unsigned int Rec_port, send_port, track_id = 0;
extern volatile unsigned char fill_em_data_once;
extern volatile unsigned char pushdata_retry_timer;
extern volatile unsigned char senddata_retry_timer;
extern volatile unsigned char event_push_retry_timer;
extern unsigned char lograte,    Push_Energy_meter_data, Energy_back_data[];
/////////////////////////////////////////////////////////////////////
unsigned char RTC_need_to_varify = 0, dimming_driver_sect = 0, reboot_on_cmd = 0;

void checkfream(unsigned char *temp)
    {
    unsigned long int temp_amr;
    unsigned int m = 0, n = 0;
    unsigned char check_time = 0, Temp_Slc_Group_Match = 0, Temp_Slc_Multi_broadcast_group = 0;
    unsigned int i = 0, k = 0, j = 0;
    RTCDate LocalDate;
    RTCTime LocalTime;


    if ((temp[0] == 0x0A) && (temp[1] == 0x03)&& (temp[2] == 0x06)&& (temp[3] == 0x0A))
        {
        Save_BH_kWh();
        enter_boot_mode = 30;
        WriteByte_EEPROM(EE_ENT_BOOT, enter_boot_mode);
        delay(100);
        asm("reset");
        }
    /*
    if((temp[0] == 'I') && (temp[1] == 'N')&&(temp[2] == 'O') && (temp[3] == 'W')&&(temp[4] == 'N'))
{
        Interface_down =1;
        temp_hour = Time.Hour;
    } 
     */


    if ((temp[0] == 'H') && (temp[1] == 'E')&&(temp[2] == 'O') && (temp[3] == 'K'))
        {
        heart_beat_ok = 1;
        heart_beat_response = 0;
        heart_beat_done = 1;
        //	sprintf(buffer,"HEART_OK");	
        //    WriteData_UART2(buffer,strlen(buffer));
        }

    /*
	
        if((temp[0] == 'G') && (temp[1] == 'E')&&(temp[2] == 'U') && (temp[3] == 'I')&&(temp[4] == 'D') && (temp[5] == ':'))
        {
        

                    for(m=0;m<16;m++)
            Geuid[m] = temp[6+m];

    //		sscanf(&Geuid[10], "%2hhx", &timeslice[0]);
    //		sscanf(&Geuid[14], "%2hhx", &timeslice[1]);
    //        timeslice2 = timeslice[0] * 256;
    //        timeslice2 = timeslice2 + timeslice[1];

		
        }*/

    if ((temp[0] == 'N') && (temp[1] == 'I')&&(temp[2] == 'C') && (temp[3] == 'I')&&(temp[4] == 'P') && (temp[5] == '='))
        {
        //    for(m=0;m<39;m++)
        //    nicipv6[m] = temp[6+m];
        send_id_frame_flage = 1;
        Id_frame_received = 0;
        Id_Frame_Cnt = 0;
        RF_communication_check_Counter = 0;
        slice_milli = 0;
        //    for(n=0;n<39;n++)
        //    WriteByte_EEPROM(EE_NICIPV6 + n,nicipv6[n]);

        IPack_buff[0] = 'I';
        IPack_buff[1] = 'P';
        IPack_buff[2] = 'V';
        IPack_buff[3] = '6';
        IPack_buff[4] = 'O';
        IPack_buff[5] = 'K';

        WriteData_UART1(IPack_buff, 6);
        }


    if ((temp[0] == 0x88) && (temp[1] == 0x39))
        {
        packet_length = temp[0] - 0x43;
        temp[0] = temp[0] - packet_length;
        }
    else if ((temp[0] != 'I') && (temp[1] != 'D'))
        {
        packet_length = temp[0] - 0x53;
        temp[0] = temp[0] - packet_length;
        }


    if ((temp[0] == 'I') && (temp[1] == 'D'))
        {
        /*        temp_amr = temp[2] * 256;
                temp_amr = temp_amr + temp[3];
                amr_id = temp_amr;*/

        pulseCounter.byte[3] = temp[2];
        pulseCounter.byte[2] = temp[3];
        pulseCounter.byte[1] = temp[4];
        pulseCounter.byte[0] = temp[5];
        amr_id = pulseCounter.long_data;

        /*
        WriteByte_EEPROM(EE_AMR_ID,temp[2]);
        WriteByte_EEPROM(EE_AMR_ID+1,temp[3]);*/

        WriteByte_EEPROM(EE_AMR_ID_4byte + 0, pulseCounter.byte[3]);
        WriteByte_EEPROM(EE_AMR_ID_4byte + 1, pulseCounter.byte[2]);
        WriteByte_EEPROM(EE_AMR_ID_4byte + 2, pulseCounter.byte[1]);
        WriteByte_EEPROM(EE_AMR_ID_4byte + 3, pulseCounter.byte[0]);

        temp_client = temp[6] * 256;
        temp_client = temp_client + temp[7];
        Client = temp_client;

        //EE_CLIENT_ID

        WriteByte_EEPROM(EE_CLIENT_ID, temp[6]);
        WriteByte_EEPROM(EE_CLIENT_ID + 1, temp[7]);

        //		Write_NI_Para(amr_id);
        Id_frame_received = 1;

        send_id_frame_flage = 0;
        Time.Hour = temp[11]; // if currept then set Default RTC Time
        Time.Min = temp[12];
        Time.Sec = temp[13];
        Date.Date = temp[8];
        Date.Month = temp[9];
        Date.Year = temp[10];
        RTC_SET_TIME(Time);
        RTC_SET_DATE(Date);
        Local_Time.Hour = Time.Hour;
        Local_Time.Min = Time.Min;
        Local_Time.Sec = Time.Sec;
        Local_Date.Date = Date.Date;
        Local_Date.Month = Date.Month;
        Local_Date.Year = Date.Year;

        RTC_need_to_varify = 1;
        RTC_power_on_Logic_Timer = 0; // for testing
        RTC_Faulty_Shift_to_Local_Timer = 1; //When GPS not available and RTC becomes faulty then shift to local timer on DCU time sync

        if (GPS_error_condition.bits.b7 == 1) // invoke GPS if available so if sender's time wrong then SLC correct it by GPS.
            {
            //send_gps_command();
            Time_set_by_GPS = 0;
            need_to_send_GPS_command = 1;
            }

        Rec_port = temp[15] * 256;
        Rec_port = Rec_port + temp[16];

        send_port = temp[17] * 256;
        send_port = Rec_port + temp[18];

        temp_hour = Time.Hour;

        nic_reset_cnt2 = 0;
        nic_reset_cnt3 = 0;
        nic_reset_timer = 0;
        Interface_down = 0;
        Multicast_Interface_down = 0;

        RF_network_time_out = 0;
        Valid_DCU = 1;
        WriteByte_EEPROM(EE_Valid_DCU, Valid_DCU);
        LED_2 = 0;

        }
        //   else if((temp[0] == 'B') && (temp[1] == 'T') && (temp[2] == 0x55) && (temp[3] == 0x55))
        //   {
        //       asm("reset");
        //   }
        /*
        else if((temp[0] == 'D') && (temp[1] == 'M'))
        {
            if((temp[2] == 'I') && (temp[3] == 'D') && (temp[4] == 'R'))
            {
                if(temp[5] == 1)
                {
                    idimmer_Id_frame_received = 1;
                }	
            }
            else if((temp[2] == 'V') && (temp[3] == 'R'))
            {
                if(temp[4] == 1)
                {
                    dimming_cmd_ack = 1;
                    current_dimval = idimmer_dimval;		    			 
                }	
            }
            else if(temp[2] == 'R')	
            {
                Dimvalue = temp[3];	
                Dimvalue = 100 - Dimvalue;
            }
            else if(temp[2] == 'T')
            {
                if(temp[3] == 1)
                {
                    Pro_count = 5;
                    Send_dimming_short_ack = 1;
                }	
            }			
        }*/
    else if ((temp[0] == 'S') && (temp[1] == 'L'))
        {/*
        temp_amr = temp[2] * 256;
        temp_amr = temp_amr + temp[3];*/

        pulseCounter.byte[3] = temp[2];
        pulseCounter.byte[2] = temp[3];
        pulseCounter.byte[1] = temp[4];
        pulseCounter.byte[0] = temp[5];
        temp_amr = pulseCounter.long_data;


        if ((temp_amr == amr_id) || (temp_amr == 0x00)) // if generated SLC ID match with acthual SLC ID or broadcast message receive then parse it else ignore it.
            {

            nic_reset_cnt2 = 0;
            nic_reset_cnt3 = 0;
            nic_reset_timer = 0;


            if (RF_Modem_detected != 1)
                {
                track_res[0] = temp[0];
                track_res[1] = temp[1];
                track_res[2] = temp[2];
                track_res[3] = temp[3];
                track_res[4] = temp[4];
                track_res[5] = temp[5];
                track_res[6] = Client / 256;
                track_res[7] = Client % 256;
                track_res[8] = temp[6];
                track_res[9] = temp[7];
                track_res[10] = temp[packet_length - 4];
                track_res[11] = temp[packet_length - 3];
                track_res[12] = temp[packet_length - 2];
                track_res[13] = temp[packet_length - 1];

                if ((temp[6] != 'A') && (temp[7] != GET_MODE) \
	                && (temp[7] != GET_DIMM_SCH_PARA) && (temp[7] != GET_VERSION_PARA) \
	                && (temp[7] != GET_ADAPTIVE_DIMMING) && (temp[7] != GET_MOTION_DIMMING) \
	                && (temp[7] != GET_FAULT_PARA) && (temp[7] != GET_SCH) && (temp[7] != GET_CMD_DALI_AO_CONFIG) \
	                && (temp[7] != GET_DIMMING_SCH)&& (temp[7] != GET_VERSION_INFO) \
	                && (temp[7] != GET_PHOTOCELL_CONFIG)&& (temp[7] != GET_ADAPTIVE_DIMMING_PARA) \
	                && (temp[7] != GET_SLC_DST_CONFIG_PARA)&& (temp[7] != GET_SLC_CONFIG_PARA) \
	                && (temp[7] != GET_SLC_MIX_MODE_SCH) && (temp[7] != GET_GPS_CONFIG) \
	                && (temp[6] != 0xed) \
	                && ((temp[10] != 'L') && (temp[11] != 'D'))	\
					&&((temp[7] != 0X22) && (temp[8] != 'A'))	\
					&&((temp[8] != 0X0f) && (temp[9] != 0xf0)) \
					&& (temp[6] != 'E') \
					&& (temp[7] != 0xEB) \
					&& (temp[7] != 0xEA) \
					&& ((temp[7] != 0xEF)&&(temp[8] != 0xE4)&&(temp[9] != 0x4E)) \
					&& (temp[6] != 0xee) \
					&& (temp[7] != CURV_TYPE_GET)) \

                    Send_data_RF1(track_res, 14, 0);

                if ((temp[10] == 'E') &&(temp[11] == 'R') &&(temp[12] == 'S'))
                    Send_data_RF1(track_res, 14, 0);

                if ((temp[7] == 0x35) || (temp[6] == 0x53) || (temp[6] == 0x5A) || (temp[6] == 0x58))
                    Send_data_RF1(track_res, 14, 0);

                if ((temp[6] == 0x43)&&(temp[7] == 0x06)&&(temp[8] == 0x41))
                    Send_data_RF1(track_res, 14, 0);

                if ((temp[7] == 0xD5)&&(temp[8] == 0x0f)&&(temp[9] == 0xf0))
                    Send_data_RF1(track_res, 14, 0);
                if ((temp[7] == RESET_CMD)&&(temp[8] == 0x0f)&&(temp[9] == 0xf0))
                    Send_data_RF1(track_res, 14, 0);
                if ((temp[7] == EM_RESET_CMD)&&(temp[8] == 0x0f)&&(temp[9] == 0xf0))
                    Send_data_RF1(track_res, 14, 0);

                if ((temp[7] == SET_CMD_DALI_AO_CONFIG)&&(temp[8] == 0xaf)&&(temp[9] == 0xfa))
                    Send_data_RF1(track_res, 14, 0);

                if ((temp[6] == 0x43)&&(temp[7] == 0x02))
                    Send_data_RF1(track_res, 14, 0);

                if ((temp[6] == 0x43)&&(temp[7] == SET_AUTO_RECOVER_FAULT)&&(temp[8] == 0xE4)&&(temp[9] == 0x4E))
                    Send_data_RF1(track_res, 14, 0);

                if (temp[6] == 0xEC)
                    Send_data_RF1(track_res, 14, 0);


                }
            if (temp_amr == 0x00)
                {
                broadcast_received = 1;
                broadcsat_received_timer = 0;
                }

            switch (temp[6])
                {
                case CMD:
                    if (temp[7] == DATA_QUERY)
                        {
                        Frm_no = 255;
                        RF_network_time_out = 0;
                        RF_network_faulty = 0;
                        if (temp_amr == 0x00)
                            {

                            }
                        else
                            {
                            Send_Data = 1;
                            fill_em_data_once = 0;
                            read_dim_val_timer = 2;
                            Read_db_val = 1;
                            senddata_retry_timer = 6;
                            retry_send_data_cnt = 4;
                            read_dim_val_timer1 = push_time_slice_ms;
                            nic_reset_cnt2 = 0;
                            nic_reset_cnt3 = 0;
                            nic_reset_timer = 0;
                            Interface_down = 0;
                            Multicast_Interface_down = 0;
                            temp_hour = Time.Hour;

                            }
                        }
                    else if (temp[7] == SET_RTC)
                        {
                        if (temp[8] > 0 && temp[8] <= 31)
                            {
                            LocalDate.Date = temp[8];
                            }
                        else
                            {
                            check_time = 1;
                            }

                        if (temp[9] > 0 && temp[9] <= 12)
                            {
                            LocalDate.Month = temp[9];
                            }
                        else
                            {
                            check_time = 1;
                            }


                        if (temp[10] > 0 && temp[10] <= 99)
                            {
                            LocalDate.Year = temp[10];
                            }
                        else
                            {
                            check_time = 1;
                            }

                        if (temp[11] < 24)
                            {
                            LocalTime.Hour = temp[11];
                            }
                        else
                            {
                            check_time = 1;
                            }

                        if (temp[12] < 60)
                            {
                            LocalTime.Min = temp[12];
                            }
                        else
                            {
                            check_time = 1;
                            }

                        if (temp[13] < 60)
                            {
                            LocalTime.Sec = temp[13];
                            }
                        else
                            {
                            check_time = 1;
                            }

                        automode = temp[14] - 0x30;

                        WriteByte_EEPROM(EE_LOGICMODE, automode);
                        previous_mode = automode;
                        WriteByte_EEPROM(EE_PREV_MODE, previous_mode); // set time and date here and check give replay.


                        }
                    else if (temp[7] == GET_RTC)
                        {
                        Get_rtc_Replay = 1;
                        }
                    else if (temp[7] == ERS_EEP)
                        {
                        pulses_r = 0; //erase KWH reading.
                        pulseCounter.long_data = pulses_r;
                        WriteByte_EEPROM(EE_KWH + 0, pulseCounter.byte[0]);
                        WriteByte_EEPROM(EE_KWH + 1, pulseCounter.byte[1]);
                        WriteByte_EEPROM(EE_KWH + 2, pulseCounter.byte[2]);
                        WriteByte_EEPROM(EE_KWH + 3, pulseCounter.byte[3]);

                        newkwh = 0;
                        kwh = 0;

                        pulseCounter.float_data = 0; //	erase lemp burn hour.
                        WriteByte_EEPROM(EE_BURN_HOUR + 0, pulseCounter.byte[3]);
                        WriteByte_EEPROM(EE_BURN_HOUR + 1, pulseCounter.byte[2]);
                        WriteByte_EEPROM(EE_BURN_HOUR + 2, pulseCounter.byte[1]);
                        WriteByte_EEPROM(EE_BURN_HOUR + 3, pulseCounter.byte[0]);
                        lamp_burn_hour = 0;

                        detect_lamp_current = 0;
                        WriteByte_EEPROM(EE_LAMP_STADY_CURRENT_SET, detect_lamp_current);

                        pulseCounter.float_data = 0.0;
                        WriteByte_EEPROM(EE_LAMP_STADY_CURRENT + 0, pulseCounter.byte[3]);
                        WriteByte_EEPROM(EE_LAMP_STADY_CURRENT + 1, pulseCounter.byte[2]);
                        WriteByte_EEPROM(EE_LAMP_STADY_CURRENT + 2, pulseCounter.byte[1]);
                        WriteByte_EEPROM(EE_LAMP_STADY_CURRENT + 3, pulseCounter.byte[0]);
                        lamp_current = 0;
                        check_current_firsttime_counter = 0;
                        current_creep_limit = lamp_current * 0.1;
                        if (current_creep_limit < 0.05)
                            {
                            current_creep_limit = 0.05; // if current creep limit less then 0.03 make it 0.03	
                            }
                        else if (current_creep_limit > 0.1)
                            {
                            current_creep_limit = 0.1; // if current creep limit more then 0.1 make it 0.1
                            }
                        else
                            {

                            }


#ifdef NEW_LAMP_FAULT_LOGIC
                        KW_Threshold = 0;
                        //halCommonSetToken(TOKEN_KW_Threshold,&KW_Threshold);    //EEPROMSAN


                        //halCommonSetToken(TOKEN_KW_Threshold,&KW_Threshold);   //EEPROMSAN

                        pulseCounter.float_data = 0.0;
                        WriteByte_EEPROM(EE_KW_Threshold + 0, pulseCounter.byte[3]);
                        WriteByte_EEPROM(EE_KW_Threshold + 1, pulseCounter.byte[2]);
                        WriteByte_EEPROM(EE_KW_Threshold + 2, pulseCounter.byte[1]);
                        WriteByte_EEPROM(EE_KW_Threshold + 3, pulseCounter.byte[0]);
#endif	

                        // cisco_nic_reset =0;
                        }
                    else if (temp[7] == LL_AMR)
                        {
                        pulseCounter.byte[3] = temp[8];
                        pulseCounter.byte[2] = temp[9];
                        pulseCounter.byte[1] = temp[10];
                        pulseCounter.byte[0] = temp[11];
                        Latitude = pulseCounter.float_data;
                        WriteByte_EEPROM(EE_LATITUDE + 0, temp[8]);
                        WriteByte_EEPROM(EE_LATITUDE + 1, temp[9]);
                        WriteByte_EEPROM(EE_LATITUDE + 2, temp[10]);
                        WriteByte_EEPROM(EE_LATITUDE + 3, temp[11]);


                        pulseCounter.byte[3] = temp[12];
                        pulseCounter.byte[2] = temp[13];
                        pulseCounter.byte[1] = temp[14];
                        pulseCounter.byte[0] = temp[15];
                        longitude = pulseCounter.float_data;
                        WriteByte_EEPROM(EE_LONGITUDE + 0, temp[12]);
                        WriteByte_EEPROM(EE_LONGITUDE + 1, temp[13]);
                        WriteByte_EEPROM(EE_LONGITUDE + 2, temp[14]);
                        WriteByte_EEPROM(EE_LONGITUDE + 3, temp[15]);


                        pulseCounter.byte[3] = temp[16];
                        pulseCounter.byte[2] = temp[17];
                        pulseCounter.byte[1] = temp[18];
                        pulseCounter.byte[0] = temp[19];
                        utcOffset = pulseCounter.float_data;
                        WriteByte_EEPROM(EE_TIMEZONE + 0, temp[16]);
                        WriteByte_EEPROM(EE_TIMEZONE + 1, temp[17]);
                        WriteByte_EEPROM(EE_TIMEZONE + 2, temp[18]);
                        WriteByte_EEPROM(EE_TIMEZONE + 3, temp[19]);


                        pulseCounter.byte[1] = temp[20];
                        pulseCounter.byte[0] = temp[21];
                        Sunset_delay = pulseCounter.int_data;
                        WriteByte_EEPROM(EE_Sunset_delay + 0, temp[20]);
                        WriteByte_EEPROM(EE_Sunset_delay + 1, temp[21]);

                        pulseCounter.byte[1] = temp[22];
                        pulseCounter.byte[0] = temp[23];
                        Sunrise_delay = pulseCounter.int_data;
                        WriteByte_EEPROM(EE_Sunrise_delay + 0, temp[22]);
                        WriteByte_EEPROM(EE_Sunrise_delay + 1, temp[23]);


                        GetSCHTimeFrom_LAT_LOG();
                        EEWriteAstroSch();
                        }
                    else if (temp[7] == SYNC_RTC)
                        {
                        if (temp[8] > 0 && temp[8] <= 31)
                            {
                            LocalDate.Date = temp[8];
                            }
                        else
                            {
                            check_time = 1;
                            }

                        if (temp[9] > 0 && temp[9] <= 12)
                            {
                            LocalDate.Month = temp[9];
                            }
                        else
                            {
                            check_time = 1;
                            }


                        if (temp[10] > 0 && temp[10] <= 99)
                            {
                            LocalDate.Year = temp[10];
                            }
                        else
                            {
                            check_time = 1;
                            }

                        if (temp[11] < 24)
                            {
                            LocalTime.Hour = temp[11];
                            }
                        else
                            {
                            check_time = 1;
                            }

                        if (temp[12] < 60)
                            {
                            LocalTime.Min = temp[12];
                            }
                        else
                            {
                            check_time = 1;
                            }

                        if (temp[13] < 60)
                            {
                            LocalTime.Sec = temp[13];
                            }
                        else
                            {
                            check_time = 1;
                            }


                        if (check_time == 0)
                            {
                            RTC_SET_TIME(LocalTime);
                            RTC_SET_DATE(LocalDate);
                            if (temp[14] <= 1)
                                {
                                Time_change_dueto_DST = temp[14];
                                WriteByte_EEPROM(EE_Time_change_dueto_DST, Time_change_dueto_DST);
                                }
                            Local_Time.Hour = LocalTime.Hour;
                            Local_Time.Min = LocalTime.Min;
                            Local_Time.Sec = LocalTime.Sec;
                            Local_Date.Date = LocalDate.Date;
                            Local_Date.Month = LocalDate.Month;
                            Local_Date.Year = LocalDate.Year;
                            RTC_need_to_varify = 1;
                            RTC_power_on_Logic_Timer = 0; // for testing
                            RTC_Faulty_Shift_to_Local_Timer = 1; //When GPS not available and RTC becomes faulty then shift to local timer on DCU time sync
                            if (GPS_error_condition.bits.b7 == 1) // invoke GPS if available so if sender's time wrong then SLC correct it by GPS.
                                {
                                //send_gps_command();
                                Time_set_by_GPS = 0;
                                need_to_send_GPS_command = 1;
                                }
                            }
                        }
                    else if (temp[7] == CLOUDY_FLAG)
                        {
                        Cloudy_Flag = temp[8];
                        }
                    else if (temp[7] == ERASE_TRIP_SLC)
                        {
                        lamp_lock = 0;
                        low_current_counter = 0;
                        cycling_lamp_fault = 0;
                        photo_cell_toggel_counter = 0;
                        photo_cell_toggel_timer = 0;
                        photo_cell_timer = 0;
                        Photo_Cell_Ok = 1;
                        Master_event_generation = 0;
                        event_counter = 0;
                        energy_meter_ok = 0;
                        energy_time = 0;
                        error_condition.bits.b4 = 0;
                        error_condition.bits.b6 = 0;
                        error_condition.bits.b3 = 0;
                        error_condition1.bits.b0 = 0;
                        error_condition1.bits.b3 = 0;
                        error_condition1.bits.b4 = 0;

                        error_condition1.bits.b6 = 0; // Rest Dimming short circuit event.  // SAN	
                        Protec_lock = 0; // Rest Dimming Lock // SAN
                        Pro_count = 0;
                        Tilt_send_cnt = 0;


                        if (Lamp_lock_condition == 1)
                            {
                            Set_Do(0);
                            }
                        //delet_all_data();
                        Motion_broadcast_miss_counter = 0;
                        }
                    else if (temp[7] == EVENT_START)
                        {
                        if (temp[8] == 1)
                            {
                            stop_event = 0;
                            }
                        }
                    else if (temp[7] == EVENT_QUERY)
                        {
                        //	event_no = temp[9];
                        read_event_flag = 1;
                        rf_event_send_counter = Unicast_time_delay;
                        }
                    else if (temp[7] == GET_MODE)
                        {
                        send_get_mode_replay = 1;
                        }
                    else if (temp[7] == SET_FAULT_PARA)
                        {
                        pulseCounter.byte[1] = temp[8];
                        pulseCounter.byte[0] = temp[9];
                        Vol_hi = pulseCounter.int_data;
                        WriteByte_EEPROM(EE_Vol_hi + 0, temp[8]);
                        WriteByte_EEPROM(EE_Vol_hi + 1, temp[9]);

                        pulseCounter.byte[1] = temp[10];
                        pulseCounter.byte[0] = temp[11];
                        Vol_low = pulseCounter.int_data;
                        WriteByte_EEPROM(EE_Vol_low + 0, temp[10]);
                        WriteByte_EEPROM(EE_Vol_low + 1, temp[11]);

                        pulseCounter.byte[1] = temp[12];
                        pulseCounter.byte[0] = temp[13];
                        Curr_Steady_Time = pulseCounter.int_data;
                        WriteByte_EEPROM(EE_Curr_Steady_Time + 0, temp[12]);
                        WriteByte_EEPROM(EE_Curr_Steady_Time + 1, temp[13]);

                        pulseCounter.byte[1] = temp[14];
                        pulseCounter.byte[0] = temp[15];
                        Per_Val_Current = pulseCounter.int_data;
                        WriteByte_EEPROM(EE_Per_Val_Current + 0, temp[14]);
                        WriteByte_EEPROM(EE_Per_Val_Current + 1, temp[15]);

                        pulseCounter.byte[1] = temp[16];
                        pulseCounter.byte[0] = temp[17];
                        Lamp_Fault_Time = pulseCounter.int_data;
                        WriteByte_EEPROM(EE_Lamp_Fault_Time + 0, temp[16]);
                        WriteByte_EEPROM(EE_Lamp_Fault_Time + 1, temp[17]);

                        pulseCounter.byte[1] = temp[18];
                        pulseCounter.byte[0] = temp[19];
                        Lamp_off_on_Time = pulseCounter.int_data;
                        WriteByte_EEPROM(EE_Lamp_off_on_Time + 0, temp[18]);
                        WriteByte_EEPROM(EE_Lamp_off_on_Time + 1, temp[19]);

                        pulseCounter.byte[1] = temp[20];
                        pulseCounter.byte[0] = temp[21];
                        Lamp_faulty_retrieve_Count = pulseCounter.int_data;
                        WriteByte_EEPROM(EE_Lamp_faulty_retrieve_Count + 0, temp[20]);
                        WriteByte_EEPROM(EE_Lamp_faulty_retrieve_Count + 1, temp[21]);

                        pulseCounter.byte[1] = temp[22];
                        pulseCounter.byte[0] = temp[23];
                        Lamp_lock_condition = pulseCounter.int_data;
                        WriteByte_EEPROM(EE_Lamp_lock_condition + 0, temp[22]);
                        WriteByte_EEPROM(EE_Lamp_lock_condition + 1, temp[23]);

                        }
                    else if (temp[7] == GET_FAULT_PARA)
                        {
                        Send_Fault_Para_Replay = 1;
                        }
                    else if (temp[7] == SET_NETWORK_PARA)
                        {
                        pulseCounter.byte[1] = temp[10];
                        pulseCounter.byte[0] = temp[11];
                        Unicast_time_delay = pulseCounter.int_data;
                        WriteByte_EEPROM(EE_Unicast_time_delay + 0, temp[10]);
                        WriteByte_EEPROM(EE_Unicast_time_delay + 1, temp[11]);
                        }
                    else if (temp[7] == GET_NETWORK_PARA)
                        {
                        Send_Network_Para_Replay = 1;
                        }
                    else if (temp[7] == GET_SCH)
                        {
                        Send_Get_Sch_Replay = 1;
                        Get_Sch_timer = 5;
                        for (k = 0; k < 7; k++)
                            {
                            sch_day_send[k] = 0;
                            }
                        }
                    else if (temp[7] == SLC_TEST_QUERY_1)
                        {
                        // commented 
                        Master_event_generation = 1;

                        Date.Date = temp[8];
                        Date.Month = temp[9];
                        Date.Year = temp[10];
                        RTC_SET_DATE(Date);
                        Time.Hour = temp[11]; // if currept then set Default RTC Time
                        Time.Min = temp[12];
                        Time.Sec = temp[13];
                        photocell_test_dicision = temp[14];
                        Dimming_test_dicision = temp[15];
                        RS485_0_Or_DI2_1 = temp[16];
                        WriteByte_EEPROM(EE_RS485_0_Or_DI2_1, RS485_0_Or_DI2_1);
                        RTC_SET_TIME(Time);

                        if (SLC_Test_Query_2_received == 0)
                            {
                            Check_Slc_Test_1();
                            }

                        SLC_Test_Query_2_received = 1;
                        Commissioning_flag = 1; // Set commissioning flag in SLC.

                        // halCommonSetToken(TOKEN_Commissioning_flag,&Commissioning_flag);

                        GPS_Read_Enable = 0; //GPS changes the coordinate so get impact in testing so make it disable in test routine.

                        }
                    else if (temp[7] == SLC_TEST_QUERY_1_ACK)
                        {
                        if ((temp[8] == 'A') && (temp[9] == 'C') && (temp[10] == 'K'))
                            {
                            send_SLC_test_query_1 = 0;
                            Master_event_generation = 0;
                            }
                        }
                        //---------------------------------------------------------//
                    else if (temp[7] == SLC_TEST_QUERY_2)
                        {
                        send_SLC_test_query_2 = 1;
                        test_query_1_send_timer = 0;
                        Master_event_generation = 1;
                        SLC_Test_Query_2_received = 1;
                        GPS_Read_Enable = 0;
                        Commissioning_flag = 1; // Set commissioning flag in SLC.

                        }
                    else if (temp[7] == SLC_TEST_QUERY_2_ACK)
                        {
                        if ((temp[8] == 'A') && (temp[9] == 'C') && (temp[10] == 'K'))
                            {
                            send_SLC_test_query_2 = 0;
                            Master_event_generation = 0;
                            test_query_1_send_timer = 0;
                            }
                        }
                    else if (temp[7] == GET_DIMMING_SCH) // Master sch 15/04/11	// V6.1.10
                        {
                        Send_Get_Master_Sch_Replay = 1;
                        Get_Master_Sch_timer = 5;
                        for (k = 0; k < 7; k++)
                            {
                            sch_Master_day_send[k] = 0;
                            }
                        }
                    else if (temp[7] == GET_VERSION_INFO) // V6.1.10
                        {
                        Send_Get_Version_info_Replay = 1;
                        }
                    else if (temp[7] == ADAPTIVE_DIMMING_PARA)
                        {

                        adaptive_light_dimming = temp[16];
                        WriteByte_EEPROM(EE_adaptive_light_dimming, temp[16]);

                        if (adaptive_light_dimming == 0) // normal dimming schedule
                            {
                            ballast_type = 1;
                            WriteByte_EEPROM(EE_ballast_type, ballast_type);

                            dim_applay_time = temp[22] * 60; // Dimming_Apply_delay 

                            WriteByte_EEPROM(EE_dim_applay_time + 0, dim_applay_time / 256);
                            WriteByte_EEPROM(EE_dim_applay_time + 1, dim_applay_time % 256);

                            dim_inc_val = temp[23]; // gradually_dimming_time 																	
                            WriteByte_EEPROM(EE_dim_inc_val, dim_inc_val);

                            }
                        if (adaptive_light_dimming == 1) // day light harvestin
                            {
                            pulseCounter.byte[1] = temp[8];
                            pulseCounter.byte[0] = temp[9];
                            analog_input_scaling_high_Value = pulseCounter.int_data;
                            WriteByte_EEPROM(EE_analog_input_scaling_high_Value + 0, temp[8]);
                            WriteByte_EEPROM(EE_analog_input_scaling_high_Value + 1, temp[9]);


                            pulseCounter.byte[1] = temp[10];
                            pulseCounter.byte[0] = temp[11];
                            analog_input_scaling_low_Value = pulseCounter.int_data;
                            WriteByte_EEPROM(EE_analog_input_scaling_low_Value + 0, temp[10]);
                            WriteByte_EEPROM(EE_analog_input_scaling_low_Value + 1, temp[11]);

                            pulseCounter.byte[1] = temp[12];
                            pulseCounter.byte[0] = temp[13];
                            desir_lamp_lumen = pulseCounter.int_data;
                            WriteByte_EEPROM(EE_desir_lamp_lumen + 0, temp[12]);
                            WriteByte_EEPROM(EE_desir_lamp_lumen + 1, temp[13]);

                            lumen_tollarence = temp[14];
                            WriteByte_EEPROM(EE_lumen_tollarence, temp[14]);

                            dim_applay_time = temp[22] * 60; // Dimming_Apply_delay 
                            //WriteByte_EEPROM(EE_dim_applay_time,dim_applay_time);
                            WriteByte_EEPROM(EE_dim_applay_time + 0, dim_applay_time / 256);
                            WriteByte_EEPROM(EE_dim_applay_time + 1, dim_applay_time % 256);

                            dim_inc_val = temp[23]; // gradually_dimming_time 																	
                            WriteByte_EEPROM(EE_dim_inc_val, dim_inc_val);

                            pulseCounter.byte[1] = temp[25];
                            pulseCounter.byte[0] = temp[26];

                            Day_light_harvesting_start_offset = pulseCounter.int_data;
                            Day_light_harvesting_start_offset = Day_light_harvesting_start_offset * 60;
                            pulseCounter.int_data = Day_light_harvesting_start_offset;

                            WriteByte_EEPROM(EE_Day_light_harvesting_start_offset + 0, pulseCounter.byte[1]);
                            WriteByte_EEPROM(EE_Day_light_harvesting_start_offset + 1, pulseCounter.byte[0]);

                            pulseCounter.byte[1] = temp[27];
                            pulseCounter.byte[0] = temp[28];
                            Day_light_harvesting_stop_offset = pulseCounter.int_data;
                            Day_light_harvesting_stop_offset = Day_light_harvesting_stop_offset * 60;

                            pulseCounter.int_data = Day_light_harvesting_stop_offset;
                            WriteByte_EEPROM(EE_Day_light_harvesting_stop_offset + 0, pulseCounter.byte[1]);
                            WriteByte_EEPROM(EE_Day_light_harvesting_stop_offset + 1, pulseCounter.byte[0]);

                            ballast_type = 1;
                            WriteByte_EEPROM(EE_ballast_type, ballast_type);

                            Day_light_harves_time();
                            }
                        else if (adaptive_light_dimming == 2) // motion based dimming
                            {

                            Motion_pulse_rate = temp[17];
                            if (Motion_pulse_rate == 0)
                                {
                                Motion_pulse_rate = 1;
                                }
                            WriteByte_EEPROM(EE_Motion_pulse_rate, Motion_pulse_rate);

                            Motion_dimming_percentage = temp[18];
                            WriteByte_EEPROM(EE_Motion_dimming_percentage, Motion_dimming_percentage);

                            Motion_dimming_time = (temp[19] * 256) + temp[20];
                            if (Motion_dimming_time < 10)
                                {
                                Motion_dimming_time = 10;
                                }

                            if (Motion_dimming_time > 59)
                                {
                                Motion_Rebroadcast_timeout = 58;
                                }
                            else
                                {
                                Motion_Rebroadcast_timeout = Motion_dimming_time - 2;
                                }

                            WriteByte_EEPROM(EE_Motion_dimming_time + 0, Motion_dimming_time / 256);
                            WriteByte_EEPROM(EE_Motion_dimming_time + 1, Motion_dimming_time % 256);

                            Motion_group_id = temp[21];
                            Group_val1.Val = Motion_group_id;
                            WriteByte_EEPROM(EE_Motion_group_id, Motion_group_id);

                            dim_applay_time = temp[22] * 60; // Dimming_Apply_delay 

                            WriteByte_EEPROM(EE_dim_applay_time + 0, dim_applay_time / 256);
                            WriteByte_EEPROM(EE_dim_applay_time + 1, dim_applay_time % 256);

                            dim_inc_val = temp[23]; // gradually_dimming_time 																	
                            WriteByte_EEPROM(EE_dim_inc_val, dim_inc_val);

                            Motion_normal_dimming_percentage = temp[24];
                            WriteByte_EEPROM(EE_Motion_normal_dimming_percentage, Motion_normal_dimming_percentage);

                            Motion_Detect_Timeout = temp[29];
                            if ((Motion_Detect_Timeout >= Motion_dimming_time) || (Motion_Detect_Timeout > 59) || (Motion_Detect_Timeout == 0))
                                {
                                Motion_Detect_Timeout = 5;
                                }
                            WriteByte_EEPROM(EE_Motion_Detect_Timeout, Motion_Detect_Timeout);

                            Motion_Broadcast_Timeout = temp[30];
                            if ((Motion_Broadcast_Timeout >= Motion_dimming_time) || (Motion_Broadcast_Timeout > 59) || (Motion_Broadcast_Timeout == 0))
                                {
                                Motion_Broadcast_Timeout = 5;
                                }
                            WriteByte_EEPROM(EE_Motion_Broadcast_Timeout, Motion_Broadcast_Timeout);

                            Motion_Sensor_Type = temp[31];
                            WriteByte_EEPROM(EE_Motion_Sensor_Type, Motion_Sensor_Type);

                            ballast_type = 1;
                            WriteByte_EEPROM(EE_ballast_type, ballast_type);
                            }
                        else if (adaptive_light_dimming == 3) // discrite dimming
                            {
                            ballast_type = 0;
                            WriteByte_EEPROM(EE_ballast_type, temp[15]);
                            }
                        else
                            {
                            }
                        }
                    else if (temp[7] == GET_ADAPTIVE_DIMMING_PARA)
                        {
                        Send_Get_Adaptive_dimming_para_Replay = 1;
                        }
                    else if (temp[7] == ID_FRAME_STOP)
                        {
                        if ((temp[8] == 0x0f)&&(temp[9] == 0xf0))
                            {
                            Id_frame_received = 1;


                            }
                        }
                    else if (temp[7] == MOTION_DETECT)
                        {
                        Motion_broadcast_Receive_counter++;
                        pulseCounter.byte[3] = temp[9];
                        pulseCounter.byte[2] = temp[10];
                        pulseCounter.byte[1] = temp[11];
                        pulseCounter.byte[0] = temp[12];
                        Temp_Motion_broadcast_Receive_counter = pulseCounter.long_data;
                        if (Motion_broadcast_Receive_counter < Temp_Motion_broadcast_Receive_counter)
                            {
                            Motion_broadcast_miss_counter++;
                            }
                        Motion_broadcast_Receive_counter = Temp_Motion_broadcast_Receive_counter;

                        Temp_Slc_Group_Match = check_group(temp[8]);
                        Temp_Slc_Multi_broadcast_group = check_multi_group(temp[8]);

                        if ((Temp_Slc_Multi_group == 1) && (Temp_Slc_Multi_broadcast_group == 1))
                            {
                            if ((Temp_Slc_Group_Match == 1))
                                {
                                Motion_intersection_detected = 1;
                                motion_intersection_dimming_timer = 0;
                                }
                            }
                        else
                            {

                            if ((Temp_Slc_Group_Match == 1))
                                {
                                Motion_intersection_detected = 0;
                                Motion_detected_broadcast = 1;
                                if (Temp_Slc_Multi_group == 0)
                                    {
                                    Motion_Received_broadcast = 1;
                                    Motion_Continiue = 0;
                                    motion_broadcast_timer = 0;
                                    Motion_continue_timer = 0;
                                    }
                                else
                                    {
                                    Motion_Received_broadcast = 1;
                                    Motion_detected = 0;
                                    }
                                //							Motion_Continiue = 0;
                                //							motion_broadcast_timer = 0;
                                //							Motion_continue_timer = 0;
                                if (Motion_detected == 0)
                                    {
                                    //								Motion_broadcast_Receive_counter++;
                                    //								pulseCounter.byte[3] = temp[7];
                                    //								pulseCounter.byte[2] = temp[8];
                                    //								pulseCounter.byte[1] = temp[9];
                                    //								pulseCounter.byte[0] = temp[10];
                                    //								Temp_Motion_broadcast_Receive_counter = pulseCounter.long_data;
                                    //								if(Motion_broadcast_Receive_counter < Temp_Motion_broadcast_Receive_counter)
                                    //								{
                                    //									Motion_broadcast_Receive_counter = Temp_Motion_broadcast_Receive_counter;
                                    //									Motion_broadcast_miss_counter++;		
                                    //								}	
                                    }
                                else
                                    {

                                    motion_dimming_Broadcast_send = 1;
                                    }

                                motion_dimming_timer = 0;
                                Motion_detect_sec = 0;

                                if (temp_amr != 0)
                                    {
                                    Motion_broadcast_miss_counter = 0;
                                    //WriteDebugMsg("\n Motion Unicast received"); //SAN
                                    }
                                else
                                    {
                                    //WriteDebugMsg("\n Motion broadcast received"); //SAN
                                    }
                                }
                            }
                        }
                    else if (temp[7] == SET_RF_NETWORK_PARA)
                        {

                        }
                    else if (temp[7] == CHANGE_RF_NETWORK_PARA)
                        {

                        }
                    else if (temp[7] == SLC_CONFIG_PARA)
                        {
                        Tilt_Enable = temp[8];
                        WriteByte_EEPROM(EE_Lamp_type, temp[8]);
                        Auto_event = temp[9];
                        WriteByte_EEPROM(EE_Auto_event, Auto_event);
                        Schedule_offset = temp[10];
                        WriteByte_EEPROM(EE_Schedule_offset, Schedule_offset);
                        if ((temp[11] > 0) && (temp[11] < 3))
                            {
                            if ((RTC_New_fault_logic_Enable != 1) && (temp[11] == 1))
                                {
                                RTC_power_on_Logic_Timer = 0;
                                }

                            RTC_New_fault_logic_Enable = temp[11]; // 1 = enable, 2 = disable
                            //halCommonSetToken(EE_RTC_New_fault_logic_Enable, &RTC_New_fault_logic_Enable);
                            WriteByte_EEPROM(EE_RTC_New_fault_logic_Enable, RTC_New_fault_logic_Enable);

                            }
                        }
                        //////////////////////////////
                    else if (temp[7] == GPS_CONFIG)
                        {
                        if ((temp[9] == 1) || (temp[9] == 2))
                            {
                            if (GPS_Read_Enable == 1)
                                {
                                GPS_Rescan = temp[9];
                                if (GPS_Rescan == 1)
                                    {
                                    need_Average_value_Gps = 1;
                                    }
                                else
                                    {
                                    need_Average_value_Gps = 0;
                                    }
                                GPS_read_success_counter = 0;
                                need_to_send_GPS_command = 1;
                                averaging_Timer = 0;
                                Start_averaging_of_GPS = 0;
                                averaging_counter = 0;
                                }
                            }
                        else
                            {
                            if (temp[8] == 1)
                                {
                                GPS_Read_Enable = 1;
                                //halCommonSetToken(TOKEN_GPS_Read_Enable,&GPS_Read_Enable);
                                WriteByte_EEPROM(EE_GPS_Read_Enable, GPS_Read_Enable);
                                }
                            else
                                {
                                GPS_Read_Enable = 0;
                                //halCommonSetToken(TOKEN_GPS_Read_Enable,&GPS_Read_Enable);
                                WriteByte_EEPROM(EE_GPS_Read_Enable, GPS_Read_Enable);

                                if (Packate_send_success == 0)
                                    {
                                    shutdown_GPS();
                                    }
                                }

                            GPS_Wake_timer = (temp[10] * 256) + temp[11];
                            //halCommonSetToken(TOKEN_GPS_Wake_timer,&GPS_Wake_timer);
                            pulseCounter.int_data = GPS_Wake_timer;
                            WriteByte_EEPROM(EE_GPS_Wake_timer + 0, pulseCounter.byte[1]);
                            WriteByte_EEPROM(EE_GPS_Wake_timer + 1, pulseCounter.byte[0]);

                            No_of_setalite_threshold = temp[12];
                            WriteByte_EEPROM(EE_No_of_setalite_threshold, No_of_setalite_threshold);
                            //halCommonSetToken(TOKEN_No_of_setalite_threshold,&No_of_setalite_threshold);
                            Setalite_angle_threshold = temp[13];
                            WriteByte_EEPROM(EE_Setalite_angle_threshold, Setalite_angle_threshold);
                            //halCommonSetToken(TOKEN_Setalite_angle_threshold,&Setalite_angle_threshold);
                            setalite_Rssi_threshold = temp[14];
                            WriteByte_EEPROM(EE_setalite_Rssi_threshold, setalite_Rssi_threshold);
                            //halCommonSetToken(TOKEN_setalite_Rssi_threshold,&setalite_Rssi_threshold);
                            Maximum_no_of_Setalite_reading = temp[15];
                            WriteByte_EEPROM(EE_Maximum_no_of_Setalite_reading, Maximum_no_of_Setalite_reading);
                            //halCommonSetToken(TOKEN_Maximum_no_of_Setalite_reading,&Maximum_no_of_Setalite_reading);

                            pulseCounter.byte[3] = temp[16];
                            pulseCounter.byte[2] = temp[17];
                            pulseCounter.byte[1] = temp[18];
                            pulseCounter.byte[0] = temp[19];
                            HDOP_Gps_threshold = pulseCounter.float_data;
                            pulseCounter.float_data = HDOP_Gps_threshold;
                            WriteByte_EEPROM(EE_HDOP_Gps_threshold + 0, pulseCounter.byte[3]);
                            WriteByte_EEPROM(EE_HDOP_Gps_threshold + 1, pulseCounter.byte[2]);
                            WriteByte_EEPROM(EE_HDOP_Gps_threshold + 2, pulseCounter.byte[1]);
                            WriteByte_EEPROM(EE_HDOP_Gps_threshold + 3, pulseCounter.byte[0]);
                            //halCommonSetToken(TOKEN_HDOP_Gps_threshold,&HDOP_Gps_threshold);

                            Check_Sbas_Enable = temp[20];
                            WriteByte_EEPROM(EE_Check_Sbas_Enable, Check_Sbas_Enable);
                            //halCommonSetToken(TOKEN_Check_Sbas_Enable,&Check_Sbas_Enable);

                            Sbas_setalite_Rssi_threshold = temp[21];
                            WriteByte_EEPROM(EE_Sbas_setalite_Rssi_threshold, Sbas_setalite_Rssi_threshold);
                            //halCommonSetToken(TOKEN_Sbas_setalite_Rssi_threshold,&Sbas_setalite_Rssi_threshold);

                            Auto_Gps_Data_Send = temp[22];

                            WriteByte_EEPROM(EE_Auto_Gps_Data_Send, Auto_Gps_Data_Send);
                            //halCommonSetToken(TOKEN_Auto_Gps_Data_Send,&Auto_Gps_Data_Send);
                            }

                        }
                    else if (temp[7] == GET_GPS_CONFIG)
                        {

                        send_get_Gps_Config = 1;

                        }
                        /////////////////////////////////
                    else if (temp[7] == GET_SLC_CONFIG_PARA)
                        {
                        Send_Get_Slc_Config_para_Replay = 1;
                        }

                    else if (temp[7] == SLC_DST_CONFIG_PARA)
                        {
                        SLC_DST_En = temp[8];
                        WriteByte_EEPROM(EE_SLC_DST_En, SLC_DST_En);
                        if (SLC_DST_En == 1)
                            {
                            SLC_DST_Start_Month = temp[9];
                            WriteByte_EEPROM(EE_SLC_DST_Start_Month, SLC_DST_Start_Month);
                            SLC_DST_Start_Rule = temp[10];
                            WriteByte_EEPROM(EE_SLC_DST_Start_Rule, SLC_DST_Start_Rule);
                            SLC_DST_Start_Time = temp[11];
                            WriteByte_EEPROM(EE_SLC_DST_Start_Time, SLC_DST_Start_Time);
                            SLC_DST_Stop_Month = temp[12];
                            WriteByte_EEPROM(EE_SLC_DST_Stop_Month, SLC_DST_Stop_Month);
                            SLC_DST_Stop_Rule = temp[13];
                            WriteByte_EEPROM(EE_SLC_DST_Stop_Rule, SLC_DST_Stop_Rule);
                            SLC_DST_Stop_Time = temp[14];
                            WriteByte_EEPROM(EE_SLC_DST_Stop_Time, SLC_DST_Stop_Time);

                            pulseCounter.byte[3] = temp[15];
                            pulseCounter.byte[2] = temp[16];
                            pulseCounter.byte[1] = temp[17];
                            pulseCounter.byte[0] = temp[18];
                            SLC_DST_Time_Zone_Diff = pulseCounter.float_data;
                            WriteByte_EEPROM(EE_SLC_DST_Time_Zone_Diff + 0, temp[15]);
                            WriteByte_EEPROM(EE_SLC_DST_Time_Zone_Diff + 1, temp[16]);
                            WriteByte_EEPROM(EE_SLC_DST_Time_Zone_Diff + 2, temp[17]);
                            WriteByte_EEPROM(EE_SLC_DST_Time_Zone_Diff + 3, temp[18]);

                            SLC_DST_Rule_Enable = temp[19];
                            WriteByte_EEPROM(EE_SLC_DST_Rule_Enable, SLC_DST_Rule_Enable);

                            SLC_DST_Start_Date = temp[20];
                            WriteByte_EEPROM(EE_SLC_DST_Start_Date, SLC_DST_Start_Date);
                            SLC_DST_Stop_Date = temp[21];
                            WriteByte_EEPROM(EE_SLC_DST_Stop_Date, SLC_DST_Stop_Date);

                            if (SLC_DST_Rule_Enable == 1)
                                {
                                SLC_DST_R_Start_Date = find_dst_date_from_rule(SLC_DST_Start_Rule, SLC_DST_Start_Month);
                                SLC_DST_R_Stop_Date = find_dst_date_from_rule(SLC_DST_Stop_Rule, SLC_DST_Stop_Month);
                                }
                            else
                                {
                                SLC_DST_R_Start_Date = SLC_DST_Start_Date;
                                SLC_DST_R_Stop_Date = SLC_DST_Stop_Date;
                                }

                            }
                        }
                    else if (temp[7] == GET_SLC_DST_CONFIG_PARA)
                        {
                        send_get_slc_dst_config_para = 1;
                        }
                        //				else if(temp[5] == SLC_MIX_MODE_SCH)
                        //				{
                        //					Configure_Slc_Mix_Mode_Sch(&temp[6]);
                        //				}
                    else if (temp[7] == GET_SLC_MIX_MODE_SCH)
                        {
                        if (temp[8] < 10)
                            {
                            Send_Get_Mix_Mode_Sch = 1;
                            Get_Mix_mode_Sch_Loc = temp[8];
                            }
                        }
                    else if (temp[7] == SLC_MMOD)
                        {
                        if (temp[8] < 101)
                            {
                            SLC_New_Manula_Mode_En = 1; // enable new manual mode
                            SLC_New_Manual_Mode_Timer = (temp[9] * 256) + temp[10];
                            WriteByte_EEPROM(EE_SLC_New_Manual_Mode_Timer + 0, SLC_New_Manual_Mode_Timer / 256);
                            WriteByte_EEPROM(EE_SLC_New_Manual_Mode_Timer + 1, SLC_New_Manual_Mode_Timer % 256);
                            }
                        else
                            {
                            SLC_New_Manula_Mode_En = 0; // disable new manual mode	
                            }

                        SLC_New_Manula_Mode_Val = temp[8];
                        WriteByte_EEPROM(EE_SLC_New_Manula_Mode_Val, SLC_New_Manula_Mode_Val);
                        }
                    else if (temp[7] == SET_LINK_KEY_SLC)
                        {

                        }
                    else if (temp[7] == GET_LINK_KEY_SLC)
                        {
                        Send_Link_key = 1;
                        }
                    else if (temp[7] == CHANGE_KEY)
                        {
                        change_Link_key = 1;
                        change_Link_key_timer = 0;
                        }
                    else if (temp[7] == PHOTOCELL_CONFIG)
                        {
                        Photocell_steady_timeout_Val = temp[8];
                        WriteByte_EEPROM(EE_Photocell_steady_timeout_Val, Photocell_steady_timeout_Val);

                        photo_cell_toggel_counter_Val = temp[9];
                        WriteByte_EEPROM(EE_photo_cell_toggel_counter_Val, photo_cell_toggel_counter_Val);

                        photo_cell_toggel_timer_Val = temp[10];
                        WriteByte_EEPROM(EE_photo_cell_toggel_timer_Val, photo_cell_toggel_timer_Val);

                        Photo_cell_Frequency = temp[11];
                        WriteByte_EEPROM(EE_Photo_cell_Frequency, Photo_cell_Frequency);

                        pulseCounter.byte[1] = temp[12];
                        pulseCounter.byte[0] = temp[13];
                        Photocell_unsteady_timeout_Val = pulseCounter.int_data;
                        WriteByte_EEPROM(EE_Photocell_unsteady_timeout_Val + 0, temp[12]);
                        WriteByte_EEPROM(EE_Photocell_unsteady_timeout_Val + 1, temp[13]);
                        }
                    else if (temp[7] == GET_PHOTOCELL_CONFIG)
                        {
                        send_get_photocell_config = 1;
                        }
                        /****************Added by Parvez Footcandle limit access from LG start*******************/
                    else if (temp[7] == SET_FOOT_CANDLE) // Get SLC configuration parameter command received from DCU.
                        {
                        pulseCounter.byte[3] = temp[8];
                        pulseCounter.byte[2] = temp[9];
                        pulseCounter.byte[1] = temp[10];
                        pulseCounter.byte[0] = temp[11];
                        Photosensor_Set_FC = pulseCounter.float_data;
                        WriteByte_EEPROM(EE_Photosensor_Set_FC + 3, pulseCounter.byte[3]);
                        WriteByte_EEPROM(EE_Photosensor_Set_FC + 2, pulseCounter.byte[2]);
                        WriteByte_EEPROM(EE_Photosensor_Set_FC + 1, pulseCounter.byte[1]);
                        WriteByte_EEPROM(EE_Photosensor_Set_FC + 0, pulseCounter.byte[0]);

                        pulseCounter.byte[3] = temp[12];
                        pulseCounter.byte[2] = temp[13];
                        pulseCounter.byte[1] = temp[14];
                        pulseCounter.byte[0] = temp[15];
                        Photosensor_Set_FC_Lamp_OFF = pulseCounter.float_data;
                        WriteByte_EEPROM(EE_Photosensor_Set_FC_Lamp_OFF + 3, pulseCounter.byte[3]);
                        WriteByte_EEPROM(EE_Photosensor_Set_FC_Lamp_OFF + 2, pulseCounter.byte[2]);
                        WriteByte_EEPROM(EE_Photosensor_Set_FC_Lamp_OFF + 1, pulseCounter.byte[1]);
                        WriteByte_EEPROM(EE_Photosensor_Set_FC_Lamp_OFF + 0, pulseCounter.byte[0]);
                        }
                        /****************Added by Parvez Footcandle limit access from LG stop*******************/
                    else if (temp[7] == GET_FOOT_CANDLE) // Get SLC configuration parameter command received from DCU.
                        {
                        uChar_GET_FOOT_CANDLE = 1;
                        }
                    else if (temp[7] == TEST_DIMMING) // received messag for dimming test from test application.
                        {
                        Test_dimming_val = temp[8];
                        if (Test_dimming_val <= 100)
                            {
                            Start_dimming_test = 1;
                            }
                        else
                            {
                            Start_dimming_test = 0; // if dimming value received more then 100 then stop dimming test.
                            }
                        }

                    else if (temp[7] == CONF_CMD_IN_TEST_MODE)
                        {
                        if ((temp[8] == 0x0f)&&(temp[9] == 0xf0))
                            {
                            automode = temp[10];
                            WriteByte_EEPROM(EE_LOGICMODE, automode);
                            automode = 0;
                            pulseCounter.byte[3] = temp[11];
                            pulseCounter.byte[2] = temp[12];
                            pulseCounter.byte[1] = temp[13];
                            pulseCounter.byte[0] = temp[14];
                            Latitude = pulseCounter.float_data; // Store Latitude
                            WriteByte_EEPROM(EE_LATITUDE + 0, temp[11]);
                            WriteByte_EEPROM(EE_LATITUDE + 1, temp[12]);
                            WriteByte_EEPROM(EE_LATITUDE + 2, temp[13]);
                            WriteByte_EEPROM(EE_LATITUDE + 3, temp[14]);


                            pulseCounter.byte[3] = temp[15];
                            pulseCounter.byte[2] = temp[16];
                            pulseCounter.byte[1] = temp[17];
                            pulseCounter.byte[0] = temp[18];
                            longitude = pulseCounter.float_data; // Store Longitude
                            WriteByte_EEPROM(EE_LONGITUDE + 0, temp[15]);
                            WriteByte_EEPROM(EE_LONGITUDE + 1, temp[16]);
                            WriteByte_EEPROM(EE_LONGITUDE + 2, temp[17]);
                            WriteByte_EEPROM(EE_LONGITUDE + 3, temp[18]);


                            pulseCounter.byte[3] = temp[19];
                            pulseCounter.byte[2] = temp[20];
                            pulseCounter.byte[1] = temp[21];
                            pulseCounter.byte[0] = temp[22];
                            utcOffset = pulseCounter.float_data; // Store TimeZone
                            WriteByte_EEPROM(EE_TIMEZONE + 0, temp[19]);
                            WriteByte_EEPROM(EE_TIMEZONE + 1, temp[20]);
                            WriteByte_EEPROM(EE_TIMEZONE + 2, temp[21]);
                            WriteByte_EEPROM(EE_TIMEZONE + 3, temp[22]);

                            pulseCounter.byte[1] = temp[23];
                            pulseCounter.byte[0] = temp[24];
                            Sunset_delay = pulseCounter.int_data;

                            WriteByte_EEPROM(EE_Sunset_delay + 0, temp[23]);
                            WriteByte_EEPROM(EE_Sunset_delay + 1, temp[24]);

                            pulseCounter.byte[1] = temp[25];
                            pulseCounter.byte[0] = temp[26];
                            Sunrise_delay = pulseCounter.int_data; // Store Sun rise delay

                            WriteByte_EEPROM(EE_Sunrise_delay + 0, temp[25]);
                            WriteByte_EEPROM(EE_Sunrise_delay + 1, temp[26]);
                            GetSCHTimeFrom_LAT_LOG(); // compute Astro time from lat/long
                            EEWriteAstroSch();
                            SLC_DST_En = temp[27];
                            WriteByte_EEPROM(EE_SLC_DST_En, SLC_DST_En);

                            if (SLC_DST_En == 1)
                                {
                                SLC_DST_Start_Month = temp[28];
                                WriteByte_EEPROM(EE_SLC_DST_Start_Month, SLC_DST_Start_Month);
                                SLC_DST_Start_Rule = temp[29];
                                WriteByte_EEPROM(EE_SLC_DST_Start_Rule, SLC_DST_Start_Rule);
                                SLC_DST_Start_Time = temp[30];
                                WriteByte_EEPROM(EE_SLC_DST_Start_Time, SLC_DST_Start_Time);
                                SLC_DST_Stop_Month = temp[31];
                                WriteByte_EEPROM(EE_SLC_DST_Stop_Month, SLC_DST_Stop_Month);
                                SLC_DST_Stop_Rule = temp[32];
                                WriteByte_EEPROM(EE_SLC_DST_Stop_Rule, SLC_DST_Stop_Rule);
                                SLC_DST_Stop_Time = temp[33];
                                WriteByte_EEPROM(EE_SLC_DST_Stop_Time, SLC_DST_Stop_Time);
                                pulseCounter.byte[3] = temp[34];
                                pulseCounter.byte[2] = temp[35];
                                pulseCounter.byte[1] = temp[36];
                                pulseCounter.byte[0] = temp[37];
                                SLC_DST_Time_Zone_Diff = pulseCounter.float_data;
                                WriteByte_EEPROM(EE_SLC_DST_Time_Zone_Diff + 0, temp[34]);
                                WriteByte_EEPROM(EE_SLC_DST_Time_Zone_Diff + 1, temp[35]);
                                WriteByte_EEPROM(EE_SLC_DST_Time_Zone_Diff + 2, temp[36]);
                                WriteByte_EEPROM(EE_SLC_DST_Time_Zone_Diff + 3, temp[37]);
                                // halCommonSetToken(TOKEN_SLC_DST_Time_Zone_Diff,&SLC_DST_Time_Zone_Diff);
                                SLC_DST_Rule_Enable = temp[38];
                                WriteByte_EEPROM(EE_SLC_DST_Rule_Enable, SLC_DST_Rule_Enable);
                                SLC_DST_Start_Date = temp[39];
                                WriteByte_EEPROM(EE_SLC_DST_Start_Date, SLC_DST_Start_Date);
                                SLC_DST_Stop_Date = temp[40];
                                WriteByte_EEPROM(EE_SLC_DST_Stop_Date, SLC_DST_Stop_Date);
                                if (SLC_DST_Rule_Enable == 1)
                                    {
                                    SLC_DST_R_Start_Date = find_dst_date_from_rule(SLC_DST_Start_Rule, SLC_DST_Start_Month);
                                    SLC_DST_R_Stop_Date = find_dst_date_from_rule(SLC_DST_Stop_Rule, SLC_DST_Stop_Month);
                                    }
                                else
                                    {
                                    SLC_DST_R_Start_Date = SLC_DST_Start_Date;
                                    SLC_DST_R_Stop_Date = SLC_DST_Stop_Date;
                                    }

                                }

                            }
                        }

                    else if (temp[7] == GET_CONF_CMD_IN_TEST_MODE)
                        {
                        Test_Config_Data_Send = 1;
                        }
                    else if (temp[7] == SET_CISCO_CONFIG)
                        {
                        Normal_reset_time = temp[8];
                        WriteByte_EEPROM(EE_Normal_reset_time, Normal_reset_time);
                        ID_Frame_reset_time = temp[9];
                        WriteByte_EEPROM(EE_ID_Frame_reset_time, ID_Frame_reset_time);
                        idframe_frequency = temp[10];
                        WriteByte_EEPROM(EE_idframe_frequency, idframe_frequency);
                        ID_frame_timeout = temp[11] * 256;
                        ID_frame_timeout = ID_frame_timeout + temp[12];
                        if (ID_frame_timeout <= 14400)
                            {
                            WriteByte_EEPROM(EE_ID_frame_timeout, (ID_frame_timeout / 256));
                            WriteByte_EEPROM(EE_ID_frame_timeout + 1, (ID_frame_timeout % 256));
                            }
                        }
                    else if (temp[7] == GET_CISCO_CONFIG)
                        {
                        Test_get_cisco_config_send = 1;
                        }
                    else if (temp[7] == MIX_SCHEDULE_RESET_CMD)
                        {
                        if ((temp[8] == 0x0f)&&(temp[9] == 0xf0))
                            {
                            for (j = 0; j < 10; j++)
                                {
                                Slc_Comp_Mix_Mode_schedule[j].SLC_Mix_En = 0;
                                WriteByte_EEPROM(EE_MIX_SCHEDULE + (j * 65) + 1, Slc_Comp_Mix_Mode_schedule[j].SLC_Mix_En);

                                Slc_Comp_Mix_Mode_schedule[j].SLC_Mix_Sch_Start_Date = 0;
                                WriteByte_EEPROM(EE_MIX_SCHEDULE + (j * 65) + 2, Slc_Comp_Mix_Mode_schedule[j].SLC_Mix_Sch_Start_Date);

                                Slc_Comp_Mix_Mode_schedule[j].SLC_Mix_Sch_Start_Month = 0;
                                WriteByte_EEPROM(EE_MIX_SCHEDULE + (j * 65) + 3, Slc_Comp_Mix_Mode_schedule[j].SLC_Mix_Sch_Start_Month);

                                Slc_Comp_Mix_Mode_schedule[j].SLC_Mix_Sch_Stop_Date = 0;
                                WriteByte_EEPROM(EE_MIX_SCHEDULE + (j * 65) + 4, Slc_Comp_Mix_Mode_schedule[j].SLC_Mix_Sch_Stop_Date);

                                Slc_Comp_Mix_Mode_schedule[j].SLC_Mix_Sch_Stop_Month = 0;
                                WriteByte_EEPROM(EE_MIX_SCHEDULE + (j * 65) + 5, Slc_Comp_Mix_Mode_schedule[j].SLC_Mix_Sch_Stop_Month);

                                Slc_Comp_Mix_Mode_schedule[j].SLC_Mix_Sch_WeekDay = 0;
                                WriteByte_EEPROM(EE_MIX_SCHEDULE + (j * 65) + 6, Slc_Comp_Mix_Mode_schedule[j].SLC_Mix_Sch_WeekDay);

                                Slc_Comp_Mix_Mode_schedule[j].SLC_Mix_Sch_Photocell_Override = 0;
                                WriteByte_EEPROM(EE_MIX_SCHEDULE + (j * 65) + 7, Slc_Comp_Mix_Mode_schedule[j].SLC_Mix_Sch_Photocell_Override);

                                for (i = 0; i < 9; i++)
                                    {
                                    Slc_Comp_Mix_Mode_schedule[j].Slc_Mix_Sch[i].compress1 = 0;
                                    //WriteByte_EEPROM(EE_MIX_SCHEDULE + (j * 65) + (i * 6) + 8, Slc_Comp_Mix_Mode_schedule[j].Slc_Mix_Sch[i].compress1);

                                    Slc_Comp_Mix_Mode_schedule[j].Slc_Mix_Sch[i].compress2 = 0;
                                    //WriteByte_EEPROM(EE_MIX_SCHEDULE + (j * 65) + (i * 6) + 9, Slc_Comp_Mix_Mode_schedule[j].Slc_Mix_Sch[i].compress2);

                                    Slc_Comp_Mix_Mode_schedule[j].Slc_Mix_Sch[i].compress3 = 0;
                                    //WriteByte_EEPROM(EE_MIX_SCHEDULE + (j * 65) + (i * 6) + 10, Slc_Comp_Mix_Mode_schedule[j].Slc_Mix_Sch[i].compress3);

                                    Slc_Comp_Mix_Mode_schedule[j].Slc_Mix_Sch[i].compress4 = 0;
                                    // WriteByte_EEPROM(EE_MIX_SCHEDULE + (j * 65) + (i * 6) + 11, Slc_Comp_Mix_Mode_schedule[j].Slc_Mix_Sch[i].compress4);

                                    Slc_Comp_Mix_Mode_schedule[j].Slc_Mix_Sch[i].compress5 = 0;
                                    //WriteByte_EEPROM(EE_MIX_SCHEDULE + (j * 65) + (i * 6) + 12, Slc_Comp_Mix_Mode_schedule[j].Slc_Mix_Sch[i].compress5);

                                    Slc_Comp_Mix_Mode_schedule[j].Slc_Mix_Sch[i].compress6 = 0;
                                    //WriteByte_EEPROM(EE_MIX_SCHEDULE + (j * 65) + (i * 6) + 13, Slc_Comp_Mix_Mode_schedule[j].Slc_Mix_Sch[i].compress6);
                                    Fill_Slc_Comp_Mix_Mode_schedule_compress(i, j);
                                    }
                                }

                            }
                        }
                    else if (temp[7] == RESET_CMD)
                        {
                        if ((temp[8] == 0x0f)&&(temp[9] == 0xf0))
                            {
                            Save_BH_kWh();
                            enter_boot_mode = 30;
                            WriteByte_EEPROM(EE_ENT_BOOT, enter_boot_mode);
                            delay(100);
                            asm("reset");
                            }
                        }
                    else if (temp[7] == EM_RESET_CMD)
                        {
                        if ((temp[8] == 0x0f)&&(temp[9] == 0xf0))
                            {
                            energy_meter_ok = 0; // set flag for EM Fault
                            error_condition1.bits.b4 = 0; // set Bit in digital status for EM Fault
                            //read_a_reg(0x01,0x1e);                                 // read Status resistor from EM chip
                            pulseCounter.byte[0] = datalow;
                            pulseCounter.byte[1] = datamid;
                            pulseCounter.byte[2] = datahigh;
                            pulseCounter.byte[3] = 0x00;
                            Em_status_reg = pulseCounter.long_data; // Clear Status resistor in EM chip
                            //write_to_reg(0x01,0x5e,0x00,0x00,0x80);
                            init_cs5463(); // Reset EM engine
                            energy_time = 1;
                            }
                        }

                    else if (temp[7] == SET_AUTO_RECOVER_FAULT)
                        {
                        if ((temp[8] == 0xE4)&&(temp[9] == 0x4E))
                            {
                            Lamp_Balast_fault_Remove_Time = temp[10];
                            WriteByte_EEPROM(EE_FALUT_RECOVER, Lamp_Balast_fault_Remove_Time);
                            }

                        }

                    else if (temp[7] == GET_AUTO_RECOVER_FAULT)
                        {
                        if ((temp[8] == 0xE4)&&(temp[9] == 0x4E))
                            {
                            Get_auto_recover = 1;

                            }

                        }
                    else if (temp[7] == GET_Rs485_DI2_CONFIG)
                        {
                        send_get_Rs485_DI2_Config = 1;
                        }
                    else if (temp[7] == Rs485_DI2_CONFIG)
                        {
                        if ((temp[8] == 0x0f)&&(temp[9] == 0xf0))
                            {
                            RS485_0_Or_DI2_1 = temp[10];
                            //   Select_RS485_Or_DI2(RS485_0_Or_DI2_1);
                            WriteByte_EEPROM(EE_RS485_0_Or_DI2_1, RS485_0_Or_DI2_1);
                            }
                        }
                        //                    else if (temp[7] == SET_CMD_DALI_AO_CONFIG)
                        //                        {
                        //                        if ((temp[8] == 0xAf)&&(temp[9] == 0xfA))
                        //                            {
                        //                            DimmerDriverSelectionProcess = temp[10];
                        //                            if ((DimmerDriverSelectionProcess == DIM_DRIVER_SELECT_AUTO_ON_POWER_CYCLE) || (DimmerDriverSelectionProcess == DIM_DRIVER_SELECT_AUTO_ON_REQUEST))
                        //                                {
                        //                                DimmerDriver_SELECTION = (DimmerDriverSelectionProcess << 4) | (DIM_DRIVER_STATUS_DETECTION);
                        //                                DimDriverStatus = DIM_DRIVER_STATUS_DETECTION;
                        //                                }
                        //                            else if (DimmerDriverSelectionProcess == DIM_DRIVER_SELECT_DALI)
                        //                                {
                        //                                DimmerDriver_SELECTION = (DimmerDriverSelectionProcess << 4) | (DIM_DRIVER_STATUS_DALI_SET);
                        //                                DimDriverStatus = DIM_DRIVER_STATUS_DALI_SET;
                        //                                DimDriverMuxSelect(DIM_DRIVER_MUX_DALI);
                        //                                lamp_off_on_timer = Lamp_off_on_Time;
                        //                                lamp_on_first = 0;
                        //                                default_dali = 0;
                        //                                //                       if(set_do_flag==1)
                        //                                //                       {
                        //                                //                         Set_Do(1);
                        //                                //                       }
                        //                                }
                        //                            else if (DimmerDriverSelectionProcess == DIM_DRIVER_SELECT_AO)
                        //                                {
                        //                                DimmerDriver_SELECTION = (DimmerDriverSelectionProcess << 4) | (DIM_DRIVER_STATUS_AO_SET);
                        //                                DimDriverStatus = DIM_DRIVER_STATUS_AO_SET;
                        //                                DimDriverMuxSelect(DIM_DRIVER_MUX_AO);
                        //                                lamp_on_first = 0;
                        //                                default_dali = 0;
                        //                                //                       if(set_do_flag==1)
                        //                                //                       {
                        //                                //                         Set_Do(1);
                        //                                //                       }                
                        //                                }
                        //                            WriteByte_EEPROM(EE_ADDR_DIM_DRIVER_INFO + 0, DimmerDriver_SELECTION);
                        //                            //                     if(temp[11]==1) // it is Reboot Flag if 1 than softreboot SLC
                        //                            //                     {
                        //                            //                       calculate_kwh_LampOFF();             // store last reading of KWH in to NV
                        //                            //                       Calculate_burnhour_Lampoff();        // store last reading of Burn_Hour in to NV
                        //                            //                       cheak_emt_timer_sec=0;
                        //                            //                       _ModemConfigRebootRcv_u8 = 1;
                        //                            //                     }
                        //                            }
                        //                        }
                        //                    else if (temp[7] == GET_CMD_DALI_AO_CONFIG)
                        //                        {
                        //                        if ((temp[8] == 0xAf)&&(temp[9] == 0xfA))
                        //                            {
                        //                            _SendDaliConfig_u8 = 1;
                        //                            }
                        //                        }
                    else if (temp[7] == SET_CMD_DALI_AO_CONFIG)
                        {
                        if ((temp[8] == 0xAf)&&(temp[9] == 0xfA))
                            {
                            DimmerDriverSelectionProcess = temp[10];
                            if ((DimmerDriverSelectionProcess == DIM_DRIVER_SELECT_AUTO_ON_POWER_CYCLE) || (DimmerDriverSelectionProcess == DIM_DRIVER_SELECT_AUTO_ON_REQUEST))
                                {
                                DimmerDriver_SELECTION = (DimmerDriverSelectionProcess << 4) | (DIM_DRIVER_STATUS_DETECTION);
                                DimDriverStatus = DIM_DRIVER_STATUS_DETECTION;
                                }
                            else if (DimmerDriverSelectionProcess == DIM_DRIVER_SELECT_DALI)
                                {
                                DimmerDriver_SELECTION = (DimmerDriverSelectionProcess << 4) | (DIM_DRIVER_STATUS_DALI_SET);
                                DimDriverStatus = DIM_DRIVER_STATUS_DALI_SET;
                                DimDriverMuxSelect(DIM_DRIVER_MUX_DALI);
                                lamp_off_on_timer = Lamp_off_on_Time;
                                lamp_on_first = 0;
                                default_dali = 0;
                                if (set_do_flag == 1)
                                    {
                                    Set_Do(1);
                                    }
                                }
                            else if (DimmerDriverSelectionProcess == DIM_DRIVER_SELECT_AO)
                                {
                                DimmerDriver_SELECTION = (DimmerDriverSelectionProcess << 4) | (DIM_DRIVER_STATUS_AO_SET);
                                DimDriverStatus = DIM_DRIVER_STATUS_AO_SET;
                                DimDriverMuxSelect(DIM_DRIVER_MUX_AO);
                                lamp_on_first = 0;
                                default_dali = 0;
                                if (set_do_flag == 1)
                                    {
                                    Set_Do(1);
                                    }
                                }
                            WriteByte_EEPROM(EE_ADDR_DIM_DRIVER_INFO + 0, DimmerDriver_SELECTION);
                            if (temp[11] == 1) // it is Reboot Flag if 1 than softreboot SLC
                                {
                                //                       calculate_kwh_LampOFF();             // store last reading of KWH in to NV
                                //                       Calculate_burnhour_Lampoff();        // store last reading of Burn_Hour in to NV
                                //                       cheak_emt_timer_sec=0;
                                //                       _ModemConfigRebootRcv_u8 = 1;
                                Save_BH_kWh();
                                delay(1000);
                                asm("reset");
                                }
                            }
                        }
                    else if (temp[7] == GET_CMD_DALI_AO_CONFIG)
                        {
                        if ((temp[8] == 0xAf)&&(temp[9] == 0xfA))
                            {
                            _SendDaliConfig_u8 = 1;
                            }
                        }
                else if(temp[7] == CURV_TYPE_SET)
		          {
		            if((temp[8] ==0)||(temp[8] ==1))
						{
		                CURV_TYPE = temp[8];
                        WriteByte_EEPROM(EE_CURV_TYPE_GET , CURV_TYPE);
                        
		                if(set_do_flag == 1)
							{
		                   set_Linear_Logarithmic_dimming(CURV_TYPE);
		                	}
		                
		            	}
		
		          }

				else if(temp[7] == CURV_TYPE_GET)
		          {
		            Send_curv_type =1;
		          }
                else
                    {

                    }
                    break;
                case 'A':
                    /*  pulseCounter.byte[3] = temp[7];
                      pulseCounter.byte[2] = temp[8];
                      pulseCounter.byte[1] = temp[9];
                      pulseCounter.byte[0] = temp[10];
                      ack_Rf_Track_Id = pulseCounter.long_data;
                      delet_data_from_mem(ack_Rf_Track_Id);
                      rf_event_send_counter = Unicast_time_delay;*/
                    break;
                case 'O':
                    Send_COUNT = 1;
                    break;



                case 'S':
                    temp[57] = 0x7F;
                    if ((temp[57] == 0x7F)) // V6.1.10
                        {
                        //---------------------------------------//
                        Sch_week_day = 0;
                        for (i = 0; i < 10; i++) // V6.1.10
                            {
                            sSch[Sch_week_day][i].cStartHour = temp[(i * 5) + 7]; // V6.1.10
                            WriteByte_EEPROM(EE_SCHEDULE + ((Sch_week_day * 50) + ((i * 5) + 0)), sSch[Sch_week_day][i].cStartHour); // V6.1.10
                            sSch[Sch_week_day][i].cStartMin = temp[(i * 5) + 8]; // V6.1.10
                            WriteByte_EEPROM(EE_SCHEDULE + ((Sch_week_day * 50) + ((i * 5) + 1)), sSch[Sch_week_day][i].cStartMin); // V6.1.10
                            sSch[Sch_week_day][i].cStopHour = temp[(i * 5) + 9]; // V6.1.10
                            WriteByte_EEPROM(EE_SCHEDULE + ((Sch_week_day * 50) + ((i * 5) + 2)), sSch[Sch_week_day][i].cStopHour); // V6.1.10
                            sSch[Sch_week_day][i].cStopMin = temp[(i * 5) + 10]; // V6.1.10
                            WriteByte_EEPROM(EE_SCHEDULE + ((Sch_week_day * 50) + ((i * 5) + 3)), sSch[Sch_week_day][i].cStopMin); // V6.1.10
                            sSch[Sch_week_day][i].bSchFlag = temp[(i * 5) + 11]; // V6.1.10
                            WriteByte_EEPROM(EE_SCHEDULE + ((Sch_week_day * 50) + ((i * 5) + 4)), sSch[Sch_week_day][i].bSchFlag); // V6.1.10
                            }
                        sch_day[Sch_week_day] = temp[57];
                        WriteByte_EEPROM(EE_SCH_DAY + Sch_week_day, sch_day[Sch_week_day]);
                        }
                    break;

                case 'Z':
                    // V6.1.10
                    temp[67] = 0x7F;
                    if (temp[67] == 0x7F) // V6.1.10
                        {
                        dimm_cmd_rec = 1;
                        Sch_week_day = 0;

                        for (i = 0; i < 10; i++) // SAN								// V6.1.10
                            {
                            Master_sSch[Sch_week_day][i].cStartHour = temp[(i * 6) + 7]; // V6.1.10
                            WriteByte_EEPROM(EE_MASTER_SCHEDULE + ((Sch_week_day * 60) + ((i * 6) + 0)), Master_sSch[Sch_week_day][i].cStartHour); // V6.1.10
                            Master_sSch[Sch_week_day][i].cStartMin = temp[(i * 6) + 8]; // V6.1.10
                            WriteByte_EEPROM(EE_MASTER_SCHEDULE + ((Sch_week_day * 60) + ((i * 6) + 1)), Master_sSch[Sch_week_day][i].cStartMin); // V6.1.10
                            Master_sSch[Sch_week_day][i].cStopHour = temp[(i * 6) + 9]; // V6.1.10
                            WriteByte_EEPROM(EE_MASTER_SCHEDULE + ((Sch_week_day * 60) + ((i * 6) + 2)), Master_sSch[Sch_week_day][i].cStopHour); // V6.1.10
                            Master_sSch[Sch_week_day][i].cStopMin = temp[(i * 6) + 10]; // V6.1.10
                            WriteByte_EEPROM(EE_MASTER_SCHEDULE + ((Sch_week_day * 60) + ((i * 6) + 3)), Master_sSch[Sch_week_day][i].cStopMin); // V6.1.10
                            Master_sSch[Sch_week_day][i].bSchFlag = temp[(i * 6) + 11]; // V6.1.10
                            WriteByte_EEPROM(EE_MASTER_SCHEDULE + ((Sch_week_day * 60) + ((i * 6) + 4)), Master_sSch[Sch_week_day][i].bSchFlag); // V6.1.10
                            Master_sSch[Sch_week_day][i].dimvalue = temp[(i * 6) + 12]; // V6.1.10
                            WriteByte_EEPROM(EE_MASTER_SCHEDULE + ((Sch_week_day * 60) + ((i * 6) + 5)), Master_sSch[Sch_week_day][i].dimvalue); // V6.1.10						
                            }
                        sch_Master_day[Sch_week_day] = temp[67];
                        WriteByte_EEPROM(EE_MASTER_SCH_DAY + Sch_week_day, sch_Master_day[Sch_week_day]);


                        }
                    break; // SAN


                case 'X':
                    if (automode == 0)
                        {
                        if (temp[7] == 0x01)
                            {
                            WriteByte_EEPROM(EE_DO, temp[7]);
                            Set_Do_On = 1;
                            }
                        else if (temp[7] == 0x00)
                            {
                            WriteByte_EEPROM(EE_DO, temp[7]);
                            Set_Do_On = 0;
                            }
                        }
                    break;

                case SET_LOG_CONF:

                    lograte = temp[7];
                    WriteByte_EEPROM(EE_LOGRATE, lograte);


                    pulseCounter.byte[1] = temp[8];
                    pulseCounter.byte[0] = temp[9];
                    time_slice = pulseCounter.int_data;
                    WriteByte_EEPROM(EE_TIMESLICE + 0, pulseCounter.byte[1]);
                    WriteByte_EEPROM(EE_TIMESLICE + 1, pulseCounter.byte[0]);

                    lograte = ReadByte_EEPROM(EE_LOGRATE);

                    pulseCounter.byte[1] = ReadByte_EEPROM(EE_TIMESLICE + 0);
                    pulseCounter.byte[0] = ReadByte_EEPROM(EE_TIMESLICE + 1);
                    time_slice = pulseCounter.int_data;
                    push_time_slice_ms = (amr_id % 1000) * time_slice;
                    push_time_slice_ms = push_time_slice_ms + ((rand() % (time_slice - 90)) + 100);

                    Push_Energy_meter_data = temp[10];
                    WriteByte_EEPROM(EE_PUSH_ENERGY_DATA, Push_Energy_meter_data);
                    //	WriteByte_EEPROM(EE_LOGRATE,lograte);
                    //					get_push_lograte();
                    break;

                case GET_LOG_CONF:

                    send_get_log_conf = 1;

                    break;

                case 0xEE:

                    //	WriteData_UART2("\r\npshd_ACK",strlen("\r\npshd_ACK"));
                    Send_Data_push = 0;
                    retry_push_data_cnt = 0;
                    pushdata_retry_timer = 0;
                    read_dim_val_timer1 = 0;
                    nic_reset_cnt2 = 0;
                    nic_reset_cnt3 = 0;
                    nic_reset_timer = 0;
                    Local_Time.Hour = Time.Hour;
                    Local_Time.Min = Time.Min;
                    Local_Time.Sec = Time.Sec;
                    Local_Date.Date = Date.Date;
                    Local_Date.Month = Date.Month;
                    Local_Date.Year = Date.Year;
                    //				for(i=0;i<41;i++)Energy_data[i]=0x00;
                    //				em_log_cnt=0;
                    //				Energy_data_cnt =0;
                    break;

                case 'E':

                    //		WriteData_UART2("\r\nEvent_ACK",strlen("\r\nEvent_ACK"));
                    send_data_direct = 0;
                    retry_push_event_cnt = 0;
                    event_push_retry_timer = 0;
                    nic_reset_cnt2 = 0;
                    nic_reset_cnt3 = 0;
                    nic_reset_timer = 0;
                    push_time_slice_ms = (amr_id % 1000) * time_slice;
                    push_time_slice_ms = push_time_slice_ms + ((rand() % (time_slice - 90)) + 100);

                    Local_Time.Hour = Time.Hour;
                    Local_Time.Min = Time.Min;
                    Local_Time.Sec = Time.Sec;
                    Local_Date.Date = Date.Date;
                    Local_Date.Month = Date.Month;
                    Local_Date.Year = Date.Year;

                    break;

                case 'D':

                    //		WriteData_UART2("\r\nNorm_ACK",strlen("\r\nNorm_ACK"));
                    Send_Data = 0;
                    retry_send_data_cnt = 0;
                    senddata_retry_timer = 0;
                    read_dim_val_timer1 = 0;
                    nic_reset_cnt2 = 0;
                    nic_reset_cnt3 = 0;
                    nic_reset_timer = 0;

                    Local_Time.Hour = Time.Hour;
                    Local_Time.Min = Time.Min;
                    Local_Time.Sec = Time.Sec;
                    Local_Date.Date = Date.Date;
                    Local_Date.Month = Date.Month;
                    Local_Date.Year = Date.Year;

                    break;


                default:
                    break;
                }

            }
        }
    else if ((temp[0] == CMD) && (temp[1] == SLC_MIX_MODE_SCH))
        {
        Configure_Slc_Mix_Mode_Sch(&temp[2]);


        /*
        
                        track_res[0]= temp[0];
                        track_res[1]= temp[1];
                        track_res[2]= temp[2];
                        track_res[3]= temp[3];
                        track_res[4]= temp[4];
                        track_res[5]= temp[5];
                        track_res[6]= temp[6];
                        track_res[7]= temp[7];	

                track_res[8]= temp[packet_length-4];
                track_res[9]= temp[packet_length-3];
                track_res[10]= temp[packet_length-2];
                track_res[11]= temp[packet_length-1];
        
         */

        track_res[0] = temp[0];
        track_res[1] = temp[1];
        pulseCounter.long_data = amr_id;
        temp_arr[2] = pulseCounter.byte[3];
        temp_arr[3] = pulseCounter.byte[2];
        temp_arr[4] = pulseCounter.byte[1];
        temp_arr[5] = pulseCounter.byte[0];

        track_res[6] = Client / 256;
        track_res[7] = Client % 256;

        track_res[8] = temp[4];
        track_res[9] = temp[5];


        track_res[10] = temp[packet_length - 4];
        track_res[11] = temp[packet_length - 3];
        track_res[12] = temp[packet_length - 2];
        track_res[13] = temp[packet_length - 1];
        Send_data_RF1(track_res, 14, 0);
        }
    else
        {
        }
    }

void send_fream(void)
    {
    unsigned char i, k, n = 0;

    temp_arr[0] = 'S';
    temp_arr[1] = 'L';
    pulseCounter.long_data = amr_id;
    temp_arr[2] = pulseCounter.byte[3];
    temp_arr[3] = pulseCounter.byte[2];
    temp_arr[4] = pulseCounter.byte[1];
    temp_arr[5] = pulseCounter.byte[0];

    temp_arr[6] = Client / 256;
    temp_arr[7] = Client % 256;

    if ((Send_Data_push == 1) && (read_dim_val_timer1 >= push_time_slice_ms) && (Id_frame_received == 1) && (pushdata_retry_timer >= 5))
        {
        volatile uint8_t Count = 0;
        temp_arr[8] = 0xEE;
        temp_arr[9] = Frm_no;
        Filldata();
        temp_arr[60] = 0x04;
        for (Count = 0; Count < 40; Count++)
            {
            temp_arr[61 + Count] = Energy_back_data[Count];
            }
        track_id++;
        if (track_id >= 65000)track_id = 0;

        temp_arr[101] = track_id / 256;
        temp_arr[102] = track_id % 256;

        //	temp_arr[101] = Energy_data[40];

        /////////new string added///////////////				
        //Send_data_RF1(temp_arr,52,0);				
        Send_data_RF1(temp_arr, 103, 0);
        //				sprintf(buffer,"pshd_send");	
        //				WriteData_UART2(buffer,strlen(buffer));
        Motion_broadcast_miss_counter = 0;
        if (retry_push_data_cnt >= 3)
            {

            //for(i=0;i<41;i++)Energy_data[i]=0x00;

            //Energy_data_cnt =0;
            Send_Data_push = 0;
            retry_push_data_cnt = 0;
            }
        retry_push_data_cnt++;
        pushdata_retry_timer = 0;
        read_dim_val_timer1 = 0;

        //		WriteData_UART2("\r\nEnergy_data_sent",strlen("\r\nEnergy_data_sent"));
        }

    else if (Send_Last_gasp_msg == 1)
        {
        Send_Last_gasp_msg = 0;

        temp_arr[0] = 'L';
        temp_arr[1] = 'G';

        pulseCounter.long_data = amr_id;
        temp_arr[2] = pulseCounter.byte[3];
        temp_arr[3] = pulseCounter.byte[2];
        temp_arr[4] = pulseCounter.byte[1];
        temp_arr[5] = pulseCounter.byte[0];

        temp_arr[6] = Client / 256;
        temp_arr[7] = Client % 256;

        Send_data_RF1(temp_arr, 8, 0);
        }
    else if ((Send_Data == 1)&&(read_dim_val_timer1 >= push_time_slice_ms) && (Id_frame_received == 1) && (senddata_retry_timer >= 3))
        {


        temp_arr[8] = 'D';
        temp_arr[9] = Frm_no;
        Filldata();

        /////////new string added///////////////				
        //Send_data_RF1(temp_arr,52,0);				
        Send_data_RF1(temp_arr, 60, 0);
        Motion_broadcast_miss_counter = 0;
        if (retry_send_data_cnt >= 3)
            {
            Send_Data = 0;
            retry_send_data_cnt = 0;
            }
        retry_send_data_cnt++;
        senddata_retry_timer = 0;
        read_dim_val_timer1 = 0;

        //		WriteData_UART2("\r\nNormal_data_sent",strlen("\r\nNormal_data_sent"));


        }

    else if ((send_data_direct == 1)&&(event_push_retry_timer >= 3)&&(send_event_delay_timer >= push_time_slice_ms)) // uncomment for auto event with 10 sec delay.
        {

        //		WriteData_UART2("\r\nEvent_data_sent",strlen("\r\nEvent_data_sent"));	
        /////////new string added///////////////				
        //Send_data_RF1(temp_arr,52,0);				
        Send_data_RF1(temp_arr_event, 63, 0);
        retry_push_event_cnt++;
        if (retry_push_event_cnt >= 3)
            {
            send_data_direct = 0;
            retry_push_event_cnt = 0;
            push_time_slice_ms = (amr_id % 1000) * time_slice;
            push_time_slice_ms = push_time_slice_ms + ((rand() % (time_slice - 90)) + 100);
            }
        event_push_retry_timer = 0;
        Motion_broadcast_miss_counter = 0;
        }
        ////////////////////

    else if ((send_get_mode_replay_auto == 1) || (send_get_mode_replay == 1)) // if mode change from reader then send auto responce to DCU.
        {


        temp_arr[8] = AMSTATUS;
        temp_arr[9] = automode;

        pulseCounter.float_data = Latitude;
        temp_arr[10] = pulseCounter.byte[3];
        temp_arr[11] = pulseCounter.byte[2];
        temp_arr[12] = pulseCounter.byte[1];
        temp_arr[13] = pulseCounter.byte[0];

        pulseCounter.float_data = longitude;
        temp_arr[14] = pulseCounter.byte[3];
        temp_arr[15] = pulseCounter.byte[2];
        temp_arr[16] = pulseCounter.byte[1];
        temp_arr[17] = pulseCounter.byte[0];

        pulseCounter.float_data = utcOffset;
        temp_arr[18] = pulseCounter.byte[3];
        temp_arr[19] = pulseCounter.byte[2];
        temp_arr[20] = pulseCounter.byte[1];
        temp_arr[21] = pulseCounter.byte[0];

        pulseCounter.float_data = (float) Sunset_delay;
        temp_arr[22] = pulseCounter.byte[3];
        temp_arr[23] = pulseCounter.byte[2];
        temp_arr[24] = pulseCounter.byte[1];
        temp_arr[25] = pulseCounter.byte[0];

        pulseCounter.float_data = (float) Sunrise_delay;
        temp_arr[26] = pulseCounter.byte[3];
        temp_arr[27] = pulseCounter.byte[2];
        temp_arr[28] = pulseCounter.byte[1];
        temp_arr[29] = pulseCounter.byte[0];
        if (send_get_mode_replay_auto == 1)
            {
            Send_data_RF1(temp_arr, 30, 1);
            send_get_mode_replay_auto = 0;
            }
        else if (send_get_mode_replay == 1)
            {
            Send_data_RF1(temp_arr, 30, 0);
            send_get_mode_replay = 0;
            }
        }
        ////////////////////

    else if ((send_id_frame == 1)&&(send_id_frame_flage == 1)&&(send_id_frame_time >= 5)&&(RF_Modem_detected != 1))
        {

        if (Id_Frame_Cnt >= 3)
            {
            Id_Frame_Cnt = 0;
            idframe_frequency_cnt = 0;
            }
        send_id_frame = 0;
        send_id_frame_time = 0;



        temp_arr[0] = 'I';
        temp_arr[1] = 'D';

        pulseCounter.long_data = amr_id;
        temp_arr[2] = pulseCounter.byte[3];
        temp_arr[3] = pulseCounter.byte[2];
        temp_arr[4] = pulseCounter.byte[1];
        temp_arr[5] = pulseCounter.byte[0];

        temp_arr[6] = Client / 256;
        temp_arr[7] = Client % 256;


        for (n = 0; n < 39; n++)
            temp_arr[n + 8] = 0; // nicipv6[n];

        for (n = 0; n < 16; n++)
            temp_arr[n + 47] = 0; // Geuid[n];

        temp_arr[63] = 3;
        temp_arr[64] = 1;
        temp_arr[65] = 39;

        temp_arr[66] = 3;
        temp_arr[67] = 6;
        temp_arr[68] = 0;
        temp_arr[69] = 0x01;

        pulseCounter.float_data = Latitude;
        temp_arr[70] = pulseCounter.byte[3];
        temp_arr[71] = pulseCounter.byte[2];
        temp_arr[72] = pulseCounter.byte[1];
        temp_arr[73] = pulseCounter.byte[0];

        pulseCounter.float_data = longitude;
        temp_arr[74] = pulseCounter.byte[3];
        temp_arr[75] = pulseCounter.byte[2];
        temp_arr[76] = pulseCounter.byte[1];
        temp_arr[77] = pulseCounter.byte[0];

        pulseCounter.float_data = utcOffset;
        temp_arr[78] = pulseCounter.byte[3];
        temp_arr[79] = pulseCounter.byte[2];
        temp_arr[80] = pulseCounter.byte[1];
        temp_arr[81] = pulseCounter.byte[0];

        pulseCounter.float_data = (float) Sunset_delay;
        temp_arr[82] = pulseCounter.byte[3];
        temp_arr[83] = pulseCounter.byte[2];
        temp_arr[84] = pulseCounter.byte[1];
        temp_arr[85] = pulseCounter.byte[0];

        pulseCounter.float_data = (float) Sunrise_delay;
        temp_arr[86] = pulseCounter.byte[3];
        temp_arr[87] = pulseCounter.byte[2];
        temp_arr[88] = pulseCounter.byte[1];
        temp_arr[89] = pulseCounter.byte[0];

        temp_arr[90] = 0x01;
        Send_data_RF1(temp_arr, 91, 1); // send to only coordinator

        }
    else if ((send_id_frame == 1)&&(RF_Modem_detected == 1))
        {
        temp_arr[0] = 'I';
        temp_arr[1] = 'D';

        pulseCounter.long_data = amr_id;
        temp_arr[2] = pulseCounter.byte[3];
        temp_arr[3] = pulseCounter.byte[2];
        temp_arr[4] = pulseCounter.byte[1];
        temp_arr[5] = pulseCounter.byte[0];

        temp_arr[6] = Client / 256;
        temp_arr[7] = Client % 256;
        /*
        temp_arr[2] = amr_id/256;
        temp_arr[3] = amr_id%256;*/

        Id_Frame_Cnt = 0;
        Send_data_RF1(temp_arr, 8, 1); // send to only coordinator
        send_id_frame = 0;
        }
    else if (Get_AO_DALI_AUTO_CONFIG == 1)
        {

        temp_arr[8] = 0x43;

        temp_arr[9] = 0xEA;

        temp_arr[10] = dimming_driver_sect;
        temp_arr[11] = reboot_on_cmd;




        Send_data_RF1(temp_arr, 12, 0); // send to any Device

        Get_AO_DALI_AUTO_CONFIG = 0;
        }

    else if ((send_get_mode_replay == 1) || (send_get_mode_replay_auto == 1))
        {
        temp_arr[8] = AMSTATUS;

        temp_arr[9] = automode;

        pulseCounter.float_data = Latitude;
        temp_arr[10] = pulseCounter.byte[3];
        temp_arr[11] = pulseCounter.byte[2];
        temp_arr[12] = pulseCounter.byte[1];
        temp_arr[13] = pulseCounter.byte[0];

        pulseCounter.float_data = longitude;
        temp_arr[14] = pulseCounter.byte[3];
        temp_arr[15] = pulseCounter.byte[2];
        temp_arr[16] = pulseCounter.byte[1];
        temp_arr[17] = pulseCounter.byte[0];

        pulseCounter.float_data = utcOffset;
        temp_arr[18] = pulseCounter.byte[3];
        temp_arr[19] = pulseCounter.byte[2];
        temp_arr[20] = pulseCounter.byte[1];
        temp_arr[21] = pulseCounter.byte[0];

        pulseCounter.float_data = (float) Sunset_delay;
        temp_arr[22] = pulseCounter.byte[3];
        temp_arr[23] = pulseCounter.byte[2];
        temp_arr[24] = pulseCounter.byte[1];
        temp_arr[25] = pulseCounter.byte[0];

        pulseCounter.float_data = (float) Sunrise_delay;
        temp_arr[26] = pulseCounter.byte[3];
        temp_arr[27] = pulseCounter.byte[2];
        temp_arr[28] = pulseCounter.byte[1];
        temp_arr[29] = pulseCounter.byte[0];


        if (send_get_mode_replay_auto == 1)
            {
            Send_data_RF1(temp_arr, 30, 1);
            send_get_mode_replay_auto = 0;
            }
        else if (send_get_mode_replay == 1)
            {
            Send_data_RF1(temp_arr, 30, 0); // send to any Device
            send_get_mode_replay = 0;
            }

        }

    else if (Get_auto_recover == 1)
        {
        temp_arr[8] = 0x43;

        temp_arr[9] = 0xEF;

        temp_arr[10] = Lamp_Balast_fault_Remove_Time;


        Send_data_RF1(temp_arr, 11, 0); // send to any Device

        Get_auto_recover = 0;
        }

	  else if(Send_curv_type == 1)    // Send responce of network search parameters to Dcu.
	  {
          
	    temp_arr[8] = 0x43;
        temp_arr[9] = CURV_TYPE_GET;
	    temp_arr[10] = CURV_TYPE;		
	    Send_data_RF1(temp_arr,11,0);
        Send_curv_type =0;
	  }

    else if (send_get_log_conf == 1)
        {

        temp_arr[8] = GET_LOG_CONF;

        temp_arr[9] = lograte;

        temp_arr[10] = time_slice / 256;
        temp_arr[11] = time_slice % 256;
        temp_arr[12] = Push_Energy_meter_data;


        Send_data_RF1(temp_arr, 13, 0); // send to any Device

        send_get_log_conf = 0;
        }
    else if (_SendDaliConfig_u8 == 1)
        {
        _SendDaliConfig_u8 = 0;

        temp_arr[8] = 0x00; //Dummy Field to avoid conflict with EVENT_ACK
        temp_arr[9] = GET_CMD_DALI_AO_CONFIG;
        temp_arr[10] = DimmerDriverSelectionProcess;
        temp_arr[11] = DimDriverStatus;
        Send_data_RF1(temp_arr, 12, 0);
        }
    else if (Send_Fault_Para_Replay == 1)
        {
        Send_Fault_Para_Replay = 0;

        temp_arr[8] = SEND_FAULT_PARA;

        pulseCounter.int_data = Vol_hi;
        temp_arr[9] = pulseCounter.byte[1];
        temp_arr[10] = pulseCounter.byte[0];

        pulseCounter.int_data = Vol_low;
        temp_arr[11] = pulseCounter.byte[1];
        temp_arr[12] = pulseCounter.byte[0];

        pulseCounter.int_data = Curr_Steady_Time;
        temp_arr[13] = pulseCounter.byte[1];
        temp_arr[14] = pulseCounter.byte[0];

        pulseCounter.int_data = Per_Val_Current;
        temp_arr[15] = pulseCounter.byte[1];
        temp_arr[16] = pulseCounter.byte[0];

        pulseCounter.int_data = Lamp_Fault_Time;
        temp_arr[17] = pulseCounter.byte[1];
        temp_arr[18] = pulseCounter.byte[0];

        pulseCounter.int_data = Lamp_faulty_retrieve_Count;
        temp_arr[19] = pulseCounter.byte[1];
        temp_arr[20] = pulseCounter.byte[0];

        pulseCounter.int_data = Lamp_off_on_Time;
        temp_arr[21] = pulseCounter.byte[1];
        temp_arr[22] = pulseCounter.byte[0];

        pulseCounter.int_data = Lamp_lock_condition;
        temp_arr[23] = pulseCounter.byte[1];
        temp_arr[24] = pulseCounter.byte[0];

        Send_data_RF1(temp_arr, 25, 1); // send to only DCU
        }
    else if (Send_Slc_Diagnosis == 1)
        {
        Send_Slc_Diagnosis = 0;
        temp_arr[8] = SEND_DIAGNOSIS;
        temp_arr[9] = Date.Date;
        temp_arr[10] = Date.Month;
        temp_arr[11] = Date.Year;
        temp_arr[12] = Time.Hour;
        temp_arr[13] = Time.Min;
        temp_arr[14] = Time.Sec;

        temp_arr[15] = Diagnosis_DI;

        pulseCounter.float_data = vrdoub;
        temp_arr[16] = pulseCounter.byte[3];
        temp_arr[17] = pulseCounter.byte[2];
        temp_arr[18] = pulseCounter.byte[1];
        temp_arr[19] = pulseCounter.byte[0];

        pulseCounter.float_data = irdoub;
        temp_arr[20] = pulseCounter.byte[3];
        temp_arr[21] = pulseCounter.byte[2];
        temp_arr[22] = pulseCounter.byte[1];
        temp_arr[23] = pulseCounter.byte[0];

        pulseCounter.int_data = emt_int_count;
        temp_arr[24] = pulseCounter.byte[1];
        temp_arr[25] = pulseCounter.byte[0];

        pulseCounter.int_data = Kwh_count;
        temp_arr[26] = pulseCounter.byte[1];
        temp_arr[27] = pulseCounter.byte[0];

        pulseCounter.float_data = vRCON;
        temp_arr[28] = pulseCounter.byte[3];
        temp_arr[29] = pulseCounter.byte[2];
        temp_arr[30] = pulseCounter.byte[1];
        temp_arr[31] = pulseCounter.byte[0];

        Send_data_RF1(temp_arr, 32, 1); // send to only DCU

        Master_event_generation = 0;

        }
    else if ((Send_Get_Sch_Replay == 1) && (Get_Sch_timer >= 5)) // V6.1.10
        {
        Get_Sch_timer = 0;
        temp_arr[8] = GET_SCH_PARA;
        k = 0;
        for (i = 0; i < 10; i++)
            {
            temp_arr[(i * 5) + 9] = sSch[k][i].cStartHour; // V6.1.10
            temp_arr[(i * 5) + 10] = sSch[k][i].cStartMin; // V6.1.10
            temp_arr[(i * 5) + 11] = sSch[k][i].cStopHour; // V6.1.10
            temp_arr[(i * 5) + 12] = sSch[k][i].cStopMin; // V6.1.10
            temp_arr[(i * 5) + 13] = sSch[k][i].bSchFlag; // V6.1.10				
            }
        temp_arr[59] = 0x7F;
        Send_data_RF1(temp_arr, 60, 0);
        Send_Get_Sch_Replay = 0;
        }
    else if ((send_SLC_test_query_1 == 1) && (test_query_1_send_timer >= 15))
        {
        test_query_1_send_timer = 0;
        temp_arr[8] = GET_SLC_TEST_1;

        temp_arr[9] = Date.Date;
        temp_arr[10] = Date.Month;
        temp_arr[11] = Date.Year;
        temp_arr[12] = Time.Hour;
        temp_arr[13] = Time.Min;
        temp_arr[14] = Time.Sec;

        temp_arr[15] = test_result.Val;

        pulseCounter.float_data = test_voltage;
        temp_arr[16] = pulseCounter.byte[3];
        temp_arr[17] = pulseCounter.byte[2];
        temp_arr[18] = pulseCounter.byte[1];
        temp_arr[19] = pulseCounter.byte[0];

        pulseCounter.float_data = test_current;
        temp_arr[20] = pulseCounter.byte[3];
        temp_arr[21] = pulseCounter.byte[2];
        temp_arr[22] = pulseCounter.byte[1];
        temp_arr[23] = pulseCounter.byte[0];

        pulseCounter.int_data = test_emt_int_count;
        temp_arr[24] = pulseCounter.byte[1];
        temp_arr[25] = pulseCounter.byte[0];

        pulseCounter.int_data = test_Kwh_count;
        temp_arr[26] = pulseCounter.byte[1];
        temp_arr[27] = pulseCounter.byte[0];


        if ((DI_GET_LOW == 1)&&(DI_GET_HIGH == 1))
            {
            temp_arr[28] = DI_GET_LOW;

            }
        else
            {
            temp_arr[28] = 0;
            }

        pulseCounter.float_data = ADC_Result_0;
        temp_arr[29] = pulseCounter.byte[3];
        temp_arr[30] = pulseCounter.byte[2];
        temp_arr[31] = pulseCounter.byte[1];
        temp_arr[32] = pulseCounter.byte[0];

        pulseCounter.float_data = ADC_Result_50;
        temp_arr[33] = pulseCounter.byte[3];
        temp_arr[34] = pulseCounter.byte[2];
        temp_arr[35] = pulseCounter.byte[1];
        temp_arr[36] = pulseCounter.byte[0];

        pulseCounter.float_data = ADC_Result_100;
        temp_arr[37] = pulseCounter.byte[3];
        temp_arr[38] = pulseCounter.byte[2];
        temp_arr[39] = pulseCounter.byte[1];
        temp_arr[40] = pulseCounter.byte[0];



        Send_data_RF1(temp_arr, 41, 1); // send to only DCU
        }
    else if ((send_SLC_test_query_2 == 1) && (test_query_1_send_timer >= 15))
        {
        test_query_1_send_timer = 0;
        test_result.bits.b6 = 1; //after power on EEPROM ok
        temp_arr[8] = GET_SLC_TEST_2;

        temp_arr[9] = Date.Date;
        temp_arr[10] = Date.Month;
        temp_arr[11] = Date.Year;
        temp_arr[12] = Time.Hour;
        temp_arr[13] = Time.Min;
        temp_arr[14] = Time.Sec;

        temp_arr[15] = test_result.Val;
        temp_arr[16] = RS485_0_Or_DI2_1;

        Send_data_RF1(temp_arr, 17, 1);
        }
    else if ((Send_Get_Master_Sch_Replay == 1) && (Get_Master_Sch_timer >= 5)) // V6.1.10			// Master sch 15/04/11
        {
        Get_Master_Sch_timer = 0;
        temp_arr[8] = GET_DIMM_SCH_PARA;

        k = 0;
        for (i = 0; i < 10; i++)
            {
            temp_arr[(i * 6) + 9] = Master_sSch[k][i].cStartHour; // V6.1.10
            temp_arr[(i * 6) + 10] = Master_sSch[k][i].cStartMin; // V6.1.10
            temp_arr[(i * 6) + 11] = Master_sSch[k][i].cStopHour; // V6.1.10
            temp_arr[(i * 6) + 12] = Master_sSch[k][i].cStopMin; // V6.1.10
            temp_arr[(i * 6) + 13] = Master_sSch[k][i].bSchFlag; // V6.1.10
            temp_arr[(i * 6) + 14] = Master_sSch[k][i].dimvalue; // V6.1.10

            }
        temp_arr[69] = 0x7F;
        Send_data_RF1(temp_arr, 70, 0);
        Send_Get_Master_Sch_Replay = 0;

        }
    else if (Send_Get_Version_info_Replay == 1) // V6.1.10
        {
        Send_Get_Version_info_Replay = 0;
        temp_arr[8] = GET_VERSION_PARA;

        temp_arr[9] = '3';
        temp_arr[10] = '.';
        temp_arr[11] = '1';
        temp_arr[12] = '.';
        temp_arr[13] = '0';
        temp_arr[14] = '0';
        temp_arr[15] = '3';
        temp_arr[16] = '9';
        temp_arr[17] = '.';
        temp_arr[18] = '2';
        temp_arr[19] = '8';
        Send_data_RF1(temp_arr, 20, 0);
        }
    else if (send_get_Rs485_DI2_Config == 1) // get_Rs485_DI2_Config
        {
        send_get_Rs485_DI2_Config = 0;

        temp_arr[8] = GET_Rs485_DI2_CONFIG;
        temp_arr[9] = RS485_0_Or_DI2_1;
        Send_data_RF1(temp_arr, 10, 0);
        }
    else if (Send_Get_Adaptive_dimming_para_Replay == 1)
        {
        Send_Get_Adaptive_dimming_para_Replay = 0;

        temp_arr[8] = GET_ADAPTIVE_DIMMING;

        pulseCounter.int_data = analog_input_scaling_high_Value;
        temp_arr[9] = pulseCounter.byte[1];
        temp_arr[10] = pulseCounter.byte[0];

        pulseCounter.int_data = analog_input_scaling_low_Value;
        temp_arr[11] = pulseCounter.byte[1];
        temp_arr[12] = pulseCounter.byte[0];

        pulseCounter.int_data = desir_lamp_lumen;
        temp_arr[13] = pulseCounter.byte[1];
        temp_arr[14] = pulseCounter.byte[0];

        temp_arr[15] = lumen_tollarence;
        temp_arr[16] = ballast_type;
        temp_arr[17] = adaptive_light_dimming;

        temp_arr[18] = Motion_pulse_rate;
        temp_arr[19] = Motion_dimming_percentage;
        temp_arr[20] = Motion_dimming_time / 256;
        temp_arr[21] = Motion_dimming_time % 256;
        temp_arr[22] = Motion_group_id;
        temp_arr[23] = dim_applay_time / 60;
        temp_arr[24] = dim_inc_val;
        temp_arr[25] = Motion_normal_dimming_percentage;

        pulseCounter.int_data = (int) (Day_light_harvesting_start_offset / 60);
        temp_arr[26] = pulseCounter.byte[1];
        temp_arr[27] = pulseCounter.byte[0];

        pulseCounter.int_data = (int) (Day_light_harvesting_stop_offset / 60);
        temp_arr[28] = pulseCounter.byte[1];
        temp_arr[29] = pulseCounter.byte[0];

        temp_arr[30] = Motion_Detect_Timeout;
        temp_arr[31] = Motion_Broadcast_Timeout;
        temp_arr[32] = Motion_Sensor_Type;
        Send_data_RF1(temp_arr, 33, 0);
        }
    else if (motion_dimming_send == 1)
        {
        motion_dimming_send = 0;
        temp_arr[0] = 'S';
        temp_arr[1] = 'L';
        temp_arr[2] = 0x00;
        temp_arr[3] = 0x00;
        temp_arr[4] = 0x00;
        temp_arr[5] = 0x00;

        temp_arr[6] = Client / 256;
        temp_arr[7] = Client % 256;

        temp_arr[8] = CMD;
        temp_arr[9] = MOTION_DETECT;
        temp_arr[10] = Motion_group_id;
        Motion_broadcast_Receive_counter++;
        pulseCounter.long_data = Motion_broadcast_Receive_counter;
        temp_arr[11] = pulseCounter.byte[3];
        temp_arr[12] = pulseCounter.byte[2];
        temp_arr[13] = pulseCounter.byte[1];
        temp_arr[14] = pulseCounter.byte[0];
        Send_data_RF1(temp_arr, 15, 2);
        }
    else if (Send_Get_Slc_Config_para_Replay == 1)
        {
        Send_Get_Slc_Config_para_Replay = 0;
        temp_arr[8] = GET_SLC_CONFIG_PARA;

        temp_arr[9] = Tilt_Enable;
        temp_arr[10] = Auto_event;
        temp_arr[11] = Schedule_offset;
        temp_arr[12] = RTC_New_fault_logic_Enable;
        Send_data_RF1(temp_arr, 13, 1);
        }
    else if (send_get_slc_dst_config_para == 1)
        {
        send_get_slc_dst_config_para = 0;
        temp_arr[8] = GET_SLC_DST_CONFIG_PARA;

        temp_arr[9] = SLC_DST_En;
        temp_arr[10] = SLC_DST_Start_Month;
        temp_arr[11] = SLC_DST_Start_Rule;
        temp_arr[12] = SLC_DST_Start_Time;
        temp_arr[13] = SLC_DST_Stop_Month;
        temp_arr[14] = SLC_DST_Stop_Rule;
        temp_arr[15] = SLC_DST_Stop_Time;

        pulseCounter.float_data = SLC_DST_Time_Zone_Diff;
        temp_arr[16] = pulseCounter.byte[3];
        temp_arr[17] = pulseCounter.byte[2];
        temp_arr[18] = pulseCounter.byte[1];
        temp_arr[19] = pulseCounter.byte[0];
        temp_arr[20] = SLC_DST_Rule_Enable;
        temp_arr[21] = SLC_DST_R_Start_Date;
        temp_arr[22] = SLC_DST_R_Stop_Date;

        Send_data_RF1(temp_arr, 23, 1);
        }
    else if (Send_Get_Mix_Mode_Sch == 1)
        {
        Send_Get_Mix_Mode_Sch = 0;
        temp_arr[8] = GET_SLC_MIX_MODE_SCH_RESPONCE;
        temp_arr[9] = Get_Mix_mode_Sch_Loc;

        temp_arr[10] = Slc_Comp_Mix_Mode_schedule[Get_Mix_mode_Sch_Loc].SLC_Mix_En;
        temp_arr[11] = Slc_Comp_Mix_Mode_schedule[Get_Mix_mode_Sch_Loc].SLC_Mix_Sch_Start_Date;
        temp_arr[12] = Slc_Comp_Mix_Mode_schedule[Get_Mix_mode_Sch_Loc].SLC_Mix_Sch_Start_Month;
        temp_arr[13] = Slc_Comp_Mix_Mode_schedule[Get_Mix_mode_Sch_Loc].SLC_Mix_Sch_Stop_Date;
        temp_arr[14] = Slc_Comp_Mix_Mode_schedule[Get_Mix_mode_Sch_Loc].SLC_Mix_Sch_Stop_Month;
        temp_arr[15] = Slc_Comp_Mix_Mode_schedule[Get_Mix_mode_Sch_Loc].SLC_Mix_Sch_WeekDay;
        temp_arr[16] = Slc_Comp_Mix_Mode_schedule[Get_Mix_mode_Sch_Loc].SLC_Mix_Sch_Photocell_Override;

        temp_arr[17] = Slc_Comp_Mix_Mode_schedule[Get_Mix_mode_Sch_Loc].Slc_Mix_Sch[0].compress1;
        temp_arr[18] = Slc_Comp_Mix_Mode_schedule[Get_Mix_mode_Sch_Loc].Slc_Mix_Sch[0].compress2;
        temp_arr[19] = Slc_Comp_Mix_Mode_schedule[Get_Mix_mode_Sch_Loc].Slc_Mix_Sch[0].compress3;
        temp_arr[20] = Slc_Comp_Mix_Mode_schedule[Get_Mix_mode_Sch_Loc].Slc_Mix_Sch[0].compress4;
        temp_arr[21] = Slc_Comp_Mix_Mode_schedule[Get_Mix_mode_Sch_Loc].Slc_Mix_Sch[0].compress5;
        temp_arr[22] = Slc_Comp_Mix_Mode_schedule[Get_Mix_mode_Sch_Loc].Slc_Mix_Sch[0].compress6;


        temp_arr[23] = Slc_Comp_Mix_Mode_schedule[Get_Mix_mode_Sch_Loc].Slc_Mix_Sch[1].compress1;
        temp_arr[24] = Slc_Comp_Mix_Mode_schedule[Get_Mix_mode_Sch_Loc].Slc_Mix_Sch[1].compress2;
        temp_arr[25] = Slc_Comp_Mix_Mode_schedule[Get_Mix_mode_Sch_Loc].Slc_Mix_Sch[1].compress3;
        temp_arr[26] = Slc_Comp_Mix_Mode_schedule[Get_Mix_mode_Sch_Loc].Slc_Mix_Sch[1].compress4;
        temp_arr[27] = Slc_Comp_Mix_Mode_schedule[Get_Mix_mode_Sch_Loc].Slc_Mix_Sch[1].compress5;
        temp_arr[28] = Slc_Comp_Mix_Mode_schedule[Get_Mix_mode_Sch_Loc].Slc_Mix_Sch[1].compress6;


        temp_arr[29] = Slc_Comp_Mix_Mode_schedule[Get_Mix_mode_Sch_Loc].Slc_Mix_Sch[2].compress1;
        temp_arr[30] = Slc_Comp_Mix_Mode_schedule[Get_Mix_mode_Sch_Loc].Slc_Mix_Sch[2].compress2;
        temp_arr[31] = Slc_Comp_Mix_Mode_schedule[Get_Mix_mode_Sch_Loc].Slc_Mix_Sch[2].compress3;
        temp_arr[32] = Slc_Comp_Mix_Mode_schedule[Get_Mix_mode_Sch_Loc].Slc_Mix_Sch[2].compress4;
        temp_arr[33] = Slc_Comp_Mix_Mode_schedule[Get_Mix_mode_Sch_Loc].Slc_Mix_Sch[2].compress5;
        temp_arr[34] = Slc_Comp_Mix_Mode_schedule[Get_Mix_mode_Sch_Loc].Slc_Mix_Sch[2].compress6;

        temp_arr[35] = Slc_Comp_Mix_Mode_schedule[Get_Mix_mode_Sch_Loc].Slc_Mix_Sch[3].compress1;
        temp_arr[36] = Slc_Comp_Mix_Mode_schedule[Get_Mix_mode_Sch_Loc].Slc_Mix_Sch[3].compress2;
        temp_arr[37] = Slc_Comp_Mix_Mode_schedule[Get_Mix_mode_Sch_Loc].Slc_Mix_Sch[3].compress3;
        temp_arr[38] = Slc_Comp_Mix_Mode_schedule[Get_Mix_mode_Sch_Loc].Slc_Mix_Sch[3].compress4;
        temp_arr[39] = Slc_Comp_Mix_Mode_schedule[Get_Mix_mode_Sch_Loc].Slc_Mix_Sch[3].compress5;
        temp_arr[40] = Slc_Comp_Mix_Mode_schedule[Get_Mix_mode_Sch_Loc].Slc_Mix_Sch[3].compress6;

        temp_arr[41] = Slc_Comp_Mix_Mode_schedule[Get_Mix_mode_Sch_Loc].Slc_Mix_Sch[4].compress1;
        temp_arr[42] = Slc_Comp_Mix_Mode_schedule[Get_Mix_mode_Sch_Loc].Slc_Mix_Sch[4].compress2;
        temp_arr[43] = Slc_Comp_Mix_Mode_schedule[Get_Mix_mode_Sch_Loc].Slc_Mix_Sch[4].compress3;
        temp_arr[44] = Slc_Comp_Mix_Mode_schedule[Get_Mix_mode_Sch_Loc].Slc_Mix_Sch[4].compress4;
        temp_arr[45] = Slc_Comp_Mix_Mode_schedule[Get_Mix_mode_Sch_Loc].Slc_Mix_Sch[4].compress5;
        temp_arr[46] = Slc_Comp_Mix_Mode_schedule[Get_Mix_mode_Sch_Loc].Slc_Mix_Sch[4].compress6;

        temp_arr[47] = Slc_Comp_Mix_Mode_schedule[Get_Mix_mode_Sch_Loc].Slc_Mix_Sch[5].compress1;
        temp_arr[48] = Slc_Comp_Mix_Mode_schedule[Get_Mix_mode_Sch_Loc].Slc_Mix_Sch[5].compress2;
        temp_arr[49] = Slc_Comp_Mix_Mode_schedule[Get_Mix_mode_Sch_Loc].Slc_Mix_Sch[5].compress3;
        temp_arr[50] = Slc_Comp_Mix_Mode_schedule[Get_Mix_mode_Sch_Loc].Slc_Mix_Sch[5].compress4;
        temp_arr[51] = Slc_Comp_Mix_Mode_schedule[Get_Mix_mode_Sch_Loc].Slc_Mix_Sch[5].compress5;
        temp_arr[52] = Slc_Comp_Mix_Mode_schedule[Get_Mix_mode_Sch_Loc].Slc_Mix_Sch[5].compress6;

        temp_arr[53] = Slc_Comp_Mix_Mode_schedule[Get_Mix_mode_Sch_Loc].Slc_Mix_Sch[6].compress1;
        temp_arr[54] = Slc_Comp_Mix_Mode_schedule[Get_Mix_mode_Sch_Loc].Slc_Mix_Sch[6].compress2;
        temp_arr[55] = Slc_Comp_Mix_Mode_schedule[Get_Mix_mode_Sch_Loc].Slc_Mix_Sch[6].compress3;
        temp_arr[56] = Slc_Comp_Mix_Mode_schedule[Get_Mix_mode_Sch_Loc].Slc_Mix_Sch[6].compress4;
        temp_arr[57] = Slc_Comp_Mix_Mode_schedule[Get_Mix_mode_Sch_Loc].Slc_Mix_Sch[6].compress5;
        temp_arr[58] = Slc_Comp_Mix_Mode_schedule[Get_Mix_mode_Sch_Loc].Slc_Mix_Sch[6].compress6;

        temp_arr[59] = Slc_Comp_Mix_Mode_schedule[Get_Mix_mode_Sch_Loc].Slc_Mix_Sch[7].compress1;
        temp_arr[60] = Slc_Comp_Mix_Mode_schedule[Get_Mix_mode_Sch_Loc].Slc_Mix_Sch[7].compress2;
        temp_arr[61] = Slc_Comp_Mix_Mode_schedule[Get_Mix_mode_Sch_Loc].Slc_Mix_Sch[7].compress3;
        temp_arr[62] = Slc_Comp_Mix_Mode_schedule[Get_Mix_mode_Sch_Loc].Slc_Mix_Sch[7].compress4;
        temp_arr[63] = Slc_Comp_Mix_Mode_schedule[Get_Mix_mode_Sch_Loc].Slc_Mix_Sch[7].compress5;
        temp_arr[64] = Slc_Comp_Mix_Mode_schedule[Get_Mix_mode_Sch_Loc].Slc_Mix_Sch[7].compress6;

        temp_arr[65] = Slc_Comp_Mix_Mode_schedule[Get_Mix_mode_Sch_Loc].Slc_Mix_Sch[8].compress1;
        temp_arr[67] = Slc_Comp_Mix_Mode_schedule[Get_Mix_mode_Sch_Loc].Slc_Mix_Sch[8].compress2;
        temp_arr[68] = Slc_Comp_Mix_Mode_schedule[Get_Mix_mode_Sch_Loc].Slc_Mix_Sch[8].compress3;
        temp_arr[69] = Slc_Comp_Mix_Mode_schedule[Get_Mix_mode_Sch_Loc].Slc_Mix_Sch[8].compress4;
        temp_arr[70] = Slc_Comp_Mix_Mode_schedule[Get_Mix_mode_Sch_Loc].Slc_Mix_Sch[8].compress5;
        temp_arr[71] = Slc_Comp_Mix_Mode_schedule[Get_Mix_mode_Sch_Loc].Slc_Mix_Sch[8].compress6;

        Send_data_RF1(temp_arr, 72, 1);
        }
    else if (Send_Link_key == 1)
        {
        Send_Link_key = 0;

        }
    else if (send_get_photocell_config == 1)
        {
        send_get_photocell_config = 0;
        temp_arr[8] = GET_PHOTOCELL_CONFIG;
        temp_arr[9] = Photocell_steady_timeout_Val;
        temp_arr[10] = photo_cell_toggel_counter_Val;
        temp_arr[11] = photo_cell_toggel_timer_Val;
        temp_arr[12] = Photo_cell_Frequency;
        temp_arr[13] = Photocell_unsteady_timeout_Val / 256;
        temp_arr[14] = Photocell_unsteady_timeout_Val % 256;
        Send_data_RF1(temp_arr, 15, 1);
        }
    else if (uChar_GET_FOOT_CANDLE == 1)
        {
        uChar_GET_FOOT_CANDLE = 0;
        temp_arr[8] = GET_FOOT_CANDLE;

        pulseCounter.float_data = Photosensor_Set_FC;
        temp_arr[9] = pulseCounter.byte[3];
        temp_arr[10] = pulseCounter.byte[2];
        temp_arr[11] = pulseCounter.byte[1];
        temp_arr[12] = pulseCounter.byte[0];

        pulseCounter.float_data = Photosensor_Set_FC_Lamp_OFF;
        temp_arr[13] = pulseCounter.byte[3];
        temp_arr[14] = pulseCounter.byte[2];
        temp_arr[15] = pulseCounter.byte[1];
        temp_arr[16] = pulseCounter.byte[0];

        pulseCounter.float_data = Photosensor_FootCandle;
        temp_arr[17] = pulseCounter.byte[3];
        temp_arr[18] = pulseCounter.byte[2];
        temp_arr[19] = pulseCounter.byte[1];
        temp_arr[20] = pulseCounter.byte[0];

        Send_data_RF1(temp_arr, 21, 1);
        }



    else if (Test_get_cisco_config_send == 1)
        {
        Test_get_cisco_config_send = 0;
        temp_arr[8] = 0x43;

        temp_arr[9] = GET_CISCO_CONFIG;
        temp_arr[10] = Normal_reset_time;
        temp_arr[11] = ID_Frame_reset_time;
        temp_arr[12] = idframe_frequency;
        temp_arr[13] = ID_frame_timeout / 256;
        temp_arr[14] = ID_frame_timeout % 256;

        Send_data_RF1(temp_arr, 15, 1);
        }
    else if (send_get_Gps_Config == 1)
        {
        temp_arr[8] = GET_GPS_CONFIG;
        temp_arr[9] = GPS_Read_Enable;
        temp_arr[10] = GPS_error_condition.Val;
        temp_arr[11] = GPS_Wake_timer / 256;
        temp_arr[12] = GPS_Wake_timer % 256;
        temp_arr[13] = No_of_setalite_threshold;
        temp_arr[14] = Setalite_angle_threshold;
        temp_arr[15] = setalite_Rssi_threshold;
        temp_arr[16] = Maximum_no_of_Setalite_reading;

        pulseCounter.float_data = HDOP_Gps_threshold;
        temp_arr[17] = pulseCounter.byte[3];
        temp_arr[18] = pulseCounter.byte[2];
        temp_arr[19] = pulseCounter.byte[1];
        temp_arr[20] = pulseCounter.byte[0];

        temp_arr[21] = Check_Sbas_Enable;
        temp_arr[22] = Sbas_setalite_Rssi_threshold;
        temp_arr[23] = Auto_Gps_Data_Send;

        pulseCounter.int_data = averaging_counter;
        temp_arr[24] = pulseCounter.byte[1];
        temp_arr[25] = pulseCounter.byte[0];

        Send_data_RF1(temp_arr, 26, 1);

        if (GPS_Data_Valid != 3)
            {
            GPS_error_condition.Val = 0x00;
            }

        send_get_Gps_Config = 0;
        }
    else if ((Test_Config_Data_Send == 1))
        {
        temp_arr[8] = GET_CONF_CMD_IN_TEST_MODE;
        if ((SLC_Test_Query_2_received == 1) || (Commissioning_flag == 1))
            {
            automode = ReadByte_EEPROM(EE_LOGICMODE);
            if (automode > 7)
                {
                automode = 1;
                WriteByte_EEPROM(EE_LOGICMODE, automode);
                }

            temp_arr[9] = automode;
            automode = 0;
            }
        else
            {
            temp_arr[9] = automode;
            }

        pulseCounter.float_data = Latitude;
        temp_arr[10] = pulseCounter.byte[3];
        temp_arr[11] = pulseCounter.byte[2];
        temp_arr[12] = pulseCounter.byte[1];
        temp_arr[13] = pulseCounter.byte[0];

        pulseCounter.float_data = longitude;
        temp_arr[14] = pulseCounter.byte[3];
        temp_arr[15] = pulseCounter.byte[2];
        temp_arr[16] = pulseCounter.byte[1];
        temp_arr[17] = pulseCounter.byte[0];

        pulseCounter.float_data = utcOffset;
        temp_arr[18] = pulseCounter.byte[3];
        temp_arr[19] = pulseCounter.byte[2];
        temp_arr[20] = pulseCounter.byte[1];
        temp_arr[21] = pulseCounter.byte[0];

        pulseCounter.float_data = (float) Sunset_delay;
        temp_arr[22] = pulseCounter.byte[3];
        temp_arr[23] = pulseCounter.byte[2];
        temp_arr[24] = pulseCounter.byte[1];
        temp_arr[25] = pulseCounter.byte[0];

        pulseCounter.float_data = (float) Sunrise_delay;
        temp_arr[26] = pulseCounter.byte[3];
        temp_arr[27] = pulseCounter.byte[2];
        temp_arr[28] = pulseCounter.byte[1];
        temp_arr[29] = pulseCounter.byte[0];

        temp_arr[30] = GPS_Data_throw_status;

        temp_arr[31] = SLC_DST_En;
        temp_arr[32] = SLC_DST_Start_Month;
        temp_arr[33] = SLC_DST_Start_Rule;
        temp_arr[34] = SLC_DST_Start_Time;
        temp_arr[35] = SLC_DST_Stop_Month;
        temp_arr[36] = SLC_DST_Stop_Rule;
        temp_arr[37] = SLC_DST_Stop_Time;

        pulseCounter.float_data = SLC_DST_Time_Zone_Diff;
        temp_arr[38] = pulseCounter.byte[3];
        temp_arr[39] = pulseCounter.byte[2];
        temp_arr[40] = pulseCounter.byte[1];
        temp_arr[41] = pulseCounter.byte[0];
        temp_arr[42] = SLC_DST_Rule_Enable;
        temp_arr[43] = SLC_DST_R_Start_Date;
        temp_arr[44] = SLC_DST_R_Stop_Date;

        Send_data_RF1(temp_arr, 45, 1);
        Test_Config_Data_Send = 0;

        }

    else if (Send_Voltage_calibration == 1)
        {
        Send_Voltage_calibration = 0;
        Cali_buff[0] = 0x8E;
        Cali_buff[1] = 0x4C;
        //	Cali_buff[2]=amr_id/256;
        //	Cali_buff[3]=amr_id%256;
        ///////////////////
        pulseCounter.long_data = amr_id;
        Cali_buff[2] = pulseCounter.byte[0];
        Cali_buff[3] = pulseCounter.byte[1];
        Cali_buff[4] = pulseCounter.byte[2];
        Cali_buff[5] = pulseCounter.byte[3];
        ///////////////////
        Cali_buff[6] = 0x92;
        pulseCounter.float_data = Vr_Cal;
        Cali_buff[7] = pulseCounter.byte[3];
        Cali_buff[8] = pulseCounter.byte[2];
        Cali_buff[9] = pulseCounter.byte[1];
        Cali_buff[10] = pulseCounter.byte[0];

        pulseCounter.float_data = Calculated_Frequency;
        Cali_buff[11] = pulseCounter.byte[3];
        Cali_buff[12] = pulseCounter.byte[2];
        Cali_buff[13] = pulseCounter.byte[1];
        Cali_buff[14] = pulseCounter.byte[0];

        WriteData_UART3(Cali_buff, 15);
        // send_data_on_uart((char *)Cali_buff,15);
        }
    else if (Send_Current_calibration == 1)
        {
        Send_Current_calibration = 0;
        Cali_buff[0] = 0x8E;
        Cali_buff[1] = 0x4C;
        //	Cali_buff[2]=amr_id/256;
        //	Cali_buff[3]=amr_id%256;
        ///////////////////
        pulseCounter.long_data = amr_id;
        Cali_buff[2] = pulseCounter.byte[0];
        Cali_buff[3] = pulseCounter.byte[1];
        Cali_buff[4] = pulseCounter.byte[2];
        Cali_buff[5] = pulseCounter.byte[3];
        ///////////////////
        Cali_buff[6] = 0x93;
        pulseCounter.float_data = Ir_Cal;
        Cali_buff[7] = pulseCounter.byte[3];
        Cali_buff[8] = pulseCounter.byte[2];
        Cali_buff[9] = pulseCounter.byte[1];
        Cali_buff[10] = pulseCounter.byte[0];
        WriteData_UART3(Cali_buff, 11);
        //  send_data_on_uart((char *)Cali_buff,11);
        }
    else if (Send_Kw_calibration == 1)
        {
        Send_Kw_calibration = 0;
        Cali_buff[0] = 0x8E;
        Cali_buff[1] = 0x4C;
        //	Cali_buff[2]=amr_id/256;
        //	Cali_buff[3]=amr_id%256;
        ///////////////////
        pulseCounter.long_data = amr_id;
        Cali_buff[2] = pulseCounter.byte[0];
        Cali_buff[3] = pulseCounter.byte[1];
        Cali_buff[4] = pulseCounter.byte[2];
        Cali_buff[5] = pulseCounter.byte[3];

        ///////////////////
        Cali_buff[6] = 0x94;
        pulseCounter.float_data = KW_Cal;
        Cali_buff[7] = pulseCounter.byte[3];
        Cali_buff[8] = pulseCounter.byte[2];
        Cali_buff[9] = pulseCounter.byte[1];
        Cali_buff[10] = pulseCounter.byte[0];
        WriteData_UART3(Cali_buff, 11);
        //send_data_on_uart((char *)Cali_buff,11);
        }
    else if (Read_Em_Calibration == 1)
        {
        Read_Em_Calibration = 0;
        Cali_buff[0] = 0x8E;
        Cali_buff[1] = 0x4C;
        //	Cali_buff[2]=amr_id/256;
        //	Cali_buff[3]=amr_id%256;
        ///////////////////
        pulseCounter.long_data = amr_id;
        Cali_buff[2] = pulseCounter.byte[0];
        Cali_buff[3] = pulseCounter.byte[1];
        Cali_buff[4] = pulseCounter.byte[2];
        Cali_buff[5] = pulseCounter.byte[3];

        ///////////////////
        Cali_buff[6] = 0x95;

        Cali_buff[7] = Date.Date;
        Cali_buff[8] = Date.Month;
        Cali_buff[9] = Date.Year;
        Cali_buff[10] = Time.Hour;
        Cali_buff[11] = Time.Min;
        Cali_buff[12] = Time.Sec;

        pulseCounter.float_data = vrdoub;
        Cali_buff[13] = pulseCounter.byte[3];
        Cali_buff[14] = pulseCounter.byte[2];
        Cali_buff[15] = pulseCounter.byte[1];
        Cali_buff[16] = pulseCounter.byte[0];
        pulseCounter.float_data = irdoub;
        Cali_buff[17] = pulseCounter.byte[3];
        Cali_buff[18] = pulseCounter.byte[2];
        Cali_buff[19] = pulseCounter.byte[1];
        Cali_buff[20] = pulseCounter.byte[0];
        pulseCounter.float_data = kwh;
        Cali_buff[21] = pulseCounter.byte[3];
        Cali_buff[22] = pulseCounter.byte[2];
        Cali_buff[23] = pulseCounter.byte[1];
        Cali_buff[24] = pulseCounter.byte[0];
        ///////////////////////
        //////////////////////////////
        Cali_Cnt++;
        if (Cali_Cnt == 3)//added patch on 6Jan2017 due to cali appl
            {
            Cali_Cnt = 0;
            kwh = 0;
            pulses_r = 0;
            pulseCounter.long_data = pulses_r;
            WriteByte_EEPROM(EE_KWH + 0, pulseCounter.byte[0]);
            WriteByte_EEPROM(EE_KWH + 1, pulseCounter.byte[1]);
            WriteByte_EEPROM(EE_KWH + 2, pulseCounter.byte[2]);
            WriteByte_EEPROM(EE_KWH + 3, pulseCounter.byte[3]);
            }
        ///////////////////////////////	
        ////////////////////////
        /*Frequency_Cal Added in 1.0.0014beta1 version*/
        pulseCounter.float_data = Calculated_Frequency; //SilverSpring_Frequency;
        Cali_buff[25] = pulseCounter.byte[3];
        Cali_buff[26] = pulseCounter.byte[2];
        Cali_buff[27] = pulseCounter.byte[1];
        Cali_buff[28] = pulseCounter.byte[0];

        pulseCounter.float_data = pf;
        Cali_buff[29] = pulseCounter.byte[3];
        Cali_buff[30] = pulseCounter.byte[2];
        Cali_buff[31] = pulseCounter.byte[1];
        Cali_buff[32] = pulseCounter.byte[0];
        WriteData_UART3(Cali_buff, 33);
        }
    else if (Read_KWH_Value == 1)
        {
        Read_KWH_Value = 0;

        Cali_buff[0] = 0x8E;
        Cali_buff[1] = 0x4C;
        ///////////////////
        pulseCounter.long_data = amr_id;
        Cali_buff[2] = pulseCounter.byte[0];
        Cali_buff[3] = pulseCounter.byte[1];
        Cali_buff[4] = pulseCounter.byte[2];
        Cali_buff[5] = pulseCounter.byte[3];

        ///////////////////
        Cali_buff[6] = 0x96;
        ////////////
        pulseCounter.byte[0] = 0; //ReadByte_EEPROM(EE_KWH+0);
        pulseCounter.byte[1] = 0; //ReadByte_EEPROM(EE_KWH+1);
        pulseCounter.byte[2] = 0; //ReadByte_EEPROM(EE_KWH+2);
        pulseCounter.byte[3] = 0; //ReadByte_EEPROM(EE_KWH+3);
        pulses_r = pulseCounter.long_data;
        //halCommonGetToken(&pulses_r,TOKEN_pulses_r);
        kwh = (((float) pulses_r * mf3));
        /////////////
        pulseCounter.float_data = kwh;
        Cali_buff[7] = pulseCounter.byte[3];
        Cali_buff[8] = pulseCounter.byte[2];
        Cali_buff[9] = pulseCounter.byte[1];
        Cali_buff[10] = pulseCounter.byte[0];
        WriteData_UART3(Cali_buff, 11);
        //send_data_on_uart((char *)Cali_buff,11);

        }
    else if (Send_Kw_calibration_120 == 1)
        {
        Send_Kw_calibration_120 = 0;
        Cali_buff[0] = 0x8E;
        Cali_buff[1] = 0x4C;
        //	temp_arr[2]=amr_id/256;
        //	temp_arr[3]=amr_id%256;
        ///////////////////
        pulseCounter.long_data = amr_id;
        Cali_buff[2] = pulseCounter.byte[0];
        Cali_buff[3] = pulseCounter.byte[1];
        Cali_buff[4] = pulseCounter.byte[2];
        Cali_buff[5] = pulseCounter.byte[3];

        ///////////////////
        Cali_buff[6] = 0x97;
        pulseCounter.float_data = KW_Cal_120;
        Cali_buff[7] = pulseCounter.byte[3];
        Cali_buff[8] = pulseCounter.byte[2];
        Cali_buff[9] = pulseCounter.byte[1];
        Cali_buff[10] = pulseCounter.byte[0];
        WriteData_UART3(Cali_buff, 11);
        }

    }

void Send_data_RF1(unsigned char *data, unsigned char len, unsigned char only_DCU)
    {
	unsigned int x = 0;
	unsigned int CRC = 0,i=0,index=0;
	packetlength=0;
	if(RF_Modem_detected==1)
	{
		unsigned char i=0;
		unsigned char sum=0;
		base_buff[packetlength++]=0x7E;
		base_buff[packetlength++]=0x00;
		base_buff[packetlength++]=len+8+6;
		base_buff[packetlength++]=0x10;
		base_buff[packetlength++]=0x00;							// need ack of serial or not.

		if(only_DCU == 1)
		{
			base_buff[packetlength++]=0x00;
			base_buff[packetlength++]=0x00;
			base_buff[packetlength++]=0x00;
			base_buff[packetlength++]=0x00;
			base_buff[packetlength++]=0x00;
			base_buff[packetlength++]=0x00;
			base_buff[packetlength++]=0x00;
			base_buff[packetlength++]=0x00;
		}
		else
		{
			base_buff[packetlength++] = macLongAddrArray[0];
			base_buff[packetlength++] = macLongAddrArray[1];
			base_buff[packetlength++] = macLongAddrArray[2];
			base_buff[packetlength++] = macLongAddrArray[3];
			base_buff[packetlength++] = macLongAddrArray[4];
			base_buff[packetlength++] = macLongAddrArray[5];
			base_buff[packetlength++] = macLongAddrArray[6];
			base_buff[packetlength++] = macLongAddrArray[7];
		}
		base_buff[packetlength++]=0xFF;
		base_buff[packetlength++]=0xFE;
		base_buff[packetlength++]=0x00;
		base_buff[packetlength++]=0x00;
		for(i=0; i<len; i++)
		{
			base_buff[packetlength++]=data[i];
		}
		for(i=3; i<packetlength; i++)
		{
			sum = sum + base_buff[i];
		}
		base_buff[packetlength++] = 0xFF-sum;
	//	WriteData_UART1(base_buff,packetlength);
         WriteBust(SC16IS740_CHANNEL_A,base_buff,packetlength);

	}
    else
        {

        base_buff[index++] = 0x41;
        base_buff[index++] = 0x54;
        base_buff[index++] = 0x24;
        base_buff[index++] = 0x43;
        base_buff[index++] = 0x4c; // need ack of serial or not.


        base_buff[index++] = 0x49;
        base_buff[index++] = 0x7c;

        base_buff[index++] = 0x53;
        base_buff[index++] = 0x45;


        base_buff[index++] = 0x4E;
        base_buff[index++] = 0x44;

        base_buff[index++] = 0x2e;
        base_buff[index++] = len + 4;
        base_buff[index++] = 0x2e;


        for (i = 0; i < len; i++)
            {
            base_buff[index++] = data[i];
            }

        base_buff[index++] = track_res[10];
        base_buff[index++] = track_res[11];
        base_buff[index++] = track_res[12];
        base_buff[index++] = track_res[13];

        base_buff[index++] = 0x3F;
        base_buff[index++] = 0x0D;
        base_buff[index++] = 0x0A;
        WriteData_UART1(base_buff, index);

        }
    }

void Configure_Slc_Mix_Mode_Sch(unsigned char *temp_str)
    {
    unsigned char i = 0;
    L_No = temp_str[0];

    Slc_Comp_Mix_Mode_schedule[L_No].SLC_Mix_En = temp_str[1];
    WriteByte_EEPROM(EE_MIX_SCHEDULE + (L_No * 65) + 1, temp_str[1]);

    Slc_Comp_Mix_Mode_schedule[L_No].SLC_Mix_Sch_Start_Date = temp_str[2];
    WriteByte_EEPROM(EE_MIX_SCHEDULE + (L_No * 65) + 2, temp_str[2]);

    Slc_Comp_Mix_Mode_schedule[L_No].SLC_Mix_Sch_Start_Month = temp_str[3];
    WriteByte_EEPROM(EE_MIX_SCHEDULE + (L_No * 65) + 3, temp_str[3]);

    Slc_Comp_Mix_Mode_schedule[L_No].SLC_Mix_Sch_Stop_Date = temp_str[4];
    WriteByte_EEPROM(EE_MIX_SCHEDULE + (L_No * 65) + 4, temp_str[4]);

    Slc_Comp_Mix_Mode_schedule[L_No].SLC_Mix_Sch_Stop_Month = temp_str[5];
    WriteByte_EEPROM(EE_MIX_SCHEDULE + (L_No * 65) + 5, temp_str[5]);

    Slc_Comp_Mix_Mode_schedule[L_No].SLC_Mix_Sch_WeekDay = temp_str[6];
    WriteByte_EEPROM(EE_MIX_SCHEDULE + (L_No * 65) + 6, temp_str[6]);

    Slc_Comp_Mix_Mode_schedule[L_No].SLC_Mix_Sch_Photocell_Override = temp_str[7];
    WriteByte_EEPROM(EE_MIX_SCHEDULE + (L_No * 65) + 7, temp_str[7]);

    for (i = 0; i < 9; i++)
        {
        Slc_Comp_Mix_Mode_schedule[L_No].Slc_Mix_Sch[i].compress1 = temp_str[(6 * i) + 8];
        WriteByte_EEPROM(EE_MIX_SCHEDULE + (L_No * 65) + (i * 6) + 8, temp_str[(6 * i) + 8]);

        Slc_Comp_Mix_Mode_schedule[L_No].Slc_Mix_Sch[i].compress2 = temp_str[(6 * i) + 9];
        WriteByte_EEPROM(EE_MIX_SCHEDULE + (L_No * 65) + (i * 6) + 9, temp_str[(6 * i) + 9]);

        Slc_Comp_Mix_Mode_schedule[L_No].Slc_Mix_Sch[i].compress3 = temp_str[(6 * i) + 10];
        WriteByte_EEPROM(EE_MIX_SCHEDULE + (L_No * 65) + (i * 6) + 10, temp_str[(6 * i) + 10]);

        Slc_Comp_Mix_Mode_schedule[L_No].Slc_Mix_Sch[i].compress4 = temp_str[(6 * i) + 11];
        WriteByte_EEPROM(EE_MIX_SCHEDULE + (L_No * 65) + (i * 6) + 11, temp_str[(6 * i) + 11]);

        Slc_Comp_Mix_Mode_schedule[L_No].Slc_Mix_Sch[i].compress5 = temp_str[(6 * i) + 12];
        WriteByte_EEPROM(EE_MIX_SCHEDULE + (L_No * 65) + (i * 6) + 12, temp_str[(6 * i) + 12]);

        Slc_Comp_Mix_Mode_schedule[L_No].Slc_Mix_Sch[i].compress6 = temp_str[(6 * i) + 13];
        WriteByte_EEPROM(EE_MIX_SCHEDULE + (L_No * 65) + (i * 6) + 13, temp_str[(6 * i) + 13]);
        }
    }

void unCompress_Mix_sch(unsigned char loc_no)
    {
    BYTE_VAL temp_Mix_Mode_Byte;
    DWORD_VAL temp_Mix_Mode_Word[9];
    unsigned char i = 0;

    Slc_Mix_Mode_schedule[0].SLC_Mix_En = Slc_Comp_Mix_Mode_schedule[loc_no].SLC_Mix_En;
    Slc_Mix_Mode_schedule[0].SLC_Mix_Sch_Start_Date = Slc_Comp_Mix_Mode_schedule[loc_no].SLC_Mix_Sch_Start_Date;
    Slc_Mix_Mode_schedule[0].SLC_Mix_Sch_Start_Month = Slc_Comp_Mix_Mode_schedule[loc_no].SLC_Mix_Sch_Start_Month;
    Slc_Mix_Mode_schedule[0].SLC_Mix_Sch_Stop_Date = Slc_Comp_Mix_Mode_schedule[loc_no].SLC_Mix_Sch_Stop_Date;
    Slc_Mix_Mode_schedule[0].SLC_Mix_Sch_Stop_Month = Slc_Comp_Mix_Mode_schedule[loc_no].SLC_Mix_Sch_Stop_Month;
    Slc_Mix_Mode_schedule[0].SLC_Mix_Sch_WeekDay = Slc_Comp_Mix_Mode_schedule[loc_no].SLC_Mix_Sch_WeekDay;
    Slc_Mix_Mode_schedule[0].SLC_Mix_Sch_Photocell_Override = Slc_Comp_Mix_Mode_schedule[loc_no].SLC_Mix_Sch_Photocell_Override;

    for (i = 0; i < 9; i++)
        {
        temp_Mix_Mode_Word[i].v[0] = Slc_Comp_Mix_Mode_schedule[loc_no].Slc_Mix_Sch[i].compress1;
        temp_Mix_Mode_Word[i].v[1] = Slc_Comp_Mix_Mode_schedule[loc_no].Slc_Mix_Sch[i].compress2;
        temp_Mix_Mode_Word[i].v[2] = Slc_Comp_Mix_Mode_schedule[loc_no].Slc_Mix_Sch[i].compress3;
        temp_Mix_Mode_Word[i].v[3] = Slc_Comp_Mix_Mode_schedule[loc_no].Slc_Mix_Sch[i].compress4;

        temp_Mix_Mode_Byte.Val = 0;
        temp_Mix_Mode_Byte.bits.b0 = temp_Mix_Mode_Word[i].bits.b0;
        Slc_Mix_Mode_schedule[0].Slc_Mix_Sch[i].bSchFlag = temp_Mix_Mode_Byte.Val;

        temp_Mix_Mode_Byte.Val = 0;
        temp_Mix_Mode_Byte.bits.b0 = temp_Mix_Mode_Word[i].bits.b1;
        temp_Mix_Mode_Byte.bits.b1 = temp_Mix_Mode_Word[i].bits.b2;
        temp_Mix_Mode_Byte.bits.b2 = temp_Mix_Mode_Word[i].bits.b3;
        temp_Mix_Mode_Byte.bits.b3 = temp_Mix_Mode_Word[i].bits.b4;
        temp_Mix_Mode_Byte.bits.b4 = temp_Mix_Mode_Word[i].bits.b5;
        Slc_Mix_Mode_schedule[0].Slc_Mix_Sch[i].cStartHour = temp_Mix_Mode_Byte.Val;

        temp_Mix_Mode_Byte.Val = 0;
        temp_Mix_Mode_Byte.bits.b0 = temp_Mix_Mode_Word[i].bits.b6;
        temp_Mix_Mode_Byte.bits.b1 = temp_Mix_Mode_Word[i].bits.b7;
        temp_Mix_Mode_Byte.bits.b2 = temp_Mix_Mode_Word[i].bits.b8;
        temp_Mix_Mode_Byte.bits.b3 = temp_Mix_Mode_Word[i].bits.b9;
        temp_Mix_Mode_Byte.bits.b4 = temp_Mix_Mode_Word[i].bits.b10;
        temp_Mix_Mode_Byte.bits.b5 = temp_Mix_Mode_Word[i].bits.b11;
        Slc_Mix_Mode_schedule[0].Slc_Mix_Sch[i].cStartMin = temp_Mix_Mode_Byte.Val;

        temp_Mix_Mode_Byte.Val = 0;
        temp_Mix_Mode_Byte.bits.b0 = temp_Mix_Mode_Word[i].bits.b12;
        temp_Mix_Mode_Byte.bits.b1 = temp_Mix_Mode_Word[i].bits.b13;
        temp_Mix_Mode_Byte.bits.b2 = temp_Mix_Mode_Word[i].bits.b14;
        temp_Mix_Mode_Byte.bits.b3 = temp_Mix_Mode_Word[i].bits.b15;
        temp_Mix_Mode_Byte.bits.b4 = temp_Mix_Mode_Word[i].bits.b16;
        Slc_Mix_Mode_schedule[0].Slc_Mix_Sch[i].cStopHour = temp_Mix_Mode_Byte.Val;

        temp_Mix_Mode_Byte.Val = 0;
        temp_Mix_Mode_Byte.bits.b0 = temp_Mix_Mode_Word[i].bits.b17;
        temp_Mix_Mode_Byte.bits.b1 = temp_Mix_Mode_Word[i].bits.b18;
        temp_Mix_Mode_Byte.bits.b2 = temp_Mix_Mode_Word[i].bits.b19;
        temp_Mix_Mode_Byte.bits.b3 = temp_Mix_Mode_Word[i].bits.b20;
        temp_Mix_Mode_Byte.bits.b4 = temp_Mix_Mode_Word[i].bits.b21;
        temp_Mix_Mode_Byte.bits.b5 = temp_Mix_Mode_Word[i].bits.b22;
        Slc_Mix_Mode_schedule[0].Slc_Mix_Sch[i].cStopMin = temp_Mix_Mode_Byte.Val;

        temp_Mix_Mode_Byte.Val = 0;
        temp_Mix_Mode_Byte.bits.b0 = temp_Mix_Mode_Word[i].bits.b23;
        temp_Mix_Mode_Byte.bits.b1 = temp_Mix_Mode_Word[i].bits.b24;
        temp_Mix_Mode_Byte.bits.b2 = temp_Mix_Mode_Word[i].bits.b25;
        temp_Mix_Mode_Byte.bits.b3 = temp_Mix_Mode_Word[i].bits.b26;
        temp_Mix_Mode_Byte.bits.b4 = temp_Mix_Mode_Word[i].bits.b27;
        temp_Mix_Mode_Byte.bits.b5 = temp_Mix_Mode_Word[i].bits.b28;
        temp_Mix_Mode_Byte.bits.b6 = temp_Mix_Mode_Word[i].bits.b29;
        Slc_Mix_Mode_schedule[0].Slc_Mix_Sch[i].dimvalue = temp_Mix_Mode_Byte.Val;

        temp_Mix_Mode_Byte.Val = 0;
        temp_Mix_Mode_Byte.bits.b0 = temp_Mix_Mode_Word[i].bits.b30;
        temp_Mix_Mode_Byte.bits.b1 = temp_Mix_Mode_Word[i].bits.b31;
        Slc_Mix_Mode_schedule[0].Slc_Mix_Sch[i].Dim_Mode = temp_Mix_Mode_Byte.Val;

        Slc_Mix_Mode_schedule[0].Slc_Mix_Sch[i].cStartOff = Slc_Comp_Mix_Mode_schedule[loc_no].Slc_Mix_Sch[i].compress5;
        Slc_Mix_Mode_schedule[0].Slc_Mix_Sch[i].cStopOff = Slc_Comp_Mix_Mode_schedule[loc_no].Slc_Mix_Sch[i].compress6;
        }
    }

unsigned char check_group(unsigned char temp_G1)
    {
    BYTE_VAL temp_Group_val1;


    temp_Group_val1.Val = temp_G1;

    if (temp_Group_val1.Val == 0x00)
        {
        return 1;
        }
    else if ((Group_val1.bits.b0 == temp_Group_val1.bits.b0) && (temp_Group_val1.bits.b0 == 1))
        {
        return 1;
        }
    else if ((Group_val1.bits.b1 == temp_Group_val1.bits.b1) && (temp_Group_val1.bits.b1 == 1))
        {
        return 1;
        }
    else if ((Group_val1.bits.b2 == temp_Group_val1.bits.b2) && (temp_Group_val1.bits.b2 == 1))
        {
        return 1;
        }
    else if ((Group_val1.bits.b3 == temp_Group_val1.bits.b3) && (temp_Group_val1.bits.b3 == 1))
        {
        return 1;
        }
    else if ((Group_val1.bits.b4 == temp_Group_val1.bits.b4) && (temp_Group_val1.bits.b4 == 1))
        {
        return 1;
        }
    else if ((Group_val1.bits.b5 == temp_Group_val1.bits.b5) && (temp_Group_val1.bits.b5 == 1))
        {
        return 1;
        }
    else if ((Group_val1.bits.b6 == temp_Group_val1.bits.b6) && (temp_Group_val1.bits.b6 == 1))
        {
        return 1;
        }
    else if ((Group_val1.bits.b7 == temp_Group_val1.bits.b7) && (temp_Group_val1.bits.b7 == 1))
        {
        return 1;
        }
    else
        {
        return 0;
        }
    }

unsigned char check_multi_group(unsigned char temp_G1)
    {
    BYTE_VAL temp_Group_val1;
    unsigned char inc_group_counter = 0;

    temp_Group_val1.Val = temp_G1;

    if (temp_Group_val1.bits.b0 == 1)
        {
        inc_group_counter++;
        }

    if (temp_Group_val1.bits.b1 == 1)
        {
        inc_group_counter++;
        }

    if (temp_Group_val1.bits.b2 == 1)
        {
        inc_group_counter++;
        }

    if (temp_Group_val1.bits.b3 == 1)
        {
        inc_group_counter++;
        }

    if (temp_Group_val1.bits.b4 == 1)
        {
        inc_group_counter++;
        }

    if (temp_Group_val1.bits.b5 == 1)
        {
        inc_group_counter++;
        }

    if (temp_Group_val1.bits.b6 == 1)
        {
        inc_group_counter++;
        }

    if (temp_Group_val1.bits.b7 == 1)
        {
        inc_group_counter++;
        }

    if (inc_group_counter > 1)
        {
        return 1;
        }
    else
        {
        return 0;
        }
    }

void Filldata(void)
    {
    temp_arr[10] = Date.Date;
    temp_arr[11] = Date.Month;
    temp_arr[12] = Date.Year;
    temp_arr[13] = Time.Hour;
    temp_arr[14] = Time.Min;
    temp_arr[15] = Time.Sec;


    temp_arr[16] = error_condition.Val; //remove if we use above commented section.
    temp_arr[17] = error_condition1.Val; // second char of digital data

    pulseCounter.float_data = vrdoub;
    temp_arr[18] = pulseCounter.byte[3];
    temp_arr[19] = pulseCounter.byte[2];
    temp_arr[20] = pulseCounter.byte[1];
    temp_arr[21] = pulseCounter.byte[0];

    pulseCounter.float_data = irdoub;
    temp_arr[22] = pulseCounter.byte[3];
    temp_arr[23] = pulseCounter.byte[2];
    temp_arr[24] = pulseCounter.byte[1];
    temp_arr[25] = pulseCounter.byte[0];

    pulseCounter.float_data = w1 / 1000.0;
    temp_arr[26] = pulseCounter.byte[3];
    temp_arr[27] = pulseCounter.byte[2];
    temp_arr[28] = pulseCounter.byte[1];
    temp_arr[29] = pulseCounter.byte[0];

    pulseCounter.float_data = kwh;
    temp_arr[30] = pulseCounter.byte[3];
    temp_arr[31] = pulseCounter.byte[2];
    temp_arr[32] = pulseCounter.byte[1];
    temp_arr[33] = pulseCounter.byte[0];

    pulseCounter.float_data = lamp_burn_hour;
    temp_arr[34] = pulseCounter.byte[3];
    temp_arr[35] = pulseCounter.byte[2];
    temp_arr[36] = pulseCounter.byte[1];
    temp_arr[37] = pulseCounter.byte[0];

    pulseCounter.float_data = Dimvalue;
    temp_arr[38] = pulseCounter.byte[3];
    temp_arr[39] = pulseCounter.byte[2];
    temp_arr[40] = pulseCounter.byte[1];
    temp_arr[41] = pulseCounter.byte[0];

    pulseCounter.float_data = pf; // SAN
    //	pulseCounter.float_data = power_on_count;			// SAN
    //pulseCounter.float_data = cisco_nic_reset;			// SAN

    temp_arr[42] = pulseCounter.byte[3];
    temp_arr[43] = pulseCounter.byte[2];
    temp_arr[44] = pulseCounter.byte[1];
    temp_arr[45] = pulseCounter.byte[0];

#ifdef NEW_LAMP_FAULT_LOGIC
    pulseCounter.float_data = KW_Threshold;
#elif defined(OLD_LAMP_FAULT_LOGIC)
    pulseCounter.float_data = lamp_current; // Lamp steady current
#else
#error "Please define LAMP_FAULT_LOGIC_TYPE"
#endif

    temp_arr[46] = pulseCounter.byte[3];
    temp_arr[47] = pulseCounter.byte[2];
    temp_arr[48] = pulseCounter.byte[1];
    temp_arr[49] = pulseCounter.byte[0];

    temp_arr[50] = no_of_pending_event;
    if (SLC_New_Manula_Mode_En == 1)
        {
        temp_arr[51] = 0;
        }
    else
        {
        temp_arr[51] = automode;
        }


    temp_arr[52] = (unsigned char)(vRCON/256);
    temp_arr[53] = (unsigned char)(vRCON%256);

    temp_arr[54] = Slc_db;
    temp_arr[55] = temperature;

    pulseCounter.float_data = power_on_count; // SAN
    //pulseCounter.float_data = va1/1000;			// SAN
    temp_arr[56] = pulseCounter.byte[3];
    temp_arr[57] = pulseCounter.byte[2];
    temp_arr[58] = pulseCounter.byte[1];
    temp_arr[59] = pulseCounter.byte[0];

    }

void Fill_Slc_Comp_Mix_Mode_schedule_compress(unsigned int i, unsigned int j)
    {

    WriteByte_EEPROM(EE_MIX_SCHEDULE + (j * 65) + (i * 6) + 8, Slc_Comp_Mix_Mode_schedule[j].Slc_Mix_Sch[i].compress1);
    WriteByte_EEPROM(EE_MIX_SCHEDULE + (j * 65) + (i * 6) + 9, Slc_Comp_Mix_Mode_schedule[j].Slc_Mix_Sch[i].compress2);
    WriteByte_EEPROM(EE_MIX_SCHEDULE + (j * 65) + (i * 6) + 10, Slc_Comp_Mix_Mode_schedule[j].Slc_Mix_Sch[i].compress3);
    WriteByte_EEPROM(EE_MIX_SCHEDULE + (j * 65) + (i * 6) + 11, Slc_Comp_Mix_Mode_schedule[j].Slc_Mix_Sch[i].compress4);
    WriteByte_EEPROM(EE_MIX_SCHEDULE + (j * 65) + (i * 6) + 12, Slc_Comp_Mix_Mode_schedule[j].Slc_Mix_Sch[i].compress5);
    WriteByte_EEPROM(EE_MIX_SCHEDULE + (j * 65) + (i * 6) + 13, Slc_Comp_Mix_Mode_schedule[j].Slc_Mix_Sch[i].compress6);

    }