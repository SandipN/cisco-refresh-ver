

#ifndef SERIAL_H
#define SERIAL_H
#include <xc.h>
//#define BAUD_9600	155					//25
//#define BAUD_57600	152					//25
//#define BAUD_115200	51					//25
#define BAUD_115200	12					//25
#define BAUD_9600	155					//25
extern volatile unsigned char data_rec_GPS;
extern volatile unsigned char GPS_timeout ;
extern volatile unsigned char RF_timeout ;
extern volatile unsigned char EM_timeout;
//extern unsigned char RF_serial_timeout;
extern volatile unsigned char GPS_serial_timeout;
extern unsigned char Serial_GPS_Buff[];
extern unsigned int temp_rx_GPS;

#define B_SIZE_SERIAL_RF 150
#define B_SIZE_SERIAL_GPS 600


extern unsigned char Serial_RF_Buff[];
extern unsigned char data_rec_RF;
extern unsigned int temp_rx_RF;


void Configure_UART1(void);
void Configure_UART2(void);
void Configure_UART3(void);
void Configure_UART4(uint16_t value);
void WriteData_UART1(unsigned char *dataPtr, unsigned int len);
void WriteData_UART2(unsigned char *dataPtr, unsigned int len);
void WriteData_UART3(unsigned char *dataPtr, unsigned int len);
void UART4_Write(unsigned char txData);

#endif



