
#include <xc.h>
#include <stdbool.h>
#define FCY 8000000UL
#include <libpic30.h>
#include "i2c1.h"


// CONFIG4
#pragma config DSWDTPS = DSWDTPS1F    //Deep Sleep Watchdog Timer Postscale Select bits->1:68719476736 (25.7 Days)
#pragma config DSWDTOSC = LPRC    //DSWDT Reference Clock Select->DSWDT uses LPRC as reference clock
#pragma config DSBOREN = ON    //Deep Sleep BOR Enable bit->DSBOR Enabled
#pragma config DSWDTEN = ON    //Deep Sleep Watchdog Timer Enable->DSWDT Enabled
#pragma config DSSWEN = ON    //DSEN Bit Enable->Deep Sleep is controlled by the register bit DSEN

// CONFIG3
#pragma config WPFP = WPFP127    //Write Protection Flash Page Segment Boundary->Page 127 (0x1FC00)
#pragma config VBTBOR = ON    //VBAT BOR enable bit->VBAT BOR enabled
#pragma config SOSCSEL = OFF    //SOSC Selection bits->Digital (SCLKI) mode
#pragma config WDTWIN = PS25_0    //Watch Dog Timer Window Width->Watch Dog Timer Window Width is 25 percent
#pragma config BOREN = ON    //Brown-out Reset Enable->Brown-out Reset Enable
#pragma config WPDIS = WPDIS    //Segment Write Protection Disable->Disabled
#pragma config WPCFG = WPCFGDIS    //Write Protect Configuration Page Select->Disabled
#pragma config WPEND = WPENDMEM    //Segment Write Protection End Page Select->Write Protect from WPFP to the last page of memory

// CONFIG2
#pragma config POSCMD = XT    //Primary Oscillator Select->XT Oscillator Enabled
#pragma config BOREN1 = EN    //BOR Override bit->BOR Enabled [When BOREN=1]
#pragma config IOL1WAY = ON    //IOLOCK One-Way Set Enable bit->Once set, the IOLOCK bit cannot be cleared
#pragma config OSCIOFCN = ON    //OSCO Pin Configuration->OSCO/CLKO/RC15 functions as port I/O (RC15)
#pragma config FCKSM = CSECME    //Clock Switching and Fail-Safe Clock Monitor Configuration bits->Clock switching is enabled, Fail-Safe Clock Monitor is enabled
#pragma config FNOSC = PRI    //Initial Oscillator Select->Primary Oscillator (XT, HS, EC)
#pragma config ALTVREF = ALT_AV_ALT_CV    //Alternate VREF/CVREF Pins Selection bit->Voltage reference input, ADC =RB0/RB1   Comparator =RB0/RB1
#pragma config IESO = OFF    //Internal External Switchover->Disabled

// CONFIG1
#pragma config WDTPS = PS32768    //Watchdog Timer Postscaler Select->1:32768
#pragma config FWPSA = PR128    //WDT Prescaler Ratio Select->1:128
#pragma config FWDTEN = WDT_DIS    //Watchdog Timer Enable->WDT disabled in hardware; SWDTEN bit disabled
#pragma config WINDIS = OFF    //Windowed WDT Disable->Standard Watchdog Timer
#pragma config ICS = PGx1    //Emulator Pin Placement Select bits->Emulator functions are shared with PGEC1/PGED1
#pragma config LPCFG = OFF    //Low power regulator control->Disabled
#pragma config GWRP = OFF    //General Segment Write Protect->Disabled
#pragma config GCP = OFF    //General Segment Code Protect->Code protection is disabled
#pragma config JTAGEN = OFF    //JTAG Port Enable->Disabled


#define PCF85363_ADDR 0x51
#define PCF85363_DATETIME 1

typedef struct
    {
    char Sec; /* Second value - [0,59]           RTC_Sec*/
    char Min; /* Minute value - [0,59]           RTC_Min*/
    char Hour; /* Hour value - [0,23]             RTC_Hour*/
    } RTCTime;

typedef struct
    {
    char Date; /* Day of the month value - [1,31] RTC_Mday*/
    char Month; /* Month value - [1,12]            RTC_Mon*/
    unsigned int Year; /* Year value - [0,4095]           RTC_Year*/
    unsigned char Week; /* Day of week value - [0,6]       RTC_Wday*/
    unsigned int DofYear; /* Day of year value - [1,365]     RTC_Yday */
    } RTCDate;

uint8_t readBuffer[10]; // Data from slave will be put here
uint8_t writeBuffer[10]; // Data to write to slave
I2C1_TRANSACTION_REQUEST_BLOCK readTRB[2]; // List of I2C transactions
I2C1_MESSAGE_STATUS status; // I2C status
RTCTime Time;
RTCDate Date;

uint8_t str[100];
void I2C1_Initialize(void);
void PIN_MANAGER_Initialize(void);
void CLOCK_Initialize(void);



uint32_t buffer = 0;
double voltage = 0;
double cur = 0;
uint8_t lcount = 0;

int main(void)
    {
    PIN_MANAGER_Initialize();
    CLOCK_Initialize();
    UART3_Initialize();
    UART4_Initialize();
    I2C1_Initialize();
    Time.Hour = 03; // if currept then set Default RTC Time
    Time.Min = 23;
    Time.Sec = 0;
    Date.Date = 28;
    Date.Month = 12;
    Date.Year = 19;
    PCF_Force_SW_Reset();
    RTC_SET_TIME(Time);
    RTC_SET_DATE(Date);
    TRISBbits.TRISB14 = 0;
    TRISBbits.TRISB15 = 0;
    LATBbits.LATB14 = 1;
    LATBbits.LATB15 = 0;
    while (1)
        {
        read_PCF85363_bytes();
        //*****************************************************************//
        //read_CS5480_bytes();
        }
    return 1;
    }

void read_PCF85363_bytes(void)
    {

    uint8_t temp_sec1, temp_sec2, temp_min1, temp_min2, temp_hour1, temp_hour2;
    uint8_t temp_date1, temp_date2, temp_month1, temp_month2, temp_year1, temp_year2;
    uint8_t SLC_Mix_R_weekday = 0;
    writeBuffer[0] = PCF85363_DATETIME; //  Data sent to slave (TSL2561 register for CH1)
    I2C1_MasterWriteTRBBuild(&readTRB[0], writeBuffer, 1, PCF85363_ADDR);
    I2C1_MasterReadTRBBuild(&readTRB[1], readBuffer, 7, PCF85363_ADDR);
    I2C1_MasterTRBInsert(2, readTRB, &status); // Insert TRB into I2C queue

    while (status == I2C1_MESSAGE_PENDING); // Wait end of transaction
    temp_sec1 = ((readBuffer[0]& 0x7F) - (((readBuffer[0] & 0x7F) / 16) * 6));
    temp_min1 = ((readBuffer[1]& 0x7F) - (((readBuffer[1] & 0x7F) / 16) * 6));
    temp_hour1 = ((readBuffer[2]& 0x7F) - (((readBuffer[2] & 0x7F) / 16) * 6));
    temp_date1 = ((readBuffer[3]& 0x7F) - (((readBuffer[3] & 0x7F) / 16) * 6));
    temp_month1 = ((readBuffer[5]& 0x7F) - (((readBuffer[5] & 0x7F) / 16) * 6));
    temp_year1 = ((readBuffer[6]& 0x7F) - (((readBuffer[6] & 0x7F) / 16) * 6));

    sprintf(str, "\r\nT:%02d-%02d-%02d %02d:%02d:%02d", temp_date1, temp_month1, temp_year1, temp_hour1, temp_min1, temp_sec1);
    UART3_Write(str, strlen(str));
    }

void RTC_SET_TIME(RTCTime RTC_Time)
    {

    unsigned char sdHr, sdMin, sdSs, i;

    sdHr = RTC_Time.Hour;
    i = (sdHr / 10);
    sdHr = (sdHr + (i * 6));

    sdMin = RTC_Time.Min;
    i = (sdMin / 10);
    sdMin = (sdMin + (i * 6));

    sdSs = RTC_Time.Sec;
    i = (sdSs / 10);
    sdSs = (sdSs + (i * 6));

    sdHr = sdHr & 0x3f;

    writeBuffer[0] = PCF85363_DATETIME; //  Data sent to slave (TSL2561 register for CH1)
    writeBuffer[1] = sdSs; //  Data sent to slave (TSL2561 register for CH1)
    writeBuffer[2] = sdMin; //  Data sent to slave (TSL2561 register for CH1)
    writeBuffer[3] = sdHr; //  Data sent to slave (TSL2561 register for CH1)
    I2C1_MasterWriteTRBBuild(&readTRB[0], writeBuffer, 4, PCF85363_ADDR);
    I2C1_MasterTRBInsert(2, readTRB, &status); // Insert TRB into I2C queue
    while (status == I2C1_MESSAGE_PENDING);
    }

void PCF_Force_SW_Reset(void)
    {
    writeBuffer[0] = 0x2E; //  Data sent to slave (TSL2561 register for CH1)
    writeBuffer[1] = 0x01; //  Data sent to slave (TSL2561 register for CH1)
    writeBuffer[2] = 0x2C; //  Data sent to slave (TSL2561 register for CH1)
    I2C1_MasterWriteTRBBuild(&readTRB[0], writeBuffer, 3, PCF85363_ADDR);
    I2C1_MasterTRBInsert(2, readTRB, &status);
    __delay_ms(100);
    writeBuffer[0] = 0x2E; //  Data sent to slave (TSL2561 register for CH1)
    writeBuffer[1] = 0x00; //  Data sent to slave (TSL2561 register for CH1)
    writeBuffer[2] = 0x2C; //  Data sent to slave (TSL2561 register for CH1)
    I2C1_MasterWriteTRBBuild(&readTRB[0], writeBuffer, 2, PCF85363_ADDR);
    I2C1_MasterTRBInsert(2, readTRB, &status);
    while (status == I2C1_MESSAGE_PENDING);

    }

void RTC_SET_DATE(RTCDate RTC_Date)
    {

    unsigned char sdDt, sdMn, sdYr, i;

    sdDt = RTC_Date.Date;
    i = (sdDt / 10);
    sdDt = (sdDt + (i * 6));

    sdMn = RTC_Date.Month;
    i = (sdMn / 10);
    sdMn = (sdMn + (i * 6));

    sdYr = RTC_Date.Year;
    i = (sdYr / 10);
    sdYr = (sdYr + (i * 6));

    writeBuffer[0] = 0x04; //  Data sent to slave (TSL2561 register for CH1)
    writeBuffer[1] = sdDt; //  Data sent to slave (TSL2561 register for CH1)
    I2C1_MasterWriteTRBBuild(&readTRB[0], writeBuffer, 2, PCF85363_ADDR);
    I2C1_MasterTRBInsert(2, readTRB, &status);
    __delay_ms(100);
    writeBuffer[0] = 0x06; //  Data sent to slave (TSL2561 register for CH1)
    writeBuffer[1] = sdMn; //  Data sent to slave (TSL2561 register for CH1)
    writeBuffer[2] = sdYr; //  Data sent to slave (TSL2561 register for CH1)
    I2C1_MasterWriteTRBBuild(&readTRB[0], writeBuffer, 3, PCF85363_ADDR);
    I2C1_MasterTRBInsert(2, readTRB, &status);
    while (status == I2C1_MESSAGE_PENDING);

    }

void PIN_MANAGER_Initialize(void)
    {
    /****************************************************************************
     * Setting the Output Latch SFR(s)
     ***************************************************************************/
    LATB = 0x0000;
    LATC = 0x0000;
    LATD = 0x0000;
    LATE = 0x0000;
    LATF = 0x0000;
    LATG = 0x0000;

    /****************************************************************************
     * Setting the GPIO Direction SFR(s)
     ***************************************************************************/
    TRISB = 0xFFFF;
    TRISC = 0x1000;
    TRISD = 0x0DF7;
    TRISE = 0x00FF;
    TRISF = 0x0077;
    TRISG = 0x038C;

    /****************************************************************************
     * Setting the Weak Pull Up and Weak Pull Down SFR(s)
     ***************************************************************************/
    CNPD1 = 0x0000;
    CNPD2 = 0x0000;
    CNPD3 = 0x0000;
    CNPD4 = 0x0000;
    CNPD5 = 0x0000;
    CNPD6 = 0x0000;
    CNPU1 = 0x0000;
    CNPU2 = 0x0000;
    CNPU3 = 0x0000;
    CNPU4 = 0x0000;
    CNPU5 = 0x0000;
    CNPU6 = 0x0000;

    /****************************************************************************
     * Setting the Open Drain SFR(s)
     ***************************************************************************/
    ODCB = 0x0000;
    ODCC = 0x0000;
    ODCD = 0x0000;
    ODCE = 0x0000;
    ODCF = 0x0000;
    ODCG = 0x0000;

    /****************************************************************************
     * Setting the Analog/Digital Configuration SFR(s)
     ***************************************************************************/
    ANSB = 0xFFFC;
    ANSD = 0x08C0;
    ANSE = 0x00F0;
    ANSG = 0x0340;


    /****************************************************************************
     * Set the PPS
     ***************************************************************************/
    __builtin_write_OSCCONL(OSCCON & 0xbf); // unlock PPS

    RPINR17bits.U3RXR = 0x0019; //RD4->UART3:U3RX
    RPOR8bits.RP16R = 0x0003; //RF3->UART1:U1TX
    RPOR10bits.RP21R = 0x001E; //RG6->UART4:U4TX
    RPINR27bits.U4RXR = 0x001A; //RG7->UART4:U4RX
    RPOR2bits.RP4R = 0x0005; //RD9->UART2:U2TX
    RPINR19bits.U2RXR = 0x0003; //RD10->UART2:U2RX
    RPINR18bits.U1RXR = 0x001E; //RF2->UART1:U1RX
    RPOR11bits.RP22R = 0x001C; //RD3->UART3:U3TX

    __builtin_write_OSCCONL(OSCCON | 0x40); // lock PPS

    }

void CLOCK_Initialize(void)
    {
    // RCDIV FRC/2; DOZE 1:8; DOZEN disabled; ROI disabled; 
    CLKDIV = 0x3100;
    // TUN Center frequency; 
    OSCTUN = 0x00;
    // ROEN disabled; ROSEL FOSC; RODIV 0; ROSSLP disabled; 
    REFOCON = 0x00;
    // ADC1MD enabled; T3MD enabled; T4MD enabled; T1MD enabled; U2MD enabled; T2MD enabled; U1MD enabled; SPI2MD enabled; SPI1MD enabled; T5MD enabled; I2C1MD enabled; 
    PMD1 = 0x00;
    // OC5MD enabled; OC6MD enabled; OC7MD enabled; OC1MD enabled; IC2MD enabled; OC2MD enabled; IC1MD enabled; OC3MD enabled; OC4MD enabled; IC6MD enabled; IC7MD enabled; IC5MD enabled; IC4MD enabled; IC3MD enabled; 
    PMD2 = 0x00;
    // DSMMD enabled; PMPMD enabled; U3MD enabled; RTCCMD enabled; CMPMD enabled; CRCMD enabled; I2C2MD enabled; 
    PMD3 = 0x00;
    // U4MD enabled; UPWMMD enabled; CTMUMD enabled; REFOMD enabled; LVDMD enabled; 
    PMD4 = 0x00;
    // SPI3MD enabled; LCDMD enabled; 
    PMD6 = 0x00;
    // DMA1MD enabled; DMA0MD enabled; 
    PMD7 = 0x00;
    // CF no clock failure; NOSC PRI; SOSCEN disabled; POSCEN disabled; CLKLOCK unlocked; OSWEN Switch is Complete; IOLOCK not-active; 
    __builtin_write_OSCCONH((uint8_t) (0x02));
    __builtin_write_OSCCONL((uint8_t) (0x00));
    }

void UART4_Initialize(void)
    {
    /**    
         Set the UART4 module to the options selected in the user interface.
         Make sure to set LAT bit corresponding to TxPin as high before UART initialization
     */
    // STSEL 1; IREN disabled; PDSEL 8N; UARTEN enabled; RTSMD disabled; USIDL disabled; WAKE disabled; ABAUD disabled; LPBACK disabled; BRGH enabled; RXINV disabled; UEN TX_RX; 
    // Data Bits = 8; Parity = None; Stop Bits = 1;
    U4MODE = (0x8008 & ~(1 << 15)); // disabling UARTEN bit
    // UTXISEL0 TX_ONE_CHAR; UTXINV disabled; OERR NO_ERROR_cleared; URXISEL RX_ONE_CHAR; UTXBRK COMPLETED; UTXEN disabled; ADDEN disabled; 
    U4STA = 0x00;
    // BaudRate = 600; Frequency = 4000000 Hz; BRG 1666; 
    U4BRG = 0x682;

    U4MODEbits.UARTEN = 1; // enabling UART ON bit
    U4STAbits.UTXEN = 1;
    }

void UART4_ReInitialize(void)
    {
    /**    
         Set the UART4 module to the options selected in the user interface.
         Make sure to set LAT bit corresponding to TxPin as high before UART initialization
     */
    // STSEL 1; IREN disabled; PDSEL 8N; UARTEN enabled; RTSMD disabled; USIDL disabled; WAKE disabled; ABAUD disabled; LPBACK disabled; BRGH enabled; RXINV disabled; UEN TX_RX; 
    // Data Bits = 8; Parity = None; Stop Bits = 1;
    U4MODE = (0x8008 & ~(1 << 15)); // disabling UARTEN bit
    // UTXISEL0 TX_ONE_CHAR; UTXINV disabled; OERR NO_ERROR_cleared; URXISEL RX_ONE_CHAR; UTXBRK COMPLETED; UTXEN disabled; ADDEN disabled; 
    U4STA = 0x00;
    // BaudRate = 600; Frequency = 4000000 Hz; BRG 1666; 
    U4BRG = 0x67;

    U4MODEbits.UARTEN = 1; // enabling UART ON bit
    U4STAbits.UTXEN = 1;
    }

uint8_t UART4_Read(void)
    {
    while (!(U4STAbits.URXDA == 1))
        {

        }

    if ((U4STAbits.OERR == 1))
        {
        U4STAbits.OERR = 0;
        }

    return U4RXREG;
    }

void UART4_Write(uint8_t txData)
    {
    while (U4STAbits.UTXBF == 1)
        {

        }

    U4TXREG = txData; // Write the data byte to the USART.
    }

void UART3_Initialize(void)
    {
    /**    
         Set the UART3 module to the options selected in the user interface.
         Make sure to set LAT bit corresponding to TxPin as high before UART initialization
     */
    // STSEL 1; IREN disabled; PDSEL 8N; UARTEN enabled; RTSMD disabled; USIDL disabled; WAKE disabled; ABAUD disabled; LPBACK disabled; BRGH enabled; RXINV disabled; UEN TX_RX; 
    // Data Bits = 8; Parity = None; Stop Bits = 1;
    U3MODE = (0x8008 & ~(1 << 15)); // disabling UARTEN bit
    // UTXISEL0 TX_ONE_CHAR; UTXINV disabled; OERR NO_ERROR_cleared; URXISEL RX_ONE_CHAR; UTXBRK COMPLETED; UTXEN disabled; ADDEN disabled; 
    U3STA = 0x00;
    // BaudRate = 9600; Frequency = 4000000 Hz; BRG 103; 
    U3BRG = 0x67;

    U3MODEbits.UARTEN = 1; // enabling UART ON bit
    U3STAbits.UTXEN = 1;
    }

uint8_t UART3_Read(void)
    {
    while (!(U3STAbits.URXDA == 1))
        {

        }

    if ((U3STAbits.OERR == 1))
        {
        U3STAbits.OERR = 0;
        }

    return U3RXREG;
    }

void UART3_Write(uint8_t *txData, uint8_t len)
    {
    uint8_t i = 0;
    for (i = 0; i < len; i++)
        {
        while (U3STAbits.UTXBF == 1)
            {

            }

        U3TXREG = txData[i]; // Write the data byte to the USART.
        }

    }