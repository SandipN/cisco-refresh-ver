

#include <math.h>

#include "Compiler.h"
#include "generic.h"
#include "AMR_RTC.h"
#include "zigbee.def"
#include "MSPI.h"
#include "Application_logic.h"		
#include "AMR_cs5463.h"
#include "protocol.h"
#include "ADC_VI.h"
#include "Gps.h"

//#define DALI_SUPPORT

//#if defined(DALI_SUPPORT)
extern unsigned char f_dalitx;
//extern unsigned char f_dalirx;

#define DALIE			LATBbits.LATB7
#define DALIT			LATBbits.LATB3
//#endif

struct Schedule sSch[1][10], Astro_sSch[2], DLH_sSch[2]; // V6.1.10
struct DimSchedule Master_sSch[1][10];
struct Mix_DimSch Mix_sSch[9];
struct push_sch push[24];
struct Slc_M_Sch Slc_Mix_Mode_schedule[1];
struct Slc_Compress_M_Sch Slc_Comp_Mix_Mode_schedule[10];
void Fault_Remove_Logic(void);
unsigned char sendDaliCommand_ToCheck_DimmerDriver();
unsigned char detectDimmerDriver();

void DALI_ON(unsigned char);
void DALI_Send(void);
void make_default(void);
void unicast_shortaddress(void);
void DALI_DIMMING_CMD(unsigned char dim);
//*********************Ballast failsafe**************************//
volatile unsigned int Lamp_Balast_fault_Remove_Cnt = 0;
unsigned char Lamp_Balast_fault_Remove_Time = 12;
unsigned char No_check_Lamp_After_This_Event = 0;
unsigned char Ballast_Fault_Remove_Retry_Cnt_3 = 0;
unsigned char Lamp_Fault_Remove_Retry_Cnt_3 = 0;
unsigned char Ballast_Fault_Occured = 0;
unsigned char Lamp_Fault_Occured = 0;
unsigned long int Open_circuit = 0, Lamp_Failure = 0;
unsigned char automode = 0;
volatile unsigned int Sec_Counter = 0;
unsigned char Photo_Cell_Ok = 1;
unsigned char Set_Do_On = 0;
unsigned char Cloudy_Flag = 0;
unsigned char DaliHwSelect_u8 = 0;
unsigned char CURV_TYPE=1; // 0 = Logarithamic , 1 = linear .
struct Stree_light_Lat Lat;
struct Stree_light_Log Log;
struct Stree_light_UTC UTC;
long sunriseTime, sunsetTime;

float Latitude, longitude;
float Dimvalue; // SAN
float utcOffset;
//
//unsigned char sun_StartHour;
//unsigned char sun_StartMin;
//unsigned char sun_StopHour;
//unsigned char sun_StopMin;
//unsigned char sun_Flag;

BYTE_VAL error_condition, error_condition1, test_result, day_sch, day_sch_R;

unsigned char chk_interlock;
unsigned char photo_cell_toggel;
unsigned char photo_cell_toggel_counter;
volatile unsigned char photo_cell_timer;
volatile unsigned char photo_cell_toggel_timer;

volatile unsigned char check_counter=0;
unsigned char lamp_lock = 0;
volatile unsigned int check_current_counter = 0;

volatile unsigned int check_current_firsttime_counter = 0;
unsigned char check_cycling_current_counter = 0;
unsigned char cycling_lamp_fault = 0;
unsigned char detect_lamp_current = 0;
float lamp_current;
unsigned char lamp_on_first = 0;
unsigned char lamp_current_low = 0;
unsigned char check_half_current_counter = 0;
unsigned char half_current_counter = 0;
unsigned char lamp_off_to_on = 0;
volatile unsigned int lamp_off_on_timer = 0; // SAN
unsigned char low_current_counter = 0;
unsigned char detect_lamp_current_first_time_on = 0;
extern ALLTYPES pulseCounter;
volatile unsigned int  Dimming_start_timer = 0;
unsigned char low_voltage_generate = 0;
extern unsigned char temp_slc_event, temp_slc_event1;

unsigned int Vol_hi;
unsigned int Vol_low;
unsigned int Curr_Steady_Time;
unsigned int Per_Val_Current;
unsigned int Lamp_Fault_Time;
unsigned int Lamp_off_on_Time;
unsigned int Lamp_faulty_retrieve_Count;

float Per_Val;

volatile unsigned char energy_time = 0;
unsigned char energy_meter_ok = 0;
float current_creep_limit = 0.0;
unsigned int Lamp_lock_condition = 0;

unsigned char set_do_flag = 0;
extern unsigned char Start_dim_flag;

unsigned char Master_Sch_Match = 0; // Master sch 15/04/11	// V6.1.10
//unsigned char Photo_feedback=0;						// POH					//SAN

unsigned char ballast_type = 0;
unsigned char adaptive_light_dimming = 0;

volatile unsigned int duty_cycle;
unsigned char old_dim_val = 100;
volatile unsigned char dim_inc_timer = 0;
unsigned char dim_inc_val = 5; // 5 sec default.
unsigned int dim_applay_time = 900; // 15 min default.  900
unsigned char default_dali = 0, dim_cycle;
unsigned int desir_lamp_lumen = 40000;
unsigned char lumen_tollarence = 1;

unsigned int analog_input_scaling_high_Value = 0;
unsigned int analog_input_scaling_low_Value = 0;


unsigned char Motion_pre_val = 1;
unsigned char Motion_countr = 0;
unsigned char Motion_detected = 0;
unsigned int motion_dimming_timer = 0;
unsigned int motion_intersection_dimming_timer = 0;

unsigned int Motion_dimming_time = 0;
unsigned char Motion_dimming_percentage = 0;
unsigned char motion_dimming_Broadcast_send = 0;
unsigned char motion_dimming_send = 0;
unsigned char Motion_detected_broadcast = 0;
unsigned char Motion_normal_dimming_percentage = 0;
unsigned char Motion_group_id = 0;
unsigned char Motion_pulse_rate = 0;
volatile unsigned char motion_broadcast_timer = 0;
unsigned char Motion_half_sec = 0;
unsigned char Motion_Continiue = 0;
volatile unsigned char Motion_detect_sec = 0;
unsigned char Motion_Detect_Timeout = 0;
unsigned char Motion_Broadcast_Timeout = 0;
unsigned char Motion_Sensor_Type = 0;
unsigned char Motion_continue_timer = 0;
unsigned char Motion_Received_broadcast = 0;
unsigned char Motion_Received_broadcast_counter = 0;
unsigned char Motion_Rebroadcast_timeout = 59;
unsigned char Motion_intersection_detected = 0;
volatile unsigned char Relay_weld_timer = 0;

unsigned char run_Mix_Mode_sch = 0;
unsigned char Civil_Twilight = 0;
unsigned char Schedule_offset = 0;
volatile unsigned char day_burner_timer = 0; //Day burner ember logic SAN
extern unsigned char dimm_cmd_rec;
extern unsigned char  dali_delay;
unsigned char Photocell_steady_timeout_Val = 18; // hour
unsigned char photo_cell_toggel_counter_Val = 10; // no of count 
unsigned char photo_cell_toggel_timer_Val = 18; // hour
unsigned char Photo_cell_Frequency = 30; // 30 pulse per sec.
unsigned int Photocell_unsteady_timeout_Val = 5; //180  // up to this sec photocell not steady.
unsigned char RTC_faulty_detected = 1; // initialy RTC OK
unsigned char previous_mode = 0;
unsigned char RTC_New_fault_logic_Enable = 1;
unsigned int RTC_drift = 0;
extern BYTE_VAL GPS_error_condition;
#define NEW_LAMP_FAULT_LOGIC

unsigned char _DimmingTypeApply = 0;
unsigned char DimmerDriverSelectionProcess = DIM_DRIVER_SELECT_AO;
unsigned char DimDriverStatus = DIM_DRIVER_STATUS_AO_SET;
unsigned char DimmerDriver_SELECTION = 0;
void AppDimmerAutoDetectInit(void);
unsigned char detectDimmerDriver(void);
unsigned char sendDaliCommand_ToCheck_DimmerDriver(void);
void DimDriverMuxSelect(unsigned char);

#ifdef NEW_LAMP_FAULT_LOGIC

float KW_Reference = 0.0;
float current_Threshold = 0.3;
float KW_Threshold = 70.0;
float KW_average = 0.0;
unsigned char Lock_cyclic_check = 0;
volatile unsigned int Lock_cyclic_Counter = 0;
unsigned char one_sec_cyclic = 0;
unsigned char KW_average_counter = 0;
unsigned char test_Dim = 0;

#elif defined(OLD_LAMP_FAULT_LOGIC)

#else
#error "Please define LAMP_FAULT_LOGIC_TYPE"
#endif

#if defined(NEW_LAMP_FAULT_LOGIC) && defined(OLD_LAMP_FAULT_LOGIC)
#error "More than one LAMP fault type can not define"
#endif
//+++++++++++++++++RS485 And DI2+++++++++++++++++++++++++

unsigned char RS485_0_Or_DI2_1 = 0;

/**********************************************************/
/*
        Function: Operate Lamp ON/OFF control by Scheduling 
                          when in Auto mode.
                          Returns 0 
				
        Modified By : Samir Malvi
        Date of Modification : 19/10/2010 
 */

/**********************************************************/
void CheckSchedule(void)
    {
    unsigned char n;
    unsigned char scF[20];

    //	for(n=0;n<10;n++)
    //	{
    //		scF[n] = 0;
    //		if(sSch[Date.Week][n].bSchFlag)			// V6.1.10
    //		{
    //			if((Time.Hour >= sSch[Date.Week][n].cStartHour) && (Time.Hour <= sSch[Date.Week][n].cStopHour)) //10:00 11:00 //1 within sch  // V6.1.10
    //				scF[n] = 1;
    //			if((Time.Hour == sSch[Date.Week][n].cStartHour) && (Time.Min < sSch[Date.Week][n].cStartMin))									// V6.1.10
    //				scF[n] = 0;
    //			if((Time.Hour == sSch[Date.Week][n].cStopHour) && (Time.Min >= sSch[Date.Week][n].cStopMin))									// V6.1.10
    //				scF[n] = 0;
    //			if(((Time.Hour == sSch[Date.Week][n].cStopHour) && (sSch[Date.Week][n].cStopHour == 23)) && ((Time.Min == sSch[Date.Week][n].cStopMin) && (sSch[Date.Week][n].cStopMin == 59)))		// V6.1.10
    //				scF[n] = 1;
    //		}
    //	}

    for (n = 0; n < 10; n++)
        {
        scF[n] = 0;
        if (sSch[0][n].bSchFlag) // V6.1.10
            {
            if ((Time.Hour >= sSch[0][n].cStartHour) && (Time.Hour <= sSch[0][n].cStopHour)) //10:00 11:00 //1 within sch  // V6.1.10
                scF[n] = 1;
            if ((Time.Hour == sSch[0][n].cStartHour) && (Time.Min < sSch[0][n].cStartMin)) // V6.1.10
                scF[n] = 0;
            if ((Time.Hour == sSch[0][n].cStopHour) && (Time.Min >= sSch[0][n].cStopMin)) // V6.1.10
                scF[n] = 0;
            if (((Time.Hour == sSch[0][n].cStopHour) && (sSch[0][n].cStopHour == 23)) && ((Time.Min == sSch[0][n].cStopMin) && (sSch[0][n].cStopMin == 59))) // V6.1.10
                scF[n] = 1;
            }
        }

    if ((scF[0] == 1) || (scF[1] == 1) || (scF[2] == 1) || (scF[3] == 1) || (scF[4] == 1) || (scF[5] == 1) || (scF[6] == 1) || (scF[7] == 1) || (scF[8] == 1) || (scF[9] == 1))
        {
        //		 Set_Do(1);
        if (adaptive_light_dimming == 2)
            {
            Mix_MOD_Motion_Based_dimming();
            }
        else
            {
            Set_Do(1);
            }
        }
    else
        {
        Set_Do(0);
        }
    }

void Check_MASTER_Schedule(void) // Master sch 15/04/11
    {
    unsigned char n;
    unsigned char scF[20];

    //	for(n=0;n<10;n++)
    //	{
    //		scF[n] = 0;
    //		if(Master_sSch[Date.Week][n].bSchFlag)
    //		{
    //			if((Time.Hour >= Master_sSch[Date.Week][n].cStartHour) && (Time.Hour <= Master_sSch[Date.Week][n].cStopHour)) //10:00 11:00 //1 within sch		// V6.1.10
    //				scF[n] = 1;
    //			if((Time.Hour == Master_sSch[Date.Week][n].cStartHour) && (Time.Min < Master_sSch[Date.Week][n].cStartMin))					// V6.1.10
    //				scF[n] = 0;
    //			if((Time.Hour == Master_sSch[Date.Week][n].cStopHour) && (Time.Min >= Master_sSch[Date.Week][n].cStopMin))					// V6.1.10
    //				scF[n] = 0;
    //			if(((Time.Hour == Master_sSch[Date.Week][n].cStopHour) && (Master_sSch[Date.Week][n].cStopHour == 23)) && ((Time.Min == Master_sSch[Date.Week][n].cStopMin) && (Master_sSch[Date.Week][n].cStopMin == 59)))    // V6.1.10
    //				scF[n] = 1;
    //		}
    //	}

    for (n = 0; n < 10; n++)
        {
        scF[n] = 0;
        if (Master_sSch[0][n].bSchFlag)
            {
            if ((Time.Hour >= Master_sSch[0][n].cStartHour) && (Time.Hour <= Master_sSch[0][n].cStopHour)) //10:00 11:00 //1 within sch		// V6.1.10
                scF[n] = 1;
            if ((Time.Hour == Master_sSch[0][n].cStartHour) && (Time.Min < Master_sSch[0][n].cStartMin)) // V6.1.10
                scF[n] = 0;
            if ((Time.Hour == Master_sSch[0][n].cStopHour) && (Time.Min >= Master_sSch[0][n].cStopMin)) // V6.1.10
                scF[n] = 0;
            if (((Time.Hour == Master_sSch[0][n].cStopHour) && (Master_sSch[0][n].cStopHour == 23)) && ((Time.Min == Master_sSch[0][n].cStopMin) && (Master_sSch[0][n].cStopMin == 59))) // V6.1.10
                scF[n] = 1;
            }
        }

    /////////////////////////// for normal lamp dimming schedule ///////////////////////
    if (RTC_faulty_detected == 1)
        {
        if (ballast_type == 0) // normal magnetic ballast.
            {
            if ((scF[0] == 1) || (scF[1] == 1) || (scF[2] == 1) || (scF[3] == 1) || (scF[4] == 1) || (scF[5] == 1) || (scF[6] == 1) || (scF[7] == 1) || (scF[8] == 1) || (scF[9] == 1))
                {
                Set_Do(0);
                Master_Sch_Match = 1; // V6.1.10
                }
            else
                {
                Master_Sch_Match = 0; // V6.1.10
                }
            }
            /////////////////////////// for electronic ballast lamp dimming schedule ///////////////////////
        else if (ballast_type == 1) // dimming based on electronic ballast
            {
            if (scF[0] == 1)
                {
                dimming_applied(Master_sSch[0][0].dimvalue);
                }
            else if (scF[1] == 1)
                {
                dimming_applied(Master_sSch[0][1].dimvalue);
                }
            else if (scF[2] == 1)
                {
                dimming_applied(Master_sSch[0][2].dimvalue);
                }
            else if (scF[3] == 1)
                {
                dimming_applied(Master_sSch[0][3].dimvalue);
                }
            else if (scF[4] == 1)
                {
                dimming_applied(Master_sSch[0][4].dimvalue);
                }
            else if (scF[5] == 1)
                {
                dimming_applied(Master_sSch[0][5].dimvalue);
                }
            else if (scF[6] == 1)
                {
                dimming_applied(Master_sSch[0][6].dimvalue);
                }
            else if (scF[7] == 1)
                {
                dimming_applied(Master_sSch[0][7].dimvalue);
                }
            else if (scF[8] == 1)
                {
                dimming_applied(Master_sSch[0][8].dimvalue);
                }
            else if (scF[9] == 1)
                {
                dimming_applied(Master_sSch[0][9].dimvalue);
                }
            else if ((scF[0] == 0)&&(scF[1] == 0)&&(scF[2] == 0)&&(scF[3] == 0)&&(scF[4] == 0)&&(scF[5] == 0)&&(scF[6] == 0)&&(scF[7] == 0)&&(scF[8] == 0)&&(scF[9] == 0))
                {
                dimming_applied(0);
                }
            }
        else
            {
            }
        }
    else
        {
        dimming_applied(0);
        }
    }

unsigned char CheckSchedule_sun(void) // check astro schedule
    {
    unsigned char n;
    unsigned char scF[20];

    for (n = 0; n < 2; n++)
        {
        scF[n] = 0;
        if (Astro_sSch[n].bSchFlag)
            {
            if ((Time.Hour >= Astro_sSch[n].cStartHour) && (Time.Hour <= Astro_sSch[n].cStopHour)) //10:00 11:00 //1 within sch
                scF[n] = 1;
            if ((Time.Hour == Astro_sSch[n].cStartHour) && (Time.Min < Astro_sSch[n].cStartMin))
                scF[n] = 0;
            if ((Time.Hour == Astro_sSch[n].cStopHour) && (Time.Min > Astro_sSch[n].cStopMin))
                scF[n] = 0;
            }
        }

    if ((scF[0] == 1) || (scF[1] == 1))
        {
        return 1;
        }
    else
        {
        return 0;
        }
    //  if((scF[0] == 1) || (scF[1] == 1))
    //  {
    //    //		 Set_Do(1);
    //    if(adaptive_light_dimming == 2)    // if dimming mode is motion based dimming then do lamp off/on according to motion sensor input.
    //    {
    //      Mix_MOD_Motion_Based_dimming();
    //    }
    //    else
    //    {
    //      Set_Do(1);
    //    }
    //  }
    //  else
    //  {
    //    Set_Do(0);           // if schedule not match then off lamp.
    //  }
    }

/*************************************************************************
Function Name: CheckSchedule_DLH Day light harvesting
input: none.
Output: none.
Discription: This function is use for checking Astro based schedule with day light harvesting.
 *************************************************************************/
void CheckSchedule_DLH(void)
    {
    unsigned char n;
    unsigned char scF[20];

    for (n = 0; n < 2; n++)
        {
        scF[n] = 0;
        if (DLH_sSch[n].bSchFlag)
            {
            if ((Time.Hour >= DLH_sSch[n].cStartHour) && (Time.Hour <= DLH_sSch[n].cStopHour)) // if RTC hour is in between start and stop hour then run schedule.
                scF[n] = 1;
            if ((Time.Hour == DLH_sSch[n].cStartHour) && (Time.Min < DLH_sSch[n].cStartMin)) // if RTC hour match to start hour and RTC minute is less then start minute then stop schedule.  								
                scF[n] = 0;
            if ((Time.Hour == DLH_sSch[n].cStopHour) && (Time.Min > DLH_sSch[n].cStopMin)) // if RTC hour match to stop hour and RTC minute is greater then stop minute then stop schedule.  								
                scF[n] = 0;
            }
        }
    if ((scF[0] == 1) || (scF[1] == 1))
        {
        //		 Set_Do(1);
        if (adaptive_light_dimming == 2) // if dimming mode is motion based dimming then do lamp off/on according to motion sensor input.
            {
            Mix_MOD_Motion_Based_dimming();
            }
        else
            {
            Set_Do(1);
            }
        }
    else
        {
        Set_Do(0); // if schedule not match then off lamp.
        }
    }

/*************************************************************************
Function Name: Set_Do
input: lamp action 0 = off, 1 = On.
Output: none.
Discription: This function is use turn lamp on or off.
 *************************************************************************/


void Set_Do(unsigned char i)
    {
    unsigned char j = 0;
    if (i == 0)
        {
        chk_interlock = 0;
        lamp_on_first = 0;
        if (lamp_off_to_on == 0) // check for on to off transection and make 60 sec delay between off to on.
            {
            lamp_off_to_on = 1;
            lamp_off_on_timer = 0;
            Relay_weld_timer = 0;
            DO_1 = 0;
            DO_2 = 0;
            if (energy_meter_ok == 1) // energy meter faulty so display lamp off when command fire for off
                {
                error_condition.bits.b0 = 0;
                }
            dimming_applied(100);
            set_do_flag = 0;
            Start_dim_flag = 0;
            old_dim_val = 100;
            old_dim_val = 100 - old_dim_val;

            //			if(idimmer_en == 1)
            //			{
            ////				idimmer_dimming_applied(old_dim_val);
            //				send_idimmer_Val_frame = 1;
            //				idimmer_dimval = old_dim_val;
            //			}
            // #if (defined(DALI_SUPPORT))
            if (DaliHwSelect_u8)
                {
                }
                //   #else
            else
                {
                duty_cycle = (PWM_CONSTANT * (old_dim_val / 100.0));
                OC2R = duty_cycle; // Initialize Compare Register1 with 0x0026 	 
                OC2RS = duty_cycle; // Initialize Secondary Compare Register1 with 0x0026
                Dimvalue = (duty_cycle * 100.0 / PWM_CONSTANT);
                Dimvalue = 100 - Dimvalue;
                }
            //  #endif
            check_day_burnar_flag = 1; //Ember Day burner logic SAN
            day_burner_timer = 0;
            Save_BH_kWh();

            }
        }
    else
        {
        if (lamp_lock == 0)
            {
            if ((lamp_off_to_on == 1) && (lamp_off_on_timer < Lamp_off_on_Time)) // do not on lamp if time is less then 60 sec.
                {

                }
            else
                {
                lamp_off_to_on = 0;
                chk_interlock = 1;
                if (lamp_on_first == 0)
                    {
                    detect_lamp_current_first_time_on = 0;
                    check_current_firsttime_counter = 0;
                    lamp_on_first = 1;
                    lamp_current_low = 0;
                    check_current_counter = 0;
                    check_half_current_counter = 0;
                    DO_1 = 0;
                    DO_2 = 1;

                    Dimming_start_timer = 0; // SAN
                    Start_dim_flag = 0; // SAN
                    set_do_flag = 1; // SAN
                    if (energy_meter_ok == 1) // energy meter faulty so display lamp ON when command fire for ON
                        {
                        error_condition.bits.b0 = 1;
                        }
                    //PDO = 1;
                    //					dimming_applied(0);

                    check_day_burnar_flag = 1; // Ember Day burner logic SAN
                    day_burner_timer = 0;
                    //					if(idimmer_en == 1)
                    //					{
                    ////						idimmer_dimming_applied(old_dim_val);
                    //						send_idimmer_Val_frame = 1;
                    //						idimmer_dimval = old_dim_val;
                    //					}
                    // #if (defined(DALI_SUPPORT))
                    if (DaliHwSelect_u8)
                        {


                        if (!default_dali)
                            {
                            for (j = 0; j < 35; j++)Delay(60060); // 55 msec
                            default_dali = 1;

                            Delay(60060); // 55 msec
                            unicast_shortaddress();
                            make_default();
                            Delay(60060); // 55 msec
                            Delay(60060); // 55 msec	
                            for(j=0;j<24;j++)Delay(60060);     // 55 msec
                            for(j=0;j<24;j++)Delay(60060);     // 55 msec
                            set_Linear_Logarithmic_dimming(CURV_TYPE);
                            Delay(60060); 
                            dimming_applied(0); // Set dimming percentage to full bright.
                            }
                        //for(j=0;j<24;j++)Delay(60060);     // 55 msec
                        //for(j=0;j<24;j++)Delay(60060);     // 55 msec
                        //set_Linear_Logarithmic_dimming(_DimmingCurveType);
                        //Delay(60060); 
                        dimming_applied(0); // Set dimming percentage to full bright.
                        //DALIE = 1;
                        }
                        // #else
                    else
                        {
                        dimming_applied(0); // Set dimming percentage to full bright.
                        old_dim_val = 0;
                        old_dim_val = 100 - old_dim_val;
                        duty_cycle = (PWM_CONSTANT * (old_dim_val / 100.0));
                        OC2R = duty_cycle; // Initialize Compare Register1 with 0x0026 	 
                        OC2RS = duty_cycle; // Initialize Secondary Compare Register1 with 0x0026
                        Dimvalue = (duty_cycle * 100.0 / PWM_CONSTANT);
                        Dimvalue = 100 - Dimvalue;
                        // #endif			
                        }
                    }
                }
            }
        }
    }
//void set_Linear_Logarithmic_dimming(unsigned char Curv_type)
//{
//	///////////DTR//////////////////////////////
////	forward    = (0xA3 << 8) |  0x00; 
//	forward    = (0xA3 << 8) |  Curv_type; 
//	f_dalitx =  1;
//	if (f_dalitx)                                      
//	{ 
//	      f_dalitx =  0;                                // clear DALI send flag 
//	      f_dalirx =  0;                                // clear DALI receive (answer) flag 
//	      DALI_Send();                                  // DALI send data to slave(s) 
//	} 
//	Delay(60060);     // 55 msec
//	Delay(60060);     // 55 msec
//
//	forward    = (0xC1 << 8) |  0x06; 
//	f_dalitx =  1;
//	 if (f_dalitx)                                      
//	 { 
//	      f_dalitx =  0;                                // clear DALI send flag 
//	      f_dalirx =  0;                                // clear DALI receive (answer) flag 
//	      DALI_Send();                                  // DALI send data to slave(s) 
//	 } 
//	
//	Delay(60060);     // 55 msec
//	Delay(60060);     // 55 msec
//	
//	////////////////
//		forward    = (0xFF << 8) |  0xE3; 
//		f_dalitx =  1;
//		 if (f_dalitx)                                      
//		 { 
//		      f_dalitx =  0;                                // clear DALI send flag 
//		      f_dalirx =  0;                                // clear DALI receive (answer) flag 
//		      DALI_Send();                                  // DALI send data to slave(s) 
//		 } 
//		
//		
//		Delay(60060);     // 55 msec
//		Delay(60060);     // 55 msec
//	/////////////////
//	forward    = (0xFF << 8) |  0xE3; 
//		f_dalitx =  1;
//		 if (f_dalitx)                                      
//		 { 
//		      f_dalitx =  0;                                // clear DALI send flag 
//		      f_dalirx =  0;                                // clear DALI receive (answer) flag 
//		      DALI_Send();                                  // DALI send data to slave(s) 
//		 } 
//		
//		Delay(60060);     // 55 msec
//		Delay(60060);     // 55 msec
//}

void InitTimer1()
    {
    /* ensure Timer 1 is in reset state */
    T1CON = 0x0020;

    /* reset Timer 1 interrupt flag */
    IFS0bits.T1IF = 0;

    /* set Timer1 interrupt priority level to 4 */
    // IPC0bits.T1IP = 7;

    /* enable Timer 1 interrupt */
    IEC0bits.T1IE = 1;

    /* set Timer 1 period register */

    //PR1 = 0xB71B;		//this is for with PLL enable
    //PR1 = 0x00F9; // 1ms timer.
    PR1 = 0x04E1; // 1ms timer.

    /* enable Timer 1 and start the count */
    T1CONbits.TON = 1;
    }

void InitTimer4() // SAN
    {
    /* ensure Timer 1 is in reset state */
    T4CON = 0x0020;

    /* reset Timer 1 interrupt flag */
    IFS1bits.T4IF = 0;

    /* set Timer1 interrupt priority level to 4 */
    IPC6bits.T4IP = 4;

    /* enable Timer 1 interrupt */
    IEC1bits.T4IE = 1;


    /* set Timer 4 period register */
    PR4 = 0x3F79;
    /* enable Timer 4 and start the count */
    T4CONbits.TON = 1;

    }

void InitTimer3(void)
    {

    T3CON = 0x0020;

    /* reset Timer 1 interrupt flag */
    IFS0bits.T3IF = 0;

    /* set Timer1 interrupt priority level to 4 */
    //  IPC2bits.T3IP = 7;

    /* enable Timer 1 interrupt */
    IEC0bits.T3IE = 1;

    /* set Timer 1 period register */

    //PR3 = 0x006A; //this is for with PLL enable  62.5 msec..
    PR3 = 0x0068; //this is for with PLL enable  62.5 msec.. //Workimg
    //PR3 = 0x001A; 
    //PR3 = 0x0027; //this is for with PLL enable  62.5 msec..

    //PR3 = 0x04e3;		//this is for with PLL enable  62.5 msec..

    /* enable Timer 1 and start the count */
    T3CONbits.TON = 1;
    }

void check_logic(void)
    {
    unsigned char temp = 0;
    if ((error_condition.bits.b2 == 0) || (Lamp_lock_condition == 0))
        {
        if (adaptive_light_dimming != 1)
            {
            if (automode == 0) // manual mode
                {
                if (Set_Do_On == 1)
                    {
                    if (adaptive_light_dimming == 2)
                        {
                        Mix_MOD_Motion_Based_dimming();
                        }
                    else
                        {
                        Set_Do(1);
                        }
                    }
                else
                    {
                    Set_Do(0);
                    }
                }
            else if (automode == 2) //schedual control mode.	
                {
                if (RTC_faulty_detected == 1)
                    {
                    CheckSchedule(); // check schedule if match thne make lamp On.
                    }
                else
                    {
                    previous_mode = automode; // store mode value before chage.
                    WriteByte_EEPROM(EE_PREV_MODE, previous_mode);
                    //  halCommonSetToken(TOKEN_previous_mode, &previous_mode);   // store mode in to NVM.
                    automode = 1;
                    //  halCommonSetToken(TOKEN_automode, &automode);
                    WriteByte_EEPROM(EE_LOGICMODE, automode);
                    }
                }
            else if (automode == 1) //photo control mode.
                {
                if (Photo_Cell_Ok == 1)
                    {
                    if (Photo_feedback == 0) // reverse from hardware.
                        {
                        //						Set_Do(1);
                        if (adaptive_light_dimming == 2)
                            {
                            Mix_MOD_Motion_Based_dimming();
                            }
                        else
                            {
                            Set_Do(1);
                            }
                        }
                    else
                        {
                        Set_Do(0);
                        }
                    }
                else
                    {
                    if ((RTC_faulty_detected == 1) || (RTC_New_fault_logic_Enable != 1)) // if RTC OK then change mode to astro from photocell.
                        {
                        automode = 4;
                        WriteByte_EEPROM(EE_LOGICMODE, automode);
                        //  halCommonSetToken(TOKEN_automode, &automode);
                        previous_mode = automode; // store mode value before chage.
                        //  halCommonSetToken(TOKEN_previous_mode, &previous_mode);
                        WriteByte_EEPROM(EE_PREV_MODE, previous_mode);
                        //				send_get_mode_replay_auto = 1;
                        GetSCHTimeFrom_LAT_LOG();
                        EEWriteAstroSch();
                        }
                    else
                        {
                        automode = 8;
                        WriteByte_EEPROM(EE_LOGICMODE, automode);
                        //   halCommonSetToken(TOKEN_automode, &automode);
                        }
                    }
                }
            else if (automode == 3) //timer mode.
                {
                if ((RTC_faulty_detected == 1) || (RTC_New_fault_logic_Enable == 0))
                    {
                    temp = CheckSchedule_sun(); //  check Astro schedule if match then make Lamp On.
                    if (temp == 1)
                        {
                        if (adaptive_light_dimming == 2) // if dimming mode is motion based dimming then do lamp off/on according to motion sensor input.
                            {
                            Mix_MOD_Motion_Based_dimming();
                            }
                        else
                            {
                            Set_Do(1);
                            }
                        }
                    else
                        {
                        Set_Do(0); // if schedule not match then off lamp.
                        }
                    }
                else
                    {
                    previous_mode = automode; // store mode value before chage.
                    WriteByte_EEPROM(EE_PREV_MODE, previous_mode); // store mode in to NVM.
                    automode = 1;
                    WriteByte_EEPROM(EE_LOGICMODE, automode);
                    }
                }
            else if (automode == 4) //timer mode with photo control over ride.
                {
                if (RTC_New_fault_logic_Enable == 1)
                    {
                    if ((RTC_faulty_detected == 0) && (Photo_Cell_Ok == 0))
                        {
                        previous_mode = automode; // store mode value before chage.
                        WriteByte_EEPROM(EE_PREV_MODE, previous_mode); // store mode in to NVM.
                        automode = 8;
                        WriteByte_EEPROM(EE_LOGICMODE, automode);

                        }
                    else
                        {
                        temp = CheckSchedule_sun(); //  check Astro schedule if match then make Lamp On.

                        if (((temp == 1) && (RTC_faulty_detected == 1)) || (Cloudy_Flag == 1) || ((Photo_feedback == 0) && (Photo_Cell_Ok == 1)))
                            {
                            if (adaptive_light_dimming == 2) // if dimming mode is motion based dimming then check motion first.
                                {
                                Mix_MOD_Motion_Based_dimming();
                                }
                            else
                                {
                                Set_Do(1);
                                }
                            }
                        else
                            {
                            Set_Do(0);
                            }
                        }
                    }
                else
                    {
                    temp = CheckSchedule_sun(); //  check Astro schedule if match then make Lamp On.
                    if ((temp == 1) || (Cloudy_Flag == 1))
                        {
                        if (adaptive_light_dimming == 2) // if dimming mode is motion based dimming then check motion first.
                            {
                            Mix_MOD_Motion_Based_dimming();
                            }
                        else
                            {
                            Set_Do(1);
                            }
                        }
                    else
                        {
                        Set_Do(0);
                        }
                    }

                }
            else if (automode == 5) // civil twilight mode
                {
                if ((RTC_faulty_detected == 1) || (RTC_New_fault_logic_Enable == 0))
                    {
                    temp = CheckSchedule_sun(); //  check Astro schedule if match then make Lamp On.
                    if (temp == 1)
                        {
                        //		 Set_Do(1);
                        if (adaptive_light_dimming == 2) // if dimming mode is motion based dimming then do lamp off/on according to motion sensor input.
                            {
                            Mix_MOD_Motion_Based_dimming();
                            }
                        else
                            {
                            Set_Do(1);
                            }
                        }
                    else
                        {
                        Set_Do(0); // if schedule not match then off lamp.
                        }
                    }
                else
                    {
                    previous_mode = automode; // store mode value before chage.
                    WriteByte_EEPROM(EE_PREV_MODE, previous_mode); // store mode in to NVM.
                    automode = 1;
                    WriteByte_EEPROM(EE_LOGICMODE, automode);
                    }
                }
            else if (automode == 6) // civil twilight mode
                {
                if (RTC_New_fault_logic_Enable == 1)
                    {
                    if ((RTC_faulty_detected == 0) && (Photo_Cell_Ok == 0))
                        {
                        previous_mode = automode; // store mode value before chage.
                        WriteByte_EEPROM(EE_PREV_MODE, previous_mode); // store mode in to NVM.
                        automode = 8;
                        WriteByte_EEPROM(EE_LOGICMODE, automode);

                        }
                    else
                        {
                        temp = CheckSchedule_sun(); //  check Astro schedule if match then make Lamp On.

                        if (((temp == 1) && (RTC_faulty_detected == 1)) || (Cloudy_Flag == 1) || ((Photo_feedback == 0) && (Photo_Cell_Ok == 1)))
                            {
                            if (adaptive_light_dimming == 2) // if dimming mode is motion based dimming then check motion first.
                                {
                                Mix_MOD_Motion_Based_dimming();
                                }
                            else
                                {
                                Set_Do(1);
                                }
                            }
                        else
                            {
                            Set_Do(0);
                            }
                        }
                    }
                else
                    {
                    temp = CheckSchedule_sun(); //  check Astro schedule if match then make Lamp On.
                    if ((temp == 1) || (Cloudy_Flag == 1))
                        {
                        if (adaptive_light_dimming == 2) // if dimming mode is motion based dimming then check motion first.
                            {
                            Mix_MOD_Motion_Based_dimming();
                            }
                        else
                            {
                            Set_Do(1);
                            }
                        }
                    else
                        {
                        Set_Do(0);
                        }
                    }
                }
            else if (automode == 8) // fail safe mode.
                {
                if (adaptive_light_dimming == 2) // if dimming mode is motion based dimming then check motion first.
                    {
                    Mix_MOD_Motion_Based_dimming();
                    }
                else
                    {
                    Set_Do(1);
                    }
                }
            else
                {
                }
            }
        else
            {
            if ((RTC_New_fault_logic_Enable == 1) && (RTC_faulty_detected == 0))
                {
                if (automode != 8)
                    {
                    //previous_mode = automode;  // store mode value before chage.
                    //halCommonSetToken(TOKEN_previous_mode, &previous_mode);   // store mode in to NVM.
                    automode = 8;
                    WriteByte_EEPROM(EE_LOGICMODE, automode);
                    }
                Set_Do(1);
                }
            else
                {
                CheckSchedule_DLH(); //  check Day light Harvesting schedule if match then make Lamp On.
                //Set_Do(1); //for testing
                }
            //CheckSchedule_DLH(); //  check Day light Harvesting schedule if match then make Lamp On.
            }
        }
    }

void GetSCHTimeFrom_LAT_LOG(void)
    {
    //	Sunset_delay = 30;
    sunriseTime = Calculate(SUNRISE);
    sunriseTime = sunriseTime - Sunrise_delay;
    sunsetTime = Calculate(SUNSET);
    sunsetTime = sunsetTime + Sunset_delay;

    Astro_sSch[0].cStartHour = sunsetTime / 3600;
    Astro_sSch[0].cStartMin = (sunsetTime % 3600) / 60;
    Astro_sSch[0].cStopHour = 23;
    Astro_sSch[0].cStopMin = 59;
    Astro_sSch[0].bSchFlag = 1;

    Astro_sSch[1].cStartHour = 00;
    Astro_sSch[1].cStartMin = 00;
    Astro_sSch[1].cStopHour = sunriseTime / 3600;
    Astro_sSch[1].cStopMin = (sunriseTime % 3600) / 60;
    Astro_sSch[1].bSchFlag = 1;
    //	get_push_lograte();
    }

void Day_light_harves_time(void)
    {
    Day_light_harvesting_start_time = Calculate(SUNSET);
    Day_light_harvesting_start_time = Day_light_harvesting_start_time - Day_light_harvesting_start_offset;

    Day_light_harvesting_stop_time = Calculate(SUNRISE);
    Day_light_harvesting_stop_time = Day_light_harvesting_stop_time + Day_light_harvesting_stop_offset;

    DLH_sSch[0].cStartHour = Day_light_harvesting_start_time / 3600; // V6.1.10		
    DLH_sSch[0].cStartMin = (Day_light_harvesting_start_time % 3600) / 60; // V6.1.10
    DLH_sSch[0].cStopHour = 23; // V6.1.10
    DLH_sSch[0].cStopMin = 59; // V6.1.10
    DLH_sSch[0].bSchFlag = 1; // V6.1.10

    DLH_sSch[1].cStartHour = 00; // V6.1.10
    DLH_sSch[1].cStartMin = 00; // V6.1.10
    DLH_sSch[1].cStopHour = Day_light_harvesting_stop_time / 3600; // V6.1.10
    DLH_sSch[1].cStopMin = (Day_light_harvesting_stop_time % 3600) / 60; // V6.1.10									
    DLH_sSch[1].bSchFlag = 1;
    }

long int Calculate(unsigned char direction)
    {
    /* doy (N) */
    int i = 0;
    double lngHour = 0.0;
    double t = 0.0;
    double M = 0.0;
    double L = 0.0;
    double RA = 0.0;
    double Lquadrant = 0.0;
    double RAquadrant = 0.0;
    double sinDec = 0.0;
    double cosDec = 0.0;
    double cosH = 0.0;
    double H = 0.0;
    double T = 0.0;
    double zenith = 0.0;
    double UT = 0.0;
    int N = 0;
    long temp_hour = 0, temp_min = 0;
    double temp_utcoffset = 0.0; //06/07/09

    if ((automode == 5) || (automode == 6) || (Civil_Twilight == 1)) // civil twilight mode
        {
        zenith = 96000;
        }
    else
        {
        zenith = 90500;
        }
    if ((automode == 3) || (automode == 4))
        {
        zenith = 90500;
        }

    for (i = 1; i < Date.Month; i++)
        {
        if (i == 1 || i == 3 || i == 5 || i == 7 || i == 8 || i == 10 || i == 12)
            {
            N = N + 31;
            }
        else if (i == 4 || i == 6 || i == 9 || i == 11)
            {
            N = N + 30;
            }

        if (i == 2)
            {
            if (Date.Year % 4 == 0)
                N = N + 29;
            else
                N = N + 28;

            }
        }

    N = N + Date.Date;
    lngHour = (double) longitude / 15.0; /* appr. time (t) */

    if (direction == SUNRISE)
        t = N + ((6.0 - lngHour) / 24.0);
    else
        t = N + ((18.0 - lngHour) / 24.0);

    M = (0.9856 * t) - 3.289; /* mean anomaly (M) */
    L = M + (1.916 * sin(Deg2Rad(M))) + (0.020 * sin(Deg2Rad(2 * M))) + 282.634; /* true longitude (L) */
    L = FixValue(L, 0, 360); /* right asc (RA) */
    RA = Rad2Deg(atan(0.91764 * tan(Deg2Rad(L))));
    RA = FixValue(RA, 0, 360);
    Lquadrant = (floor(L / 90.0)) * 90.0; /* adjust quadrant of RA */
    RAquadrant = (floor(RA / 90.0)) * 90.0;
    RA = RA + (Lquadrant - RAquadrant);
    RA = RA / 15.0;
    sinDec = 0.39782 * sin(Deg2Rad(L)); /* sin cos DEC (sinDec / cosDec) */
    cosDec = cos(asin(sinDec));
    cosH = (cos(Deg2Rad((double) zenith / 1000.0f)) - (sinDec * sin(Deg2Rad((double) Latitude)))) / (cosDec * cos(Deg2Rad((double) Latitude))); /* local hour angle (cosH) */

    if (direction == SUNRISE)
        H = 360.0 - Rad2Deg(acos(cosH));
    else
        H = Rad2Deg(acos(cosH));


    H = H / 15.0;

    /* time (T) */
    T = H + RA - (0.06571 * t) - 6.622;

    UT = T - lngHour;

    //    if(Time_change_dueto_DST == 1)
    //    {
    //		utcOffset = utcOffset + SLC_DST_Time_Zone_Diff;    
    //    }

    temp_utcoffset = utcOffset * 100;

    temp_hour = temp_utcoffset / 100;
    temp_min = ((unsigned long) temp_utcoffset % 100);

    if (Time_change_dueto_DST == 1)
        {
        temp_min = temp_min + (int) SLC_DST_Time_Zone_Diff;
        }

    temp_utcoffset = (temp_hour + ((float) temp_min / 60));


    UT += (double) temp_utcoffset; // local UTC offset

    UT = FixValue(UT, 0, 24);

    UT = (UT * 3600); // convert computed time from hour to sec.
    return (UT); // Convert to seconds
    }

double Deg2Rad(double angle)
    {
    return (PI * angle / 180.0);
    }

double Rad2Deg(double angle)
    {
    return (180.0 * angle / PI);
    }

double FixValue(double value, double min, double max)
    {
    while (value < min)
        value += (max - min);

    while (value >= max)
        value -= (max - min);

    return value;
    }

double DegreesToAngle(double degrees, double minutes, double seconds)
    {
    if (degrees < 0)
        return ((double) (degrees - (minutes / 60.0) - (seconds / 3600.0)));
    else
        return ((double) (degrees + (minutes / 60.0) + (seconds / 3600.0)));
    }


/*************************************************************************
Function Name: check_interlock
input: None.
Output: None.
Discription: this function is use for checking all voltage and current related
             interlocks.
 *************************************************************************/


#ifdef NEW_LAMP_FAULT_LOGIC

void check_interlock(void)
    {
    //  unsigned char Display[100];
    float Per_Val;

    Per_Val = (float) Per_Val_Current / 100;
    //  if((energy_time == 0) && (Commissioning_flag == 0))
    // {
    if ((energy_time == 0)&&(Commissioning_flag == 0))
        {

        if ((chk_interlock == 1) && (Dimvalue == 0)) // V6.1.12		//check interlock after do_on command and dimm value at 0 persent.
            {
            if (Lamp_Balast_fault_Remove_Time != 0)
                {
                Fault_Remove_Logic();
                }
            if (detect_lamp_current == 0) // check if steady current not saved
                {
                if (check_current_firsttime_counter > Curr_Steady_Time)
                    {
                    check_current_firsttime_counter = 0;
                    lamp_current = irdoub;
                    //         lamp_current = w1;        // this will store steady state watt.
                    KW_Threshold = (float) w1;
                    //          halCommonSetToken(TOKEN_KW_Threshold,&KW_Threshold);			//EEPROMSAN

                    pulseCounter.float_data = KW_Threshold;
                    WriteByte_EEPROM(EE_KW_Threshold + 0, pulseCounter.byte[3]);
                    WriteByte_EEPROM(EE_KW_Threshold + 1, pulseCounter.byte[2]);
                    WriteByte_EEPROM(EE_KW_Threshold + 2, pulseCounter.byte[1]);
                    WriteByte_EEPROM(EE_KW_Threshold + 3, pulseCounter.byte[0]);
                    pulseCounter.float_data = lamp_current;

                    if (lamp_current > 0.05)
                        {
                        current_creep_limit = lamp_current * 0.1;
                        if (current_creep_limit < 0.05)
                            {
                            current_creep_limit = 0.05; // if current creep limit less then 0.03 make it 0.03	
                            }
                        else if (current_creep_limit > 0.1)
                            {
                            current_creep_limit = 0.1; // if current creep limit more then 0.1 make it 0.1
                            }
                        else
                            {

                            }
                        WriteByte_EEPROM(EE_LAMP_STADY_CURRENT + 0, pulseCounter.byte[3]);
                        WriteByte_EEPROM(EE_LAMP_STADY_CURRENT + 1, pulseCounter.byte[2]);
                        WriteByte_EEPROM(EE_LAMP_STADY_CURRENT + 2, pulseCounter.byte[1]);
                        WriteByte_EEPROM(EE_LAMP_STADY_CURRENT + 3, pulseCounter.byte[0]);
                        //halCommonSetToken(TOKEN_lamp_current,&lamp_current); //EEPROMSAN

                        detect_lamp_current = 1;
                        WriteByte_EEPROM(EE_LAMP_STADY_CURRENT_SET, detect_lamp_current);
                        //halCommonSetToken(TOKEN_detect_lamp_current,&detect_lamp_current); //EEPROMSAN
                        KW_Reference = 0;
                        cycling_lamp_fault = 0;
                        KW_average = 0;
                        }
                    }
                }
            else if ((detect_lamp_current_first_time_on == 0) && (check_current_firsttime_counter > Curr_Steady_Time))
                {
                detect_lamp_current_first_time_on = 1;
                check_current_firsttime_counter = 0;
                check_half_current_counter = 0;
                KW_Reference = 0;
                cycling_lamp_fault = 0;
                KW_average = 0;
                KW_average_counter = 0;
                Lock_cyclic_check = 0;
                error_condition.bits.b6 = 0;
                check_current_counter = 0;
                }
            else
                {
                }

            // ballast fault trip checking logic
            //      if((irdoub < current_Threshold) && (w1 < KW_Threshold) && (detect_lamp_current == 1) && (detect_lamp_current_first_time_on == 1))
            if ((irdoub < current_creep_limit) && (detect_lamp_current == 1) && (detect_lamp_current_first_time_on == 1))
                {
                if (check_current_counter >= Lamp_Fault_Time)
                    {
                    check_current_counter = 0;
                    if (error_condition1.bits.b0 == 0)
                        {
                        if (Lamp_lock_condition == 1)
                            {
                            Set_Do(0);
                            }
                        }
                    low_current_counter++;
                    //sprintf(Display,"\nlow_current_counter = %u",low_current_counter);		
                    //emberAfGuaranteedPrint("\r\n%p", Display);
                    if (low_current_counter >= Lamp_faulty_retrieve_Count) // check current low condition 3 times then lock the lamp.
                        {
                        error_condition1.bits.b0 = 1; // ballast fail trip
                        Ballast_Fault_Occured = 1;
                        low_current_counter = 0;
                        check_current_counter = 0;
                        if (Lamp_lock_condition == 1)
                            {
                            lamp_lock = 1;
                            Set_Do(0);
                            }
                        }
                    }
                }
                // ballast fault trip checking logic

                // Lamp fault trip checking logic
                //      else if((irdoub >= current_Threshold) && (w1 < KW_Threshold) && (detect_lamp_current == 1) && (detect_lamp_current_first_time_on == 1))
            else if ((w1 < (KW_Threshold * (1.0 - Per_Val))) &&
                     (detect_lamp_current == 1) &&
                     (detect_lamp_current_first_time_on == 1)
                     )
                {
                if (check_current_counter >= Lamp_Fault_Time)
                    {
                    check_current_counter = 0;
                    if (error_condition.bits.b3 == 0)
                        {
                        if (Lamp_lock_condition == 1)
                            {
                            Set_Do(0);
                            }
                        }
                    low_current_counter++;
                    //sprintf(Display,"\nlow_current_counter = %u",low_current_counter);		
                    //emberAfGuaranteedPrint("\r\n%p", Display);
                    if (low_current_counter >= Lamp_faulty_retrieve_Count) // check current low condition 3 times then lock the lamp.
                        {
                        error_condition.bits.b3 = 1;
                        Lamp_Fault_Occured = 1; // ballast fail trip
                        low_current_counter = 0;
                        check_current_counter = 0;
                        if (Lamp_lock_condition == 1)
                            {
                            lamp_lock = 1;
                            Set_Do(0);
                            }
                        }
                    }
                }
                // Lamp fault trip checking logic
            else
                {
                check_current_counter = 0;
                if ((detect_lamp_current == 1) && (detect_lamp_current_first_time_on == 1))
                    {
                    low_current_counter = 0;
                    }
                }

            // cyclic lamp fault
            if ((detect_lamp_current == 1) && (detect_lamp_current_first_time_on == 1) && (Lock_cyclic_check == 0))
                {
                if (one_sec_cyclic == 1)
                    {
                    one_sec_cyclic = 0;
                    KW_average = KW_average + w1;
                    KW_average_counter++;
                    if (KW_average_counter >= 10)
                        {

                        KW_average = KW_average / KW_average_counter;
                        if (KW_Reference < KW_average)
                            {
                            KW_Reference = KW_average;
                            }
                        KW_average_counter = 0;
                        KW_average = 0;
                        }
                    //sprintf(Display,"KW_Reference = %f,KW_average = %f,w1 = %f",KW_Reference,KW_average,w1);		
                    //emberAfGuaranteedPrint("\r\n%p", Display);
                    if (w1 < (KW_Reference * (1 - 0.50))) //|| (w1 > (KW_Reference * (1+0.50)))
                        {
                        KW_Reference = 0;
                        KW_average_counter = 0;
                        KW_average = 0;
                        cycling_lamp_fault++;
                        //sprintf(Display,"\r\ncycling_lamp_fault = %d",cycling_lamp_fault);		
                        //emberAfGuaranteedPrint("\r\n%p", Display);
                        if (cycling_lamp_fault >= 10)
                            {
                            cycling_lamp_fault = 0;
                            error_condition.bits.b6 = 1;
                            if (Lamp_lock_condition == 1)
                                {
                                lamp_lock = 1;
                                Set_Do(0);
                                }
                            }
                        else
                            {
                            //error_condition.bits.b6 = 0;
                            }
                        Lock_cyclic_check = 1;
                        Lock_cyclic_Counter = 0;
                        }
                    }
                }
            // cyclic lamp fault
            }
        }

    if ((vrdoub < Vol_low) || (vrdoub > Vol_hi)) //Slc low voltage trip generate
        {
        if (check_counter >= 5)
            {
            error_condition.bits.b2 = 1;
            if (Lamp_lock_condition == 1)
                {
                Set_Do(0);
                }
            }
        }
    else
        {
        error_condition.bits.b2 = 0;
        check_counter = 0;
        }

    if (irdoub < current_creep_limit)
        {
        error_condition.bits.b0 = 0; //SLC Lamp ON feedback.
        if (lamp_off_to_on == 1)
            {
            error_condition1.bits.b5 = 0; // relay weld condition remove.	
            Relay_weld_timer = 0;
            }
        }
    else
        {
        error_condition.bits.b0 = 1; //SLC Lamp ON feedback.
        if ((lamp_off_to_on == 1) && (Relay_weld_timer > 3))
            {
            error_condition1.bits.b5 = 1; // relay weld condition generated			
            }
        }

    }

#elif defined(OLD_LAMP_FAULT_LOGIC)

void check_interlock(void)
    {

    if ((energy_time == 0) && (Commissioning_flag == 0)) // if Energy meter not working properly or SLC in commissioning then not check interlock.
        {
        if ((chk_interlock == 1) && (Dimvalue == 0)) // if dimming percentage set to non 0 value or lamp off then not check interlock.
            {
            if (Lamp_Balast_fault_Remove_Time != 0)
                {
                Fault_Remove_Logic();
                }

            if (detect_lamp_current == 0) // check if steady current not saved
                {
                if (check_current_firsttime_counter > Curr_Steady_Time) // wait for current become stady.
                    {
                    check_current_firsttime_counter = 0;
                    lamp_current = irdoub; // store currnet in to current variable.
                    pulseCounter.float_data = lamp_current;

                    if (lamp_current > 0.03) // if proper current detect then generate creep limit value for fault detection.
                        {
                        current_creep_limit = lamp_current * 0.1;
                        if (current_creep_limit < 0.03)
                            {
                            current_creep_limit = 0.03; // if current creep limit less then 0.03 make it 0.03	
                            }
                        else if (current_creep_limit > 0.1)
                            {
                            current_creep_limit = 0.1; // if current creep limit more then 0.1 make it 0.1
                            }
                        else
                            {

                            }
                        //halCommonSetToken(TOKEN_lamp_current,&lamp_current);
                        detect_lamp_current = 1; // set flag for lamp stady current stored.
                        //halCommonSetToken(TOKEN_detect_lamp_current,&detect_lamp_current);
                        }
                    }
                }
            else if ((detect_lamp_current_first_time_on == 0) &&
                     (check_current_firsttime_counter > Curr_Steady_Time)) // if steady current already stored then wait up to steady current time when every lamp on so current became steady.
                {
                detect_lamp_current_first_time_on = 1;
                check_current_firsttime_counter = 0;
                check_half_current_counter = 0;
                }
            else
                {
                }


            if ((irdoub < current_creep_limit) && (detect_lamp_current == 1)
                && (detect_lamp_current_first_time_on == 1)) // after steady current time over then check lamp current
                {
                if (check_current_counter >= Lamp_Fault_Time) // check if current remain law up to fault time or not
                    {
                    if (error_condition1.bits.b0 == 0) // check if lamp fault declered or not.
                        {
                        if (Lamp_lock_condition == 1) // if lamp lock required then do lamp off.
                            {
                            Set_Do(0);
                            }
                        }
                    low_current_counter++; // increase fault counter if greater then retrieve count then declare fault condtion.
                    if (low_current_counter >= Lamp_faulty_retrieve_Count) // check current low condition 3 times then lock the lamp.
                        {
                        error_condition1.bits.b0 = 1; // balast fail trip
                        Ballast_Fault_Occured = 1;
                        low_current_counter = 0;
                        check_current_counter = 0;
                        if (Lamp_lock_condition == 1) // if lamp lock required then off lamp.
                            {
                            lamp_lock = 1;
                            Set_Do(0);
                            }
                        }
                    }

                if (lamp_current_low == 0) // if perviously lamp current high and become low thne increase cyclic counter.
                    {
                    cycling_lamp_fault++;
                    lamp_current_low = 1;
                    }
                }
            else
                {
                if ((check_cycling_current_counter >= 1) && (detect_lamp_current == 1) && (detect_lamp_current_first_time_on == 1)) // check cyclic current at every sec after current steady time over.
                    {
                    check_current_counter = 0;
                    low_current_counter = 0;
                    check_cycling_current_counter = 0;
                    Per_Val = (float) Per_Val_Current / 100;
                    if ((irdoub < (lamp_current * (1.0 - Per_Val))) || (irdoub >= (lamp_current * (1.0 + Per_Val)))) // check if curret less then 20% or greter then 20%
                        {
                        if (lamp_current_low == 0) // if current become law then increase counter
                            {
                            cycling_lamp_fault++;
                            lamp_current_low = 1;
                            }
                        if (check_half_current_counter >= Lamp_Fault_Time) // SLC low current trip.
                            {
                            half_current_counter++;
                            if (half_current_counter >= Lamp_faulty_retrieve_Count) // if current become law up to retrive count then declear Lamp fault.
                                {
                                error_condition.bits.b3 = 1;
                                Lamp_Fault_Occured = 1;
                                half_current_counter = 0;
                                check_half_current_counter = 0;
                                if (Lamp_lock_condition == 1) // if lamp lock condtion required then make lamp off
                                    {
                                    lamp_lock = 1;
                                    Set_Do(0);
                                    }
                                }
                            if (error_condition.bits.b3 == 0)
                                {
                                if (Lamp_lock_condition == 1) // if lamp lock condtion required then make lamp off
                                    {
                                    Set_Do(0);
                                    }
                                }
                            }
                        }
                    else
                        {
                        if (lamp_current_low == 1)
                            {
                            cycling_lamp_fault++;
                            lamp_current_low = 0;
                            }
                        check_half_current_counter = 0;
                        half_current_counter = 0;
                        }

                    if (cycling_lamp_fault >= 10) // if current become high to law or low to high 10 times then declere lamp cycling.
                        {
                        error_condition.bits.b6 = 1;
                        if (Lamp_lock_condition == 1) // if lamp lock condtion required then make lamp off
                            {
                            lamp_lock = 1;
                            Set_Do(0);
                            }
                        }
                    else
                        {
                        error_condition.bits.b6 = 0;
                        }
                    }
                }
            }
        }

    if ((vrdoub < Vol_low) || (vrdoub > Vol_hi)) //Slc low/high voltage trip generate
        {
        if (check_counter >= 5)
            {
            error_condition.bits.b2 = 1;
            if (Lamp_lock_condition == 1) // if lamp lock condtion required then make lamp off
                {
                Set_Do(0);
                }
            }
        }
    else
        {
        error_condition.bits.b2 = 0;
        check_counter = 0;
        }

    if (irdoub < current_creep_limit)
        {
        error_condition.bits.b0 = 0; //SLC Lamp ON feedback.
        if (lamp_off_to_on == 1)
            {
            error_condition1.bits.b5 = 0; // relay weld condition remove.	
            Relay_weld_timer = 0;
            }
        }
    else
        {
        error_condition.bits.b0 = 1; //SLC Lamp ON feedback.
        if ((lamp_off_to_on == 1) && (Relay_weld_timer > 3))
            {
            error_condition1.bits.b5 = 1; // relay weld condition generated			
            }
        }

    }

#else
#error "Please define LAMP_FAULT_LOGIC_TYPE"
#endif

/*************************************************************************
Function Name: check_photocell_interlock
input: None.
Output: None.
Discription: this function is use for checking all Photocell related
             interlocks like photocell faults, photocell ossilating.
 *************************************************************************/

void check_photocell_interlock(void)
    {
    if (Photo_Cell_Ok == 1) // if photocell ok then only generate event of photocell status.
        {
        if (Photo_feedback == 1)
            {
            error_condition.bits.b5 = 1;
            }
        else
            {
            error_condition.bits.b5 = 0;
            }

        if (Commissioning_flag == 0)
            {

            if (Photo_feedback == 1)
                {
                if (photo_cell_toggel == 1)
                    {
                    photo_cell_timer = 0;
                    photo_cell_toggel = 0;
                    photo_cell_toggel_counter++;
                    }
                else
                    {
                    photo_cell_toggel = 0;
                    }
                }
            else
                {
                if (photo_cell_toggel == 0)
                    {
                    photo_cell_timer = 0;
                    photo_cell_toggel = 1;
                    photo_cell_toggel_counter++;
                    }
                else
                    {
                    photo_cell_toggel = 1;
                    }
                }

            if (photo_cell_timer >= Photocell_steady_timeout_Val)
                {
                Photo_Cell_Ok = 0;
                error_condition.bits.b1 = 1;
                }
            else
                {
                error_condition.bits.b1 = 0;
                }
            if (Photocell_unsteady_timeout_Val > 5) //osscilation trip genrerate after valid dcu gets
                {
                if ((photo_cell_toggel_counter >= photo_cell_toggel_counter_Val) &&
                    (photo_cell_toggel_timer <= photo_cell_toggel_timer_Val))
                    {
                    photo_cell_toggel_timer = 0;
                    photo_cell_toggel_counter = 0;
                    error_condition.bits.b4 = 1; // photo_cell oscillating error.
                    Photo_Cell_Ok = 0;
                    }
                else
                    {
                    if (photo_cell_toggel_timer > photo_cell_toggel_timer_Val)
                        {
                        photo_cell_toggel_timer = 0;
                        photo_cell_toggel_counter = 0;
                        }
                    }
                }
            else
                {
                photo_cell_toggel_timer = 0;
                photo_cell_toggel_counter = 0;

                }
            }
        }
    }

void dimming_applied(unsigned char dim_val)
    {
    idimmer_en = 0;
    if (idimmer_en == 0)
        {
        //#if defined(DALI_SUPPORT)
        if (DaliHwSelect_u8)
            {
            // DALIE = 0;
            dim_val = 100 - dim_val;
            if (dim_inc_val > 0)
                {
                if ((old_dim_val < dim_val) && (dim_inc_timer >= dim_inc_val))
                    {
                    dim_inc_timer = 0;

                    dim_cycle = (254 * (old_dim_val / 100.0)); //SAN
                    DALI_DIMMING_CMD(dim_cycle); //SAN

                    old_dim_val++;
                    if (old_dim_val > 100)
                        {
                        old_dim_val = 100;
                        }
                    }
                else if ((old_dim_val > dim_val) && (dim_inc_timer >= dim_inc_val))
                    {
                    dim_inc_timer = 0;

                    dim_cycle = (254 * (old_dim_val / 100.0)); //SAN
                    DALI_DIMMING_CMD(dim_cycle); //SAN

                    old_dim_val--;
                    if (old_dim_val < 1)
                        {
                        old_dim_val = 1;
                        }
                    }
                else if ((old_dim_val == dim_val) && (dim_inc_timer >= dim_inc_val))
                    {
                    dim_inc_timer = 0;

                    dim_cycle = (254 * (old_dim_val / 100.0)); //SAN
                    DALI_DIMMING_CMD(dim_cycle); //SAN
                    }
                else
                    {
                    }
                }
            else
                {
                if (dimm_cmd_rec == 1)
                    {
                    dimm_cmd_rec = 0;
                    old_dim_val = dim_val + 1;
                    }

                if ((old_dim_val != dim_val)&&(dali_delay >= 3)&&(set_do_flag == 1)) //SAN
                    {
                    dali_delay = 0;
                    old_dim_val = dim_val;
                    dim_cycle = (254 * (dim_val / 100.0)); // PR2_VAL for 32Mhz
                    DALI_DIMMING_CMD(dim_cycle);
                    }
                }
            Dimvalue = dim_val; // load current dim value in to Data string resistor.
            Dimvalue = 100 - Dimvalue; // reverse value for disply proper.
            //  DALIE = 1;
            }
        else
            {
            dim_val = 100 - dim_val;
            if (dim_inc_val > 0)
                {
                if ((old_dim_val < dim_val) && (dim_inc_timer >= dim_inc_val))
                    {
                    dim_inc_timer = 0;
                    duty_cycle = (PWM_CONSTANT * (old_dim_val / 100.0));
                    old_dim_val++;
                    if (old_dim_val > 100)
                        {
                        old_dim_val = 100;
                        }
                    OC2R = duty_cycle; // Initialize Compare Register1 with 0x0026 	 
                    OC2RS = duty_cycle; // Initialize Secondary Compare Register1 with 0x0026	
                    }
                else if ((old_dim_val > dim_val) && (dim_inc_timer >= dim_inc_val))
                    {
                    dim_inc_timer = 0;
                    duty_cycle = (PWM_CONSTANT * (old_dim_val / 100.0));
                    old_dim_val--;
                    if (old_dim_val < 1)
                        {
                        old_dim_val = 1;
                        }
                    OC2R = duty_cycle; // Initialize Compare Register1 with 0x0026 	 
                    OC2RS = duty_cycle; // Initialize Secondary Compare Register1 with 0x0026		
                    }
                else if ((old_dim_val == dim_val) && (dim_inc_timer >= dim_inc_val))
                    {
                    dim_inc_timer = 0;
                    duty_cycle = (PWM_CONSTANT * (old_dim_val / 100.0));
                    OC2R = duty_cycle; // Initialize Compare Register1 with 0x0026 	 
                    OC2RS = duty_cycle; // Initialize Secondary Compare Register1 with 0x0026		
                    }
                else
                    {
                    }

                }
            else
                {
                duty_cycle = (PWM_CONSTANT * (dim_val / 100.0));
                OC2R = duty_cycle; // Initialize Compare Register1 with 0x0026 	 
                OC2RS = duty_cycle; // Initialize Secondary Compare Register1 with 0x0026	
                }
            Dimvalue = dim_val;
            Dimvalue = 100 - Dimvalue;

            }
        //#endif
        }
    else
        {
        dim_val = 100 - dim_val;
        idimmer_dimming_applied(dim_val);
        Dimvalue = 100 - dim_val;
        }
    test_Dim = dim_val;
    }

void adaptive_dimming(void)
    {
    unsigned char temp = 0;
    unsigned long int temp1 = 0;

    idimmer_en = 0;
    if (idimmer_en == 0)
        {
        if (dim_inc_timer >= dim_inc_val)
            {
            dim_inc_timer = 0;
            if (lamp_lumen <= (desir_lamp_lumen))
                {

                temp1 = desir_lamp_lumen - lamp_lumen;
                temp1 = temp1 * 100;
                temp = (unsigned char) (temp1 / desir_lamp_lumen);
                old_dim_val = temp;
                duty_cycle = (PWM_CONSTANT * (old_dim_val / 100.0));
                OC2R = duty_cycle; // Initialize Compare Register1 with 0x0026 	 
                OC2RS = duty_cycle; // Initialize Secondary Compare Register1 with 0x0026
                }
            else
                {
                old_dim_val = 0;
                duty_cycle = (PWM_CONSTANT * (old_dim_val / 100.0));
                OC2R = duty_cycle; // Initialize Compare Register1 with 0x0026 	 
                OC2RS = duty_cycle; // Initialize Secondary Compare Register1 with 0x0026
                }
            Dimvalue = (duty_cycle * 100.0 / PWM_CONSTANT);
            Dimvalue = 100 - Dimvalue;
            }
        }
    else
        {
        if (dim_inc_timer >= dim_inc_val)
            {
            dim_inc_timer = 0;
            if (lamp_lumen <= (desir_lamp_lumen))
                {

                temp1 = desir_lamp_lumen - lamp_lumen;
                temp1 = temp1 * 100;
                temp = (unsigned char) (temp1 / desir_lamp_lumen);
                old_dim_val = temp;

                }
            else
                {
                old_dim_val = 0;
                }
            }
        idimmer_dimming_applied(old_dim_val);
        Dimvalue = 100 - old_dim_val;
        }
    }

void Mix_MOD_Motion_Based_dimming(void)
    {
    if ((MOTION_DI1 == 1) && (Motion_pre_val == 0))
        {
        Motion_pre_val = 1;

        if (Motion_Sensor_Type == 1)
            {
            Motion_countr++;
            if (Motion_detected == 1)
                {
                if (Motion_continue_timer > Motion_Detect_Timeout)
                    {
                    Motion_Continiue = 1;
                    motion_dimming_timer = 0;
                    Motion_detect_sec = 0;
                    }
                }
            }
        }
    else if ((MOTION_DI1 == 0) && (Motion_pre_val == 1))
        {

        Motion_pre_val = 0;

        if (Motion_Sensor_Type == 0)
            {
            Motion_countr++;
            if (Motion_detected == 1)
                {
                if (Motion_continue_timer > Motion_Detect_Timeout)
                    {
                    Motion_Continiue = 1;
                    motion_dimming_timer = 0;
                    Motion_detect_sec = 0;
                    }
                }
            }
        }
    else if ((MOTION_DI1 == 0) && (Motion_pre_val == 0) && (Motion_Sensor_Type == 0))
        {
        if (Motion_half_sec == 1)
            {
            Motion_countr++;
            Motion_half_sec = 0;
            //WriteDebugMsg("\n Motion Timer reset");
            if (Motion_detected == 1)
                {
                //				Motion_continue_timer++;
                if (Motion_continue_timer > Motion_Detect_Timeout)
                    {
                    Motion_Continiue = 1;
                    motion_dimming_timer = 0;
                    Motion_detect_sec = 0;
                    }
                }
            }
        }
    else if ((MOTION_DI1 == 1) && (Motion_pre_val == 1) && (Motion_Sensor_Type == 1))
        {
        if (Motion_half_sec == 1)
            {
            Motion_countr++;
            Motion_half_sec = 0;
            //WriteDebugMsg("\n Motion Timer reset");
            if (Motion_detected == 1)
                {
                //				Motion_continue_timer++;
                if (Motion_continue_timer > Motion_Detect_Timeout)
                    {
                    Motion_Continiue = 1;
                    motion_dimming_timer = 0;
                    Motion_detect_sec = 0;
                    }
                }
            }
        }


    if (Motion_detected == 1)
        {
        if (motion_dimming_timer < Motion_dimming_time)
            {
            Set_Do(1);
            check_dimming_protection();
            if (lamp_lock == 0)
                {
                if ((Start_dim_flag == 1) && (set_do_flag == 1))
                    {
                    dimming_applied(Motion_dimming_percentage);
                    }
                }
            if ((motion_dimming_timer == 0) && (Motion_detect_sec == 0) && (motion_dimming_Broadcast_send == 0) && (Motion_Received_broadcast == 0))
                {
                motion_dimming_Broadcast_send = 1;
                motion_dimming_send = 1;
                motion_broadcast_timer = 0;
                //WriteDebugMsg("\n Motion broadcast send");
                //				Motion_broadcast_Receive_counter++;
                motion_dimming_timer = 0;
                Motion_detect_sec = 0;
                }
            else if ((Motion_Continiue == 1) && (motion_broadcast_timer >= Motion_Rebroadcast_timeout))
                {
                motion_dimming_Broadcast_send = 0;
                motion_broadcast_timer = 0;
                Motion_Continiue = 0;
                motion_dimming_timer = 0;
                Motion_detect_sec = 0;
                //WriteDebugMsg("\n Motion broadcast re-send");
                //				Motion_broadcast_Receive_counter++;						
                }
            else
                {
                }
            }
        else
            {
            Motion_detected = 0;
            motion_dimming_Broadcast_send = 0;
            //WriteDebugMsg("\n Motion dimming timer over");
            }
        }
    else if (Motion_detected_broadcast == 1)
        {
        if (motion_dimming_timer < Motion_dimming_time)
            {
            //			dimming_applied(Motion_dimming_percentage);							
            Set_Do(1);
            check_dimming_protection();
            if (lamp_lock == 0)
                {
                if ((Start_dim_flag == 1) && (set_do_flag == 1))
                    {
                    dimming_applied(Motion_dimming_percentage);
                    }
                }
            }
        else
            {
            Motion_detected_broadcast = 0;
            }
        }
    else if (Motion_intersection_detected == 1)
        {
        if (motion_intersection_dimming_timer < Motion_dimming_time)
            {
            //			dimming_applied(Motion_dimming_percentage);							
            Set_Do(1);
            check_dimming_protection();
            if (lamp_lock == 0)
                {
                if ((Start_dim_flag == 1) && (set_do_flag == 1))
                    {
                    dimming_applied(Motion_dimming_percentage);
                    }
                }
            }
        else
            {
            Motion_intersection_detected = 0;
            }
        }
    else
        {
        //		dimming_applied(Motion_normal_dimming_percentage);		

        if (Motion_normal_dimming_percentage != 100)
            {
            Set_Do(1);
            check_dimming_protection();
            if (lamp_lock == 0)
                {
                if ((Start_dim_flag == 1) && (set_do_flag == 1))
                    {
                    dimming_applied(Motion_normal_dimming_percentage);
                    }
                }
            }
        else
            {
            Set_Do(0);
            }
        }
    }
////////////////// idimmer ///////////

void idimmer_dimming_applied(unsigned char dim_val)
    {

    if (dim_val != current_dimval)
        {
        if ((Send_dimming_cmd >= 5) && (dimming_cmd_ack == 0) && (dimming_cmd_cnt <= 2))
            {
            dimming_cmd_cnt++;
            if (dimming_cmd_cnt >= 3) dimming_cmd_cnt = 4;
            idimmer_dimval = dim_val;
            send_idimmer_Val_frame = 1;
            Send_dimming_cmd = 0;
            }
        else if (dimming_cmd_ack == 1)
            {
            dimming_cmd_cnt = 0;
            //current_dimval = dim_val;
            //idimmer_dimval = dim_val;
            dimming_cmd_ack = 0;
            idimmer_trip_erase = 0;
            }
        else if ((dimming_cmd_cnt == 4)&&(dimming_cmd_ack == 0))
            {
            dimming_cmd_cnt = 0;
            current_dimval = dim_val;
            dimming_cmd_ack = 0;
            Send_dimming_cmd = 0;
            }
        }

    if (Send_dimming_cmd >= 600)
        {
        idimmer_dimval = dim_val;
        send_idimmer_Val_frame = 1;
        Send_dimming_cmd = 0;
        dimming_cmd_cnt = 0;
        current_dimval = dim_val;
        dimming_cmd_ack = 0;
        }

    }

///////////////// idimmer ///////////	

void Check_day_burning_fault(void)
    {
    unsigned char n;
    unsigned char scF[20];

    for (n = 0; n < 2; n++)
        {
        scF[n] = 0;
        if (Astro_sSch[n].bSchFlag)
            {
            if ((Time.Hour >= Astro_sSch[n].cStartHour) && (Time.Hour <= Astro_sSch[n].cStopHour)) //10:00 11:00 //1 within sch
                scF[n] = 1;
            if ((Time.Hour == Astro_sSch[n].cStartHour) && (Time.Min < Astro_sSch[n].cStartMin))
                scF[n] = 0;
            if ((Time.Hour == Astro_sSch[n].cStopHour) && (Time.Min > Astro_sSch[n].cStopMin))
                scF[n] = 0;
            }
        }

    if ((scF[0] == 1) || (scF[1] == 1)) // night time
        {
        error_condition1.bits.b7 = 0; // day burning fault clear.
        }
    else
        {
        if (error_condition.bits.b0 == 1)
            {
            error_condition1.bits.b7 = 1; // day burning fault detected.
            }
        else
            {
            error_condition1.bits.b7 = 0; // day burning fault clear.
            }
        }
    }

void check_Mix_Mode_schedule(void)
    {
    unsigned char i = 0, j = 0, temp, Calendar_sch_match = 0, n = 0, temp_sch_flag = 0;
    long temp_sunset = 0, temp_sunrise = 0;
    unsigned char Sch_Date, Sch_Month;

    if (Schedule_offset == 1)
        {
        Sch_Date = PDate.Date;
        Sch_Month = PDate.Month;
        Date.Week = week_day(Date.Year, Sch_Date, Sch_Month); // V6.1.10

        if (Date.Week == 0) // sunday
            {
            day_sch_R.Val = 1;
            }
        else if (Date.Week == 1) // monday
            {
            day_sch_R.Val = 64;
            }
        else if (Date.Week == 2) // tuesday
            {
            day_sch_R.Val = 32;
            }
        else if (Date.Week == 3) // wenesday
            {
            day_sch_R.Val = 16;
            }
        else if (Date.Week == 4) // thrusday
            {
            day_sch_R.Val = 8;
            }
        else if (Date.Week == 5) // friday
            {
            day_sch_R.Val = 4;
            }
        else if (Date.Week == 6) // seturday
            {
            day_sch_R.Val = 2;
            }
        else
            {
            }
        }
    else
        {
        Sch_Date = Date.Date;
        Sch_Month = Date.Month;
        }

    run_Mix_Mode_sch = 0;
    Mix_photo_override = 0;

    for (n = 0; n < 9; n++)
        {
        Mix_sSch[n].bSchFlag = 0;
        }

    for (j = 0; j < 5; j++)
        {
        unCompress_Mix_sch(j);
        if (Slc_Mix_Mode_schedule[0].SLC_Mix_En == 1)
            {
            if (Sch_Month == Slc_Mix_Mode_schedule[0].SLC_Mix_Sch_Start_Month)
                {
                if (Sch_Date == Slc_Mix_Mode_schedule[0].SLC_Mix_Sch_Start_Date)
                    {
                    run_Mix_Mode_sch = 1;
                    Calendar_sch_match = 1;
                    Mix_photo_override = Slc_Mix_Mode_schedule[0].SLC_Mix_Sch_Photocell_Override;
                    for (i = 0; i < 9; i++)
                        {
                        if (Slc_Mix_Mode_schedule[0].Slc_Mix_Sch[i].bSchFlag == 1)
                            {
                            Mix_sSch[i].bSchFlag = 1;
                            if ((Slc_Mix_Mode_schedule[0].Slc_Mix_Sch[i].cStartHour == 24) && (Slc_Mix_Mode_schedule[0].Slc_Mix_Sch[i].cStartMin == 24)) // 55
                                {
                                Civil_Twilight = 0;
                                temp_sunset = Calculate(SUNSET);
                                temp_sunset = temp_sunset + (Slc_Mix_Mode_schedule[0].Slc_Mix_Sch[i].cStartOff * 60);
                                Mix_sSch[i].cStartHour = temp_sunset / 3600;
                                Mix_sSch[i].cStartMin = (temp_sunset % 3600) / 60;
                                }
                            else if ((Slc_Mix_Mode_schedule[0].Slc_Mix_Sch[i].cStartHour == 26) && (Slc_Mix_Mode_schedule[0].Slc_Mix_Sch[i].cStartMin == 26)) // 77
                                {
                                Civil_Twilight = 1;
                                temp_sunset = Calculate(SUNSET);
                                temp_sunset = temp_sunset + (Slc_Mix_Mode_schedule[0].Slc_Mix_Sch[i].cStartOff * 60);
                                Mix_sSch[i].cStartHour = temp_sunset / 3600;
                                Mix_sSch[i].cStartMin = (temp_sunset % 3600) / 60;
                                }
                            else
                                {
                                Mix_sSch[i].cStartHour = Slc_Mix_Mode_schedule[0].Slc_Mix_Sch[i].cStartHour;
                                Mix_sSch[i].cStartMin = Slc_Mix_Mode_schedule[0].Slc_Mix_Sch[i].cStartMin;
                                }

                            if ((Slc_Mix_Mode_schedule[0].Slc_Mix_Sch[i].cStopHour == 25) && (Slc_Mix_Mode_schedule[0].Slc_Mix_Sch[i].cStopMin == 25)) // 66
                                {
                                Civil_Twilight = 0;
                                temp_sunrise = Calculate(SUNRISE);
                                temp_sunrise = temp_sunrise - (Slc_Mix_Mode_schedule[0].Slc_Mix_Sch[i].cStopOff * 60);
                                Mix_sSch[i].cStopHour = temp_sunrise / 3600;
                                Mix_sSch[i].cStopMin = (temp_sunrise % 3600) / 60;
                                }
                            else if ((Slc_Mix_Mode_schedule[0].Slc_Mix_Sch[i].cStopHour == 27) && (Slc_Mix_Mode_schedule[0].Slc_Mix_Sch[i].cStopMin == 27)) // 88
                                {
                                Civil_Twilight = 1;
                                temp_sunrise = Calculate(SUNRISE);
                                temp_sunrise = temp_sunrise - (Slc_Mix_Mode_schedule[0].Slc_Mix_Sch[i].cStopOff * 60);
                                Mix_sSch[i].cStopHour = temp_sunrise / 3600;
                                Mix_sSch[i].cStopMin = (temp_sunrise % 3600) / 60;
                                }
                            else
                                {
                                Mix_sSch[i].cStopHour = Slc_Mix_Mode_schedule[0].Slc_Mix_Sch[i].cStopHour;
                                Mix_sSch[i].cStopMin = Slc_Mix_Mode_schedule[0].Slc_Mix_Sch[i].cStopMin;
                                }
                            Mix_sSch[i].dimvalue = Slc_Mix_Mode_schedule[0].Slc_Mix_Sch[i].dimvalue;
                            Mix_sSch[i].Dim_Mode = Slc_Mix_Mode_schedule[0].Slc_Mix_Sch[i].Dim_Mode;
                            }
                        else
                            {
                            Mix_sSch[i].bSchFlag = 0;
                            }
                        }
                    break;
                    }
                }
            }
        }

    temp_sch_flag = 0;
    if (Calendar_sch_match == 0)
        {

        for (j = 5; j < 10; j++)
            {
            unCompress_Mix_sch(j);
            if (Slc_Mix_Mode_schedule[0].SLC_Mix_En == 1)
                {
                if ((Date.Month >= Slc_Mix_Mode_schedule[0].SLC_Mix_Sch_Start_Month) && (Date.Month <= Slc_Mix_Mode_schedule[0].SLC_Mix_Sch_Stop_Month))
                    {
                    //					if((Date.Date >= Slc_Mix_Mode_schedule[0].SLC_Mix_Sch_Start_Date) && (Date.Date <= Slc_Mix_Mode_schedule[0].SLC_Mix_Sch_Stop_Date) || (Slc_Mix_Mode_schedule[0].SLC_Mix_Sch_Start_Month != Slc_Mix_Mode_schedule[0].SLC_Mix_Sch_Stop_Month))

                    //					if((((Date.Date >= Slc_Mix_Mode_schedule[0].SLC_Mix_Sch_Start_Date) &&
                    //						 (Date.Month == Slc_Mix_Mode_schedule[0].SLC_Mix_Sch_Start_Month)) ||
                    //						 ((Date.Month != Slc_Mix_Mode_schedule[0].SLC_Mix_Sch_Start_Month) &&
                    //						 (Date.Month != Slc_Mix_Mode_schedule[0].SLC_Mix_Sch_Stop_Month)) ||
                    //						 ((Date.Date <= Slc_Mix_Mode_schedule[0].SLC_Mix_Sch_Stop_Date) &&
                    //						 (Date.Month == Slc_Mix_Mode_schedule[0].SLC_Mix_Sch_Stop_Month))) &&
                    //						 (Slc_Mix_Mode_schedule[0].SLC_Mix_Sch_Start_Month != Slc_Mix_Mode_schedule[0].SLC_Mix_Sch_Stop_Month)
                    //						||((Date.Date >= Slc_Mix_Mode_schedule[0].SLC_Mix_Sch_Start_Date) && 
                    //						(Date.Date <= Slc_Mix_Mode_schedule[0].SLC_Mix_Sch_Stop_Date) && 
                    //						(Slc_Mix_Mode_schedule[0].SLC_Mix_Sch_Start_Month == Slc_Mix_Mode_schedule[0].SLC_Mix_Sch_Stop_Month)))
                    //					{

                    if (Slc_Mix_Mode_schedule[0].SLC_Mix_Sch_Start_Month != Slc_Mix_Mode_schedule[0].SLC_Mix_Sch_Stop_Month)
                        {
                        if (((Sch_Date >= Slc_Mix_Mode_schedule[0].SLC_Mix_Sch_Start_Date) &&
                             (Sch_Month == Slc_Mix_Mode_schedule[0].SLC_Mix_Sch_Start_Month)) ||
                            ((Sch_Month != Slc_Mix_Mode_schedule[0].SLC_Mix_Sch_Start_Month) &&
                             (Sch_Month != Slc_Mix_Mode_schedule[0].SLC_Mix_Sch_Stop_Month)) ||
                            ((Sch_Date <= Slc_Mix_Mode_schedule[0].SLC_Mix_Sch_Stop_Date) &&
                             (Sch_Month == Slc_Mix_Mode_schedule[0].SLC_Mix_Sch_Stop_Month)))
                            {
                            temp_sch_flag = 1;
                            }
                        else
                            {
                            temp_sch_flag = 0;
                            }
                        }
                    else
                        {
                        if ((Sch_Date >= Slc_Mix_Mode_schedule[0].SLC_Mix_Sch_Start_Date) &&
                            (Sch_Date <= Slc_Mix_Mode_schedule[0].SLC_Mix_Sch_Stop_Date))
                            {
                            temp_sch_flag = 1;
                            }
                        else
                            {
                            temp_sch_flag = 0;
                            }
                        }

                    if (temp_sch_flag == 1)
                        {
                        temp = check_mix_mode_week_day(Slc_Mix_Mode_schedule[0].SLC_Mix_Sch_WeekDay);
                        if (temp == 1)
                            {
                            run_Mix_Mode_sch = 1;
                            Mix_photo_override = Slc_Mix_Mode_schedule[0].SLC_Mix_Sch_Photocell_Override;
                            for (i = 0; i < 9; i++)
                                {
                                if (Slc_Mix_Mode_schedule[0].Slc_Mix_Sch[i].bSchFlag == 1)
                                    {
                                    Mix_sSch[i].bSchFlag = 1;
                                    if ((Slc_Mix_Mode_schedule[0].Slc_Mix_Sch[i].cStartHour == 24) && (Slc_Mix_Mode_schedule[0].Slc_Mix_Sch[i].cStartMin == 24))
                                        {
                                        Civil_Twilight = 0;
                                        temp_sunset = Calculate(SUNSET);
                                        temp_sunset = temp_sunset + (Slc_Mix_Mode_schedule[0].Slc_Mix_Sch[i].cStartOff * 60);
                                        Mix_sSch[i].cStartHour = temp_sunset / 3600;
                                        Mix_sSch[i].cStartMin = (temp_sunset % 3600) / 60;
                                        }
                                    else if ((Slc_Mix_Mode_schedule[0].Slc_Mix_Sch[i].cStartHour == 26) && (Slc_Mix_Mode_schedule[0].Slc_Mix_Sch[i].cStartMin == 26))
                                        {
                                        Civil_Twilight = 1;
                                        temp_sunset = Calculate(SUNSET);
                                        temp_sunset = temp_sunset + (Slc_Mix_Mode_schedule[0].Slc_Mix_Sch[i].cStartOff * 60);
                                        Mix_sSch[i].cStartHour = temp_sunset / 3600;
                                        Mix_sSch[i].cStartMin = (temp_sunset % 3600) / 60;
                                        }
                                    else
                                        {
                                        Mix_sSch[i].cStartHour = Slc_Mix_Mode_schedule[0].Slc_Mix_Sch[i].cStartHour;
                                        Mix_sSch[i].cStartMin = Slc_Mix_Mode_schedule[0].Slc_Mix_Sch[i].cStartMin;
                                        }

                                    if ((Slc_Mix_Mode_schedule[0].Slc_Mix_Sch[i].cStopHour == 25) && (Slc_Mix_Mode_schedule[0].Slc_Mix_Sch[i].cStopMin == 25))
                                        {
                                        Civil_Twilight = 0;
                                        temp_sunrise = Calculate(SUNRISE);
                                        temp_sunrise = temp_sunrise - (Slc_Mix_Mode_schedule[0].Slc_Mix_Sch[i].cStopOff * 60);
                                        Mix_sSch[i].cStopHour = temp_sunrise / 3600;
                                        Mix_sSch[i].cStopMin = (temp_sunrise % 3600) / 60;
                                        }
                                    else if ((Slc_Mix_Mode_schedule[0].Slc_Mix_Sch[i].cStopHour == 27) && (Slc_Mix_Mode_schedule[0].Slc_Mix_Sch[i].cStopMin == 27))
                                        {
                                        Civil_Twilight = 1;
                                        temp_sunrise = Calculate(SUNRISE);
                                        temp_sunrise = temp_sunrise - (Slc_Mix_Mode_schedule[0].Slc_Mix_Sch[i].cStopOff * 60);
                                        Mix_sSch[i].cStopHour = temp_sunrise / 3600;
                                        Mix_sSch[i].cStopMin = (temp_sunrise % 3600) / 60;
                                        }
                                    else
                                        {
                                        Mix_sSch[i].cStopHour = Slc_Mix_Mode_schedule[0].Slc_Mix_Sch[i].cStopHour;
                                        Mix_sSch[i].cStopMin = Slc_Mix_Mode_schedule[0].Slc_Mix_Sch[i].cStopMin;
                                        }
                                    Mix_sSch[i].dimvalue = Slc_Mix_Mode_schedule[0].Slc_Mix_Sch[i].dimvalue;
                                    Mix_sSch[i].Dim_Mode = Slc_Mix_Mode_schedule[0].Slc_Mix_Sch[i].Dim_Mode;
                                    }
                                else
                                    {
                                    Mix_sSch[i].bSchFlag = 0;
                                    }
                                }
                            }
                        break;
                        }
                    //					}
                    }
                }
            }
        }
    }

/*************************************************************************
Function Name: Run_Mix_Mode_Sch
input: None.
Output: None.
Discription: this function is use to run logic from mix-mode matched schedule.
 *************************************************************************/
void Run_Mix_Mode_Sch(void)
    {
    unsigned char n, break_flag = 0;
    unsigned char scF;

    if (run_Mix_Mode_sch == 1)
        {
        for (n = 0; n < 9; n++)
            {
            scF = 0;
            if (Mix_sSch[n].bSchFlag) // V6.1.10
                {
                if ((Time.Hour >= Mix_sSch[n].cStartHour) && (Time.Hour <= Mix_sSch[n].cStopHour))
                    scF = 1;
                if ((Time.Hour == Mix_sSch[n].cStartHour) && (Time.Min < Mix_sSch[n].cStartMin))
                    scF = 0;
                if ((Time.Hour == Mix_sSch[n].cStopHour) && (Time.Min >= Mix_sSch[n].cStopMin))
                    scF = 0;
                if (((Time.Hour == Mix_sSch[n].cStopHour) && (Mix_sSch[n].cStopHour == 23)) && ((Time.Min == Mix_sSch[n].cStopMin) && (Mix_sSch[n].cStopMin == 59)))
                    scF = 1;

                if (scF == 1)
                    {
                    if (Mix_sSch[n].Dim_Mode == 0)
                        {
                        if (Mix_sSch[n].dimvalue == 0)
                            {
                            Set_Do(1);
                            dimming_applied(Mix_sSch[n].dimvalue);
                            }
                        else if (Mix_sSch[n].dimvalue == 100)
                            {
                            Set_Do(0);
                            dimming_applied(Mix_sSch[n].dimvalue);
                            }
                        else if ((Mix_sSch[n].dimvalue > 0) && (Mix_sSch[n].dimvalue < 100))
                            {
                            Set_Do(1);
                            check_dimming_protection();
                            if ((Start_dim_flag == 1) && (set_do_flag == 1))
                                {
                                dimming_applied(Mix_sSch[n].dimvalue);
                                }
                            }
                        else
                            {

                            }
                        }
                    else if (Mix_sSch[n].Dim_Mode == 3) // Motion based logic
                        {
                        //						Set_Do(1);
                        //						//check_dimming_protection();					
                        //						if(lamp_lock == 0)
                        //						{
                        //							if((Start_dim_flag == 1) && (set_do_flag == 1))
                        //							{
                        //								Motion_Based_dimming();
                        //							}
                        //						}	
                        Mix_MOD_Motion_Based_dimming();
                        }
                    else if (Mix_sSch[n].Dim_Mode == 2) // Day lighting Harvesting dimming
                        {
                        Set_Do(1);
                        check_dimming_protection();
                        if (lamp_lock == 0)
                            {
                            if ((Start_dim_flag == 1) && (set_do_flag == 1))
                                {
                                adaptive_dimming();
                                }
                            }

                        }
                    break_flag = 1;
                    break;
                    }
                }
            }
        }

    if (break_flag == 0)
        {
        if (Mix_photo_override == 1)
            {
            if ((Cloudy_Flag == 1) || ((Photo_feedback == 0) && (Photo_Cell_Ok == 1)))
                {
                Set_Do(1);
                dimming_applied(0);
                }
            else
                {
                Set_Do(0);
                } // Photocell override logic
            }
        else
            {
            Set_Do(0);
            }
        }
    }

/*************************************************************************
Function Name: check_Day_Light_Saving
input: None.
Output: None.
Discription: this function is use to check day light saveing rule and if RTC
             time in between rule then shift colck according to timezone offset.
 *************************************************************************/
void check_Day_Light_Saving(void)
    {
    unsigned char DST_On = 0;
    //  unsigned char Display[50];
    int temp;
    char temp_hour, temp_min;

    if (SLC_DST_En == 1) // check if DST enable?
        {

        if (old_year != Date.Year) // if year change then find new date from DST Rule.
            {
            if (SLC_DST_Rule_Enable == 1)
                {
                SLC_DST_R_Start_Date = find_dst_date_from_rule(SLC_DST_Start_Rule, SLC_DST_Start_Month);
                SLC_DST_R_Stop_Date = find_dst_date_from_rule(SLC_DST_Stop_Rule, SLC_DST_Stop_Month);
                }
            else
                {
                SLC_DST_R_Start_Date = SLC_DST_Start_Date;
                SLC_DST_R_Stop_Date = SLC_DST_Stop_Date;
                }
            old_year = Date.Year;
            }

        if (SLC_DST_Start_Month <= SLC_DST_Stop_Month) // first check like start month is always less then stop month.
            {
            if ((Date.Month >= SLC_DST_Start_Month) && (Date.Month <= SLC_DST_Stop_Month)) // if rtc moth is greater then start month and less then stop month then check other field.
                {
                if ((SLC_DST_Start_Month == SLC_DST_Stop_Month) &&
                    (SLC_DST_R_Start_Date > SLC_DST_R_Stop_Date)) // in same moth if configured start date is greater then stop date then wrong rule so no need to check any thing and make DST off.
                    {
                    DST_On = 0; // DST date not configured properly.
                    }
                else
                    {
                    if (SLC_DST_Start_Month != SLC_DST_Stop_Month) // if start month is not same as stop month then check date checking.
                        {
                        if (Date.Month == SLC_DST_Start_Month) // if RTC month is same as start moth and RTC date is same as start date amd RTC hour is greater then start hour then start DST.
                            {
                            if ((Date.Date == SLC_DST_R_Start_Date))
                                {
                                if ((Time.Hour >= SLC_DST_Start_Time))
                                    {
                                    DST_On = 1;
                                    }
                                else
                                    {
                                    DST_On = 0;
                                    }
                                }
                            else if ((Date.Date > SLC_DST_R_Start_Date)) // if RTC date is greater then start date then start DST.
                                {
                                DST_On = 1;
                                }
                            else
                                {
                                DST_On = 0;
                                }

                            }
                        else if (Date.Month == SLC_DST_Stop_Month) // if rtc month is equal to stop month then check other field.
                            {
                            if (Date.Date == SLC_DST_R_Stop_Date) // if RTC date is same as stop date and RTC hour is less then stop hour then start DST.
                                {
                                if (Time.Hour < SLC_DST_Stop_Time)
                                    {
                                    DST_On = 1;
                                    }
                                else
                                    {
                                    DST_On = 0;
                                    }
                                }
                            else if (Date.Date < SLC_DST_R_Stop_Date) // if RTC date is less then stop date then start DST.
                                {
                                DST_On = 1;
                                }
                            else
                                {
                                DST_On = 0;
                                }
                            }
                        else if ((Date.Month > SLC_DST_Start_Month) && (Date.Month < SLC_DST_Stop_Month)) // if RTC month is in between start month and stop month then start DST.
                            {
                            DST_On = 1;
                            }
                        else
                            {
                            DST_On = 0;
                            }
                        }
                    else
                        {

                        if (SLC_DST_R_Start_Date != SLC_DST_R_Stop_Date) // if start month and stop month is same and start date and stop date is same then check other field.
                            {
                            if ((Date.Date == SLC_DST_R_Start_Date)) // if RTC Date is equal to start date and RTC hour is greaterthen start time then start DST.
                                {
                                if ((Time.Hour >= SLC_DST_Start_Time))
                                    {
                                    DST_On = 1;
                                    }
                                else
                                    {
                                    DST_On = 0;
                                    }
                                }
                            else if (Date.Date == SLC_DST_R_Stop_Date) // if start date and stop date is same then check only time.
                                {
                                if (Time.Hour < SLC_DST_Stop_Time) // if RTC time is less then stop time then start DST.
                                    {
                                    DST_On = 1;
                                    }
                                else
                                    {
                                    DST_On = 0;
                                    }
                                }
                            else if ((Date.Date > SLC_DST_R_Start_Date) && (Date.Date < SLC_DST_R_Stop_Date)) // if RTC date is in between start date and stop date then start DST.
                                {
                                DST_On = 1;
                                }
                            else
                                {
                                DST_On = 0;
                                }

                            }
                        else
                            {
                            if ((Time.Hour >= SLC_DST_Start_Time) &&
                                (Time.Hour < SLC_DST_Stop_Time)&&
                                (Date.Date == SLC_DST_R_Start_Date)) // if star and stop date are same and RTC time is in between start and stop time then start DST.
                                {
                                DST_On = 1;
                                }
                            else
                                {
                                DST_On = 0;
                                }
                            }

                        }
                    }
                }
            else
                {
                DST_On = 0; // RTC month not match with Start month and stop month then DST not match.
                }
            }
        else
            {
            ////////////////////////reverse DST checking//////////////////////////////////////////
            if ((Date.Month <= SLC_DST_Stop_Month) || (Date.Month >= SLC_DST_Start_Month)) // if start month is greater then stop month then check reverse condition like if RTC month is in between stop month and start month then DST stop else DST Start.
                {
                if (SLC_DST_Start_Month != SLC_DST_Stop_Month) // check if start month and stop month is not same.
                    {
                    if (Date.Month == SLC_DST_Start_Month)
                        {
                        if ((Date.Date == SLC_DST_R_Start_Date))
                            {
                            if ((Time.Hour >= SLC_DST_Start_Time))
                                {
                                DST_On = 1; // if RTC date and month are same as start date and month and RTC Hour is greater then start hour then start DST.
                                }
                            else
                                {
                                DST_On = 0;
                                }
                            }
                        else if ((Date.Date > SLC_DST_R_Start_Date)) // if date is greater then start date then start DST.
                            {
                            DST_On = 1;
                            }
                        else
                            {
                            DST_On = 0;
                            }

                        }
                    else if (Date.Month == SLC_DST_Stop_Month) // if rtc month is match with stop month and RTC date is mathc with stop date and rtc hour is less then stop hour then start DST.
                        {
                        if (Date.Date == SLC_DST_R_Stop_Date)
                            {
                            if (Time.Hour < SLC_DST_Stop_Time)
                                {
                                DST_On = 1;
                                }
                            else
                                {
                                DST_On = 0;
                                }
                            }
                        else if (Date.Date < SLC_DST_R_Stop_Date) // if RTC date is less then stop date then start DST.
                            {
                            DST_On = 1;
                            }
                        else
                            {
                            DST_On = 0;
                            }
                        }
                    else if ((Date.Month > SLC_DST_Start_Month) || (Date.Month < SLC_DST_Stop_Month)) // if RTC month is in between start and stop month then start DST.
                        {
                        DST_On = 1;
                        }
                    else
                        {
                        DST_On = 0;
                        }
                    }
                else
                    {
                    DST_On = 0;
                    }
                }
            else
                {
                DST_On = 0; // RTC month not match with Start month and stop month then DST not match.
                }
            ////////////////////////////reverse DST checking/////////////////////////////////////
            }

        //    if(DST_On == 1)
        //    {
        //      emberAfCorePrintln("\r\nDST Match");
        //    }
        //    else
        //    {
        //      emberAfCorePrintln("\r\nDST Note Match");
        //    }

        if ((DST_On == 1) && (Time_change_dueto_DST == 0)) // if DST start and time not change then add time zone diff in to local time.
            {
            Time_change_dueto_DST = 1;
            //WriteByte_EEPROM(EE_Time_change_dueto_DST,Time_change_dueto_DST);
            //   halCommonSetToken(TOKEN_Time_change_dueto_DST,&Time_change_dueto_DST);
            //   emberAfCorePrintln("\r\nTime change due to DST");

            if (SLC_DST_Time_Zone_Diff > 0) // if time zone diff is positive then add diff
                {
                temp = (unsigned char) SLC_DST_Time_Zone_Diff; // convert time zone diff in to hour and min.
                temp_hour = temp / 60;
                temp_min = temp % 60;
                Time.Min = Time.Min + temp_min; // add min to RTC min.
                if (Time.Min >= 60) // if min greater then 59 then add hour in to RTC Hour.
                    {
                    Time.Min = Time.Min - 60;
                    Time.Hour = Time.Hour + 1;
                    }
                Time.Hour = Time.Hour + temp_hour;
                if (Time.Hour >= 24) // if RTC hour greater then 23 then increase RTC Date.
                    {
                    Time.Hour = Time.Hour - 24;
                    Increment_Date();
                    }
                RTC_SET_TIME(Time); // Set RTC time.

                Local_Time.Hour = Time.Hour;
                Local_Time.Min = Time.Min;
                Local_Time.Sec = Time.Sec;
                Local_Date.Date = Date.Date;
                Local_Date.Month = Date.Month;
                Local_Date.Year = Date.Year;

                GetSCHTimeFrom_LAT_LOG(); // compute Astro time from new RTC time.
                }
            else // if time zone diff is negative then subtrect diff.
                {
                temp = (signed char) SLC_DST_Time_Zone_Diff; // convert time zone diff in to hour and min.
                temp_hour = temp / 60;
                temp_min = temp % 60;

                Time.Min = Time.Min - temp_min; // subtract min from RTC min.
                if (Time.Min < 0)
                    {
                    Time.Min = Time.Min + 60; // if min lessthen 0 then subtract hour from RTC hour.
                    Time.Hour = Time.Hour - 1;
                    }
                Time.Hour = Time.Hour - temp_hour; // subtract RTC hour.
                if (Time.Hour < 0)
                    {
                    Time.Hour = Time.Hour + 24; // if hour < 0 then decrement date from RTC Date.
                    Decrement_Date();
                    }

                RTC_SET_TIME(Time); // Set RTC new time.

                Local_Time.Hour = Time.Hour;
                Local_Time.Min = Time.Min;
                Local_Time.Sec = Time.Sec;
                Local_Date.Date = Date.Date;
                Local_Date.Month = Date.Month;
                Local_Date.Year = Date.Year;

                GetSCHTimeFrom_LAT_LOG(); // compute Astro time from new RTC time.
                }

            }

        if ((DST_On == 0) && (Time_change_dueto_DST == 1)) // if DST over then roll back RTC time.
            {
            Time_change_dueto_DST = 2;

            //      halCommonSetToken(TOKEN_Time_change_dueto_DST,&Time_change_dueto_DST);
            //      emberAfCorePrintln("\r\nTime Revert back from DST");
            if (SLC_DST_Time_Zone_Diff > 0) // subtract time zone diff from current time.
                {
                temp = (unsigned char) SLC_DST_Time_Zone_Diff;
                temp_hour = temp / 60;
                temp_min = temp % 60;
                Time.Min = Time.Min - (int) temp_min;
                if (Time.Min < 0)
                    {
                    Time.Min = Time.Min + 60;
                    Time.Hour = Time.Hour - 1;
                    }
                Time.Hour = Time.Hour - temp_hour;
                if (Time.Hour < 0)
                    {
                    Time.Hour = Time.Hour + 24;
                    Decrement_Date();
                    }

                RTC_SET_TIME(Time); // Set RTC time.

                Local_Time.Hour = Time.Hour;
                Local_Time.Min = Time.Min;
                Local_Time.Sec = Time.Sec;
                Local_Date.Date = Date.Date;
                Local_Date.Month = Date.Month;
                Local_Date.Year = Date.Year;

                GetSCHTimeFrom_LAT_LOG(); // compute Astro time from new time.
                }
            else // add time zone diff to RTC time.
                {
                temp = (signed char) SLC_DST_Time_Zone_Diff;
                temp_hour = temp / 60;
                temp_min = temp % 60;

                Time.Min = Time.Min + temp_min;
                if (Time.Min >= 60)
                    {
                    Time.Min = Time.Min - 60;
                    Time.Hour = Time.Hour + 1;
                    }
                Time.Hour = Time.Hour + temp_hour;
                if (Time.Hour >= 24)
                    {
                    Time.Hour = Time.Hour - 24;
                    Increment_Date();
                    }

                RTC_SET_TIME(Time); // Set RTC Time

                Local_Time.Hour = Time.Hour;
                Local_Time.Min = Time.Min;
                Local_Time.Sec = Time.Sec;
                Local_Date.Date = Date.Date;
                Local_Date.Month = Date.Month;
                Local_Date.Year = Date.Year;

                GetSCHTimeFrom_LAT_LOG(); // Compute Astro time from RTC new time.
                }
            }
        }
    else // if DST disable from application and SLC running in DST then roll back time.
        {
        if ((Time_change_dueto_DST == 1))
            {
            Time_change_dueto_DST = 0;

            //      halCommonSetToken(TOKEN_Time_change_dueto_DST,&Time_change_dueto_DST);
            //      emberAfCorePrintln("\r\nTime Revert back from DST");
            if (SLC_DST_Time_Zone_Diff > 0) // if time zone diff is positive then subtract diff from RTC time.
                {
                temp = (unsigned char) SLC_DST_Time_Zone_Diff;
                temp_hour = temp / 60;
                temp_min = temp % 60;

                Time.Min = Time.Min - temp_min;
                if (Time.Min < 0)
                    {
                    Time.Min = Time.Min + 60;
                    Time.Hour = Time.Hour - 1;
                    }
                Time.Hour = Time.Hour - temp_hour;
                if (Time.Hour < 0)
                    {
                    Time.Hour = Time.Hour + 24;
                    Decrement_Date();
                    }

                RTC_SET_TIME(Time); // Set RTC new time.

                Local_Time.Hour = Time.Hour;
                Local_Time.Min = Time.Min;
                Local_Time.Sec = Time.Sec;
                Local_Date.Date = Date.Date;
                Local_Date.Month = Date.Month;
                Local_Date.Year = Date.Year;

                GetSCHTimeFrom_LAT_LOG(); // Compute Astro time from new time.
                }
            else // if time zone diff is negative then add diff in to time.
                {
                temp = (signed char) SLC_DST_Time_Zone_Diff;
                temp_hour = temp / 60;
                temp_min = temp % 60;

                Time.Min = Time.Min + temp_min;
                if (Time.Min >= 60)
                    {
                    Time.Min = Time.Min - 60;
                    Time.Hour = Time.Hour + 1;
                    }
                Time.Hour = Time.Hour + temp_hour;
                if (Time.Hour >= 24)
                    {
                    Time.Hour = Time.Hour - 24;
                    Increment_Date();
                    }

                RTC_SET_TIME(Time); // set RTC new time.

                Local_Time.Hour = Time.Hour;
                Local_Time.Min = Time.Min;
                Local_Time.Sec = Time.Sec;
                Local_Date.Date = Date.Date;
                Local_Date.Month = Date.Month;
                Local_Date.Year = Date.Year;

                GetSCHTimeFrom_LAT_LOG(); // compute Astro time from new time.
                }

            }
        }
    }

/*************************************************************************
Function Name: find_dst_date_from_rule
input: occurance and occurance of month.
Output: None.
Discription: this function is use to find date of rule like if configured second
             monday of february then this fnction finds date of this occurance.
 *************************************************************************/
unsigned char find_dst_date_from_rule(unsigned char occurance, unsigned char occurance_month)
    {
    unsigned char occ_day = 0, occ_weekday = 0, i = 0, find_date = 0, N = 0;
    unsigned char temp_week_day = 0, temp_occ_day = 0;

    occ_day = occurance / 10;
    occ_weekday = occurance % 10;
    if ((occurance_month == 1) || (occurance_month == 3) || (occurance_month == 5) || (occurance_month == 7) || (occurance_month == 8) || (occurance_month == 10) || (occurance_month == 12))
        {
        N = 31;
        }
    else if ((occurance_month == 4) || (occurance_month == 6) || (occurance_month == 9) || (occurance_month == 11))
        {
        N = 30;
        }

    if (occurance_month == 2)
        {
        if (Date.Year % 4 == 0)
            N = 29;
        else
            N = 28;
        }


    for (i = 1; i <= N; i++)
        {
        temp_week_day = week_day(Date.Year, i, occurance_month);
        if (temp_week_day == occ_weekday)
            {
            temp_occ_day++;
            if (temp_occ_day == occ_day)
                {
                find_date = i;
                break;
                }
            }
        }

    if (find_date == 0)
        {
        occ_day = occ_day - 1;
        temp_occ_day = 0;
        for (i = 1; i <= N; i++)
            {
            temp_week_day = week_day(Date.Year, i, occurance_month);
            if (temp_week_day == occ_weekday)
                {
                temp_occ_day++;
                if (temp_occ_day == occ_day)
                    {
                    find_date = i;
                    break;
                    }
                }
            }
        }

    return find_date;
    }

unsigned char check_mix_mode_week_day(unsigned char sch_weekday)
    {

    day_sch.Val = sch_weekday;

    if (((day_sch.bits.b0 == day_sch_R.bits.b0) && (day_sch_R.bits.b0 == 1)) ||
        ((day_sch.bits.b1 == day_sch_R.bits.b1) && (day_sch_R.bits.b1 == 1)) ||
        ((day_sch.bits.b2 == day_sch_R.bits.b2) && (day_sch_R.bits.b2 == 1)) ||
        ((day_sch.bits.b3 == day_sch_R.bits.b3) && (day_sch_R.bits.b3 == 1)) ||
        ((day_sch.bits.b4 == day_sch_R.bits.b4) && (day_sch_R.bits.b4 == 1)) ||
        ((day_sch.bits.b5 == day_sch_R.bits.b5) && (day_sch_R.bits.b5 == 1)) ||
        ((day_sch.bits.b6 == day_sch_R.bits.b6) && (day_sch_R.bits.b6 == 1)))
        {
        return 1;
        }
    else
        {
        return 0;
        }
    }

/*************************************************************************
Function Name: Increment_Date
input: None.
Output: None.
Discription: this function is use increment current date and if due to this
             change in month and year then do it.
 *************************************************************************/
void Increment_Date(void)
    {
    unsigned char N = 0;
    /* find number of days in to given month */
    if ((Date.Month == 1) || (Date.Month == 3) || (Date.Month == 5) || (Date.Month == 7) || (Date.Month == 8) || (Date.Month == 10) || (Date.Month == 12))
        {
        N = 31;
        }
    else if ((Date.Month == 4) || (Date.Month == 6) || (Date.Month == 9) || (Date.Month == 11))
        {
        N = 30;
        }

    if (Date.Month == 2) // check for february
        {
        if (Date.Year % 4 == 0) // check for leap year
            N = 29;
        else
            N = 28;
        }
    /* find number of days in to given month */



    if (Date.Date == N) // if RTC date is last date of given month then make date = 1 and increase month.
        {
        Date.Date = 1;
        Date.Month = Date.Month + 1;
        if (Date.Month > 12) // if month is last month then increase year and make month = 1.
            {
            Date.Month = 1;
            Date.Year = Date.Year + 1;
            }
        }
    else
        {
        Date.Date = Date.Date + 1; // if RTC date is not last for given month then increase date.
        }
    RTC_SET_DATE(Date); // Set new date in to RTC.
    }

/*************************************************************************
Function Name: Decrement_Date
input: None.
Output: None.
Discription: this function is use decrement current date and if due to this
             change in month and year then do it.
 *************************************************************************/
void Decrement_Date(void)
    {
    unsigned char N = 0;
    Date.Date = Date.Date - 1; // decrement RTC date by one
    if (Date.Date == 0) // if date is 0 means current date is first date of that month.
        {
        Date.Month = Date.Month - 1; // decrement RTC Month.
        if (Date.Month == 0) // if month is 0 means it is a first month so after decrement it has to 12.
            {
            Date.Month = 12;
            Date.Year = Date.Year - 1; // decrement year by 1.
            }

        /* calculate no of days in new month */
        if ((Date.Month == 1) || (Date.Month == 3) || (Date.Month == 5) || (Date.Month == 7) || (Date.Month == 8) || (Date.Month == 10) || (Date.Month == 12))
            {
            N = 31;
            }
        else if ((Date.Month == 4) || (Date.Month == 6) || (Date.Month == 9) || (Date.Month == 11))
            {
            N = 30;
            }

        if (Date.Month == 2) // check for february
            {
            if (Date.Year % 4 == 0) // check for leap year
                N = 29;
            else
                N = 28;
            }
        /* calculate no of days in new month */
        Date.Date = N;
        }
    RTC_SET_DATE(Date); // Set new date in to RTC.
    }

/*************************************************************************
Function Name: Increment_Local_Timer
input: None.
Output: None.
Discription: this function is use increment current date and if due to this
             change in month and year then do it.
 *************************************************************************/
void Increment_Local_Timer(void)
    {
    unsigned char N = 0;
    //    unsigned char temp_week_day = 0, temp_occ_day = 0;


    Local_Time.Sec = Local_Time.Sec + 1;
    if (Local_Time.Sec > 59)
        {
        Local_Time.Sec = 0;
        Local_Time.Min = Local_Time.Min + 1;
        if (Local_Time.Min > 59)
            {
            Local_Time.Min = 0;
            Local_Time.Hour = Local_Time.Hour + 1;
            if (Local_Time.Hour > 23)
                {
                Local_Time.Hour = 0;
                Local_Date.Date = Local_Date.Date + 1;
                if ((Local_Date.Month == 1) || (Local_Date.Month == 3) || (Local_Date.Month == 5) || (Local_Date.Month == 7) || (Local_Date.Month == 8) || (Local_Date.Month == 10) || (Local_Date.Month == 12))
                    {
                    N = 31;
                    }
                else if ((Local_Date.Month == 4) || (Local_Date.Month == 6) || (Local_Date.Month == 9) || (Local_Date.Month == 11))
                    {
                    N = 30;
                    }

                if (Local_Date.Month == 2) // check for february
                    {
                    if (Local_Date.Year % 4 == 0) // check for leap year
                        N = 29;
                    else
                        N = 28;
                    }

                if (Local_Date.Date > N) // if RTC date is last date of given month then make date = 1 and increase month.
                    {
                    Local_Date.Date = 1;
                    Local_Date.Month = Local_Date.Month + 1;
                    if (Local_Date.Month > 12) // if month is last month then increase year and make month = 1.
                        {
                        Local_Date.Month = 1;
                        Local_Date.Year = Local_Date.Year + 1;
                        }
                    }
                }
            }
        }
    }

void check_RTC_faulty_logic(void)
    {
    signed int Temp_RTC_drift = 0;
    if (check_RTC_logic_cycle == 1)
        {
        check_RTC_logic_cycle = 0;
        if (((Time.Hour == 23)&&(Time.Min >= 30)) || ((Local_Time.Hour == 23) && (Local_Time.Min >= 30)))
            {
            }
        else
            {
            Temp_RTC_drift = (((Time.Hour * 60 * 60) + (Time.Min * 60) + (Time.Sec)) - ((Local_Time.Hour * 60 * 60) + (Local_Time.Min * 60) + (Local_Time.Sec)));
            if (Temp_RTC_drift < 0)
                {
                Temp_RTC_drift = Temp_RTC_drift * -1;
                }
            RTC_drift = Temp_RTC_drift;
            if (RTC_drift > 25000)
                {
                return;
                }

            if ((RTC_drift > 60) && (GPS_error_condition.bits.b7 == 1)) // 60
                {
                //        send_gps_command();
                Time_set_by_GPS = 0;
                need_to_send_GPS_command = 1;

                }


            if ((RTC_drift > 900) && (RTC_faulty_detected == 1)) // 900
                {
                RTC_faulty_detected = 0; // 0 = faulty.
                error_condition1.bits.b2 = 1;

                if (RTC_Faulty_Shift_to_Local_Timer == 1) //this shows GPS is available and shift to previous mode as RTC  ok and driven by local timer
                    {
                    RTC_faulty_detected = 1; // 1 = RTC Ok after new configuration.
                    if (previous_mode != automode)
                        {
                        automode = previous_mode;
                        WriteByte_EEPROM(EE_LOGICMODE, automode); // store mode in to NVM.
                        }
                    return;
                    }
                if ((automode == 2) || (automode == 3) || (automode == 5) || (automode == 7))
                    {
                    previous_mode = automode; // store mode value before chage.
                    WriteByte_EEPROM(EE_PREV_MODE, previous_mode); // store mode in to NVM.
                    automode = 1; // move SLC in to Photocell mode
                    WriteByte_EEPROM(EE_LOGICMODE, automode); // store mode in to NVM.
                    }

                }
            else
                {
                if (RTC_need_to_varify == 1)
                    {
                    RTC_need_to_varify = 0;
                    if (RTC_drift < 10)
                        {
                        RTC_faulty_detected = 1; // 1 = RTC Ok after new configuration.
                        error_condition1.bits.b2 = 0;

                        if (previous_mode != automode)
                            {
                            automode = previous_mode;
                            WriteByte_EEPROM(EE_LOGICMODE, automode); // store mode in to NVM.
                            }
                        }
                    else
                        {
                        error_condition1.bits.b2 = 1;
                        RTC_faulty_detected = 0; // 1 = RTC Ok after new configuration.
                        if (RTC_Faulty_Shift_to_Local_Timer == 1) //this shows GPS is available and shift to previous mode as RTC  ok and driven by local timer
                            {
                            RTC_faulty_detected = 1; // 1 = RTC Ok after new configuration.
                            if (previous_mode != automode)
                                {
                                automode = previous_mode;
                                WriteByte_EEPROM(EE_LOGICMODE, automode); // store mode in to NVM.
                                }
                            return;
                            }
                        if ((automode == 2) || (automode == 3) || (automode == 5) || (automode == 7))
                            {
                            previous_mode = automode; // store mode value before chage.
                            WriteByte_EEPROM(EE_PREV_MODE, previous_mode); // store mode in to NVM.
                            automode = 1; // move SLC in to Photocell mode
                            WriteByte_EEPROM(EE_LOGICMODE, automode);
                            }

                        }
                    }
                }
            }
        }
    }

void Fill_RTC_BY_Local_Timer(void)
    {
    unsigned char Local_SLC_Mix_R_weekday = 0;
    //  Local_Time.Hour = Time.Hour;																										
    //  Local_Time.Min = Time.Min;
    //  Local_Time.Sec = Time.Sec;
    //  Local_Date.Date = Date.Date;
    //  Local_Date.Month = Date.Month;
    //  Local_Date.Year = Date.Year;

    Time.Sec = Local_Time.Sec;
    Time.Min = Local_Time.Min;
    Time.Hour = Local_Time.Hour;
    Date.Date = Local_Date.Date;
    Date.Month = Local_Date.Month;
    Date.Year = Local_Date.Year;
    Date.Week = week_day(Date.Year, Date.Date, Date.Month);



    if (Date.Week == 0) // sunday
        {
        Local_SLC_Mix_R_weekday = 1;
        }
    else if (Date.Week == 1) // monday
        {
        Local_SLC_Mix_R_weekday = 64;
        }
    else if (Date.Week == 2) // tuesday
        {
        Local_SLC_Mix_R_weekday = 32;
        }
    else if (Date.Week == 3) // wenesday
        {
        Local_SLC_Mix_R_weekday = 16;
        }
    else if (Date.Week == 4) // thrusday
        {
        Local_SLC_Mix_R_weekday = 8;
        }
    else if (Date.Week == 5) // friday
        {
        Local_SLC_Mix_R_weekday = 4;
        }
    else if (Date.Week == 6) // seturday
        {
        Local_SLC_Mix_R_weekday = 2;
        }
    else
        {
        }

    day_sch_R.Val = Local_SLC_Mix_R_weekday;


    if (Time.Hour < 12) // for schedule off set find date and time from function for 12 hour offset.
        {
        Find_Previous_date_Month();
        }
    else
        {
        PDate.Date = Date.Date; // after 12 o'clock change date and time to acthual value.
        PDate.Month = Date.Month;
        }
    }

void Delay(long K)
    {
    volatile long i; //Delay(1092) = 1msec ...
    for (i = 0; i < K; i++)
        {
        ;
        }

    }

void Fault_Remove_Logic(void)
    {
    if ((Lamp_Balast_fault_Remove_Cnt >= Lamp_Balast_fault_Remove_Time))
        {
        Lamp_Balast_fault_Remove_Cnt = 0;
        if (Ballast_Fault_Occured == 1)
            {
            if ((irdoub < current_creep_limit) && (detect_lamp_current == 1) && (detect_lamp_current_first_time_on == 1))// ballast fault trip checking logic
                {
                Ballast_Fault_Remove_Retry_Cnt_3 = 0;
                }
            else
                {
                Ballast_Fault_Remove_Retry_Cnt_3++;
                if (Ballast_Fault_Remove_Retry_Cnt_3 >= 3)
                    {
                    Ballast_Fault_Remove_Retry_Cnt_3 = 0;
                    Open_circuit = 0;
                    error_condition1.bits.b0 = 0; // ballast fail trip //open circuit trip	
                    low_current_counter = 0;
                    check_current_counter = 0;
                    Ballast_Fault_Occured = 0;
                    //	Lamp__Ballast_Fault_Occured =0;		
                    //	No_check_Ballast_After_This_Event =1;
                    }
                }
            }
        if (Lamp_Fault_Occured == 1)
            {
            if ((w1 < (KW_Threshold * (1.0 - Per_Val))) &&
                (detect_lamp_current == 1) &&
                (detect_lamp_current_first_time_on == 1)
                )// LAmp fault trip checking logic
                {
                Lamp_Fault_Remove_Retry_Cnt_3 = 0;
                }
            else
                {
                Lamp_Fault_Remove_Retry_Cnt_3++;
                if (Lamp_Fault_Remove_Retry_Cnt_3 >= 3)
                    {
                    Lamp_Fault_Remove_Retry_Cnt_3 = 0;
                    Lamp_Failure = 0;
                    error_condition.bits.b3 = 0; // lamp fail trip	
                    low_current_counter = 0;
                    check_current_counter = 0;
                    Lamp_Fault_Occured = 0;

                    }
                }
            }

        }
    else
        {
        }

    if ((Ballast_Fault_Occured == 0)&&(Lamp_Fault_Occured == 0))
        {
        Lamp_Balast_fault_Remove_Cnt = 0;
        }

    }

void Save_BH_kWh(void)
    {

    //************************Burnhour***************************//
    pulseCounter.float_data = lamp_burn_hour;
    WriteByte_EEPROM(EE_BURN_HOUR + 0, pulseCounter.byte[3]);
    WriteByte_EEPROM(EE_BURN_HOUR + 1, pulseCounter.byte[2]);
    WriteByte_EEPROM(EE_BURN_HOUR + 2, pulseCounter.byte[1]);
    WriteByte_EEPROM(EE_BURN_HOUR + 3, pulseCounter.byte[0]);

    //************************kWh********************************//
    zempbyte = 0;
    if (irdoub > MIN_CREEP_VALUE)
        {
        zempbyte = intesec;
        }
    intesec = 0;
    pulses_r += zempbyte;

    kwh = (((float) pulses_r * mf3));

    pulseCounter.long_data = pulses_r;
    WriteByte_EEPROM(EE_KWH + 0, pulseCounter.byte[0]);
    WriteByte_EEPROM(EE_KWH + 1, pulseCounter.byte[1]);
    WriteByte_EEPROM(EE_KWH + 2, pulseCounter.byte[2]);
    WriteByte_EEPROM(EE_KWH + 3, pulseCounter.byte[3]);
    }

/*************************************************************************
Function Name: Decrement_Date
input: None.
Output: None.
Discription: this function is use decrement current date and if due to this
             change in month and year then do it.
 *************************************************************************/
void Fill_Decrement_Date(void)
    {
    unsigned char N = 0;



    EMDate.Date = EMDate.Date - 1; // decrement RTC date by one
    if (EMDate.Date == 0) // if date is 0 means current date is first date of that month.
        {
        EMDate.Month = EMDate.Month - 1; // decrement RTC Month.
        if (EMDate.Month == 0) // if month is 0 means it is a first month so after decrement it has to 12.
            {
            EMDate.Month = 12;
            EMDate.Year = EMDate.Year - 1; // decrement year by 1.
            }

        /* calculate no of days in new month */
        if ((EMDate.Month == 1) || (EMDate.Month == 3) || (EMDate.Month == 5) || (EMDate.Month == 7) || (EMDate.Month == 8) || (EMDate.Month == 10) || (EMDate.Month == 12))
            {
            N = 31;
            }
        else if ((EMDate.Month == 4) || (EMDate.Month == 6) || (EMDate.Month == 9) || (EMDate.Month == 11))
            {
            N = 30;
            }

        if (EMDate.Month == 2) // check for february
            {
            if (EMDate.Year % 4 == 0) // check for leap year
                N = 29;
            else
                N = 28;
            }
        /* calculate no of days in new month */
        EMDate.Date = N;
        }

    }

void EEWriteAstroSch()
    {
    WriteByte_EEPROM(ASTRO_EE_SCHEDULE + (0 * 5) + 0, Astro_sSch[0].cStartHour); // V6.1.10
    WriteByte_EEPROM(ASTRO_EE_SCHEDULE + (0 * 5) + 1, Astro_sSch[0].cStartMin); // V6.1.10
    WriteByte_EEPROM(ASTRO_EE_SCHEDULE + (0 * 5) + 2, Astro_sSch[0].cStopHour); // V6.1.10
    WriteByte_EEPROM(ASTRO_EE_SCHEDULE + (0 * 5) + 3, Astro_sSch[0].cStopMin); // V6.1.10
    WriteByte_EEPROM(ASTRO_EE_SCHEDULE + (0 * 5) + 4, Astro_sSch[0].bSchFlag); // V6.1.10

    WriteByte_EEPROM(ASTRO_EE_SCHEDULE + (1 * 5) + 0, Astro_sSch[1].cStartHour); // V6.1.10
    WriteByte_EEPROM(ASTRO_EE_SCHEDULE + (1 * 5) + 1, Astro_sSch[1].cStartMin); // V6.1.10
    WriteByte_EEPROM(ASTRO_EE_SCHEDULE + (1 * 5) + 2, Astro_sSch[1].cStopHour); // V6.1.10
    WriteByte_EEPROM(ASTRO_EE_SCHEDULE + (1 * 5) + 3, Astro_sSch[1].cStopMin); // V6.1.10
    WriteByte_EEPROM(ASTRO_EE_SCHEDULE + (1 * 5) + 4, Astro_sSch[1].bSchFlag); // V6.1.10
    }

#if 0
Higher 4 bit of DimmerDriver_SELECTION is DimmerDriverSelectionProcess and Lower 4bit is DimDriverStatus
        == > DimmerDriverSelectionProcess
        0 = Auto Detection In Each Power Cycle
        1 = Auto Detection In NextPower Cycle
        2 = Default Dali
        3 = Default AO
        == > DimDriverStatus
        0 = AO As Driver
        1 = Dali As Driver
        2 = Driver Needs To Detect
        3 = Error in Driver detection AO is Selected
#endif


        /*************************************************************************************
         * Function 		: sendDaliCommand_ToCheck_DimmerDriver
         * Use			: Broadcast message for QUERY STATUS
         * Argument		: 
         * return		: 1 - Dali Reply Received	||	0 - Dali Reply Not Received
         **************************************************************************************/
        unsigned char sendDaliCommand_ToCheck_DimmerDriver(void){
                                                                 volatile unsigned char j;
                                                                 Delay(60060);
                                                                 Delay(60060);

                                                                 forward = ((0xFF << 8) | (0x97)); //version no

                                                                 f_dalitx = 0; // clear DALI send flag 
                                                                 f_dalirx = 0; // clear DALI receive (answer) flag
                                                                 DaliRx_Status = 0;
                                                                 DALI_Send(); // DALI send data to slave(s)

                                                                 for (j = 0; j < 100; j++)
        {
        Delay(60060);
        if (f_dalirx)
            {
            return 0x01;
            }
        }
                                                                 return 0x00;
    }

void AppDimmerAutoDetectInit(void)
    {
    /*DimmerDriver_SELECTION Already read from EEPROM */
    DimmerDriverSelectionProcess = (DimmerDriver_SELECTION >> 4); // Higher 4bits as 
    DimDriverStatus = DimmerDriver_SELECTION & 0x0F; // Lower 4bits as dali_support
    if (DimmerDriverSelectionProcess == DIM_DRIVER_SELECT_AUTO_ON_POWER_CYCLE)
        {
        DimDriverStatus = DIM_DRIVER_STATUS_DETECTION;
        DimDriverMuxSelect(DIM_DRIVER_MUX_DALI);
        }
    else if (DimmerDriverSelectionProcess == DIM_DRIVER_SELECT_AUTO_ON_REQUEST)
        {
        if ((DimDriverStatus == DIM_DRIVER_STATUS_AO_SET) || (DimDriverStatus == DIM_DRIVER_STATUS_ERR_AO_SET)) // AO as Dimmer
            {
            DimDriverMuxSelect(DIM_DRIVER_MUX_AO);
            }
        else if (DimDriverStatus == DIM_DRIVER_STATUS_DALI_SET) // Dali as Dimmer
            {
            DimDriverMuxSelect(DIM_DRIVER_MUX_DALI);
            }
        else
            {
            DimDriverMuxSelect(DIM_DRIVER_MUX_DALI);
            DimDriverStatus = DIM_DRIVER_STATUS_DETECTION; // Need To detect Driver
            }
        }
    else if (DimmerDriverSelectionProcess == DIM_DRIVER_SELECT_DALI)
        {
        DimDriverMuxSelect(DIM_DRIVER_MUX_DALI);
        DimDriverStatus = DIM_DRIVER_STATUS_DALI_SET; // Need To detect Driver
        }
    else if (DimmerDriverSelectionProcess >= DIM_DRIVER_SELECT_AO)
        {
        DimDriverMuxSelect(DIM_DRIVER_MUX_AO);
        DimDriverStatus = DIM_DRIVER_STATUS_AO_SET;
        }
    else
        {
        /* Do Nothing */
        }
    }

/*************************************************************************************
 * Function 		: detectDimmerDriver
 * Use			: 
 * Argument		: 
 * return		: 1 - Dali 	||	0 - AO
 **************************************************************************************/
unsigned char detectDimmerDriver(void)
    {
    unsigned char dali_detect_count = 0;

    DimDriverMuxSelect(DIM_DRIVER_MUX_DALI);
    Delay(10000);

    //----------- NO Relay -------------//  
    if (set_do_flag == 0)
        {
        lamp_off_on_timer = Lamp_off_on_Time;
        Set_Do(1);

        }
    //DimDriverMuxSelect(DIM_DRIVER_MUX_DALI);
    //delay(10000);

    dali_detect_count = sendDaliCommand_ToCheck_DimmerDriver();
    dali_detect_count = dali_detect_count + sendDaliCommand_ToCheck_DimmerDriver();
    dali_detect_count = dali_detect_count + sendDaliCommand_ToCheck_DimmerDriver();
    if (dali_detect_count > 0)
        {
        DimDriverStatus = DIM_DRIVER_STATUS_DALI_SET;
        DimmerDriver_SELECTION = (DimmerDriverSelectionProcess << 4) | (DimDriverStatus);
        WriteByte_EEPROM(EE_ADDR_DIM_DRIVER_INFO + 0, DimmerDriver_SELECTION);
        //halCommonSetToken(TOKEN_DimmerDriver_SELECTION,&DimmerDriver_SELECTION);
        DimDriverMuxSelect(DIM_DRIVER_MUX_DALI);
        //halStackIndicatePresence();
        }
    else
        {
        DimDriverStatus = DIM_DRIVER_STATUS_AO_SET;
        DimDriverMuxSelect(DIM_DRIVER_MUX_AO);
        Delay(1000);
        //      halStackIndicatePresence();

        duty_cycle = (PWM_CONSTANT * (100 / 100.0));
        OC2R = duty_cycle; // Initialize Compare Register1 with 0x0026 	 
        OC2RS = duty_cycle; // Initialize Secondary Compare Register1 with 0x0026
        CheckAoDimmerForDetectionTimmer = 0;
        while (CheckAoDimmerForDetectionTimmer <= 5)
            {
            Read_EnergyMeter(); // resent power on flag
            calculate_3p4w(); // claculate en
            delay(20);
            }
        full_brightPower = w1;

        duty_cycle = (PWM_CONSTANT * (30 / 100.0));
        OC2R = duty_cycle; // Initialize Compare Register1 with 0x0026 	 
        OC2RS = duty_cycle; // Initialize Secondary Compare Register1 with 0x0026
        CheckAoDimmerForDetectionTimmer = 0;
        while (CheckAoDimmerForDetectionTimmer <= 5)
            {
            Read_EnergyMeter(); // resent power on flag
            calculate_3p4w();
            delay(20);
            }
        full_brightPower_at_30 = full_brightPower * 0.2;
        if ((full_brightPower - w1)>(full_brightPower_at_30))
            {
            DimDriverStatus = DIM_DRIVER_STATUS_AO_SET;
            DimmerDriver_SELECTION = (DimmerDriverSelectionProcess << 4) | (DimDriverStatus);
            WriteByte_EEPROM(EE_ADDR_DIM_DRIVER_INFO + 0, DimmerDriver_SELECTION);
            }
        else
            {
            DimDriverStatus = DIM_DRIVER_STATUS_ERR_AO_SET;
            DimmerDriver_SELECTION = (DimmerDriverSelectionProcess << 4) | (DimDriverStatus);
            WriteByte_EEPROM(EE_ADDR_DIM_DRIVER_INFO + 0, DimmerDriver_SELECTION);
            }

        }
    lamp_off_on_timer = Lamp_off_on_Time;
    lamp_on_first = 0;
    default_dali = 0;
    Set_Do(1);

    return DimDriverStatus;
    }

void DimDriverMuxSelect(unsigned char DimDriverType)
    {
    /**
     * IN1 & IN2  0 0  mean DALI and 1 1 mean AO 
     * 
     */
    if (DimDriverType == DIM_DRIVER_MUX_AO)
        {
        HI_PIN_OUT_IN1 = 1;
        HI_PIN_OUT_IN2 = 1;
        DALIE = 1;
        DaliHwSelect_u8 = DIM_DRIVER_MUX_AO;
        WriteData_UART2("\r\n...AO", strlen("\r\n...AO"));
        }
    else
        {
        HI_PIN_OUT_IN1 = 0;
        HI_PIN_OUT_IN2 = 0;
        DALIE = 0;
        DaliHwSelect_u8 = DIM_DRIVER_MUX_DALI;
        WriteData_UART2("\r\n...DALI", strlen("\r\n...DALI"));
        }
    }


