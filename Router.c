/*********************************************************************

    Microchip ZigBee Stack

    Sample Demo Router for the PIC18/PIC24 ZigBee 2006 Residential Stack
    This demonstration shows how a ZigBee Router can be set up.  
    This demo allows the PICDEM Z/Explorer 16 Demostration Board to act as ZigBee
    protocol Router. It is designed to interact with other ZigBee protocol 
    devices - Routers and End Devices.
    
    Switch and LED functionality are as follows:

    RB4/RD6:  Adds a node to, or removes a node from Group 4.  
    RA0/D10:  Used to indicate if a node is a member of Group 4 (Lit � yes; unlit � no) 

    At startup the devices do not belong to a group, and the lit LEDs just indicate
    they are on the network.
    
    RB5/RD7:   Is used to send a messages to nodes in Group 4.    
               The actual message is a request for all the Group 4 nodes to end 
               back to the requester 10 bytes.
    RA1/D09:   Toggles to indicate that the receiving node is in Group 4 
               and received requests for data.  


    NOTE: To speed network formation, ALLOWED_CHANNELS has been set to
    channel 21 only.  
    Please consult the PICDEM Z Zigbee2006 Residential StackQTGuide.pdf for how to 
    run this sample application.
 *********************************************************************
 * FileName:        Router.c
 *  Dependencies:
 * Processor:       PIC18F/PIC24f
 * Complier:        MCC18 v3.20 or higher
 * Complier:        MCC30 v3.10 or higher
 * Company:         Microchip Technology, Inc.
 *
 * Software License Agreement
 *
 * Copyright � 2004-2007 Microchip Technology Inc.  All rights reserved.
 *
 * Microchip licenses to you the right to use, copy and distribute Software 
 * only when embedded on a Microchip microcontroller or digital signal 
 * controller and used with a Microchip radio frequency transceiver, which 
 * are integrated into your product or third party product (pursuant to the 
 * sublicense terms in the accompanying license agreement).  You may NOT 
 * modify or create derivative works of the Software.  
 *
 * If you intend to use this Software in the development of a product for 
 * sale, you must be a member of the ZigBee Alliance.  For more information, 
 * go to www.zigbee.org.
 *
 * You should refer to the license agreement accompanying this Software for 
 * additional information regarding your rights and obligations.
 *
 * SOFTWARE AND DOCUMENTATION ARE PROVIDED �AS IS� WITHOUT WARRANTY OF ANY 
 * KIND, EITHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION, ANY WARRANTY 
 * OF MERCHANTABILITY, TITLE, NON-INFRINGEMENT AND FITNESS FOR A PARTICULAR 
 * PURPOSE. IN NO EVENT SHALL MICROCHIP OR ITS LICENSORS BE LIABLE OR OBLIGATED 
 * UNDER CONTRACT, NEGLIGENCE, STRICT LIABILITY, CONTRIBUTION, BREACH OF 
 * WARRANTY, OR OTHER LEGAL EQUITABLE THEORY ANY DIRECT OR INDIRECT DAMAGES OR 
 * EXPENSES INCLUDING BUT NOT LIMITED TO ANY INCIDENTAL, SPECIAL, INDIRECT, 
 * PUNITIVE OR CONSEQUENTIAL DAMAGES, LOST PROFITS OR LOST DATA, COST OF 
 * PROCUREMENT OF SUBSTITUTE GOODS, TECHNOLOGY, SERVICES, OR ANY CLAIMS BY 
 * THIRD PARTIES (INCLUDING BUT NOT LIMITED TO ANY DEFENSE THEREOF), OR OTHER 
 * SIMILAR COSTS.
 *
 * Author               Date    Comment
 *~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * DF/KO                01/09/06 Microchip ZigBee Stack v1.0-3.5
 * DPL                  08/01/08 Microchip ZigBee Stack v2.0-2.6
 ********************************************************************/

//******************************************************************************
// Header Files
//******************************************************************************

// Include the main ZigBee header file.



#include "Zigbee.def"
#include "Compiler.h"
#include "Generic.h"
#include "MSPI.h"
#include "AMR_RTC.h"
#include "AMR_CS5463.h"
#include "Application_logic.h"
#include "protocol.h"
#include "serial.h"
#include "p24FJ128GA306.h"
#include "DEE Emulation 16-bit.h"
#include "time.h"
#include "Gps.h"
#include "ADC_VI.h"
#include <xc.h>
#include <stdbool.h>
#include <string.h>
#include "SC16IS740.h"

#ifndef _XTAL_FREQ
#define _XTAL_FREQ  8000000UL
#endif

#define CLOCK_SystemFrequencyGet()        (32000000UL)
#define CLOCK_PeripheralFrequencyGet()    (CLOCK_SystemFrequencyGet() / 2)
#define CLOCK_InstructionFrequencyGet()   (CLOCK_SystemFrequencyGet() / 2)


#pragma config DSWDTPS = DSWDTPS1F    //Deep Sleep Watchdog Timer Postscale Select bits->1:68719476736 (25.7 Days)
#pragma config DSWDTOSC = LPRC    //DSWDT Reference Clock Select->DSWDT uses LPRC as reference clock
#pragma config DSBOREN = ON    //Deep Sleep BOR Enable bit->DSBOR Enabled
#pragma config DSWDTEN = ON    //Deep Sleep Watchdog Timer Enable->DSWDT Enabled
#pragma config DSSWEN = ON    //DSEN Bit Enable->Deep Sleep is controlled by the register bit DSEN

// CONFIG3
#pragma config WPFP = WPFP127    //Write Protection Flash Page Segment Boundary->Page 127 (0x1FC00)
#pragma config VBTBOR = ON    //VBAT BOR enable bit->VBAT BOR enabled
#pragma config SOSCSEL = OFF    //SOSC Selection bits->Digital (SCLKI) mode
#pragma config WDTWIN = PS25_0    //Watch Dog Timer Window Width->Watch Dog Timer Window Width is 25 percent
#pragma config BOREN = ON    //Brown-out Reset Enable->Brown-out Reset Enable
#pragma config WPDIS = WPDIS    //Segment Write Protection Disable->Disabled
#pragma config WPCFG = WPCFGDIS    //Write Protect Configuration Page Select->Disabled
#pragma config WPEND = WPENDMEM    //Segment Write Protection End Page Select->Write Protect from WPFP to the last page of memory

// CONFIG2
#pragma config POSCMD = XT    //Primary Oscillator Select->XT Oscillator Enabled
#pragma config BOREN1 = EN    //BOR Override bit->BOR Enabled [When BOREN=1]
#pragma config IOL1WAY = OFF    //IOLOCK One-Way Set Enable bit->Once set, the IOLOCK bit cannot be cleared
#pragma config OSCIOFCN = ON    //OSCO Pin Configuration->OSCO/CLKO/RC15 functions as port I/O (RC15)
#pragma config FCKSM = CSDCMD    //Clock Switching and Fail-Safe Clock Monitor Configuration bits->Clock switching and Fail-Safe Clock Monitor are disabled
#pragma config FNOSC = PRIPLL    //Initial Oscillator Select->Primary Oscillator with PLL module (XTPLL,HSPLL, ECPLL)
#pragma config ALTVREF = ALT_AV_ALT_CV    //Alternate VREF/CVREF Pins Selection bit->Voltage reference input, ADC =RB0/RB1   Comparator =RB0/RB1
#pragma config IESO = ON    //Internal External Switchover->Enabled

// CONFIG1
#pragma config WDTPS = PS32768    //Watchdog Timer Postscaler Select->1:32768
#pragma config FWPSA = PR128    //WDT Prescaler Ratio Select->1:128
#pragma config FWDTEN = WDT_HW    //Watchdog Timer Enable->WDT disabled in hardware; SWDTEN bit disabled
#pragma config WINDIS = OFF    //Windowed WDT Disable->Standard Watchdog Timer
#pragma config ICS = PGx1    //Emulator Pin Placement Select bits->Emulator functions are shared with PGEC1/PGED1
#pragma config LPCFG = OFF    //Low power regulator control->Disabled
#pragma config GWRP = OFF    //General Segment Write Protect->Disabled
#pragma config GCP = OFF    //General Segment Code Protect->Code protection is disabled
#pragma config JTAGEN = OFF    //JTAG Port Enable->Disabled

#define TILT_VAL 240
//#define DEBUG

//PWM
//#define PWMTCY 16000
//#define TIMERREG(FREQ) ((PWMTCY/FREQ)-1)
//uint8_t _5k_PWM_FREQ = 5;
//uint8_t _2k_PWM_FREQ = 2; //kHz
//uint8_t _1k_PWM_FREQ = 1;
//uint16_t PR2_VAL = 0;

uint8_t ReadEM = 0;
uint32_t loopcounter = 0;
uint8_t ZC_count = 0;
void SET_PWM_FREQ(void);
uint8_t mode_str[8] = {0};

//*******************************************************************************//
//******************************************************************************//
// Function Prototypes															//
//******************************************************************************//

unsigned char Tilt_Occured = 0, Tilt_Get_Previously = 0, Tilt_send_cnt = 0;
unsigned char Send_Last_gasp_msg = 0;
volatile unsigned int Tilt_Cnt = 0;
unsigned char Tilt_detect_flag = 0;
volatile unsigned int Tilt_Timeout_Cnt = 0;
volatile unsigned char sendonce = 0, lastgasp_cnt = 0, lastgasp_retry_timer = 0, last_gasp_detected = 0, lastgasp_retry = 0, lasgap_buff[10];

#define CISCO_EPF		 LATCbits.LATC13
static volatile unsigned long low_time; // captured pulse low time
static volatile unsigned long high_time; // captured pulse high time
volatile unsigned int DaliTime_u16[30];
volatile unsigned int DaliLowTime_u16[30];
volatile unsigned int DaliHighTime_u16[30];
static unsigned char previous;
volatile unsigned int cap_cnt = 0, cnt1 = 0, cnt2 = 0, cnt3 = 0, count11, answer1, cnt5, cnt6;
volatile unsigned char cn1 = 0, cn2 = 0, cn3 = 0, cn4 = 0, cn5 = 0, cn6 = 0, cn7 = 0, cn8, cn9, swrite = 0;
volatile unsigned long int loop_cnt2 = 0;
volatile unsigned char DaliTotalCntr = 0;
volatile unsigned char DaliHighTmrCntr = 0;
volatile unsigned char DaliLowTmrCntr = 0;
#define USE_VECTOR_SPACE
#define storedataTime 10	//store Burn hour and kWh after #define storedataTime time.
#define TE          (208/2)          // half bit time = 417 usec
#define MIN_TE      (TE-32)    // minimum half bit time
#define MAX_TE       (TE+32)   // maximum half bit time
#define MIN_2TE     ((2*TE)-32)   // minimum full bit time
#define MAX_2TE     ((2*TE)+32) // maximum full bit time
//****************************************

//Bootloader Vectors *********************
#ifdef USE_VECTOR_SPACE
/*
        Store delay timeout value & user reset vector at 0x100 
        (can't be used with bootloader's vector protect mode).
		
        Value of userReset should match reset vector as defined in linker script.
        BLreset space must be defined in linker script.
 */
unsigned int userReset __attribute__((space(prog), section(".BLreset"))) = 0x1000;
unsigned char timeout __attribute__((space(prog), section(".BLreset"))) = 0xA;
#else
/*
        Store delay timeout value & user reset vector at start of user space
	
        Value of userReset should be the start of actual program code since 
        these variables will be stored in the same area.
 */
unsigned int userReset __attribute__((space(prog), section(".init"))) = 0x1004;
unsigned char timeout __attribute__((space(prog), section(".init"))) = 0x0A;
#endif
//Bootloader Vectors *********************
void PIN_Initialize(void);
void CLOCK_Initialize(void);
void INTERRUPT_Initialize(void);
void AO_DALI_selection(void);
void Read_para_EEPROM(void);
void HardwareInit(void);
void ProcessNONZigBeeTasks(void);
void Parse_RF_Packet(unsigned char *data_rf);

void Check_RTC(unsigned char time);
void Check_EMT(unsigned char time);
void Check_Lamp(unsigned char time);
void Check_photocell(unsigned char time);
void Check_CT(unsigned char time);
void Check_eeprom(void);
void check_power_cycle(void);
void Tilt_Sensor_Detect(void);
void check_dimming_hardware(void);
static void DALI_Shift_Bit(BYTE val);
static void DALI_Decode(void);
void capture(void);
extern volatile unsigned char GPS_Read_stop;
BYTE CAL_page;
////////////////////////////////////////////////////////////////////////////////
float Vr_Cal = 0.0, Ir_Cal = 0.0, KW_Cal = 0.0, KW_Cal_120 = 0.0;
#define NEW_LAMP_FAULT_LOGIC

#ifdef NEW_LAMP_FAULT_LOGIC
extern unsigned char Lock_cyclic_check, send_id_frame_flage;
extern volatile unsigned int Lock_cyclic_Counter;
extern float KW_Threshold;
#elif defined(OLD_LAMP_FAULT_LOGIC)

#else
#error "Please define LAMP_FAULT_LOGIC_TYPE"
#endif


//#define DALI_SUPPORT
unsigned char dali_support = 0; //0-Ao 1-Dali
//#if defined(DALI_SUPPORT)
//extern unsigned char f_dalitx;
//extern unsigned char f_dalirx;
volatile unsigned char dali_delay = 0;
//#endif




//#if defined(DALI_SUPPORT)


#define DALIE			LATBbits.LATB7
#define DALIT			LATBbits.LATB3
#define DALIR           PORTDbits.RD11

#define DALI_DIM_INIT_VAL 255

void send_push_data(void);

static unsigned char value; // used for dali send bit
static unsigned char position; // keeps track of sending bit position
unsigned short int forward = 0xff00;
static volatile unsigned int frame;
unsigned char answer = 0; // DALI slave answer
unsigned char f_dalitx = 0;
volatile unsigned char f_dalirx = 0;
static unsigned char f_repeat = 0; // flag command shall be repeated
static unsigned char f_busy = 0;
unsigned int ONOFF_CNT = 0;
volatile unsigned char DaliRx_Status = 0;
//#endif
void reset_nic(void);
void Fill_Energy_data(void);
//########################################
volatile unsigned int mscnt = 0;
volatile unsigned char LastGasp_Timer = 0, RF_serial_timeout = 0;
unsigned char heart_buff[5] = {0}, Id_Frame_Cnt = 0;
unsigned char calibmode, bhip_idframe;
volatile unsigned char Photo_feedback = 1;
volatile unsigned char half_sec=0;
unsigned char temp_frame_time;
volatile unsigned int minute_timer = 0;
volatile unsigned int  idframe_frequency_cnt = 0;
unsigned int idframe_frequency = 0;
volatile unsigned int RF_communication_check_Counter = 0;
unsigned int ID_frame_timeout = 0;
unsigned int HH = 0;
unsigned char sec_flag = 0, heart_beat_done = 0;

unsigned char read_energy_meter = 0;
unsigned char sec_timer_flag = 0;
unsigned char Do_force_on = 1;
unsigned char save_run_hour = 0;
unsigned char one_sec = 0;
unsigned char temp_slc_event = 0, temp_slc_event1 = 0;
volatile unsigned int rf_event_send_counter = 0;
unsigned int temp_timer_delay = 0;

unsigned char send_all_rf_event = 0;

float power_on_count = 0.0;
unsigned char Master_event_generation = 0;
unsigned char event_counter = 0;

unsigned char Time_Out = 0;
volatile unsigned char DaliTimeOut = 0;
volatile unsigned char GPS_timeout = 0;
volatile unsigned char RF_timeout = 0;
volatile unsigned char EM_timeout=0;
volatile unsigned char cheak_emt_timer_sec = 0;

volatile unsigned int Photocell_int_counter = 0;
volatile unsigned int Photocell_int_timer = 0;

unsigned char Pro_count = 0, Protec_lock = 0, protec_flag = 0;
unsigned int Protec_sec = 0;
unsigned char Start_dim_flag = 0;

unsigned char send_event_with_delay = 0;
volatile unsigned long int send_event_delay_timer = 0;

extern unsigned char Dimming_test_dicision; // SAN
extern float Dimvalue; // SAN

unsigned int Time_change_dueto_DST_Timer = 0;
unsigned char run_m_sch = 0;
unsigned char packet_length, temp_var = 0;
unsigned char Cali_Cnt = 0;
extern unsigned int Client, track_id;
volatile unsigned int nic_reset_timer = 0; //,temp_timeslice;
unsigned char ID_Frame_reset_time = 0, Normal_reset_time = 0, nic_reset_cnt3 = 0, nic_reset_cnt2;
unsigned char Commissioning_flag = 0;
unsigned char DI_GET_LOW = 0, DI_GET_HIGH = 0, power_on_batt = 0;
float ADC_Result_100 = 0, ADC_Result_50 = 0, ADC_Result_0 = 0;
unsigned long int push_time_slice_ms = 0;
volatile unsigned char ext;
///////////////////////////////////Production ////////////////////////////

unsigned char macLongAddrArray[10], RF_Modem_detected = 0, enter_boot_mode;
volatile unsigned long int slice_milli = 0;
unsigned char start_milli_sec = 0, heart_beat_ok = 0, NIC_check_cnt = 0, heart_cnt = 0, do_reset_once = 0, heart_beat_response = 1;
unsigned int cn = 0;
unsigned char lograte = 1, Energy_data[41]={0};
volatile unsigned char push_data_send_cnt = 0;
volatile unsigned char send_data_send_cnt = 0;
unsigned char push_data_send = 1,  Energy_data_cnt = 0, Push_Energy_meter_data = 1, send_data_send = 1;
unsigned char EnergyMin[] = {15, 30, 45, 0}, Energy_back_data[41];
volatile unsigned char energy_data_send = 0, energy_data_send_cnt = 0, fill_em_data_once = 0, check_reset_once = 0;
volatile unsigned char feel_event_cnt = 0;
volatile unsigned char pushdata_retry_timer = 0;
volatile unsigned char senddata_retry_timer;
volatile unsigned char  event_push_retry_timer = 0;
unsigned char Push_backward_comp, push_cnt = 0, energy_cnt = 0,  cert_load = 0;
unsigned int amr_id_2byte = 0; //SCAL 
extern BYTE_VAL GPS_error_condition;
extern unsigned char CURV_TYPE; 
/////////////////////////////////////////////////////////////////////////
#define IO_Control1			LATFbits.LATF1
#define IO_Control2			LATFbits.LATF0 
extern float utcOffset;
extern unsigned char Tilt_Enable;
extern volatile unsigned char day_burner_timer;
extern unsigned int  cn, time_slice;
extern unsigned char Interface_down, Multicast_Interface_down, temp_hour, temp_arr_event[];
//unsigned char DimmerDriverSelectionProcess = 0, DimmerDriver_SELECTION = 0x02;
volatile unsigned char CheckAoDimmerForDetectionTimmer = 0, CheckAoDimmerForDetection = 0;
float full_brightPower, full_brightPower_at_30;
unsigned char test_EEPROM(void);
void get_push_lograte(void);

//********************************************//
unsigned char Time_Sync_GPS = 0, Lmin = 0;
volatile unsigned char Store_LastGasp_Data = 0;
unsigned int vRCON = 0;
extern unsigned char test_Dim;
uint8_t temp_flag = 1, Lsec1 = 0;
uint8_t temp_buf12[130] = {0};
uint8_t serialprint_flag = 1;
volatile unsigned int SSN_CNT = 0;

int main(void)
    {
    NOP();
    NOP();
    NOP();
    NOP();
    vRCON = (unsigned int)RCON;
    delay(1000); // initial start up delay
    RCON = 0;
    ClrWdt();
    HardwareInit();
    LATBbits.LATB12 = 1;
    delay(1000); // delay after hardware initialization
    sendonce = 1;
    Read_para_EEPROM(); // read value from EEPROM  
    //TRISBbits.TRISB12 = 1;
    ClrWdt();
    check_reset_once = 1;
    do_reset_once = 1;
    calibmode = 0; // after calibration over make this flag as zero for normal 
    //#if defined(DALI_SUPPORT)
    DALIT = 1;
    DALIE = 0;
    AppDimmerAutoDetectInit();
    //#endif

    temp_slc_event = 1;
    Motion_detected = 0;
    Commissioning_flag = 0;
    Set_Do(0);
    Dimming_start_timer = 0;
    Start_dim_flag = 0;
    error_condition1.bits.b6 = 0; // clear dimming trip if occurse faulse.
    Pro_count = 0;
    Temp_Slc_Multi_group = check_multi_group(Group_val1.Val);
    if (GPS_Read_Enable == 1)
        {
        need_to_send_GPS_command = 1;
        }
    else
        {
        need_to_send_Shutdown_command = 1;
        }
    power_on_count++;
    pulseCounter.float_data = power_on_count;
    WriteEEPROM(EE_POWERONCNT, pulseCounter);

    push_time_slice_ms = ((amr_id % 1000) * time_slice) + ((rand() % (time_slice - 90)) + 100);
    get_push_lograte();
    send_id_frame_flage = 1;
   Initialize(); // Force initialize, since we're initializing both channels at once
    beginA(SC16IS740_DEFAULT_SPEED);
    /*
RF_Modem_detected = 1;
idframe_frequency = 0;
ID_frame_timeout = 30;
push_time_slice_ms = 0;*/
    while (1)
        {
        ClrWdt();
        ProcessNONZigBeeTasks();
        }
    return 1;
    }

void ProcessNONZigBeeTasks(void)
    {
    time_t t_of_day;
    struct tm t, *timeinfo;
    unsigned char TERMSTR[10];
    char buffer[80];

    // *********************************************************************
    // Place any non-ZigBee related processing here.  Be sure that the code
    // will loop back and execute ZigBeeTasks() in a timely manner.
    // *********************************************************************
    last_gasp_detect();

    if (DimDriverStatus == DIM_DRIVER_STATUS_DETECTION)
        {
        ClrWdt();
        detectDimmerDriver();
        }
    //***********************************************************************//
    if (Push_Energy_meter_data == 1)
        {
        lograte = 1;
        get_push_lograte();
        Fill_Energy_data();
        send_push_data();
        }
    else if (Push_Energy_meter_data == 0)
        {
        get_push_lograte();
        send_push_data();
        }

    if (Tilt_Enable == TILT_VAL)//#define TILT_VAL 240
        {
        Tilt_Sensor_Detect();
        }
    ADC_Process();
    fill_event_data();
    reset_nic();
    //   I2CRxData();
    if (one_sec == 1)
        {
        one_sec = 0;
        if ((fill_em_data_once == 0x20)&&(Id_frame_received == 1))
            {

            fill_em_data_once = 0;

            //Fill_Energy_data_PowerON();
            Fill_Energy_data_PowerON(15, 4, 0);
            }

        GPS_Delay_timer++;
        if (GPS_Delay_timer >= 5)
            {
            GPS_Delay_timer = 0;
            if (need_to_send_GPS_command == 1)
                {
                send_gps_command();
                }
            else if (need_to_send_Shutdown_command == 1)
                {
                shutdown_GPS();
                }
            }
        if ((error_condition1.bits.b2 == 1)&&(RTC_Faulty_Shift_to_Local_Timer == 1))
            {
            Fill_RTC_BY_Local_Timer();
            if ((Time.Hour == 0) || (Time.Hour == 8) || (Time.Hour == 16))
                {
                if (Time_Sync_GPS == 0)
                    {
                    Time_Sync_GPS = 1;
                    Time_set_by_GPS = 0; // set time by gps, while gps valid string available.
                    if (GPS_error_condition.bits.b7 == 1) // invoke GPS if available so if sender's time wrong then SLC correct it by GPS.
                        {
                        need_to_send_GPS_command = 1;
                        }
                    }
                }
            else
                {
                Time_Sync_GPS = 0;
                }
            }
        else
            {
            read_PCF85363_bytes();
            }
        t.tm_year = (Date.Year + 2000) - 1900;
        t.tm_mon = Date.Month - 1; // Month, 0 - jan
        t.tm_mday = Date.Date; // Day of the month
        t.tm_hour = Time.Hour;
        t.tm_min = Time.Min;
        t.tm_sec = Time.Sec;
        t.tm_isdst = 0; // Is DST on? 1 = yes, 0 = no, -1 = unknown

        t_of_day = mktime(&t);
        t_of_day = t_of_day - 43200;
        timeinfo = localtime(&t_of_day); //Get time 			
        strftime(buffer, 80, "%H%M%S%d%m%y", timeinfo); //Get min fromTime string   %02H%02M%02S%02d%02m%02y

        strncpy((char *) TERMSTR, (const char *) &buffer[0], 2);
        t.tm_hour = atoi((char *) TERMSTR);
        TERMSTR[0] = TERMSTR[1] = TERMSTR[2] = '\0';
        strncpy((char *) TERMSTR, (const char *) &buffer[2], 2);
        t.tm_min = atoi((char *) TERMSTR);
        TERMSTR[0] = TERMSTR[1] = TERMSTR[2] = '\0';
        strncpy((char *) TERMSTR, (const char *) &buffer[4], 2);
        t.tm_sec = atoi((char *) TERMSTR);
        TERMSTR[0] = TERMSTR[1] = TERMSTR[2] = '\0';
        strncpy((char *) TERMSTR, (const char *) &buffer[6], 2);
        t.tm_mday = atoi((char *) TERMSTR);
        TERMSTR[0] = TERMSTR[1] = TERMSTR[2] = '\0';
        strncpy((char *) TERMSTR, (const char *) &buffer[8], 2);
        t.tm_mon = atoi((char *) TERMSTR);
        TERMSTR[0] = TERMSTR[1] = TERMSTR[2] = '\0';
        strncpy((char *) TERMSTR, (const char *) &buffer[10], 2);
        t.tm_year = atoi((char *) TERMSTR);
        TERMSTR[0] = TERMSTR[1] = TERMSTR[2] = '\0';
        PTime.Sec = t.tm_sec;
        PTime.Min = t.tm_min;
        PTime.Hour = t.tm_hour;
        PDate.Date = t.tm_mday;
        PDate.Month = t.tm_mon;
        PDate.Year = t.tm_year;

        if (Id_frame_received == 1) // wait for ID frame sync with DCU to start event generation
            {
            if (Master_event_generation == 0) // check the master event generation for generate event
                {
                if ((temp_slc_event != error_condition.Val) || (temp_slc_event1 != error_condition1.Val)) // compare previous digital val with new digital val if miss-match generate event
                    {
                    Frm_no = 255;
                    if (Auto_event == 1)
                        {
                        if (((temp_slc_event | 0x20) != (error_condition.Val | 0x20)) ||
                            (temp_slc_event1 != error_condition1.Val)) // compare previous digital val with new digital val if miss-match generate event
                            {
                            send_event_with_delay = 1; // uncomment for auto event with 10 sec delay.
                            send_event_delay_timer = 0; // uncomment for auto event with 10 sec delay.
                            send_data_direct_counter = 0;
                            feel_event_cnt = 0;
                            }
                        }
                    temp_slc_event = error_condition.Val;
                    temp_slc_event1 = error_condition1.Val; // store event into local memory
                    }
                }
            }

#ifdef DEBUG
        if ((Time.Min != Lsec1)&&(serialprint_flag == 1))
            {

            sprintf(temp_buf12, "\r\nT:%02u/%02u/%02u %02u:%02u:%02u", Date.Date, Date.Month, Date.Year, Time.Hour, Time.Min, Time.Sec);
            WriteData_UART2(temp_buf12, strlen(temp_buf12));
       
                        sprintf(temp_buf12, " SLC:%lu ID:%u V:%4.2f I:%4.2f W:%4.2f KWH:%f F:%4.2f PF:%4.2f Dim:%d ", amr_id, Id_frame_received, vrdoub, irdoub, w1, kwh, Calculated_Frequency, pf, test_Dim);
                        WriteData_UART2(temp_buf12, strlen(temp_buf12));
                        if (set_do_flag)
                            {
                            WriteData_UART2(" RLY=ON", strlen(" RLY=ON"));
                            }
                        else
                            {
                            WriteData_UART2(" RLY=OF", strlen(" RLY=OF"));
                            }
//                        sprintf(temp_buf12, " RCON:0x%x uC:%.0f Mode:%d RTCFaulty:%u TimeByGPS:%u", vRCON, power_on_count, automode, RTC_faulty_detected, Time_set_by_GPS);
//                        WriteData_UART2(temp_buf12, strlen(temp_buf12));
                            sprintf(temp_buf12, " BH:%4.2f Photo:%4.2f TILT:%u DI=%u AI:%4.2f RCON:0x%x uC:%.0f ZC:%d ", lamp_burn_hour, Photosensor_FootCandle, TILT_DI1, MOTION_DI1, (((float) Analog_Result) / 1024)*3.3, vRCON, power_on_count, ZC_count);
                            WriteData_UART2(temp_buf12, strlen(temp_buf12));
            //                            sprintf(temp_buf12, "\n\r DALI Prc= %d, Driver = %d, Mux =%d, Resp=%d", DimmerDriverSelectionProcess, DimDriverStatus, DaliHwSelect_u8, answer1);
            //                            WriteData_UART2(temp_buf12, strlen(temp_buf12));

            //                sprintf(temp_buf12, "\r\nlat:%f long:%f", Latitude, longitude);
            //                WriteData_UART2(temp_buf12, strlen(temp_buf12)); //            WriteData_UART2(temp_buf12, strlen(temp_buf12));

            Lsec1 = Time.Min;
            }
#endif
        }
    send_fream();
    if ((SLC_Test_Query_2_received == 0)&& (Start_dimming_test == 0))
        {
        Read_EnergyMeter(); // read energy meter parameter
        if ((power_on_flag == 1) && (energy_meter_ok == 0)) // wait for power on flag for check logic and calculate energy parameter
            {
            power_on_flag = 0;
            error_condition1.bits.b4 = 0; // resent power on flag
            calculate_3p4w(); // need to change.  																					// claculate energy parameter
            check_interlock(); // check interlock 				
            }

        if (energy_time >= 60)
            {
            energy_meter_ok = 1;
            error_condition1.bits.b4 = 1;
            em_reset_counter++;

            //read_a_reg(0x01,0x1e);

            pulseCounter.byte[0] = datalow;
            pulseCounter.byte[1] = datamid;
            pulseCounter.byte[2] = datahigh;
            pulseCounter.byte[3] = 0x00;
            Em_status_reg = pulseCounter.long_data;
            //write_to_all_regs(0x74,0x00,0x00,0x80);
            init_cs5463();
            energy_time = 1;
            //					emt_pin_toggel = 0;																		// EMT fail Trip
            }

        if ((automode == 1) || (automode == 4) || (automode == 6) || (automode == 7)) //1 photocell 4 astro with photocell 6 civil twilight with photocell 7 mix mode trip check only in photocell mode
            {
            check_photocell_interlock(); // check photo cell related trip.

            }
        else
            {
            photo_cell_timer = 0;
            photo_cell_toggel_timer = 0;
            photo_cell_toggel_counter = 0;

            if (Photo_feedback == 1)
                {
                error_condition.bits.b5 = 1;
                }
            else
                {
                error_condition.bits.b5 = 0;
                }
            }
        ////////DST Logic///////
        check_Day_Light_Saving();
        ////////DST Logic///////

        ////////dimming logic////////////////////////////
        if (SLC_New_Manula_Mode_En == 0)
            {
            if (automode != 7)
                {
                if (ballast_type == 1) // electronic ballast selected
                    {
                    Master_Sch_Match = 0;
                    if (adaptive_light_dimming == 0) // normal dimming schedule based
                        {
                        check_dimming_protection();
                        if ((Start_dim_flag == 1) && (set_do_flag == 1))
                            {
                            Check_MASTER_Schedule(); // Master sch 15/04/11
                            }
                        }
                    else if (adaptive_light_dimming == 1) // adaptive light based dimming
                        {
                        check_dimming_protection();
                        if (lamp_lock == 0)
                            {
                            if ((Start_dim_flag == 1) && (set_do_flag == 1))
                                {
                                adaptive_dimming();
                                }
                            }
                        }

                    }
                else
                    {
                    Check_MASTER_Schedule(); // Master sch 15/04/11	
                    }
                ////////dimming logic////////////////////////////

                if (Master_Sch_Match == 0) // Master sch 15/04/11
                    {
                    check_logic();
                    }
                }
            else
                {
                if ((RTC_faulty_detected == 1) || (RTC_New_fault_logic_Enable == 0))
                    {
                    if (run_m_sch == 1)
                        {
                        run_m_sch = 0;
                        if ((error_condition.bits.b2 == 0) || (Lamp_lock_condition == 0))
                            {
                            check_Mix_Mode_schedule();
                            }
                        }
                    if ((error_condition.bits.b2 == 0) || (Lamp_lock_condition == 0)) // check if vlotage under-over trip or Lamp lock condition not occur
                        {
                        Run_Mix_Mode_Sch(); // if mix-mode schedule match then do lamp operation according to value.
                        }
                    }
                else
                    {
                    previous_mode = automode; // store mode value before chage.
                    WriteByte_EEPROM(EE_PREV_MODE, previous_mode); // store mode in to NVM.
                    automode = 1;
                    WriteByte_EEPROM(EE_LOGICMODE, automode);

                    }
                /////////////Mix mode logic////////////																									
                }
            }
        else
            {
            if ((error_condition.bits.b2 == 0) || (Lamp_lock_condition == 0))
                {
                if (SLC_New_Manula_Mode_Val == 100)
                    {
                    Set_Do(0);
                    }
                else
                    {
                    Set_Do(1);
                    check_dimming_protection();
                    if ((Start_dim_flag == 1) && (set_do_flag == 1))
                        {
                        dimming_applied(SLC_New_Manula_Mode_Val); // Master sch 15/04/11
                        }
                    }
                if (SLC_New_Manual_Mode_Timer > 0)
                    {
                    if (SLC_New_Manual_Mode_counter >= (SLC_New_Manual_Mode_Timer * 60))
                        {
                        SLC_New_Manual_Mode_Timer = 0;
                        SLC_New_Manual_Mode_counter = 0;
                        SLC_New_Manula_Mode_Val = 101;
                        SLC_New_Manula_Mode_En = 0;
                        WriteByte_EEPROM(EE_SLC_New_Manual_Mode_Timer + 0, SLC_New_Manual_Mode_Timer / 256);
                        WriteByte_EEPROM(EE_SLC_New_Manual_Mode_Timer + 1, SLC_New_Manual_Mode_Timer % 256);
                        WriteByte_EEPROM(EE_SLC_New_Manula_Mode_Val, SLC_New_Manula_Mode_Val);
                        }
                    }
                }
            }

        if ((Time.Hour == 12) && (Time.Min == 0) && (Time.Sec == 0)) // calculate the new sunset or sunrise time at every 12:00 PM o'clock
            {
            GetSCHTimeFrom_LAT_LOG();
            EEWriteAstroSch();
            Day_light_harves_time();
            }
        ///////////////////////////// implemented as per new day burner logic in to ember SAN //////////////////////				
        if ((check_day_burnar_flag == 1) && (day_burner_timer >= 3)) //check day burnar happening of not after Lamp switch from on/off 
            {
            day_burner_timer = 0;
            check_day_burnar_flag = 0; // read lamp status after 3 sec to became current stable.
            Check_day_burning_fault(); // check day burning fault.
            }

        }
    else
        {
        if (Start_dimming_test == 0)
            {
            Set_Do(0);
            }
        else
            {
            Lamp_off_on_Time = 0;
            Set_Do(1);
            dim_inc_val = 0;
            dimming_applied(Test_dimming_val); // set percentage of dimming to configured value.

            }

        }
    // check slc is working in which logic
    ///////////////////////////
    if ((need_to_send_lat_long == 1) && (Auto_Gps_Data_Send == 1)&&(Id_frame_received == 1)) //&& (GPS_Data_Valid == 3)
        {
        need_to_send_lat_long = 0;
        send_get_mode_replay_auto = 1; // send lat/long value automatically when SLC join valid DCU first time.
        //        halCommonSetToken(TOKEN_need_to_send_lat_long,&need_to_send_lat_long);
        }
    GPS_timeout = 0;
//    if ((I2C_data_rec_flag == 1)&&(I2C_Timeout >= 2))
//        {
//        I2C_Timeout = 0;
//        I2C_data_rec_flag = 0;
//        WriteData_UART2("\r\nI2C Rx: ", strlen("\r\nI2C Rx: "));
//        WriteData_UART2(I2C_data_Buff, Rec_count);
//
//        Rec_count = 0;
//        }
    if ((data_rec_GPS == 1) && (GPS_serial_timeout >= 3))
        {
        if (GPS_Read_Enable == 1)
            {
            GPS_Read_stop = 1; // wait for receive compleate string 
            Parse_GPS_Query(Serial_GPS_Buff, temp_rx_GPS);
            check_Gps_Statistics();
            }
        for (HH = 0; HH < B_SIZE_SERIAL_GPS; HH++)
            {
            Serial_GPS_Buff[HH] = 0;
            }
        GPS_serial_timeout = 0; // parse RF string
        temp_rx_GPS = 0; // flush buffer
        data_rec_GPS = 0;
        GPS_Read_stop = 0;
        if (U2STAbits.OERR != 0)
            {
            U2STAbits.OERR = 0;
            }
        }
    /////////////////////////////
    RF_timeout=0;
    if ((data_rec_RF == 1)&&(RF_serial_timeout >= 4)) // ##Rht check if data comes on RF serail port																							
        {
        delay(8000); // wait for receive compleate string 
        Parse_RF_Packet(Serial_RF_Buff); // parse RF string
        temp_rx_RF = 0; // flush buffer
        data_rec_RF = 0;
        RF_serial_timeout = 0;
        if (U1STAbits.OERR != 0)
            {
            U1STAbits.OERR = 0;
            }
        LED_2 = !LED_2; // Togeel LED2 if RF packet received
        }
    else
        {

        if (Id_frame_received == 0) // 30									// Send ID fream after every 30 second until not get replay from DC
            {
            if (idframe_frequency_cnt >= idframe_frequency)
                {
                if (Id_Frame_Cnt < 3)
                    {
                    if (RF_communication_check_Counter >= ID_frame_timeout)
                        {
                        start_milli_sec = 1;
                        if (slice_milli >= push_time_slice_ms)
                            {
                            start_milli_sec = 0;
                            Id_Frame_Cnt++;
                            send_id_frame = 1;
                            RF_communication_check_Counter = 0;
                            slice_milli = 0;
                            }
                        }
                    }
                }
            }
        else if (Id_frame_received == 1) // If ID received from DCU then send Data or event when generate
            {
            if (read_event_flag == 1) // Send pending event when DCU send read event command
                {
                if ((Unicast_time_delay / 3) >= 5)
                    {
                    if (rf_event_send_counter >= Unicast_time_delay / 3)
                        {
                        rf_event_send_counter = 0;
                        send_all_rf_event = 1;
                        }
                    }
                else
                    {
                    if (rf_event_send_counter >= 5)
                        {
                        rf_event_send_counter = 0;
                        send_all_rf_event = 1;
                        }
                    }
                }


            }
        else
            {
            }
        }
    if (RF_network_faulty == 0)
        {
        if (RF_network_time_out >= 24) // acthual value 48 for timeout after 2 days
            {

            RF_network_time_out = 0;

            }
        }
    //  Check_Slc_Peripheral(); // Remove as per Nsandip n Vk discussion



    if (Read_db_val == 1)
        {
        Read_db_val = 0;

        }

    if ((change_Link_key_timer > 30) && (change_Link_key == 1))
        {
        change_Link_key = 0;
        change_Link_key_timer = 0;

        }
    if (RTC_New_fault_logic_Enable == 1) // check for logic enable or not.
        {
        if ((error_condition1.bits.b2 == 1)&&(RTC_Faulty_Shift_to_Local_Timer == 1)&&(RTC_need_to_varify == 1)&&(check_RTC_logic_cycle == 1))//condition becomes true  when synce either by GPS or DCU.
            {
            read_PCF85363_bytes(); //to check if RTC is ok after sync by GPS or DCu.
            check_RTC_faulty_logic();
            Fill_RTC_BY_Local_Timer(); //IF rtc NOT OK file time by local timer
            }
        else
            {
            check_RTC_faulty_logic();
            }
        }

    }


/*******************************************************************************
HardwareInit

All port directioning and SPI must be initialized before calling ZigBeeInit().

For demonstration purposes, required signals are configured individually.
 *******************************************************************************/
#if defined(__C30__)

void HardwareInit(void)
    {
    PIN_Initialize();
    INTERRUPT_Initialize();
    CLOCK_Initialize();
    TRISFbits.TRISF5 = 0; //I2C clock pin, will always remain in output mode.
    TRISFbits.TRISF4 = 0; // No Open Drain config, used as IO change mode.

    LATFbits.LATF5 = 0;
    LATFbits.LATF4 = 0;
    IFS0bits.T2IF = 0; // Clear Output Compare 1 interrupt flag 	 
    IEC0bits.T2IE = 0; // Enable Output Compare 1 interrupts 	 
    T2CONbits.TON = 0; // Start Timer2 with assumed settings
    T3CONbits.TON = 0; // Start Timer2 with assumed settings
    T2CONbits.T32 = 0;
#ifdef USE_INTERNAL_FLASH  //added by hitesh
    DataEEInit();
    dataEEFlags.val = 0;
#endif
    //******************KWh***************************//
    IFS0bits.INT0IF = 0;
    INTCON2bits.INT0EP = 0;
    IEC0bits.INT0IE = 1;

    //******************ZC***************************//
    IFS1bits.INT1IF = 0; // 
    INTCON2bits.INT1EP = 1; //falling edge
    IEC1bits.INT1IE = 1; // Enable INT1 Interrupt Service Routine 

    //******************PWM 5k***************************//
    SET_PWM_FREQ();
    CISCO_EPF = 1;
    //#if defined(DALI_SUPPORT)
    DALIT = 1;
    //#endif 

    Dimvalue = 100.0;
    error_condition1.bits.b6 = 0; // Dimming short circuit event generation.

    InitTimer1();
    InitTimer4();
    Configure_UART1(); //RF
    Configure_UART3(); //GPS
    Configure_UART4(baudRate_default); //EM

    init_PCF85363();
    init_cs5463();
    AO_DALI_selection(); //AI-DI or RS485 selection
    Configure_UART2(); //RS485
    ADCInit();
    ADCStart();
    check_power_cycle();
    cert_load = 0x7f; // Init Energy Meter IC
    //WriteData_UART2("\r\n\r\n......APP CODE", strlen("\r\n\r\n......APP CODE"));
    }

#endif

/*******************************************************************************
User Interrupt Handler

The stack uses some interrupts for its internal processing.  Once it is done
checking for its interrupts, the stack calls this function to allow for any
additional interrupt processing.
 *******************************************************************************/
#if defined(__C30__)

void __attribute__((interrupt, no_auto_psv)) _ISR _IC4Interrupt(void)
    {
    volatile uint8_t dumyRead = 0;
    dumyRead = TMR3;
    TMR3 = 0;
    IFS2bits.IC4IF = 0; // Reset respective interrupt flag
    cap_cnt++;
    loop_cnt2 = 0;
    //Time_Out_1 =0;
    //WriteData_ModbusUART2("T", 1);
    if (DALIR == 1)
        {
        while (IC4CON1bits.ICBNE)
            {
            loop_cnt2++;
            if (loop_cnt2 > 100)
                {
                break;
                }
            //low_time = IC4BUF; // rising, so capture low tim
            }
        low_time = dumyRead;
        DaliLowTime_u16[DaliLowTmrCntr++] = low_time;
        DaliTime_u16[DaliTotalCntr] = low_time;
        //WriteData_ModbusUART2("H", 1);
        if (frame != 0) // not first pulse ?
            {
            cnt1++;
            cnt5++;
            DALI_Decode(); // decode received bit
            }
        else
            {
            cnt2++;
            previous = 1; // first pulse, so shift 1
            DALI_Shift_Bit(1);
            }
        }
    else
        {
        loop_cnt2 = 0;
        //WriteData_ModbusUART2("L", 1);
        high_time = dumyRead;
        while (IC4CON1bits.ICBNE)
            {
            loop_cnt2++;
            if (loop_cnt2 > 100)
                {
                break;
                }
            //high_time = IC4BUF; // falling, so capture high time

            }
        DaliTime_u16[DaliTotalCntr] = high_time;
        DaliHighTime_u16[DaliHighTmrCntr++] = high_time;
        cnt6++;
        cnt3++;
        }
    DaliTotalCntr++;
    count11++;
    answer = count11;
    answer1 = frame;
    //IC4BUF =0x00;
    TMR3 = 0;
    //IC4TMR = 0;
    //IC4CON2bits.TRIGSTAT = 1;

    }


void __attribute__((interrupt, auto_psv)) _T1Interrupt(void)
    {
    IFS0bits.T1IF = 0;

    mscnt++;
    LastGasp_Timer++;
    read_dim_val_timer1=read_dim_val_timer1+5;
    if (start_milli_sec == 1)
        {
        slice_milli = slice_milli + 5;
        }
    I2C_Timeout++;
    RF_serial_timeout++;
    GPS_serial_timeout++;

    if (LastGasp_Timer <= 9) //no need to reset this timer since hardware must be restart befor overflow the variable.
        {
        lastgasp_cnt = 0;
        lastgasp_retry = 0;
        last_gasp_detected = 0;
        }
    else
        {
        last_gasp_detected = 1;
        }

    if (TILT_DI1 == 1) //reverse logic
        {
        Tilt_Cnt = 0;
        }
    Tilt_Cnt++;

    lastgasp_retry_timer++;
    send_event_delay_timer = send_event_delay_timer + 5; // uncomment for auto event with 10 sec delay.
    if (mscnt >= 100)
        {
        ReadEM = 1;
        mscnt = 0;
        half_sec++;
        feel_event_cnt++; // increment Half_sec counter
        Tilt_Timeout_Cnt++;
        one_sec = 1;
        Motion_half_sec = 1;
        run_m_sch = 1;
        if (half_sec >= 2)
            {
            half_sec = 0;
            CheckAoDimmerForDetectionTimmer++;
            push_data_send_cnt++;
            send_data_send_cnt++;
            read_dim_val_timer++;
            if (push_data_send_cnt >= 180)
                {
                push_data_send_cnt = 0;
                push_data_send = 1;
                }

            if (send_data_send_cnt >= 180)
                {
                send_data_send_cnt = 0;
                send_data_send = 1;
                }
            energy_data_send_cnt++;

            if (energy_data_send_cnt >= 180)
                {
                energy_data_send_cnt = 0;
                energy_data_send = 1;
                }

            if (DaliHwSelect_u8)
                {
                dali_delay++;
                }
            nic_reset_timer++;
            RF_communication_check_Counter++;
            rf_event_send_counter++;
            Dimming_start_timer++;
            lamp_off_on_timer++;
            RF_timeout++;
            GPS_timeout++;
            EM_timeout++;
            Time_Out++;
            DaliTimeOut++;
            test_query_1_send_timer++;
            cheak_emt_timer_sec++;
            Get_Sch_timer++; // V6.1.10
            Get_Master_Sch_timer++; // V6.1.10
            dim_inc_timer++;
            Motion_detect_sec++;
            motion_broadcast_timer++;
            ///////// idimmer /////////		
            Send_dimming_cmd++;

            send_id_frame_time++; // SAN

            day_burner_timer++; //Day burner logic as per ember SAN

            energy_time++;
            if (lamp_off_on_timer >= Lamp_off_on_Time)
                {
                lamp_off_on_timer = Lamp_off_on_Time;
                }
            if (set_do_flag == 1)
                {
                if (Dimming_start_timer >= dim_applay_time)
                    {
                    Start_dim_flag = 1;
                    }
                }
            Increment_Local_Timer();
            Sec_Counter++;
            sec_flag = 1;
            if (chk_interlock == 1) // if lamp on increment timer related current interlock
                {
                check_current_counter++;
                check_current_firsttime_counter++;
                SSN_CNT++;
                if (SSN_CNT > 3600)
                    {
                    SSN_CNT = 0;
                    Lamp_Balast_fault_Remove_Cnt++;
                    }
                }

            Relay_weld_timer++;
            check_counter++;
            if (broadcast_received == 1)
                {
                broadcsat_received_timer++;
                if (broadcsat_received_timer > 3)
                    {
                    broadcsat_received_timer = 0;
                    broadcast_received = 0;
                    }
                }
            if (change_Link_key == 1)
                {
                change_Link_key_timer++;
                }

            if ((SLC_New_Manual_Mode_Timer > 0) && (SLC_New_Manula_Mode_En == 1))
                {
                SLC_New_Manual_Mode_counter++;
                }

            ////////////////////////////////////////////////////////////////////////////////////////	

#ifdef NEW_LAMP_FAULT_LOGIC
            one_sec_cyclic = 1;
            if (Lock_cyclic_check == 1)
                {
                Lock_cyclic_Counter++;
                if (Lock_cyclic_Counter >= 1200)
                    {
                    Lock_cyclic_check = 0;
                    }
                }
#elif defined(OLD_LAMP_FAULT_LOGIC)

#else
#error "Please define LAMP_FAULT_LOGIC_TYPE"
#endif
            RTC_power_on_Logic_Timer++;
            if (RTC_power_on_Logic_Timer >= 60)
                {
                RTC_power_on_Logic_Timer = 0;
                check_RTC_logic_cycle = 1;
                }
            //////////////////////////////////////////////////////////////////////////////////////
            if (Sec_Counter > 59)
                {
                idframe_frequency_cnt++;
                Sec_Counter = 0;
                minute_timer++; // increment minute counter
                pushdata_retry_timer++;
                senddata_retry_timer++;
                event_push_retry_timer++;
               
                if ((minute_timer >= storedataTime)&&(Store_LastGasp_Data == 0))
                    {
                    Store_LastGasp_Data = 1;
                    }
                if (minute_timer > 1)
                    {
                    serialprint_flag = 1;
                    IO_Control1 = 1; //RS485
                    IO_Control2 = 0; //Normal
                    }
                ///////////////////////////////////////////////////////////////////////////////////	

#ifdef NEW_LAMP_FAULT_LOGIC

#elif defined(OLD_LAMP_FAULT_LOGIC)
                if (minute_timer == 30)
                    {
                    cycling_lamp_fault = 0; // reset cyclic lamp fault trip at every 30 minute.
                    }
#else
#error "Please define LAMP_FAULT_LOGIC_TYPE"
#endif

                //////////////////////////////////////////////////////////////////////////////////////////////////////			

                if (error_condition.bits.b0 == 1) // check lamp on DI for increment run hour.
                    {
                    lamp_burn_hour++; // if Lamp on set Lamp burn hour inc flag
                    }
                if (minute_timer > 59)
                    {
                    minute_timer = 0;
                    event_counter = 0; // reset event counter at every hour
                    photo_cell_timer++;
                    photo_cell_toggel_timer++;
                    //				power_on_count++;													// increment power on counter at every hour
                    if (RF_network_faulty == 0)
                        {
                        RF_network_time_out++;
                        }
                    }

                if ((Time_change_dueto_DST == 2))
                    {
                    Time_change_dueto_DST_Timer++;
                    if (Time_change_dueto_DST_Timer > (SLC_DST_Time_Zone_Diff + 5))
                        {
                        Time_change_dueto_DST = 0;
                        Time_change_dueto_DST_Timer = 0;
                        }
                    }
                }

            Photocell_int_timer++;

            if (Photocell_int_timer >= Photocell_unsteady_timeout_Val) // read photocell at every 15 sec.
                {
                Photocell_int_timer = 0;
                if (Photo_feedback_pin == 1)
                    {
                    Photo_feedback = 0;
                    Photocell_Off_Condition = 0;
                    photocell_off_Cnt = 0;
                    }

                }
            if (Photo_feedback_pin == 1) //off time 
                {
                photocell_off_Cnt = 0;
                Photocell_Off_Condition = 0;
                }
            else
                {
                Photocell_Off_Condition = 1;
                }
            if (Photocell_Off_Condition == 1)
                {
                photocell_off_Cnt++;
                if (photocell_off_Cnt > Photocell_unsteady_timeout_Val)
                    {
                    photocell_off_Cnt = 0;
                    Photo_feedback = 1;
                    Photocell_Off_Condition = 0;
                    Photocell_int_timer = 0;
                    }
                }
            motion_dimming_timer++;
            motion_intersection_dimming_timer++;
            if ((Motion_Continiue == 0) && (Motion_detected == 1))
                {
                Motion_continue_timer++;
                }
            else
                {
                Motion_continue_timer = 0;
                }

            if (Motion_Received_broadcast == 1)
                {
                Motion_Received_broadcast_counter++;
                if (Motion_Received_broadcast_counter > Motion_Broadcast_Timeout)
                    {
                    Motion_Received_broadcast_counter = 0;
                    Motion_Received_broadcast = 0;
                    }
                }

            if (Motion_countr >= Motion_pulse_rate)
                {
                if (Motion_detected == 0)
                    {
                    Motion_detected = 1;
                    Motion_intersection_detected = 0;
                    if (Temp_Slc_Multi_group == 1)
                        {
                        Motion_Received_broadcast = 0;
                        motion_dimming_Broadcast_send = 0;
                        }
                    if (Motion_Received_broadcast == 0)
                        {
                        motion_dimming_timer = 0;
                        Motion_detect_sec = 0;
                        }

                    }
                }
            Motion_countr = 0;
            }
        }
    }

void __attribute__((interrupt, auto_psv)) _INT0Interrupt(void)
    {
    if (!(calibmode))
        {
        intesec++;
        }
    else
        {
        pulses_r++;
        }
    Kwh_count++;
    IFS0bits.INT0IF = 0;
    }

void __attribute__((interrupt, auto_psv)) _INT1Interrupt(void)
    {
    //	Photocell_int_counter++;
    IFS1bits.INT1IF = 0;
    // ZC_count++;
    LastGasp_Timer = 0; //Clear the INT1 interrupt flag or else
    }

void __attribute__((interrupt, auto_psv)) _T2Interrupt(void)
    {
    IFS0bits.T2IF = 0;
    }

void __attribute__((interrupt, auto_psv)) _T4Interrupt(void)
    {
    IFS1bits.T4IF = 0; //Clear the TIMER 4 interrupt flag or else
    Protec_sec++;
    if (Protec_sec >= 1500) protec_flag = 1;
    // increment Half_sec counter
    }

void __attribute__((interrupt, auto_psv)) _T3Interrupt(void) //SAN   DALI TIMER ISR
    {
    if (value)
        DALIT = 1; //DALIT=1;
    else if (value == 0x00)
        DALIT = 0; //DALIT=0;

    if (position == 0) // 0TE second half of start bit = 1 
        {
        value = 1;
        }
    else if (position < 33) // 1TE - 32TE, so address + command 
        {

        value = (forward >> ((32 - position) / 2)) & 1;
        if (position & 1)
            value = !value; // invert if first half of data bit 
        }
    else if (position == 33) // 33TE start of stop bit (4TE) 
        { // and start of min settling time (7TE) 
        value = 1;
        }
    else if (position == 44) // 44TE, end stop bits + settling time 
        { // receive slave answer, timeout 
        //PR2 = 0x019A;								 // this is for with PLL enable =  9174;                               
        PR3 = 0x07D0;
        capture();
        //init_capture();   //look
        //T0CCR =  0x0007;                              // enable rx, capture on both edges 
        }
        //else if (position ==  68)                            // end of transfer 
    else if (position >= 45)
        {

        T3CONbits.TON = 0;
        TMR3 = 0;

        //T0TCR =  2;                                   // stop and reset timer 
        if (frame & 0x100) // backward frame (answer) completed? 
            {
            answer = (unsigned char) frame; // OK ! save answer 
            f_dalirx = 1; // and set flag to signal application
            DaliRx_Status = 1;
            }
        frame = 0; // reset receive frame 
        f_busy = 0; // end of transmission 
        //if (f_repeat) 
        //	f_dalitx =  1;                                // yes, set flag to signal application 

        }
    position++;

    IFS0bits.T3IF = 0;

    }
#endif 

void capture(void)
    {
    unsigned char DummyData1 = 0, DummyData2 = 0;

    TMR3 = 0;

    IC4CON2 = 0x0000;
    IC4CON1 = 0x0001;

    // SYNCSEL TMR3; TRIGSTAT disabled; IC32 disabled; ICTRIG Sync; 
    //IC4CON2 = 0x00;
    //IC4CON2bits.SYNCSEL =0;
    //IC4CON2bits.ICTRIG = 1;
    //IC4CON2bits.TRIGSTAT = 1;
    IFS2bits.IC4IF = 0; // Clear the IC1 interrupt status flag
    IEC2bits.IC4IE = 1; // Enable IC1 interrupts
    IPC9bits.IC4IP = 1; // Set module interrupt priority as 1

    //IC4CON=0x0001;

    //IC4CON2=0x0017;
    }

void Read_para_EEPROM(void)
    {
    unsigned int temp, i, j, n = 0;
    unsigned char temp1, temp2;
    unsigned char ee_default_state = 0, Set_default_perameter = 0;

    temp1 = ReadByte_EEPROM(EE_FRESH_LOCATION_1);
    temp2 = ReadByte_EEPROM(EE_FRESH_LOCATION_2);

    if ((temp1 != 0xCC) && (temp2 != 0xCC))
        {
        for (i = 0; i < EE_END_LOC; i++)
            {
            WriteByte_EEPROM(i, 0xFF);
            }
        ee_default_state = 1;
        Set_default_perameter = 1;


        WriteByte_EEPROM(EE_FRESH_LOCATION_1, 0xCC);
        WriteByte_EEPROM(EE_FRESH_LOCATION_2, 0xCC);
        }

    automode = ReadByte_EEPROM(EE_LOGICMODE);
    if (automode > 7)
        {

        automode = 1;
        WriteByte_EEPROM(EE_LOGICMODE, automode);
        }

    temp = ReadByte_EEPROM(EE_DO);


    if (automode == 0)
        {
        if (temp == 1)
            {
            Set_Do_On = 1;
            }
        else if (temp == 0)
            {
            Set_Do_On = 0;
            }
        }
    DimmerDriver_SELECTION = ReadByte_EEPROM(EE_ADDR_DIM_DRIVER_INFO + 0);
    if (DimmerDriver_SELECTION == 0xFF)
        {
        //DimmerDriverSelectionProcess = DIM_DRIVER_SELECT_AO;
        //DimDriverStatus = DIM_DRIVER_STATUS_AO_SET;        

        DimmerDriverSelectionProcess = DIM_DRIVER_SELECT_AUTO_ON_POWER_CYCLE;
        DimDriverStatus = DIM_DRIVER_STATUS_DETECTION;

        DimmerDriver_SELECTION = (DimmerDriverSelectionProcess << 4) | (DimDriverStatus);
        WriteByte_EEPROM(EE_ADDR_DIM_DRIVER_INFO + 0, DimmerDriver_SELECTION);
        }
    Client = ((ReadByte_EEPROM(EE_CLIENT_ID) * 256) + ReadByte_EEPROM(EE_CLIENT_ID + 1));
    if ((Client > 65000) || (Client == 0))
        {
        Client = 1;
        WriteByte_EEPROM(EE_CLIENT_ID, Client / 256);
        WriteByte_EEPROM(EE_CLIENT_ID + 1, Client % 256);

        }



    enter_boot_mode = ReadByte_EEPROM(EE_ENT_BOOT);
    //sprintf(buffer,"enter_boot=%x \r\n",enter_boot_mode);	
    //WriteData_UART2(buffer,strlen(buffer));
    if (enter_boot_mode == 0xFF)
        {
        //sprintf(buffer,"Ent_boot_mode \r\n");	
        //WriteData_UART2(buffer,strlen(buffer));
        enter_boot_mode = 20;
        WriteByte_EEPROM(EE_ENT_BOOT, enter_boot_mode);
        }



    Push_backward_comp = ReadByte_EEPROM(EE_BACKWARD_COMP);
    //sprintf(buffer,"Push_back=%x \r\n",Push_backward_comp);	
    //WriteData_UART2(buffer,strlen(buffer));
    if ((Push_backward_comp == 0xFF))
        {
        //sprintf(buffer,"Push_back_ent=%x \r\n",Push_backward_comp);	
        //WriteData_UART2(buffer,strlen(buffer));
        Push_backward_comp = 20;
        WriteByte_EEPROM(EE_BACKWARD_COMP, Push_backward_comp);
        lograte = 1;
        WriteByte_EEPROM(EE_LOGRATE, lograte);
        }

    lograte = ReadByte_EEPROM(EE_LOGRATE);
    if ((lograte > 12) || (lograte == 0))
        {
        lograte = 1;
        WriteByte_EEPROM(EE_LOGRATE, lograte);
        lograte = ReadByte_EEPROM(EE_LOGRATE);
        //sprintf(buffer,"lograte=%x \r\n",lograte);	
        //WriteData_UART2(buffer,strlen(buffer));
        }

    if ((enter_boot_mode == 30) && (Push_backward_comp == 20))
        {


        Push_backward_comp = 30;
        WriteByte_EEPROM(EE_BACKWARD_COMP, Push_backward_comp);

        pulseCounter = ReadEEPROM(EE_AMR_ID_4byte);
        amr_id = pulseCounter.long_data;
        if ((pulseCounter.byte[3] == 0xFF) && (pulseCounter.byte[2] == 0xFF) && (pulseCounter.byte[1] == 0xFF) && (pulseCounter.byte[0] == 0xFF))
            {
            amr_id = 1;
            pulseCounter.long_data = amr_id;
            WriteEEPROM(EE_AMR_ID_4byte, pulseCounter);

            }
        previous_mode = automode;
        WriteByte_EEPROM(EE_PREV_MODE, previous_mode);
        }
    else
        {

        pulseCounter = ReadEEPROM(EE_AMR_ID_4byte);
        amr_id = pulseCounter.long_data;
        if ((pulseCounter.byte[3] == 0xFF) && (pulseCounter.byte[2] == 0xFF) && (pulseCounter.byte[1] == 0xFF) && (pulseCounter.byte[0] == 0xFF))
            {
            amr_id = 1;
            pulseCounter.long_data = amr_id;
            WriteEEPROM(EE_AMR_ID_4byte, pulseCounter);

            }
        }


    if (enter_boot_mode == 30)
        {

        //	WriteData_UART2("\r\nID_FRAME=1 \r\n",strlen("\r\nID_FRAME=1 \r\n"));
        send_id_frame_flage = 1;
        enter_boot_mode = 20;
        WriteByte_EEPROM(EE_ENT_BOOT, enter_boot_mode);
        }

    pulseCounter = ReadEEPROM(EE_LATITUDE);
    Latitude = pulseCounter.float_data;

    if ((Latitude > 90) || (Latitude < -90) || ((pulseCounter.byte[3] == 0xFF) && (pulseCounter.byte[2] == 0xFF) && (pulseCounter.byte[1] == 0xFF) && (pulseCounter.byte[0] == 0xFF)))
        {
        //Latitude = 23.00;
        Latitude = 42.3584;
        //Latitude = 23.0262;
        pulseCounter.float_data = Latitude;

        WriteEEPROM(EE_LATITUDE, pulseCounter);

        }

    pulseCounter = ReadEEPROM(EE_LONGITUDE);
    longitude = pulseCounter.float_data;

    if ((longitude > 180) || (longitude < -180) || ((pulseCounter.byte[3] == 0xFF) && (pulseCounter.byte[2] == 0xFF) && (pulseCounter.byte[1] == 0xFF) && (pulseCounter.byte[0] == 0xFF)))
        {
        //longitude = 72.00;
        longitude = -71.0597;
        //longitude = 72.5623;
        pulseCounter.float_data = longitude;
        WriteEEPROM(EE_LONGITUDE, pulseCounter);

        }

    pulseCounter = ReadEEPROM(EE_TIMEZONE);
    utcOffset = pulseCounter.float_data;
    if ((utcOffset > 13) || (utcOffset < -12) || ((pulseCounter.byte[3] == 0xFF) && (pulseCounter.byte[2] == 0xFF) && (pulseCounter.byte[1] == 0xFF) && (pulseCounter.byte[0] == 0xFF)))
        {
        //utcOffset = 0.0;	
        utcOffset = -4.0;
        //utcOffset = 5.30;
        pulseCounter.float_data = utcOffset;
        WriteEEPROM(EE_TIMEZONE, pulseCounter);

        }

    /////////////////GPS_Start///////////////////
    GPS_Read_Enable = ReadByte_EEPROM(EE_GPS_Read_Enable);
    if ((ee_default_state == 1) || (GPS_Read_Enable > 1))
        {
        GPS_Read_Enable = 1;
        WriteByte_EEPROM(EE_GPS_Read_Enable, GPS_Read_Enable);
        }

    GPS_Rescan = ReadByte_EEPROM(EE_GPS_Rescan);

    if ((ee_default_state == 1) || (GPS_Rescan > 3))
        {
        GPS_Rescan = 3;
        WriteByte_EEPROM(EE_GPS_Rescan, GPS_Rescan);
        }
    pulseCounter.byte[1] = ReadByte_EEPROM(EE_GPS_Wake_timer + 0);
    pulseCounter.byte[0] = ReadByte_EEPROM(EE_GPS_Wake_timer + 1);
    GPS_Wake_timer = pulseCounter.int_data;
    if ((ee_default_state == 1) || (GPS_Wake_timer > 1440))
        {
        GPS_Wake_timer = 120;
        pulseCounter.int_data = GPS_Wake_timer;
        WriteByte_EEPROM(EE_GPS_Wake_timer + 0, pulseCounter.byte[1]);
        WriteByte_EEPROM(EE_GPS_Wake_timer + 1, pulseCounter.byte[0]);
        }

    No_of_setalite_threshold = ReadByte_EEPROM(EE_No_of_setalite_threshold);
    if ((ee_default_state == 1) || (No_of_setalite_threshold > 12))
        {
        No_of_setalite_threshold = 8;
        WriteByte_EEPROM(EE_No_of_setalite_threshold, No_of_setalite_threshold);
        }
    Setalite_angle_threshold = ReadByte_EEPROM(EE_Setalite_angle_threshold);
    if ((ee_default_state == 1) || (Setalite_angle_threshold > 50) || (Setalite_angle_threshold < 10))
        {
        Setalite_angle_threshold = 15;
        WriteByte_EEPROM(EE_Setalite_angle_threshold, Setalite_angle_threshold);
        }
    setalite_Rssi_threshold = ReadByte_EEPROM(EE_setalite_Rssi_threshold);
    if ((ee_default_state == 1) || (setalite_Rssi_threshold > 50) || (setalite_Rssi_threshold < 10))
        {
        setalite_Rssi_threshold = 35;
        WriteByte_EEPROM(EE_setalite_Rssi_threshold, setalite_Rssi_threshold);
        }

    Maximum_no_of_Setalite_reading = ReadByte_EEPROM(EE_Maximum_no_of_Setalite_reading);
    if ((ee_default_state == 1) || (Maximum_no_of_Setalite_reading > 12) || (Maximum_no_of_Setalite_reading < 4))
        {
        Maximum_no_of_Setalite_reading = 4;
        WriteByte_EEPROM(EE_Maximum_no_of_Setalite_reading, Maximum_no_of_Setalite_reading);
        }

    Sbas_setalite_Rssi_threshold = ReadByte_EEPROM(EE_Sbas_setalite_Rssi_threshold);
    if ((ee_default_state == 1) || (Sbas_setalite_Rssi_threshold > 50) || (Sbas_setalite_Rssi_threshold < 10))
        {
        Sbas_setalite_Rssi_threshold = 35;
        WriteByte_EEPROM(EE_Sbas_setalite_Rssi_threshold, Sbas_setalite_Rssi_threshold);
        }
    Auto_Gps_Data_Send = ReadByte_EEPROM(EE_Auto_Gps_Data_Send);
    if ((ee_default_state == 1) || (Auto_Gps_Data_Send > 1))
        {
        Auto_Gps_Data_Send = 1;
        WriteByte_EEPROM(EE_Auto_Gps_Data_Send, Auto_Gps_Data_Send);
        }


    Check_Sbas_Enable = ReadByte_EEPROM(EE_Check_Sbas_Enable);
    if ((ee_default_state == 1) || (Check_Sbas_Enable > 1))
        {
        Check_Sbas_Enable = 1;
        WriteByte_EEPROM(EE_Check_Sbas_Enable, Check_Sbas_Enable);
        }
    pulseCounter = ReadEEPROM(EE_HDOP_Gps_threshold);
    HDOP_Gps_threshold = pulseCounter.float_data;
    if ((ee_default_state == 1) || (HDOP_Gps_threshold > 10.0) || (HDOP_Gps_threshold < 0.2) || ((pulseCounter.byte[3] == 0xFF) && (pulseCounter.byte[2] == 0xFF) && (pulseCounter.byte[1] == 0xFF) && (pulseCounter.byte[0] == 0xFF)))
        //if((HDOP_Gps_threshold>10.0)||(HDOP_Gps_threshold<0.2))
        {
        HDOP_Gps_threshold = 1.0;
        pulseCounter.float_data = HDOP_Gps_threshold;
        WriteEEPROM(EE_HDOP_Gps_threshold, pulseCounter);

        }
    /////////////////GPS_End///////////////////////
    pulseCounter.byte[1] = ReadByte_EEPROM(EE_Sunset_delay + 0);
    pulseCounter.byte[0] = ReadByte_EEPROM(EE_Sunset_delay + 1);
    Sunset_delay = pulseCounter.signed_int; //bug 1.0.0005 parvez

    if ((Sunset_delay > 7200) || (Sunset_delay < -7200) || (pulseCounter.byte[0] == 0xff) || (pulseCounter.byte[1] == 0xff))
        {
        Sunset_delay = 0;
        pulseCounter.signed_int = 0;
        WriteByte_EEPROM(EE_Sunset_delay + 0, pulseCounter.byte[1]);
        WriteByte_EEPROM(EE_Sunset_delay + 1, pulseCounter.byte[0]);
        }

    pulseCounter.byte[1] = ReadByte_EEPROM(EE_Sunrise_delay + 0);
    pulseCounter.byte[0] = ReadByte_EEPROM(EE_Sunrise_delay + 1);
    Sunrise_delay = pulseCounter.signed_int; //bug 1.0.0006 parvez
    if ((Sunrise_delay > 7200) || (Sunrise_delay < -7200) || (pulseCounter.byte[0] == 0xff) || (pulseCounter.byte[1] == 0xff))
        {
        Sunrise_delay = 0;
        pulseCounter.signed_int = 0;
        WriteByte_EEPROM(EE_Sunrise_delay + 0, pulseCounter.byte[1]);
        WriteByte_EEPROM(EE_Sunrise_delay + 1, pulseCounter.byte[0]);
        }


    GetSCHTimeFrom_LAT_LOG(); //get sunset sunrise schedule 
    EEWriteAstroSch();


    pulseCounter.byte[1] = ReadByte_EEPROM(EE_Day_light_harvesting_start_offset + 0);
    pulseCounter.byte[0] = ReadByte_EEPROM(EE_Day_light_harvesting_start_offset + 1);
    Day_light_harvesting_start_offset = pulseCounter.signed_int; //bug 1.0.0007 parvez

    if ((Day_light_harvesting_start_offset < -18000) || (Day_light_harvesting_start_offset > 18000))
        {
        Day_light_harvesting_start_offset = 0;
        }


    pulseCounter.byte[1] = ReadByte_EEPROM(EE_Day_light_harvesting_stop_offset + 0);
    pulseCounter.byte[0] = ReadByte_EEPROM(EE_Day_light_harvesting_stop_offset + 1);
    Day_light_harvesting_stop_offset = pulseCounter.signed_int; //bug 1.0.0008 parvez

    if ((Day_light_harvesting_stop_offset < -18000) || (Day_light_harvesting_stop_offset > 18000))
        {
        Day_light_harvesting_stop_offset = 0;
        }

    Day_light_harves_time();


    for (i = 0; i < 10; i++) // V6.1.10
        {

        if (i == 0)
            {


            sSch[0][i].cStartHour = ReadByte_EEPROM(EE_SCHEDULE + (((i * 5) + 0))); // V6.1.10
            if (sSch[0][i].cStartHour > 23) sSch[0][i].cStartHour = 18;
            sSch[0][i].cStartMin = ReadByte_EEPROM(EE_SCHEDULE + (((i * 5) + 1))); // V6.1.10
            if (sSch[0][i].cStartMin > 59) sSch[0][i].cStartMin = 0;
            sSch[0][i].cStopHour = ReadByte_EEPROM(EE_SCHEDULE + (((i * 5) + 2))); // V6.1.10
            if (sSch[0][i].cStopHour > 23) sSch[0][i].cStopHour = 23;
            sSch[0][i].cStopMin = ReadByte_EEPROM(EE_SCHEDULE + (((i * 5) + 3))); // V6.1.10
            if (sSch[0][i].cStopMin > 59) sSch[0][i].cStopMin = 59;
            sSch[0][i].bSchFlag = ReadByte_EEPROM(EE_SCHEDULE + (((i * 5) + 4))); // V6.1.10
            if (sSch[0][i].bSchFlag > 1) sSch[0][i].bSchFlag = 1;


            }
        else if (i == 1)
            {

            sSch[0][i].cStartHour = ReadByte_EEPROM(EE_SCHEDULE + (((i * 5) + 0))); // V6.1.10
            if (sSch[0][i].cStartHour > 23) sSch[0][i].cStartHour = 0;
            sSch[0][i].cStartMin = ReadByte_EEPROM(EE_SCHEDULE + (((i * 5) + 1))); // V6.1.10
            if (sSch[0][i].cStartMin > 59) sSch[0][i].cStartMin = 0;
            sSch[0][i].cStopHour = ReadByte_EEPROM(EE_SCHEDULE + (((i * 5) + 2))); // V6.1.10
            if (sSch[0][i].cStopHour > 23) sSch[0][i].cStopHour = 6;
            sSch[0][i].cStopMin = ReadByte_EEPROM(EE_SCHEDULE + (((i * 5) + 3))); // V6.1.10
            if (sSch[0][i].cStopMin > 59) sSch[0][i].cStopMin = 0;
            sSch[0][i].bSchFlag = ReadByte_EEPROM(EE_SCHEDULE + (((i * 5) + 4))); // V6.1.10
            if (sSch[0][i].bSchFlag > 1) sSch[0][i].bSchFlag = 1;
            }
        else
            {


            sSch[0][i].cStartHour = ReadByte_EEPROM(EE_SCHEDULE + (((i * 5) + 0))); // V6.1.10
            if (sSch[0][i].cStartHour > 23) sSch[0][i].cStartHour = 0;
            sSch[0][i].cStartMin = ReadByte_EEPROM(EE_SCHEDULE + (((i * 5) + 1))); // V6.1.10
            if (sSch[0][i].cStartMin > 59) sSch[0][i].cStartMin = 0;
            sSch[0][i].cStopHour = ReadByte_EEPROM(EE_SCHEDULE + (((i * 5) + 2))); // V6.1.10
            if (sSch[0][i].cStopHour > 23) sSch[0][i].cStopHour = 0;
            sSch[0][i].cStopMin = ReadByte_EEPROM(EE_SCHEDULE + (((i * 5) + 3))); // V6.1.10
            if (sSch[0][i].cStopMin > 59) sSch[0][i].cStopMin = 0;
            sSch[0][i].bSchFlag = ReadByte_EEPROM(EE_SCHEDULE + (((i * 5) + 4))); // V6.1.10
            if (sSch[0][i].bSchFlag > 1) sSch[0][i].bSchFlag = 0;

            }


        }
    sch_day[0] = ReadByte_EEPROM(EE_SCH_DAY + 0);
    if (sch_day[0] == 0xFF) sch_day[0] = 0x7F;



    for (i = 0; i < 10; i++) // V6.1.10
        {
        if (i == 0)
            {
            Master_sSch[0][i].cStartHour = ReadByte_EEPROM(EE_MASTER_SCHEDULE + ((i * 6) + 0)); // V6.1.10
            if (Master_sSch[0][i].cStartHour > 23) Master_sSch[0][i].cStartHour = 18;
            Master_sSch[0][i].cStartMin = ReadByte_EEPROM(EE_MASTER_SCHEDULE + ((i * 6) + 1)); // V6.1.10
            if (Master_sSch[0][i].cStartMin > 59) Master_sSch[0][i].cStartMin = 0;
            Master_sSch[0][i].cStopHour = ReadByte_EEPROM(EE_MASTER_SCHEDULE + ((i * 6) + 2)); // V6.1.10
            if (Master_sSch[0][i].cStopHour > 23) Master_sSch[0][i].cStopHour = 23;
            Master_sSch[0][i].cStopMin = ReadByte_EEPROM(EE_MASTER_SCHEDULE + ((i * 6) + 3)); // V6.1.10
            if (Master_sSch[0][i].cStopMin > 59) Master_sSch[0][i].cStopMin = 59;
            Master_sSch[0][i].bSchFlag = ReadByte_EEPROM(EE_MASTER_SCHEDULE + ((i * 6) + 4)); // V6.1.10
            if (Master_sSch[0][i].bSchFlag > 1) Master_sSch[0][i].bSchFlag = 1;
            Master_sSch[0][i].dimvalue = ReadByte_EEPROM(EE_MASTER_SCHEDULE + ((i * 6) + 5)); // V6.1.10
            if (Master_sSch[0][i].dimvalue > 100) Master_sSch[0][i].dimvalue = 0;


            }
        else if (i == 1)
            {

            Master_sSch[0][i].cStartHour = ReadByte_EEPROM(EE_MASTER_SCHEDULE + ((i * 6) + 0)); // V6.1.10
            if (Master_sSch[0][i].cStartHour > 23) Master_sSch[0][i].cStartHour = 0;
            Master_sSch[0][i].cStartMin = ReadByte_EEPROM(EE_MASTER_SCHEDULE + ((i * 6) + 1)); // V6.1.10
            if (Master_sSch[0][i].cStartMin > 59) Master_sSch[0][i].cStartMin = 0;
            Master_sSch[0][i].cStopHour = ReadByte_EEPROM(EE_MASTER_SCHEDULE + ((i * 6) + 2)); // V6.1.10
            if (Master_sSch[0][i].cStopHour > 23) Master_sSch[0][i].cStopHour = 6;
            Master_sSch[0][i].cStopMin = ReadByte_EEPROM(EE_MASTER_SCHEDULE + ((i * 6) + 3)); // V6.1.10
            if (Master_sSch[0][i].cStopMin > 59) Master_sSch[0][i].cStopMin = 0;
            Master_sSch[0][i].bSchFlag = ReadByte_EEPROM(EE_MASTER_SCHEDULE + ((i * 6) + 4)); // V6.1.10
            if (Master_sSch[0][i].bSchFlag > 1) Master_sSch[0][i].bSchFlag = 1;
            Master_sSch[0][i].dimvalue = ReadByte_EEPROM(EE_MASTER_SCHEDULE + ((i * 6) + 5)); // V6.1.10
            if (Master_sSch[0][i].dimvalue > 100) Master_sSch[0][i].dimvalue = 0;


            }
        else
            {


            Master_sSch[0][i].cStartHour = ReadByte_EEPROM(EE_MASTER_SCHEDULE + ((i * 6) + 0)); // V6.1.10
            if (Master_sSch[0][i].cStartHour > 23) Master_sSch[0][i].cStartHour = 0;
            Master_sSch[0][i].cStartMin = ReadByte_EEPROM(EE_MASTER_SCHEDULE + ((i * 6) + 1)); // V6.1.10
            if (Master_sSch[0][i].cStartMin > 59) Master_sSch[0][i].cStartMin = 0;
            Master_sSch[0][i].cStopHour = ReadByte_EEPROM(EE_MASTER_SCHEDULE + ((i * 6) + 2)); // V6.1.10
            if (Master_sSch[0][i].cStopHour > 23) Master_sSch[0][i].cStopHour = 0;
            Master_sSch[0][i].cStopMin = ReadByte_EEPROM(EE_MASTER_SCHEDULE + ((i * 6) + 3)); // V6.1.10
            if (Master_sSch[0][i].cStopMin > 59) Master_sSch[0][i].cStopMin = 0;
            Master_sSch[0][i].bSchFlag = ReadByte_EEPROM(EE_MASTER_SCHEDULE + ((i * 6) + 4)); // V6.1.10
            if (Master_sSch[0][i].bSchFlag > 1) Master_sSch[0][i].bSchFlag = 0;
            Master_sSch[0][i].dimvalue = ReadByte_EEPROM(EE_MASTER_SCHEDULE + ((i * 6) + 5)); // V6.1.10
            if (Master_sSch[0][i].dimvalue > 100) Master_sSch[0][i].dimvalue = 0;

            }

        }
    sch_Master_day[0] = ReadByte_EEPROM(EE_MASTER_SCH_DAY + 0);
    if (sch_Master_day[0] == 0xFF) sch_Master_day[0] = 0x7F;


    pulseCounter = ReadEEPROM(EE_Vr_Cal);
    Vr_Cal = pulseCounter.float_data;

    if ((ee_default_state == 1) || ((pulseCounter.byte[0] == 0xFF) && (pulseCounter.byte[1] == 0xFF) && (pulseCounter.byte[2] == 0xFF) && (pulseCounter.byte[3] == 0xFF)))
        {
        //Vr_Cal = 0.44401625;
        // Vr_Cal = 0.443671; //050318

        //Vr_Cal = 0.475; //Temporary set
        Vr_Cal = 0.491118; //Asper mail
        }

    pulseCounter = ReadEEPROM(EE_Ir_Cal);
    Ir_Cal = pulseCounter.float_data;

    if ((ee_default_state == 1) || ((pulseCounter.byte[0] == 0xFF) && (pulseCounter.byte[1] == 0xFF) && (pulseCounter.byte[2] == 0xFF) && (pulseCounter.byte[3] == 0xFF)))
        {
        //Ir_Cal = 0.249616225;
        // Ir_Cal = 0.027144; //050318

        //Ir_Cal = 0.026; //Temporary Set
        Ir_Cal = 0.11988; //As per mail
        }
    pulseCounter = ReadEEPROM(EE_KW_Cal);
    KW_Cal = pulseCounter.float_data;

    if ((ee_default_state == 1) || ((pulseCounter.byte[0] == 0xFF) && (pulseCounter.byte[1] == 0xFF) && (pulseCounter.byte[2] == 0xFF) && (pulseCounter.byte[3] == 0xFF)))
        {
        ///KW_Cal = 630;
        //KW_Cal = 650; //050318
        KW_Cal = 1744; //As per mail
        }
    ///////////////////////////////////////////

    pulseCounter = ReadEEPROM(EE_KW_Cal_120);
    KW_Cal_120 = pulseCounter.float_data;

    if ((ee_default_state == 1) || ((pulseCounter.byte[0] == 0xFF) && (pulseCounter.byte[1] == 0xFF) && (pulseCounter.byte[2] == 0xFF) && (pulseCounter.byte[3] == 0xFF)))
        {
        //KW_Cal_120 = 325;  //set 325 value date according to mail from sandip 14Dec2016
        //KW_Cal_120 = 322; //set 325 value date according to mail from sandip 14Dec2016
        KW_Cal_120 = 872; //As per mail
        }

    vrdoub = Vr_Cal;
    irdoub = Ir_Cal;
    pulses_r = KW_Cal;

    set_mf_energy();

    pulseCounter.byte[0] = ReadByte_EEPROM(EE_KWH + 0);
    pulseCounter.byte[1] = ReadByte_EEPROM(EE_KWH + 1);
    pulseCounter.byte[2] = ReadByte_EEPROM(EE_KWH + 2);
    pulseCounter.byte[3] = ReadByte_EEPROM(EE_KWH + 3);
    pulses_r = pulseCounter.long_data;

    if (pulses_r >= 10000000)
        {
        pulses_r = 0;
        pulseCounter.long_data = pulses_r;


        WriteByte_EEPROM(EE_KWH + 0, pulseCounter.byte[0]);
        WriteByte_EEPROM(EE_KWH + 1, pulseCounter.byte[1]);
        WriteByte_EEPROM(EE_KWH + 2, pulseCounter.byte[2]);
        WriteByte_EEPROM(EE_KWH + 3, pulseCounter.byte[3]);
        }

    kwh = (((float) pulses_r * mf3));
    newkwh = kwh;

    pulseCounter = ReadEEPROM(EE_BURN_HOUR);
    lamp_burn_hour = pulseCounter.float_data;
    if ((pulseCounter.byte[0] == 0xff) && (pulseCounter.byte[1] == 0xff) && (pulseCounter.byte[2] == 0xff) && (pulseCounter.byte[3] == 0xff))
        {
        pulseCounter.float_data = 0;
        lamp_burn_hour = pulseCounter.float_data;
        WriteEEPROM(EE_BURN_HOUR, pulseCounter);

        }

    pulseCounter = ReadEEPROM(EE_LAMP_STADY_CURRENT);
    lamp_current = pulseCounter.float_data;


    detect_lamp_current = ReadByte_EEPROM(EE_LAMP_STADY_CURRENT_SET);
    if (detect_lamp_current != 1)
        {
        detect_lamp_current = 0;
        WriteByte_EEPROM(EE_LAMP_STADY_CURRENT_SET, detect_lamp_current);

        pulseCounter.float_data = 0.0;
        WriteEEPROM(EE_LAMP_STADY_CURRENT, pulseCounter);

        lamp_current = 0;
        }

    //    current_creep_limit = lamp_current * 0.1; // find current creep limit for checking lamp ON/OFF
    //
    //    if (current_creep_limit < 0.03)
    //        {
    //        current_creep_limit = 0.03; // if current creep limit less then 0.03 make it 0.03	
    //        }
    //    else if (current_creep_limit > 0.1)
    //        {
    //        current_creep_limit = 0.1; // if current creep limit more then 0.1 make it 0.1
    //        }
    current_creep_limit = lamp_current * 0.1; // find current creep limit for checking lamp ON/OFF

    if (current_creep_limit < 0.05)
        {
        current_creep_limit = 0.05; // if current creep limit less then 0.03 make it 0.03	
        }
    else if (current_creep_limit > 0.1)
        {
        current_creep_limit = 0.1; // if current creep limit more then 0.1 make it 0.1
        }


    pulseCounter.byte[1] = ReadByte_EEPROM(EE_Vol_hi + 0);
    pulseCounter.byte[0] = ReadByte_EEPROM(EE_Vol_hi + 1);
    Vol_hi = pulseCounter.int_data;


    if ((Vol_hi < 30) || (Vol_hi > 400))
        {
        Vol_hi = 260;
        pulseCounter.int_data = Vol_hi;
        WriteByte_EEPROM(EE_Vol_hi + 0, pulseCounter.byte[1]);
        WriteByte_EEPROM(EE_Vol_hi + 1, pulseCounter.byte[0]);
        }

    pulseCounter.byte[1] = ReadByte_EEPROM(EE_Vol_low + 0);
    pulseCounter.byte[0] = ReadByte_EEPROM(EE_Vol_low + 1);
    Vol_low = pulseCounter.int_data;
    if ((Vol_low < 30) || (Vol_low > 400))
        {
        Vol_low = 90;
        pulseCounter.int_data = Vol_low;
        WriteByte_EEPROM(EE_Vol_low + 0, pulseCounter.byte[1]);
        WriteByte_EEPROM(EE_Vol_low + 1, pulseCounter.byte[0]);
        }

    pulseCounter.byte[1] = ReadByte_EEPROM(EE_Curr_Steady_Time + 0);
    pulseCounter.byte[0] = ReadByte_EEPROM(EE_Curr_Steady_Time + 1);
    Curr_Steady_Time = pulseCounter.int_data;
    if ((Curr_Steady_Time > 1200) || (Set_default_perameter == 1))
        {
        Curr_Steady_Time = 600;
        //halCommonSetToken(TOKEN_Curr_Steady_Time,&Curr_Steady_Time);
        pulseCounter.int_data = Curr_Steady_Time;
        WriteByte_EEPROM(EE_Curr_Steady_Time + 0, pulseCounter.byte[1]);
        WriteByte_EEPROM(EE_Curr_Steady_Time + 1, pulseCounter.byte[0]);
        }
    /*
    if((Curr_Steady_Time < 30) || (Curr_Steady_Time > 1200))
    {
            Curr_Steady_Time = 120;
            pulseCounter.int_data = Curr_Steady_Time;
            WriteByte_EEPROM(EE_Curr_Steady_Time + 0,pulseCounter.byte[1]);
            WriteByte_EEPROM(EE_Curr_Steady_Time + 1,pulseCounter.byte[0]);
    }*/

    pulseCounter.byte[1] = ReadByte_EEPROM(EE_Per_Val_Current + 0);
    pulseCounter.byte[0] = ReadByte_EEPROM(EE_Per_Val_Current + 1);
    Per_Val_Current = pulseCounter.int_data;
    if ((Per_Val_Current < 10) || (Per_Val_Current > 90))
        {
#ifdef NEW_LAMP_FAULT_LOGIC
        Per_Val_Current = 70;
#elif defined(OLD_LAMP_FAULT_LOGIC)
        Per_Val_Current = 20;
#else
#error "Please define LAMP_FAULT_LOGIC_TYPE"
#endif
        pulseCounter.int_data = Per_Val_Current;
        WriteByte_EEPROM(EE_Per_Val_Current + 0, pulseCounter.byte[1]);
        WriteByte_EEPROM(EE_Per_Val_Current + 1, pulseCounter.byte[0]);
        }



    pulseCounter.byte[1] = ReadByte_EEPROM(EE_Lamp_Fault_Time + 0);
    pulseCounter.byte[0] = ReadByte_EEPROM(EE_Lamp_Fault_Time + 1);
    Lamp_Fault_Time = pulseCounter.int_data;
#ifdef NEW_LAMP_FAULT_LOGIC
    if ((Lamp_Fault_Time > 1800) || (Set_default_perameter == 1))
        {
        Lamp_Fault_Time = 600;
        pulseCounter.int_data = Lamp_Fault_Time;
        WriteByte_EEPROM(EE_Lamp_Fault_Time + 0, pulseCounter.byte[1]);
        WriteByte_EEPROM(EE_Lamp_Fault_Time + 1, pulseCounter.byte[0]);
        }
#elif defined(OLD_LAMP_FAULT_LOGIC)
    if ((Lamp_Fault_Time > 600) || (Set_default_perameter == 1))
        {
        Lamp_Fault_Time = 30;
        pulseCounter.int_data = Lamp_Fault_Time;
        WriteByte_EEPROM(EE_Lamp_Fault_Time + 0, pulseCounter.byte[1]);
        WriteByte_EEPROM(EE_Lamp_Fault_Time + 1, pulseCounter.byte[0]);
        }
#else
#error "Please define LAMP_FAULT_LOGIC_TYPE"
#endif



#ifdef NEW_LAMP_FAULT_LOGIC

    pulseCounter = ReadEEPROM(EE_KW_Threshold);
    KW_Threshold = pulseCounter.float_data;

    if ((Set_default_perameter == 1) || (KW_Threshold == 0))
        // if((KW_Threshold == 0))
        {
        KW_Threshold = 0;
        //halCommonSetToken(TOKEN_KW_Threshold,&KW_Threshold);   //EEPROMSAN
        pulseCounter.float_data = 0.0;
        WriteEEPROM(EE_KW_Threshold, pulseCounter);

        detect_lamp_current = 0;
        }
#elif defined(OLD_LAMP_FAULT_LOGIC)

#else
#error "Please define LAMP_FAULT_LOGIC_TYPE"
#endif

    //////////////////////////////////////////////////////////////////////////////////////////////
    pulseCounter.byte[1] = ReadByte_EEPROM(EE_Lamp_off_on_Time + 0);
    pulseCounter.byte[0] = ReadByte_EEPROM(EE_Lamp_off_on_Time + 1);
    Lamp_off_on_Time = pulseCounter.int_data;
    if (Lamp_off_on_Time > 600)
        {
        Lamp_off_on_Time = 0;
        pulseCounter.int_data = Lamp_off_on_Time;
        WriteByte_EEPROM(EE_Lamp_off_on_Time + 0, pulseCounter.byte[1]);
        WriteByte_EEPROM(EE_Lamp_off_on_Time + 1, pulseCounter.byte[0]);
        }


    pulseCounter.byte[1] = ReadByte_EEPROM(EE_Lamp_faulty_retrieve_Count + 0);
    pulseCounter.byte[0] = ReadByte_EEPROM(EE_Lamp_faulty_retrieve_Count + 1);
    Lamp_faulty_retrieve_Count = pulseCounter.int_data;
    if ((Lamp_faulty_retrieve_Count < 1) || (Lamp_faulty_retrieve_Count > 20))
        {
        Lamp_faulty_retrieve_Count = 3;
        pulseCounter.int_data = Lamp_faulty_retrieve_Count;
        WriteByte_EEPROM(EE_Lamp_faulty_retrieve_Count + 0, pulseCounter.byte[1]);
        WriteByte_EEPROM(EE_Lamp_faulty_retrieve_Count + 1, pulseCounter.byte[0]);
        }

    pulseCounter.byte[1] = ReadByte_EEPROM(EE_Broadcast_time_delay + 0);
    pulseCounter.byte[0] = ReadByte_EEPROM(EE_Broadcast_time_delay + 1);
    Broadcast_time_delay = pulseCounter.int_data;
    if ((Broadcast_time_delay < 1) || (Broadcast_time_delay > 60))
        {
        Broadcast_time_delay = 2;
        pulseCounter.int_data = Broadcast_time_delay;
        WriteByte_EEPROM(EE_Broadcast_time_delay + 0, pulseCounter.byte[1]);
        WriteByte_EEPROM(EE_Broadcast_time_delay + 1, pulseCounter.byte[0]);
        }

    pulseCounter.byte[1] = ReadByte_EEPROM(EE_Unicast_time_delay + 0);
    pulseCounter.byte[0] = ReadByte_EEPROM(EE_Unicast_time_delay + 1);
    Unicast_time_delay = pulseCounter.int_data;
    if ((Unicast_time_delay < 1) || (Unicast_time_delay > 60))
        {
        Unicast_time_delay = 30;
        pulseCounter.int_data = Unicast_time_delay;
        WriteByte_EEPROM(EE_Unicast_time_delay + 0, pulseCounter.byte[1]);
        WriteByte_EEPROM(EE_Unicast_time_delay + 1, pulseCounter.byte[0]);
        }

    pulseCounter.byte[1] = ReadByte_EEPROM(EE_Lamp_lock_condition + 0);
    pulseCounter.byte[0] = ReadByte_EEPROM(EE_Lamp_lock_condition + 1);
    Lamp_lock_condition = pulseCounter.int_data;
    if (Lamp_lock_condition > 1)
        {
        Lamp_lock_condition = 0;
        pulseCounter.int_data = Lamp_lock_condition;
        WriteByte_EEPROM(EE_Lamp_lock_condition + 0, pulseCounter.byte[1]);
        WriteByte_EEPROM(EE_Lamp_lock_condition + 1, pulseCounter.byte[0]);
        }

    pulseCounter.byte[1] = ReadByte_EEPROM(EE_analog_input_scaling_high_Value + 0);
    pulseCounter.byte[0] = ReadByte_EEPROM(EE_analog_input_scaling_high_Value + 1);
    analog_input_scaling_high_Value = pulseCounter.int_data;
    if (analog_input_scaling_high_Value > 65000)
        {
        analog_input_scaling_high_Value = 50000;
        pulseCounter.int_data = 0;
        pulseCounter.int_data = analog_input_scaling_high_Value;
        WriteByte_EEPROM(EE_analog_input_scaling_high_Value + 0, pulseCounter.byte[1]);
        WriteByte_EEPROM(EE_analog_input_scaling_high_Value + 1, pulseCounter.byte[0]);
        }

    pulseCounter.byte[1] = ReadByte_EEPROM(EE_analog_input_scaling_low_Value + 0);
    pulseCounter.byte[0] = ReadByte_EEPROM(EE_analog_input_scaling_low_Value + 1);
    analog_input_scaling_low_Value = pulseCounter.int_data;
    if (analog_input_scaling_low_Value > 65000)
        {
        analog_input_scaling_low_Value = 0;
        pulseCounter.int_data = analog_input_scaling_low_Value;
        WriteByte_EEPROM(EE_analog_input_scaling_low_Value + 0, pulseCounter.byte[1]);
        WriteByte_EEPROM(EE_analog_input_scaling_low_Value + 1, pulseCounter.byte[0]);
        }

    pulseCounter.byte[1] = ReadByte_EEPROM(EE_desir_lamp_lumen + 0);
    pulseCounter.byte[0] = ReadByte_EEPROM(EE_desir_lamp_lumen + 1);
    desir_lamp_lumen = pulseCounter.int_data;
    if (desir_lamp_lumen > 65000)
        {
        desir_lamp_lumen = pulseCounter.int_data = 0;
        WriteByte_EEPROM(EE_desir_lamp_lumen + 0, pulseCounter.byte[1]);
        WriteByte_EEPROM(EE_desir_lamp_lumen + 1, pulseCounter.byte[0]);
        }

    lumen_tollarence = ReadByte_EEPROM(EE_lumen_tollarence);
    if (lumen_tollarence > 100)
        {
        lumen_tollarence = 0;
        WriteByte_EEPROM(EE_lumen_tollarence, lumen_tollarence);
        }

    ballast_type = ReadByte_EEPROM(EE_ballast_type);
    ballast_type = 1; //SAN
    if (ballast_type > 1)
        {
        ballast_type = 1;
        WriteByte_EEPROM(EE_ballast_type, ballast_type);
        }

    adaptive_light_dimming = ReadByte_EEPROM(EE_adaptive_light_dimming);
    if ((adaptive_light_dimming > 3) || (Set_default_perameter == 1))
        {
        adaptive_light_dimming = 0;
        WriteByte_EEPROM(EE_adaptive_light_dimming, adaptive_light_dimming);
        }

    /***********************Analog Photocell Start************************/
    pulseCounter.byte[1] = ReadByte_EEPROM(EE_Photocell_Off_Time + 0);
    pulseCounter.byte[0] = ReadByte_EEPROM(EE_Photocell_Off_Time + 1);
    Photocell_Off_Time = pulseCounter.int_data;

    if ((ee_default_state == 1) || (Photocell_Off_Time > 300) || (Photocell_Off_Time < 0))
        {
        Photocell_Off_Time = 5;
        //WriteByte_EEPROM(EE_Photocell_Off_Time,Photocell_Off_Time);
        pulseCounter.int_data = Photocell_Off_Time;
        WriteByte_EEPROM(EE_Photocell_Off_Time + 0, pulseCounter.byte[1]);
        WriteByte_EEPROM(EE_Photocell_Off_Time + 1, pulseCounter.byte[0]);
        }

    pulseCounter.byte[3] = ReadByte_EEPROM(EE_Photosensor_Set_FC + 3);
    pulseCounter.byte[2] = ReadByte_EEPROM(EE_Photosensor_Set_FC + 2);
    pulseCounter.byte[1] = ReadByte_EEPROM(EE_Photosensor_Set_FC + 1);
    pulseCounter.byte[0] = ReadByte_EEPROM(EE_Photosensor_Set_FC + 0);
    Photosensor_Set_FC = pulseCounter.float_data;
    if ((ee_default_state == 1) || ((pulseCounter.byte[3] == 0xFF) && (pulseCounter.byte[2] == 0xFF) && (pulseCounter.byte[1] == 0xFF) && (pulseCounter.byte[0] == 0xFF)))
        {
        Photosensor_Set_FC = 2.5; //12.0; //according to document [1.2 FC is equal to 0.076V]
        pulseCounter.float_data = Photosensor_Set_FC;
        WriteByte_EEPROM(EE_Photosensor_Set_FC + 3, pulseCounter.byte[3]);
        WriteByte_EEPROM(EE_Photosensor_Set_FC + 2, pulseCounter.byte[2]);
        WriteByte_EEPROM(EE_Photosensor_Set_FC + 1, pulseCounter.byte[1]);
        WriteByte_EEPROM(EE_Photosensor_Set_FC + 0, pulseCounter.byte[0]);
        }


    pulseCounter.byte[3] = ReadByte_EEPROM(EE_Photosensor_Set_FC_Lamp_OFF + 3);
    pulseCounter.byte[2] = ReadByte_EEPROM(EE_Photosensor_Set_FC_Lamp_OFF + 2);
    pulseCounter.byte[1] = ReadByte_EEPROM(EE_Photosensor_Set_FC_Lamp_OFF + 1);
    pulseCounter.byte[0] = ReadByte_EEPROM(EE_Photosensor_Set_FC_Lamp_OFF + 0);
    Photosensor_Set_FC_Lamp_OFF = pulseCounter.float_data; //need test

    if ((ee_default_state == 1) || ((pulseCounter.byte[3] == 0xFF) && (pulseCounter.byte[2] == 0xFF) && (pulseCounter.byte[1] == 0xFF) && (pulseCounter.byte[0] == 0xFF)))
        {
        Photosensor_Set_FC_Lamp_OFF = 3.7; //37; //according to document [3.7 FC ]
        pulseCounter.float_data = Photosensor_Set_FC_Lamp_OFF;
        WriteByte_EEPROM(EE_Photosensor_Set_FC_Lamp_OFF + 3, pulseCounter.byte[3]);
        WriteByte_EEPROM(EE_Photosensor_Set_FC_Lamp_OFF + 2, pulseCounter.byte[2]);
        WriteByte_EEPROM(EE_Photosensor_Set_FC_Lamp_OFF + 1, pulseCounter.byte[1]);
        WriteByte_EEPROM(EE_Photosensor_Set_FC_Lamp_OFF + 0, pulseCounter.byte[0]);

        }


    DIGITAL0_ANALOG1_SELECTION = ReadByte_EEPROM(EE_DIGITAL0_ANALOG1_SELECTION + 0); //need test
    if ((DIGITAL0_ANALOG1_SELECTION == 0xFF) || (DIGITAL0_ANALOG1_SELECTION > 1) || (ee_default_state == 1)) //never erase this location because hardware version is fix either for analog photo or digital photo
        {
        DIGITAL0_ANALOG1_SELECTION = 1;
        WriteByte_EEPROM(EE_DIGITAL0_ANALOG1_SELECTION + 0, DIGITAL0_ANALOG1_SELECTION);
        }

    ADCInit();
    ADCStart();

    /***********************Analog Photocell Stop************************/
    Motion_pulse_rate = ReadByte_EEPROM(EE_Motion_pulse_rate);
    if (Set_default_perameter == 1)
        {
        Motion_pulse_rate = 1;
        WriteByte_EEPROM(EE_Motion_pulse_rate, Motion_pulse_rate);
        }

    Motion_dimming_percentage = ReadByte_EEPROM(EE_Motion_dimming_percentage);
    if ((Motion_dimming_percentage > 100) || (Set_default_perameter == 1))
        {
        Motion_dimming_percentage = 0;
        WriteByte_EEPROM(EE_Motion_dimming_percentage, Motion_dimming_percentage);
        }

    Motion_dimming_time = (ReadByte_EEPROM(EE_Motion_dimming_time + 0)*256) + (ReadByte_EEPROM(EE_Motion_dimming_time + 1));
    if (Motion_dimming_time > (120 * 60))
        {
        Motion_dimming_time = 120;
        WriteByte_EEPROM(EE_Motion_dimming_time + 0, Motion_dimming_time / 256);
        WriteByte_EEPROM(EE_Motion_dimming_time + 1, Motion_dimming_time % 256);
        }

    if (Motion_dimming_time > 59)
        {
        Motion_Rebroadcast_timeout = 58;
        }
    else
        {
        Motion_Rebroadcast_timeout = Motion_dimming_time - 2;
        }

    Motion_group_id = ReadByte_EEPROM(EE_Motion_group_id);
    Group_val1.Val = Motion_group_id;
    dim_applay_time = (ReadByte_EEPROM(EE_dim_applay_time) * 256) + ReadByte_EEPROM(EE_dim_applay_time + 1); // Dimming_Apply_delay 
    if (dim_applay_time > 1800)
        {
        dim_applay_time = 0;
        WriteByte_EEPROM(EE_dim_applay_time + 0, dim_applay_time / 256);
        WriteByte_EEPROM(EE_dim_applay_time + 1, dim_applay_time % 256);
        }

    dim_inc_val = ReadByte_EEPROM(EE_dim_inc_val); // gradually_dimming_time 																	
    if (dim_inc_val > 60)
        {
        dim_inc_val = 0;
        WriteByte_EEPROM(EE_dim_inc_val, dim_inc_val);
        }

    Motion_normal_dimming_percentage = ReadByte_EEPROM(EE_Motion_normal_dimming_percentage);
    if (Motion_normal_dimming_percentage > 100)
        {
        Motion_normal_dimming_percentage = 100;
        WriteByte_EEPROM(EE_Motion_normal_dimming_percentage, Motion_normal_dimming_percentage);
        }

    Motion_Detect_Timeout = ReadByte_EEPROM(EE_Motion_Detect_Timeout);
    if (Motion_Detect_Timeout > 59)
        {
        Motion_Detect_Timeout = 5;
        WriteByte_EEPROM(EE_Motion_Detect_Timeout, Motion_Detect_Timeout);
        }

    Motion_Broadcast_Timeout = ReadByte_EEPROM(EE_Motion_Broadcast_Timeout);
    if (Motion_Broadcast_Timeout > 59)
        {
        Motion_Broadcast_Timeout = 5;
        WriteByte_EEPROM(EE_Motion_Broadcast_Timeout, Motion_Broadcast_Timeout);
        }

    Motion_Sensor_Type = ReadByte_EEPROM(EE_Motion_Sensor_Type);
    if (Motion_Sensor_Type > 1)
        {
        Motion_Sensor_Type = 0;
        WriteByte_EEPROM(EE_Motion_Sensor_Type, Motion_Sensor_Type);
        }

    if (ee_default_state == 1)
        {
        Motion_pulse_rate = 1;
        WriteByte_EEPROM(EE_Motion_pulse_rate, Motion_pulse_rate);
        Motion_group_id = 0;
        WriteByte_EEPROM(EE_Motion_group_id, Motion_group_id);
        Group_val1.Val = Motion_group_id;
        }



    Valid_DCU = ReadByte_EEPROM(EE_Valid_DCU);
    if ((Valid_DCU > 1) || (ee_default_state == 1))
        {
        Valid_DCU = 0;
        WriteByte_EEPROM(EE_Valid_DCU, Valid_DCU);
        }

    SLC_DST_En = ReadByte_EEPROM(EE_SLC_DST_En);
    old_year = Date.Year;
    if (SLC_DST_En > 1)
        {
        SLC_DST_En = 0;
        WriteByte_EEPROM(EE_SLC_DST_En, SLC_DST_En);
        SLC_DST_Start_Rule = 0;
        WriteByte_EEPROM(EE_SLC_DST_Start_Rule, SLC_DST_Start_Rule); //Change on 23/01/2018
        SLC_DST_Start_Month = 0;
        WriteByte_EEPROM(EE_SLC_DST_Start_Month, SLC_DST_Start_Month);
        SLC_DST_Start_Time = 0;
        WriteByte_EEPROM(EE_SLC_DST_Start_Time, SLC_DST_Start_Time);
        SLC_DST_Stop_Rule = 0;
        WriteByte_EEPROM(EE_SLC_DST_Stop_Rule, SLC_DST_Stop_Rule); // Change on 23/01/2018
        SLC_DST_Stop_Month = 0;
        WriteByte_EEPROM(EE_SLC_DST_Stop_Month, SLC_DST_Stop_Month);
        SLC_DST_Stop_Time = 0;
        WriteByte_EEPROM(EE_SLC_DST_Stop_Time, SLC_DST_Stop_Time);
        SLC_DST_Time_Zone_Diff = pulseCounter.float_data = 0;
        WriteByte_EEPROM(EE_SLC_DST_Time_Zone_Diff + 0, pulseCounter.byte[0]);
        WriteByte_EEPROM(EE_SLC_DST_Time_Zone_Diff + 1, pulseCounter.byte[1]);
        WriteByte_EEPROM(EE_SLC_DST_Time_Zone_Diff + 2, pulseCounter.byte[2]);
        WriteByte_EEPROM(EE_SLC_DST_Time_Zone_Diff + 3, pulseCounter.byte[3]);
        Time_change_dueto_DST = 0;
        WriteByte_EEPROM(EE_Time_change_dueto_DST, Time_change_dueto_DST);
        SLC_DST_Rule_Enable = 0;
        WriteByte_EEPROM(EE_SLC_DST_Rule_Enable, SLC_DST_Rule_Enable);
        SLC_DST_Start_Date = 0;
        WriteByte_EEPROM(EE_SLC_DST_Start_Date, SLC_DST_Start_Date);
        SLC_DST_Stop_Date = 0;
        WriteByte_EEPROM(EE_SLC_DST_Stop_Date, SLC_DST_Stop_Date);
        }
    else
        {
        SLC_DST_Start_Rule = ReadByte_EEPROM(EE_SLC_DST_Start_Rule);
        SLC_DST_Start_Month = ReadByte_EEPROM(EE_SLC_DST_Start_Month);
        SLC_DST_Start_Time = ReadByte_EEPROM(EE_SLC_DST_Start_Time);
        SLC_DST_Stop_Rule = ReadByte_EEPROM(EE_SLC_DST_Stop_Rule);
        SLC_DST_Stop_Month = ReadByte_EEPROM(EE_SLC_DST_Stop_Month);
        SLC_DST_Stop_Time = ReadByte_EEPROM(EE_SLC_DST_Stop_Time);
        pulseCounter.byte[3] = ReadByte_EEPROM(EE_SLC_DST_Time_Zone_Diff + 0);
        pulseCounter.byte[2] = ReadByte_EEPROM(EE_SLC_DST_Time_Zone_Diff + 1);
        pulseCounter.byte[1] = ReadByte_EEPROM(EE_SLC_DST_Time_Zone_Diff + 2);
        pulseCounter.byte[0] = ReadByte_EEPROM(EE_SLC_DST_Time_Zone_Diff + 3);
        SLC_DST_Time_Zone_Diff = pulseCounter.float_data;
        Time_change_dueto_DST = ReadByte_EEPROM(EE_Time_change_dueto_DST);
        SLC_DST_Rule_Enable = ReadByte_EEPROM(EE_SLC_DST_Rule_Enable);
        SLC_DST_Start_Date = ReadByte_EEPROM(EE_SLC_DST_Start_Date);
        SLC_DST_Stop_Date = ReadByte_EEPROM(EE_SLC_DST_Stop_Date);

        if (SLC_DST_Rule_Enable == 1)
            {
            SLC_DST_R_Start_Date = find_dst_date_from_rule(SLC_DST_Start_Rule, SLC_DST_Start_Month);
            SLC_DST_R_Stop_Date = find_dst_date_from_rule(SLC_DST_Stop_Rule, SLC_DST_Stop_Month);
            }
        else
            {
            SLC_DST_R_Start_Date = SLC_DST_Start_Date;
            SLC_DST_R_Stop_Date = SLC_DST_Stop_Date;
            }
        }


    if (ee_default_state == 1)
        {
        for (j = 0; j < 10; j++)
            {

            if (j == 5)
                {
                Slc_Comp_Mix_Mode_schedule[j].SLC_Mix_En = 1;
                WriteByte_EEPROM(EE_MIX_SCHEDULE + (j * 65) + 1, Slc_Comp_Mix_Mode_schedule[j].SLC_Mix_En);

                Slc_Comp_Mix_Mode_schedule[j].SLC_Mix_Sch_Start_Date = 1;
                WriteByte_EEPROM(EE_MIX_SCHEDULE + (j * 65) + 2, Slc_Comp_Mix_Mode_schedule[j].SLC_Mix_Sch_Start_Date);

                Slc_Comp_Mix_Mode_schedule[j].SLC_Mix_Sch_Start_Month = 1;
                WriteByte_EEPROM(EE_MIX_SCHEDULE + (j * 65) + 3, Slc_Comp_Mix_Mode_schedule[j].SLC_Mix_Sch_Start_Month);

                Slc_Comp_Mix_Mode_schedule[j].SLC_Mix_Sch_Stop_Date = 31;
                WriteByte_EEPROM(EE_MIX_SCHEDULE + (j * 65) + 4, Slc_Comp_Mix_Mode_schedule[j].SLC_Mix_Sch_Stop_Date);

                Slc_Comp_Mix_Mode_schedule[j].SLC_Mix_Sch_Stop_Month = 12;
                WriteByte_EEPROM(EE_MIX_SCHEDULE + (j * 65) + 5, Slc_Comp_Mix_Mode_schedule[j].SLC_Mix_Sch_Stop_Month);

                Slc_Comp_Mix_Mode_schedule[j].SLC_Mix_Sch_WeekDay = 0x7f;
                WriteByte_EEPROM(EE_MIX_SCHEDULE + (j * 65) + 6, Slc_Comp_Mix_Mode_schedule[j].SLC_Mix_Sch_WeekDay);

                Slc_Comp_Mix_Mode_schedule[j].SLC_Mix_Sch_Photocell_Override = 1;
                WriteByte_EEPROM(EE_MIX_SCHEDULE + (j * 65) + 7, Slc_Comp_Mix_Mode_schedule[j].SLC_Mix_Sch_Photocell_Override);

                for (i = 0; i < 9; i++)
                    {

                    if (i == 0)
                        {
                        Slc_Comp_Mix_Mode_schedule[j].Slc_Mix_Sch[i].compress1 = 0x31;

                        Slc_Comp_Mix_Mode_schedule[j].Slc_Mix_Sch[i].compress2 = 0x76;

                        Slc_Comp_Mix_Mode_schedule[j].Slc_Mix_Sch[i].compress3 = 0x77;

                        Slc_Comp_Mix_Mode_schedule[j].Slc_Mix_Sch[i].compress4 = 0;

                        Slc_Comp_Mix_Mode_schedule[j].Slc_Mix_Sch[i].compress5 = 0;

                        Slc_Comp_Mix_Mode_schedule[j].Slc_Mix_Sch[i].compress6 = 0;
                        Fill_Slc_Comp_Mix_Mode_schedule_compress(i, j);

                        }
                    else if (i == 1)
                        {

                        Slc_Comp_Mix_Mode_schedule[j].Slc_Mix_Sch[i].compress1 = 0x01;

                        Slc_Comp_Mix_Mode_schedule[j].Slc_Mix_Sch[i].compress2 = 0x90;

                        Slc_Comp_Mix_Mode_schedule[j].Slc_Mix_Sch[i].compress3 = 0x33;

                        Slc_Comp_Mix_Mode_schedule[j].Slc_Mix_Sch[i].compress4 = 0;

                        Slc_Comp_Mix_Mode_schedule[j].Slc_Mix_Sch[i].compress5 = 0;

                        Slc_Comp_Mix_Mode_schedule[j].Slc_Mix_Sch[i].compress6 = 0;
                        Fill_Slc_Comp_Mix_Mode_schedule_compress(i, j);

                        }

                    else
                        {

                        Slc_Comp_Mix_Mode_schedule[j].Slc_Mix_Sch[i].compress1 = 0;

                        Slc_Comp_Mix_Mode_schedule[j].Slc_Mix_Sch[i].compress2 = 0;

                        Slc_Comp_Mix_Mode_schedule[j].Slc_Mix_Sch[i].compress3 = 0;

                        Slc_Comp_Mix_Mode_schedule[j].Slc_Mix_Sch[i].compress4 = 0;

                        Slc_Comp_Mix_Mode_schedule[j].Slc_Mix_Sch[i].compress5 = 0;

                        Slc_Comp_Mix_Mode_schedule[j].Slc_Mix_Sch[i].compress6 = 0;
                        Fill_Slc_Comp_Mix_Mode_schedule_compress(i, j);

                        }
                    }
                }
            else
                {


                Slc_Comp_Mix_Mode_schedule[j].SLC_Mix_En = 0;
                WriteByte_EEPROM(EE_MIX_SCHEDULE + (j * 65) + 1, Slc_Comp_Mix_Mode_schedule[j].SLC_Mix_En);

                Slc_Comp_Mix_Mode_schedule[j].SLC_Mix_Sch_Start_Date = 0;
                WriteByte_EEPROM(EE_MIX_SCHEDULE + (j * 65) + 2, Slc_Comp_Mix_Mode_schedule[j].SLC_Mix_Sch_Start_Date);

                Slc_Comp_Mix_Mode_schedule[j].SLC_Mix_Sch_Start_Month = 0;
                WriteByte_EEPROM(EE_MIX_SCHEDULE + (j * 65) + 3, Slc_Comp_Mix_Mode_schedule[j].SLC_Mix_Sch_Start_Month);

                Slc_Comp_Mix_Mode_schedule[j].SLC_Mix_Sch_Stop_Date = 0;
                WriteByte_EEPROM(EE_MIX_SCHEDULE + (j * 65) + 4, Slc_Comp_Mix_Mode_schedule[j].SLC_Mix_Sch_Stop_Date);

                Slc_Comp_Mix_Mode_schedule[j].SLC_Mix_Sch_Stop_Month = 0;
                WriteByte_EEPROM(EE_MIX_SCHEDULE + (j * 65) + 5, Slc_Comp_Mix_Mode_schedule[j].SLC_Mix_Sch_Stop_Month);

                Slc_Comp_Mix_Mode_schedule[j].SLC_Mix_Sch_WeekDay = 0;
                WriteByte_EEPROM(EE_MIX_SCHEDULE + (j * 65) + 6, Slc_Comp_Mix_Mode_schedule[j].SLC_Mix_Sch_WeekDay);

                Slc_Comp_Mix_Mode_schedule[j].SLC_Mix_Sch_Photocell_Override = 0;
                WriteByte_EEPROM(EE_MIX_SCHEDULE + (j * 65) + 7, Slc_Comp_Mix_Mode_schedule[j].SLC_Mix_Sch_Photocell_Override);

                for (i = 0; i < 9; i++)
                    {
                    Slc_Comp_Mix_Mode_schedule[j].Slc_Mix_Sch[i].compress1 = 0;

                    Slc_Comp_Mix_Mode_schedule[j].Slc_Mix_Sch[i].compress2 = 0;

                    Slc_Comp_Mix_Mode_schedule[j].Slc_Mix_Sch[i].compress3 = 0;

                    Slc_Comp_Mix_Mode_schedule[j].Slc_Mix_Sch[i].compress4 = 0;

                    Slc_Comp_Mix_Mode_schedule[j].Slc_Mix_Sch[i].compress5 = 0;

                    Slc_Comp_Mix_Mode_schedule[j].Slc_Mix_Sch[i].compress6 = 0;
                    Fill_Slc_Comp_Mix_Mode_schedule_compress(i, j);
                    }


                }

            }
        }


    for (j = 0; j < 10; j++)
        {
        Slc_Comp_Mix_Mode_schedule[j].SLC_Mix_En = ReadByte_EEPROM(EE_MIX_SCHEDULE + (j * 65) + 1);
        Slc_Comp_Mix_Mode_schedule[j].SLC_Mix_Sch_Start_Date = ReadByte_EEPROM(EE_MIX_SCHEDULE + (j * 65) + 2);
        Slc_Comp_Mix_Mode_schedule[j].SLC_Mix_Sch_Start_Month = ReadByte_EEPROM(EE_MIX_SCHEDULE + (j * 65) + 3);
        Slc_Comp_Mix_Mode_schedule[j].SLC_Mix_Sch_Stop_Date = ReadByte_EEPROM(EE_MIX_SCHEDULE + (j * 65) + 4);
        Slc_Comp_Mix_Mode_schedule[j].SLC_Mix_Sch_Stop_Month = ReadByte_EEPROM(EE_MIX_SCHEDULE + (j * 65) + 5);
        Slc_Comp_Mix_Mode_schedule[j].SLC_Mix_Sch_WeekDay = ReadByte_EEPROM(EE_MIX_SCHEDULE + (j * 65) + 6);
        Slc_Comp_Mix_Mode_schedule[j].SLC_Mix_Sch_Photocell_Override = ReadByte_EEPROM(EE_MIX_SCHEDULE + (j * 65) + 7);
        for (i = 0; i < 9; i++)
            {
            Slc_Comp_Mix_Mode_schedule[j].Slc_Mix_Sch[i].compress1 = ReadByte_EEPROM(EE_MIX_SCHEDULE + (j * 65) + (i * 6) + 8);
            Slc_Comp_Mix_Mode_schedule[j].Slc_Mix_Sch[i].compress2 = ReadByte_EEPROM(EE_MIX_SCHEDULE + (j * 65) + (i * 6) + 9);
            Slc_Comp_Mix_Mode_schedule[j].Slc_Mix_Sch[i].compress3 = ReadByte_EEPROM(EE_MIX_SCHEDULE + (j * 65) + (i * 6) + 10);
            Slc_Comp_Mix_Mode_schedule[j].Slc_Mix_Sch[i].compress4 = ReadByte_EEPROM(EE_MIX_SCHEDULE + (j * 65) + (i * 6) + 11);
            Slc_Comp_Mix_Mode_schedule[j].Slc_Mix_Sch[i].compress5 = ReadByte_EEPROM(EE_MIX_SCHEDULE + (j * 65) + (i * 6) + 12);
            Slc_Comp_Mix_Mode_schedule[j].Slc_Mix_Sch[i].compress6 = ReadByte_EEPROM(EE_MIX_SCHEDULE + (j * 65) + (i * 6) + 13);
            }
        }

    Tilt_Enable = ReadByte_EEPROM(EE_Lamp_type);
    if (Tilt_Enable == 0xFF)
        {
        Tilt_Enable = 0;
        WriteByte_EEPROM(EE_Lamp_type, Tilt_Enable);
        }

    Auto_event = ReadByte_EEPROM(EE_Auto_event);
    if ((Auto_event != 1) || (Auto_event != 0))
        {
        Auto_event = 1; // PUSH CNG  Auto event done enable 
        WriteByte_EEPROM(EE_Auto_event, Auto_event);
        }

    SLC_New_Manula_Mode_Val = ReadByte_EEPROM(EE_SLC_New_Manula_Mode_Val);
    if (SLC_New_Manula_Mode_Val > 101)
        {
        SLC_New_Manula_Mode_Val = 101;
        WriteByte_EEPROM(EE_SLC_New_Manula_Mode_Val, SLC_New_Manula_Mode_Val);
        }

    if (SLC_New_Manula_Mode_Val < 101)
        {
        SLC_New_Manula_Mode_En = 0; //san	
        //	SLC_New_Manula_Mode_En = 1;	
        SLC_New_Manual_Mode_Timer = (ReadByte_EEPROM(EE_SLC_New_Manual_Mode_Timer + 0) * 256) + ReadByte_EEPROM(EE_SLC_New_Manual_Mode_Timer + 1);
        if (ee_default_state == 1)
            {
            SLC_New_Manual_Mode_Timer = 0;
            WriteByte_EEPROM(EE_SLC_New_Manual_Mode_Timer + 0, SLC_New_Manual_Mode_Timer / 256);
            WriteByte_EEPROM(EE_SLC_New_Manual_Mode_Timer + 1, SLC_New_Manual_Mode_Timer % 256);
            }
        }
    else
        {
        SLC_New_Manula_Mode_En = 0;
        SLC_New_Manual_Mode_Timer = 0;
        WriteByte_EEPROM(EE_SLC_New_Manual_Mode_Timer + 0, SLC_New_Manual_Mode_Timer / 256);
        WriteByte_EEPROM(EE_SLC_New_Manual_Mode_Timer + 1, SLC_New_Manual_Mode_Timer % 256);
        }



    Photocell_steady_timeout_Val = ReadByte_EEPROM(EE_Photocell_steady_timeout_Val);
    if (Photocell_steady_timeout_Val == 0xFF)
        {
        Photocell_steady_timeout_Val = 18;
        }


    photo_cell_toggel_counter_Val = ReadByte_EEPROM(EE_photo_cell_toggel_counter_Val);
    if (photo_cell_toggel_counter_Val == 0xFF)
        {
        photo_cell_toggel_counter_Val = 10;
        }


    photo_cell_toggel_timer_Val = ReadByte_EEPROM(EE_photo_cell_toggel_timer_Val);
    if (photo_cell_toggel_timer_Val == 0xFF)
        {
        photo_cell_toggel_timer_Val = 18;
        }


    Photo_cell_Frequency = ReadByte_EEPROM(EE_Photo_cell_Frequency);
    if (Photo_cell_Frequency == 0xFF)
        {
        Photo_cell_Frequency = 30;
        }

    pulseCounter.byte[1] = ReadByte_EEPROM(EE_Photocell_unsteady_timeout_Val + 0);
    pulseCounter.byte[0] = ReadByte_EEPROM(EE_Photocell_unsteady_timeout_Val + 1);
    Photocell_unsteady_timeout_Val = pulseCounter.int_data;
    if (Photocell_unsteady_timeout_Val == 0xFFFF)
        {
        //Photocell_unsteady_timeout_Val = 180;
        Photocell_unsteady_timeout_Val = 5;
        }

    Schedule_offset = ReadByte_EEPROM(EE_Schedule_offset);
    if (Schedule_offset > 1)
        {
        Schedule_offset = 0;
        WriteByte_EEPROM(EE_Schedule_offset, Schedule_offset);
        }

    pulseCounter = ReadEEPROM(EE_POWERONCNT);

    power_on_count = pulseCounter.float_data;



    if ((pulseCounter.byte[3] == 0xFF) && (pulseCounter.byte[2] == 0xFF) && (pulseCounter.byte[1] == 0xFF) && (pulseCounter.byte[0] == 0xFF) || (Set_default_perameter == 1))
        {
        power_on_count = 0;
        pulseCounter.float_data = power_on_count;
        WriteEEPROM(EE_POWERONCNT, pulseCounter);
        }



    idframe_frequency = ReadByte_EEPROM(EE_idframe_frequency);
    if (idframe_frequency == 0xFF)
        {
            idframe_frequency = 1;
        //idframe_frequency = 15;///////##Rht
        WriteByte_EEPROM(EE_idframe_frequency, idframe_frequency);
        }
    
       CURV_TYPE = ReadByte_EEPROM(EE_CURV_TYPE_GET);
    if (CURV_TYPE == 0xFF)
        {
            CURV_TYPE = 1;
        //idframe_frequency = 15;///////##Rht
        WriteByte_EEPROM(EE_CURV_TYPE_GET , CURV_TYPE);
        }
    
    

    ID_Frame_reset_time = ReadByte_EEPROM(EE_ID_Frame_reset_time);
    if (ID_Frame_reset_time == 0xFF)
        {
        ID_Frame_reset_time = 12;
        WriteByte_EEPROM(EE_ID_Frame_reset_time, ID_Frame_reset_time);
        }


    Normal_reset_time = ReadByte_EEPROM(EE_Normal_reset_time);
    if (Normal_reset_time == 0xFF)
        {
        Normal_reset_time = 120;
        WriteByte_EEPROM(EE_Normal_reset_time, Normal_reset_time);
        }

    pulseCounter.byte[1] = ReadByte_EEPROM(EE_ID_frame_timeout + 0);
    pulseCounter.byte[0] = ReadByte_EEPROM(EE_ID_frame_timeout + 1);
    ID_frame_timeout = pulseCounter.int_data;
    if (ID_frame_timeout > 14400)
        {
        ID_frame_timeout = 60;
        pulseCounter.int_data = ID_frame_timeout;
        WriteByte_EEPROM(EE_ID_frame_timeout + 0, pulseCounter.byte[1]);
        WriteByte_EEPROM(EE_ID_frame_timeout + 1, pulseCounter.byte[0]);
        }


    bhip_idframe = ReadByte_EEPROM(EE_BHIP_IDFRAME);
    if (bhip_idframe == 0xFF)
        {
        bhip_idframe = 10;
        send_id_frame_flage = 1;
        WriteByte_EEPROM(EE_BHIP_IDFRAME, bhip_idframe);
        }


    //	get_push_lograte();	

    pulseCounter.byte[1] = ReadByte_EEPROM(EE_TIMESLICE + 0);
    pulseCounter.byte[0] = ReadByte_EEPROM(EE_TIMESLICE + 1);
    time_slice = pulseCounter.int_data;
    if ((time_slice > 1000) || (time_slice == 0))
        {
        time_slice = 500;
        pulseCounter.int_data = time_slice;
        WriteByte_EEPROM(EE_TIMESLICE + 0, pulseCounter.byte[1]);
        WriteByte_EEPROM(EE_TIMESLICE + 1, pulseCounter.byte[0]);
        }

    previous_mode = ReadByte_EEPROM(EE_PREV_MODE);
    //WriteByte_EEPROM(EE_PREV_MODE,previous_mode);
    if ((previous_mode > 8) || (Set_default_perameter == 1))
        {
        previous_mode = 1; //automode;
        WriteByte_EEPROM(EE_PREV_MODE, previous_mode);

        }
    RTC_New_fault_logic_Enable = ReadByte_EEPROM(EE_RTC_New_fault_logic_Enable);
    if ((RTC_New_fault_logic_Enable < 1) || (RTC_New_fault_logic_Enable > 2) || (Set_default_perameter == 1))
        {
        RTC_New_fault_logic_Enable = 1;
        WriteByte_EEPROM(EE_RTC_New_fault_logic_Enable, RTC_New_fault_logic_Enable);

        }


    Lamp_Balast_fault_Remove_Time = ReadByte_EEPROM(EE_FALUT_RECOVER);
    if ((Lamp_Balast_fault_Remove_Time > 24) || (Set_default_perameter == 1))
        {
        Lamp_Balast_fault_Remove_Time = 12;
        WriteByte_EEPROM(EE_FALUT_RECOVER, Lamp_Balast_fault_Remove_Time);
        }



    Push_Energy_meter_data = ReadByte_EEPROM(EE_PUSH_ENERGY_DATA);

    if (((Push_Energy_meter_data != 1) && (Push_Energy_meter_data != 0)) || (Set_default_perameter == 1))
        {
        Push_Energy_meter_data = 1;
        WriteByte_EEPROM(EE_PUSH_ENERGY_DATA, Push_Energy_meter_data);
        }

    }

void Parse_RF_Packet(unsigned char *data_rf)
    {

    unsigned int i = 0, j = 0, len;
    unsigned char fream_crc = 0, sum = 0, crc = 0;

    unsigned char temp_received [150];
    for (j = 0; j < B_SIZE_SERIAL_RF; j++)
        {
        i = 0;
        sum = 0;

        if (data_rf[j + 0] == 0x7E)
            {
            len = (data_rf[j + 1] * 256) + data_rf[j + 2];

            if (len <= 100)
                {
                if (data_rf[j + 3] == 0x90)
                    {
                    fream_crc = data_rf[j + len + 3];
                    for (i = j + 3; i < j + len + 3; i++)
                        {
                        sum = sum + data_rf[i];
                        }
                    crc = 0xFF - sum;
                    if (crc == fream_crc)
                        {
                        macLongAddrArray[0] = data_rf[j + 4];
                        macLongAddrArray[1] = data_rf[j + 5];
                        macLongAddrArray[2] = data_rf[j + 6];
                        macLongAddrArray[3] = data_rf[j + 7];
                        macLongAddrArray[4] = data_rf[j + 8];
                        macLongAddrArray[5] = data_rf[j + 9];
                        macLongAddrArray[6] = data_rf[j + 10];
                        macLongAddrArray[7] = data_rf[j + 11];

                        for (i = j + 15; i < j + len + 3; i++)
                            {
                            temp_received[i - 15 - j] = data_rf[i];
                            }
                        //	mins_since_last_NIC_comm =0;
                        checkfream(temp_received);
                        }
                    else
                        {

                        }
                    j = j + len + 3;
                    }
                else if (data_rf[j + 3] == 0x8A)
                    {

                    fream_crc = data_rf[j + len + 3];

                    for (i = j + 3; i < j + len + 3; i++)
                        {
                        sum = sum + data_rf[i];
                        }

                    crc = 0xFF - sum;

                    if (crc == fream_crc)
                        {
                        //mins_since_last_NIC_comm =0;
                        if (data_rf[j + 4] == 0x02)
                            {
                            Network_found = 1; // SLC joins network
                            }
                        else if (data_rf[j + 4] == 0x03)
                            {
                            Network_found = 0; // SLC leaves network
                            }

                        RF_Modem_detected = 1;
                        idframe_frequency = 0;
                        ID_frame_timeout = 30;
                        push_time_slice_ms = 0;
                        //						timeslice2=0;
                        automode = 0;
                        Commissioning_flag = 1;
                        }
                    else
                        {
                        }
                    j = j + len + 3;
                    }

                }
            }
        else if (data_rf[j + 0] == 0x8E)
            {

            if (data_rf[j + 1] == 0x4C)
                {
                if (data_rf[j + 4] == 0x43)
                    {

                    if (data_rf[j + 5] == 0x92)
                        {
                        Voltage_calibration = 1;
                        calibmode = 1;
                        pulses_r = 0;
                        Cali_Cnt = 0; //added patch on 6Jan2017 due to cali appl
                        }
                    else if (data_rf[j + 5] == 0x93)
                        {
                        Current_calibration = 1;
                        }
                    else if (data_rf[j + 5] == 0x94)
                        {
                        KW_Calibration = 1;
                        }
                    else if (data_rf[j + 5] == 0x95)
                        {
                        Read_Em_Calibration = 1;
                        }
                    else if (data_rf[j + 5] == 0x96)
                        {
                        kwh = 0;
                        pulseCounter.long_data = kwh;
                        Read_KWH_Value = 1;

                        WriteByte_EEPROM(EE_KWH + 0, pulseCounter.byte[0]);
                        WriteByte_EEPROM(EE_KWH + 1, pulseCounter.byte[1]);
                        WriteByte_EEPROM(EE_KWH + 2, pulseCounter.byte[2]);
                        WriteByte_EEPROM(EE_KWH + 3, pulseCounter.byte[3]);
                        }
                    else if (data_rf[j + 5] == 0x97)
                        {
                        calibmode = 1;
                        KW_Calibration_120 = 1;
                        //pulses_r = 0;
                        }
                    else
                        {
                        }
                    }
                }

            }
        }

    if (RF_Modem_detected != 1)
        {
        checkfream(&Serial_RF_Buff[0]);
        }


    for (i = 0; i < B_SIZE_SERIAL_RF; i++)
        {
        Serial_RF_Buff[i] = 0;
        }
    }

void Check_Slc_Test_1(void)
    {
    Lamp_off_on_Time = 20;

    Check_RTC(10);
    Check_EMT(30);

    if (photocell_test_dicision == 1) // if photocell present in testing
        {
        Check_photocell(2);
        }

    Check_CT(2);
    Check_Lamp(2);
    Set_Do(0);
    Check_eeprom();

    check_dimming_hardware(); // check dimming hardware if set for test
    send_SLC_test_query_1 = 1; // send result of testing to application.
    test_query_1_send_timer = 0;

    }

unsigned char test_EEPROM(void)
    {
    return (1);
    }

void Check_RTC(unsigned char time)
    {
    unsigned char dd = 0, mo = 0, yy = 0, hh = 0, mm = 0, ss = 0;

    read_PCF85363_bytes();
    read_PCF85363_bytes();
    read_PCF85363_bytes();

    dd = Date.Date;
    mo = Date.Month;
    yy = Date.Year;
    hh = Time.Hour;
    mm = Time.Min;
    ss = Time.Sec;

    cheak_emt_timer_sec = 0;
    while (cheak_emt_timer_sec < time)
        {
        if (one_sec == 1)
            {
            one_sec = 0;
            ClrWdt();
            read_PCF85363_bytes();
            }
        }

    if ((dd == Date.Date) && (mo == Date.Month) && (yy == Date.Year) && (hh == Time.Hour) && (mm == Time.Min) && (ss == Time.Sec))
        {
        test_result.bits.b0 = 0; // RTC faulty
        }
    else
        {
        test_result.bits.b0 = 1; // RTC ok.
        }

    temp_arr[0] = 'S';
    temp_arr[1] = 'L';


    /*
    temp_arr[2]=amr_id/256;
    temp_arr[3]=amr_id%256;
     */
    pulseCounter.long_data = amr_id;
    temp_arr[2] = pulseCounter.byte[0];
    temp_arr[3] = pulseCounter.byte[1];
    temp_arr[4] = pulseCounter.byte[2];
    temp_arr[5] = pulseCounter.byte[3];

    temp_arr[6] = RTC_TEST_DONE;

    Send_data_RF1(temp_arr, 7, 1);

    }

void Check_EMT(unsigned char time)
    {
    emt_int_count = 0;
    Kwh_count = 0;
    dimming_applied(0); // set the dimming to 0 per for full. 
    Set_Do(1);
    cheak_emt_timer_sec = 0;
    while (cheak_emt_timer_sec < time)
        {
        dimming_applied(0); // set the dimming to 0 per for full. 
        Set_Do(1);
        Read_EnergyMeter();
        calculate_3p4w();
        if (one_sec == 1)
            {
            one_sec = 0;
            ClrWdt();
            read_PCF85363_bytes();
            }
        }

    if (emt_int_count == 0)
        {
        test_result.bits.b1 = 0; // EMT IC faulty	
        }
    else if ((emt_int_count > 0) && (irdoub > CREEP_LIMIT) && (Kwh_count == 0))
        {
        test_result.bits.b1 = 0; // EMT IC faulty
        }
    else
        {
        test_result.bits.b1 = 1; // EMT IC OK	
        }

    test_voltage = vrdoub;
    test_current = irdoub;
    test_emt_int_count = emt_int_count;
    test_Kwh_count = Kwh_count;

    temp_arr[0] = 'S';
    temp_arr[1] = 'L';
    pulseCounter.long_data = amr_id;
    temp_arr[2] = pulseCounter.byte[0];
    temp_arr[3] = pulseCounter.byte[1];
    temp_arr[4] = pulseCounter.byte[2];
    temp_arr[5] = pulseCounter.byte[3];

    /*
    temp_arr[2]=amr_id/256;
    temp_arr[3]=amr_id%256;
     */

    temp_arr[6] = EMT_TEST_DONE;

    Send_data_RF1(temp_arr, 7, 1);
    }

void Check_Lamp(unsigned char time)
    {
    unsigned char i = 0;

    test_result.bits.b2 = 1;

    for (i = 0; i < time; i++)
        {
        cheak_emt_timer_sec = 0;
        while (cheak_emt_timer_sec < 30)
            {
            Set_Do(1);
            Read_EnergyMeter();
            calculate_3p4w();
            ClrWdt();
            if (one_sec == 1)
                {
                one_sec = 0;
                ClrWdt();
                read_PCF85363_bytes();
                }
            }

        if (photocell_test_dicision == 1) // if photocell present in testing
            {
            if ((irdoub > CREEP_LIMIT) || (Photo_feedback == 1))
                {
                if (test_result.bits.b2 != 0)
                    {
                    test_result.bits.b2 = 1; // Lamp OK. 
                    }

                }
            else
                {
                test_result.bits.b2 = 0; // Lamp faulty.	
                }
            }
        else
            {
            if (irdoub < CREEP_LIMIT)
                {
                test_result.bits.b2 = 0; // Lamp faulty.	
                }
            else
                {
                if (test_result.bits.b2 != 0)
                    {
                    test_result.bits.b2 = 1; // Lamp OK.	
                    }
                }
            }

        cheak_emt_timer_sec = 0;
        while (cheak_emt_timer_sec < 30)
            {
            Set_Do(0);
            Read_EnergyMeter();
            calculate_3p4w();
            ClrWdt();
            if (one_sec == 1)
                {
                one_sec = 0;
                ClrWdt();
                read_PCF85363_bytes();
                }
            }

        if (photocell_test_dicision == 1) // if photocell present in testing
            {
            if ((irdoub < CREEP_LIMIT) || (Photo_feedback == 0))
                {
                if (test_result.bits.b2 != 0)
                    {
                    test_result.bits.b2 = 1; // Lamp OK.
                    }
                }
            else
                {
                test_result.bits.b2 = 0; // Lamp faulty.	
                }
            }
        else
            {
            if (irdoub > CREEP_LIMIT)
                {
                test_result.bits.b2 = 0; // Lamp faulty.	
                }
            else
                {
                if (test_result.bits.b2 != 0)
                    {
                    test_result.bits.b2 = 1; // Lamp OK.	
                    }
                }
            }

        }

    temp_arr[0] = 'S';
    temp_arr[1] = 'L';
    /*
    temp_arr[2]=amr_id/256;
    temp_arr[3]=amr_id%256;
     */
    pulseCounter.long_data = amr_id;
    temp_arr[2] = pulseCounter.byte[0];
    temp_arr[3] = pulseCounter.byte[1];
    temp_arr[4] = pulseCounter.byte[2];
    temp_arr[5] = pulseCounter.byte[3];
    temp_arr[6] = LAMP_TEST_DONE;

    Send_data_RF1(temp_arr, 7, 1);
    }

void Check_photocell(unsigned char time)
    {
    unsigned char i = 0;

    test_result.bits.b3 = 1;
    Photocell_unsteady_timeout_Val = 5;
    for (i = 0; i < time; i++)
        {

        cheak_emt_timer_sec = 0;
        while (cheak_emt_timer_sec < 30)
            {
            ADC_Process();
            Set_Do(1);
            Read_EnergyMeter();
            calculate_3p4w();
            ClrWdt();
            if (one_sec == 1)
                {
                one_sec = 0;
                ClrWdt();
                read_PCF85363_bytes();
                }
            }

        if (Photo_feedback == 1)
            {
            if (test_result.bits.b3 != 0)
                {
                test_result.bits.b3 = 1; // Photocell OK
                }
            }
        else
            {
            test_result.bits.b3 = 0; // Photocell Faulty	
            }


        cheak_emt_timer_sec = 0;
        while (cheak_emt_timer_sec < 30)
            {
            ADC_Process();
            Set_Do(0);
            Read_EnergyMeter();
            calculate_3p4w();
            ClrWdt();
            if (one_sec == 1)
                {
                one_sec = 0;
                ClrWdt();
                read_PCF85363_bytes();
                }
            }

        if (Photo_feedback == 0)
            {
            if (test_result.bits.b3 != 0)
                {
                test_result.bits.b3 = 1; // Photocell OK.	
                }
            }
        else
            {
            test_result.bits.b3 = 0; // Photocell Faulty.				
            }
        }

    temp_arr[0] = 'S';
    temp_arr[1] = 'L';
    /*
    temp_arr[2]=amr_id/256;
    temp_arr[3]=amr_id%256;
     */
    pulseCounter.long_data = amr_id;
    temp_arr[2] = pulseCounter.byte[0];
    temp_arr[3] = pulseCounter.byte[1];
    temp_arr[4] = pulseCounter.byte[2];
    temp_arr[5] = pulseCounter.byte[3];
    temp_arr[6] = PHOTOCELL_TEST_DONE;

    Send_data_RF1(temp_arr, 7, 1);
    }

void Check_CT(unsigned char time)
    {
    unsigned char i = 0;

    test_result.bits.b4 = 1;

    for (i = 0; i < time; i++)
        {

        cheak_emt_timer_sec = 0;
        while (cheak_emt_timer_sec < 30)
            {
            Set_Do(1);
            Read_EnergyMeter();
            calculate_3p4w();
            ClrWdt();
            if (one_sec == 1)
                {
                one_sec = 0;
                ClrWdt();
                read_PCF85363_bytes();
                }
            }

        if (photocell_test_dicision == 1) // if photocell present in testing
            {
            if ((irdoub > CREEP_LIMIT))
                {
                if (test_result.bits.b4 != 0)
                    {
                    test_result.bits.b4 = 1; // CT OK.
                    }
                }
            else
                {

                test_result.bits.b4 = 0; // CT Faulty.	

                }
            }
        else
            {
            if (irdoub < CREEP_LIMIT)
                {
                test_result.bits.b4 = 0; // CT faulty.	
                }
            else
                {
                if (test_result.bits.b4 != 0)
                    {
                    test_result.bits.b4 = 1; // CT OK.	
                    }
                }
            }


        cheak_emt_timer_sec = 0;
        while (cheak_emt_timer_sec < 30)
            {
            Set_Do(0);
            Read_EnergyMeter();
            calculate_3p4w();
            ClrWdt();
            if (one_sec == 1)
                {
                one_sec = 0;
                ClrWdt();
                read_PCF85363_bytes();
                }
            }

        if (photocell_test_dicision == 1) // if photocell present in testing
            {
            if (irdoub < CREEP_LIMIT)
                {
                if (test_result.bits.b4 != 0)
                    {
                    test_result.bits.b4 = 1; // CT OK.
                    }
                }
            else
                {
                test_result.bits.b4 = 0; // CT Faulty.	
                }
            }
        else
            {
            if ((irdoub > CREEP_LIMIT))
                {
                test_result.bits.b4 = 0; // CT faulty.	
                }
            else
                {
                if (test_result.bits.b4 != 0)
                    {
                    test_result.bits.b4 = 1; // CT OK.	
                    }
                }
            }

        }

    temp_arr[0] = 'S';
    temp_arr[1] = 'L';
    /*
    temp_arr[2]=amr_id/256;
    temp_arr[3]=amr_id%256;
     */
    pulseCounter.long_data = amr_id;
    temp_arr[2] = pulseCounter.byte[0];
    temp_arr[3] = pulseCounter.byte[1];
    temp_arr[4] = pulseCounter.byte[2];
    temp_arr[5] = pulseCounter.byte[3];
    temp_arr[6] = CT_TEST_DONE;
    Send_data_RF1(temp_arr, 7, 1);

    }

void Check_eeprom(void)
    {
    test_result.bits.b5 = 1; //EEPROM ok
    }

/*************************************************************************
Function Name: check_power_cycle
input: None.
Output: None.
Discription: this function is used to check RTC and EEPROM after power cycle.
 *************************************************************************/
void check_power_cycle(void)
    {
    unsigned char dd = 0, mo = 0, yy = 0, hh = 0, mm = 0, ss = 0;

    test_result.bits.b6 = 0; //after power on EEPROM ok

    read_PCF85363_bytes();
    dd = Date.Date;
    mo = Date.Month;
    yy = Date.Year;
    hh = Time.Hour;
    mm = Time.Min;
    ss = Time.Sec;

    cheak_emt_timer_sec = 0;
    while (cheak_emt_timer_sec < 3)
        {
        read_PCF85363_bytes();
        read_PCF85363_bytes();
        }

    if ((dd == Date.Date) && (mo == Date.Month) && (yy == Date.Year) && (hh == Time.Hour) && (mm == Time.Min) && (ss == Time.Sec))
        {
        test_result.bits.b7 = 0; // RTC faulty
        Time.Hour = 0; // if currept then set Default RTC Time
        Time.Min = 0;
        Time.Sec = 0;
        Date.Date = 1;
        Date.Month = 1;
        Date.Year = 10;
        PCF_Force_SW_Reset();
        RTC_SET_TIME(Time);
        RTC_SET_DATE(Date);
        }
    else
        {
        if ((Time.Hour > 23) || (Time.Min > 59) || (Time.Sec > 59) || (Date.Date > 31) || (Date.Date < 1) || (Date.Month > 12) || (Date.Month < 1) || (Date.Year < 1) || (Date.Year > 99)) // Check RTC time
            {
            Time.Hour = 0; // if currept then set Default RTC Time
            Time.Min = 0;
            Time.Sec = 0;
            Date.Date = 1;
            Date.Month = 1;
            Date.Year = 10;
            RTC_SET_TIME(Time);
            RTC_SET_DATE(Date);
            test_result.bits.b7 = 0;
            }
        else
            {
            test_result.bits.b7 = 1;
            }
        }
    Local_Time.Hour = Time.Hour;
    Local_Time.Min = Time.Min;
    Local_Time.Sec = Time.Sec;
    Local_Date.Date = Date.Date;
    Local_Date.Month = Date.Month;
    Local_Date.Year = Date.Year;
    error_condition1.bits.b2 = 0;
    if (RTC_New_fault_logic_Enable == 1)
        {
        if (test_result.bits.b7 == 0)
            {
            RTC_faulty_detected = 0; // 0 = faulty.
            error_condition1.bits.b2 = 1;
            //        halCommonSetToken(TOKEN_RTC_faulty_detected,&RTC_faulty_detected);   // store mode in to NVM.
            if ((automode == 2) || (automode == 3) || (automode == 5))
                {
                previous_mode = automode; // store mode value before chage.
                WriteByte_EEPROM(EE_PREV_MODE, previous_mode); // store mode in to NVM.
                automode = 1; // move SLC in to Photocell mode
                WriteByte_EEPROM(EE_LOGICMODE, automode); // store mode in to NVM.
                }
            }
        }
    }

void check_dimming_protection(void) // SAN
    {
    if ((protec_flag) && (Pro_count >= 1))
        {
        protec_flag = 0;
        T4CONbits.TON = 0;
        Protec_sec = 0;
        //		PDO = 1;
        if (Protec_lock == 0)
            {
            if (Lamp_lock_condition == 1)
                {
                PDO = 1;
                Set_Do(1);
                }
            }
        }
    if (Pro_count >= 5) // samir
        {
        Protec_lock = 1;
        Pro_count = 0;
        if (Lamp_lock_condition == 1)
            {
            lamp_lock = 1;
            Set_Do(0);
            }
        //error_condition1.bits.b6 = 1;					// Dimming short circuit event generation.
        }
    }

void Tilt_Sensor_Detect(void)
    {

    if ((TILT_DI1 == 1)&&(Tilt_Get_Previously == 1)) //Normal Tilt not detected
        {
        Tilt_Cnt = 0;
        error_condition1.bits.b6 = 0;
        Tilt_Get_Previously = 0;
        if (Tilt_detect_flag == 2)
            {
            send_data_direct = 0;
            send_event_with_delay = 0;
            Tilt_Timeout_Cnt = 360;
            }

        }

    else if ((TILT_DI1 == 0)&&(Tilt_Get_Previously == 0)) //Tilt detect on this condition
        {

        if ((Tilt_Cnt > 20))
            {
            Tilt_Cnt = 0;
            //Tilt_Occured =1;
            Tilt_Get_Previously = 1;
            error_condition1.bits.b6 = 1;


            if (Tilt_detect_flag == 2)
                {
                //Tilt_Timeout_Cnt =180;
                Tilt_Timeout_Cnt = 360;
                }


            }
        if ((Auto_event == 1)&&(Id_frame_received == 1)&&(Tilt_Get_Previously == 1))
            {
            if (Tilt_detect_flag == 0)
                {
                Tilt_detect_flag = 1;
                Tilt_Timeout_Cnt = 0;
                }
            if ((Tilt_Timeout_Cnt < 360)&&(Tilt_detect_flag == 1))
                {
                push_time_slice_ms = 10;
                send_event_with_delay = 1;
                feel_event_cnt = 4;
                fill_event_data();
                send_fream();

                }
            else
                {
                Tilt_detect_flag = 2;

                }


            }
        }
    else
        {
        if (Tilt_detect_flag == 2)
            {
            send_data_direct = 0;
            send_event_with_delay = 0;
            if (Tilt_Timeout_Cnt > 600) //if tilt not detect continously for 2 min then make it normal
                {
                Tilt_detect_flag = 0;
                Tilt_Timeout_Cnt = 0;
                }
            }
        }


    }


//#if defined(DALI_SUPPORT)

void DALI_ON_CMD(unsigned char cmd)
    {
    if (cmd == 1)
        {
        forward = (0x07 << 8) | 0x05;
        f_dalitx = 1;
        }
    else if (cmd == 0)
        {
        forward = (0x07 << 8) | 0x00;
        f_dalitx = 1;
        }

    }

void DALI_Send(void)
    {
    DALIE = 0;
    if (f_repeat) // repeat last command ? 
        {
        //f_repeat =  0; 
        }
    else if ((forward & 0xE100) == 0xA100 || (forward & 0xE100) == 0xC100)
        {
        //if ((forward &  0xFF00) == INITIALISE  || forward == RANDOMISE) 
        //f_repeat =  1;                                 // special command repeat < 100 ms 
        }
    else if ((forward & 0x1FF) >= 0x120 && (forward & 0x1FF) <= 0x180)
        {
        //f_repeat =  1;                                      // config. command repeat < 100 ms 
        }

    //while (f_busy) ;                                         // Wait until dali port is idle 
    DaliTimeOut = 0;
    while (f_busy)
        {
        if (DaliTimeOut > 2)
            {
            break;
            }
        }
    frame = 0;
    value = 0; // first half of start bit = 0
    position = 0;
    f_busy = 1; // set transfer activate flag
    InitTimer3();
    cap_cnt = 0;
    cnt1 = 0, cnt2 = 0, cnt3 = 0;
    answer = 0;
    //answer1=0;
    cnt5 = 0;
    cnt6 = 0;
    count11 = 0;
    cnt1 = 0;
    cnt2 = 0;
    cnt3 = 0, cn1 = 0, cn2 = 0, cn3 = 0, cn4 = 0, cn5 = 0, cn6 = 0, cn7 = 0, cn8, cn9;
    IC4CON1 = 0x0000;
    IFS2bits.IC4IF = 0; // Clear the IC1 interrupt status flag
    IEC2bits.IC4IE = 0; // Enable IC1 interrupts   
    }

void unicast_shortaddress(void) // send shortaddress cmd && Provide FAD TIME using uni and Broadcat
    {
    unsigned char i;
    forward = (0xA3 << 8) | 0xFF;
    f_dalitx = 0; // clear DALI send flag
    f_dalirx = 0; // clear DALI receive (answer) flag
    DALI_Send(); // DALI send data to slave(s)

    for (i = 0; i < 2; i++)
        {
        Delay(60060); // 55 msec
        Delay(60060); // 55 msec
        forward = (0xFF << 8) | 0x80;
        f_dalitx = 0; // clear DALI send flag
        f_dalirx = 0; // clear DALI receive (answer) flag
        DALI_Send(); // DALI send data to slave(s)            
        }
    for (i = 0; i < 5; i++)
        {
        Delay(60060); // 55 msec
        Delay(60060); // 55 msec
        }
    }

void make_default(void) // send shortaddress cmd && Provide FAD TIME using uni and Broadcat
    {
    unsigned char i = 0;

    forward = (0xA3 << 8) | 0x07;
    f_dalitx = 0; // clear DALI send flag
    f_dalirx = 0; // clear DALI receive (answer) flag
    DALI_Send(); // DALI send data to slave(s)

    for (i = 0; i < 2; i++)
        {
        Delay(60060); // 55 msec
        Delay(60060); // 55 msec
        forward = (0xFF << 8) | 0x80;
        f_dalitx = 0; // clear DALI send flag
        f_dalirx = 0; // clear DALI receive (answer) flag
        DALI_Send(); // DALI send data to slave(s)
        }
    }

void DALI_DIMMING_CMD(unsigned char dim)
    {
    forward = (0xfe << 8) | dim;
    f_dalitx = 0; // clear DALI send flag
    f_dalirx = 0; // clear DALI receive (answer) flag
    DALI_Send(); // DALI send data to slave(s)
    }


void set_Linear_Logarithmic_dimming(unsigned char Curv_type)
{
	///////////DTR//////////////////////////////
//	forward    = (0xA3 << 8) |  0x00; 
	forward    = (0xA3 << 8) |  Curv_type; 
	f_dalitx =  1;
	 if (f_dalitx)                                      
	 { 
	      f_dalitx =  0;                                // clear DALI send flag 
	      f_dalirx =  0;                                // clear DALI receive (answer) flag 
	      DALI_Send();                                  // DALI send data to slave(s) 
	 } 
	Delay(60060);     // 55 msec
	Delay(60060);     // 55 msec

	forward    = (0xC1 << 8) |  0x06; 
	f_dalitx =  1;
	 if (f_dalitx)                                      
	 { 
	      f_dalitx =  0;                                // clear DALI send flag 
	      f_dalirx =  0;                                // clear DALI receive (answer) flag 
	      DALI_Send();                                  // DALI send data to slave(s) 
	 } 
	
	Delay(60060);     // 55 msec
	Delay(60060);     // 55 msec
	
	////////////////
		forward    = (0xFF << 8) |  0xE3; 
		f_dalitx =  1;
		 if (f_dalitx)                                      
		 { 
		      f_dalitx =  0;                                // clear DALI send flag 
		      f_dalirx =  0;                                // clear DALI receive (answer) flag 
		      DALI_Send();                                  // DALI send data to slave(s) 
		 } 
		
		
		Delay(60060);     // 55 msec
		Delay(60060);     // 55 msec
	/////////////////
	forward    = (0xFF << 8) |  0xE3; 
		f_dalitx =  1;
		 if (f_dalitx)                                      
		 { 
		      f_dalitx =  0;                                // clear DALI send flag 
		      f_dalirx =  0;                                // clear DALI receive (answer) flag 
		      DALI_Send();                                  // DALI send data to slave(s) 
		 } 
		
		Delay(60060);     // 55 msec
		Delay(60060);     // 55 msec
}



//#endif

void reset_nic(void)
    {

    if (Id_frame_received == 1)
        {

        if (temp_hour != Time.Hour)
            {
            temp_hour = Time.Hour;
            nic_reset_cnt2++;
            nic_reset_timer = 0;
            }


        if ((nic_reset_cnt2 >= ID_Frame_reset_time) && (nic_reset_timer >= 300))
            {
            TRISBbits.TRISB12 = 0;
            nic_reset_cnt2 = 0;
            nic_reset_cnt3 = 0;
            nic_reset_timer = 0;

            delay(50000);
            delay(50000);
            NIC_RESET_PIN = 0;
            delay(50000);
            delay(50000);

            NIC_RESET_PIN = 1;
            delay(50000);
            delay(50000);
            //cisco_nic_reset++;
            TRISBbits.TRISB12 = 1;
            }
        }
    else
        {

        if (temp_hour != Time.Hour)
            {
            temp_hour = Time.Hour;
            nic_reset_cnt3++;
            nic_reset_timer = 0;
            }


        if ((nic_reset_cnt3 > Normal_reset_time) && (nic_reset_timer >= 300))
            {
            TRISBbits.TRISB12 = 0;
            nic_reset_cnt3 = 0;
            nic_reset_cnt2 = 0;
            nic_reset_timer = 0;
            delay(50000);
            delay(50000);
            NIC_RESET_PIN = 0;
            //		delay(50000);
            delay(50000);
            delay(50000);

            NIC_RESET_PIN = 1;
            //		delay(50000);
            delay(50000);
            delay(50000);
            TRISBbits.TRISB12 = 1;
            //	cisco_nic_reset++;

            }
        }

    }

/*************************************************************************
Function Name: check_dimming_hardware
input: None.
Output: None.
Discription: this function is used to check Dimming section working properly
             or not.
 *************************************************************************/
void check_dimming_hardware(void)
    {
    unsigned char temp_arr[10];

    analog_input_scaling_high_Value = 10;
    analog_input_scaling_low_Value = 0;
    DI_GET_LOW = 0;
    DI_GET_HIGH = 0;

    //emberAfGuaranteedPrint("\r\n%p", "Dimming test Start");

    cheak_emt_timer_sec = 0;
    while (cheak_emt_timer_sec < 3)
        {
        dimming_applied(0); // set dimming value to 100%

        ADC_Process(); // Read ADC value.
        ClrWdt();

        }
    //#if(defined(ISLC_3100_V7_3_0216)||defined(ISLC_3100_480V_Rev3)) //this is general feature of DI

    if (MOTION_DI1 == 0)
        {
        DI_GET_HIGH = 1;
        }

    ADC_Result_0 = Analog_Result;

    cheak_emt_timer_sec = 0;
    while (cheak_emt_timer_sec < 3)
        {
        dimming_applied(50); // Set dimming to 50%

        ADC_Process(); // Read ADC Value
        ClrWdt();
        //Ember_Stack_run_Fun();            // call all ember tick function to increase stack timeing
        }
    //        Dimming_val[1] = ADC_Result;
    ADC_Result_50 = Analog_Result;

    cheak_emt_timer_sec = 0;
    while (cheak_emt_timer_sec < 3)
        {
        dimming_applied(100); // Set dimming to 0%

        ADC_Process(); // Read ADC vlaue.
        ClrWdt();
        //Ember_Stack_run_Fun();           // call all ember tick function to increase stack timeing
        }
    //#if(defined(ISLC_3100_V7_3_0216)||defined(ISLC_3100_480V_Rev3))

    if (MOTION_DI1 == 1) //DI High detect code has reverse logic
        {
        DI_GET_LOW = 1;
        }

    //        Dimming_val[2] = ADC_Result;
    ADC_Result_100 = Analog_Result;

    temp_arr[0] = 'S';
    temp_arr[1] = 'L';
    /*
    temp_arr[2]=amr_id/256;
    temp_arr[3]=amr_id%256;*/

    pulseCounter.long_data = amr_id;
    temp_arr[2] = pulseCounter.byte[0];
    temp_arr[3] = pulseCounter.byte[1];
    temp_arr[4] = pulseCounter.byte[2];
    temp_arr[5] = pulseCounter.byte[3];


    temp_arr[6] = DIMMING_TEST_DONE;

    Send_data_RF1(temp_arr, 7, 1); // Send dimming test done message to applciation.

    //emberAfGuaranteedPrint("\r\n%p", "Dimming test End");
    }
//

void check_heart_beat(void)
    {
    heart_buff[0] = 'H';
    heart_buff[1] = 'E';
    heart_buff[2] = 'S';
    heart_buff[3] = 'N';
    // WriteData_UART1(heart_buff, 4);
    Send_data_RF1(heart_buff, 4, 0);
    }

void get_push_lograte(void)
    {
    unsigned char i = 0, k = 0;
    for (i = 0; i < (24 / lograte); i++)
        {
        push[i].PushHour = k;
        k = k + lograte;
        if (push[i].PushHour > 23) push[i].PushHour = push[i].PushHour - 24;
        push[i].PushMin = 0;
        }
    }

void send_push_data(void)
    {
    unsigned char Bcyp;

    if ((Push_Energy_meter_data == 1)&&(fill_em_data_once == 0))
        {
        for (ext = 0; ext < 24; ext++)
            {
            NOP();



            //	sprintf(PrintStr,"\r\n%SDEMP %d SNORP %d",Send_Data_push,Send_Data);
            //	WriteData_UART2(PrintStr,strlen(PrintStr));	


            if ((push[ext].PushHour == Time.Hour) && (push[ext].PushMin == Time.Min)&& (push_data_send == 1))
                {
                Send_Data_push = 1;

                pushdata_retry_timer = 4;
                push_data_send = 0;
                push_data_send_cnt = 0;
                read_dim_val_timer1 = 0;
                NOP();
                for (Bcyp = 0; Bcyp < 41; Bcyp++)
                    {
                    Energy_back_data[Bcyp] = Energy_data[Bcyp];
                    Energy_data[Bcyp] = 0x00;
                    }
                Energy_data_cnt = 0;

                //	WriteData_UART2("\r\nEMPUSH",strlen("\r\nEMPUSH"));
                }
            }

        }
    else if (Push_Energy_meter_data == 0)
        {
        for (ext = 0; ext < (24 / lograte); ext++)
            {
            NOP();
            if ((push[ext].PushHour == Time.Hour) && (push[ext].PushMin == Time.Min) && (send_data_send == 1))
                {
                Send_Data = 1;
                senddata_retry_timer = 6;
                send_data_send = 0;
                send_data_send_cnt = 0;
                read_dim_val_timer1 = 0;
                NOP();
                }
            }

        }
    }

void Fill_Energy_data(void)
    {
    for (push_cnt = 0; push_cnt < 24; push_cnt++)
        {
        if (push[push_cnt].PushHour == Time.Hour)
            {
            for (energy_cnt = 0; energy_cnt < 4; energy_cnt++)
                {
                if ((EnergyMin[energy_cnt] == Time.Min)&&(energy_data_send == 1))
                    {
                    switch (energy_cnt)
                        {
                        case 0:
                            Fill_Energy_data_PowerON(15, 4, 0);
                            break;
                        case 1:

                            Fill_Energy_data_PowerON(30, 3, 10);
                            if (Energy_data[4] != 15)
                                {
                                Energy_data[0] = Date.Date;
                                Energy_data[1] = Date.Month;
                                Energy_data[2] = Date.Year;
                                Energy_data[3] = Time.Hour;
                                Energy_data[4] = 15;
                                Energy_data[5] = Time.Sec;
                                pulseCounter.float_data = kwh;
                                Energy_data[6] = pulseCounter.byte[3];
                                Energy_data[7] = pulseCounter.byte[2];
                                Energy_data[8] = pulseCounter.byte[1];
                                Energy_data[9] = pulseCounter.byte[0];
                                }

                            break;


                        case 2:


                            Fill_Energy_data_PowerON(45, 2, 20);
                            if (Energy_data[4] != 15)
                                {
                                Energy_data[0] = Date.Date;
                                Energy_data[1] = Date.Month;
                                Energy_data[2] = Date.Year;
                                Energy_data[3] = Time.Hour;
                                Energy_data[4] = 15;
                                Energy_data[5] = Time.Sec;
                                pulseCounter.float_data = kwh;
                                Energy_data[6] = pulseCounter.byte[3];
                                Energy_data[7] = pulseCounter.byte[2];
                                Energy_data[8] = pulseCounter.byte[1];
                                Energy_data[9] = pulseCounter.byte[0];

                                }
                            if (Energy_data[14] != 30)
                                {
                                Energy_data[10] = Date.Date;
                                Energy_data[11] = Date.Month;
                                Energy_data[12] = Date.Year;
                                Energy_data[13] = Time.Hour;
                                Energy_data[14] = 30;
                                Energy_data[15] = Time.Sec;
                                pulseCounter.float_data = kwh;
                                Energy_data[16] = pulseCounter.byte[3];
                                Energy_data[17] = pulseCounter.byte[2];
                                Energy_data[18] = pulseCounter.byte[1];
                                Energy_data[19] = pulseCounter.byte[0];
                                }


                            break;

                        case 3:

                            if (Energy_data[4] != 15)
                                {
                                Energy_data[0] = Date.Date;
                                Energy_data[1] = Date.Month;
                                Energy_data[2] = Date.Year;

                                if (Time.Hour == 0x00)
                                    {
                                    EMDate.Date = Date.Date;
                                    EMDate.Month = Date.Month;
                                    EMDate.Year = Date.Year;
                                    Fill_Decrement_Date();
                                    Energy_data[0] = EMDate.Date;
                                    Energy_data[1] = EMDate.Month;
                                    Energy_data[2] = EMDate.Year;
                                    Energy_data[3] = 23;
                                    }
                                else
                                    {
                                    Energy_data[3] = Time.Hour - 1;
                                    }

                                Energy_data[4] = 15;
                                Energy_data[5] = Time.Sec;
                                pulseCounter.float_data = kwh;
                                Energy_data[6] = pulseCounter.byte[3];
                                Energy_data[7] = pulseCounter.byte[2];
                                Energy_data[8] = pulseCounter.byte[1];
                                Energy_data[9] = pulseCounter.byte[0];

                                }
                            if (Energy_data[14] != 30)
                                {
                                Energy_data[10] = Date.Date;
                                Energy_data[11] = Date.Month;
                                Energy_data[12] = Date.Year;
                                if (Time.Hour == 0x00)
                                    {
                                    EMDate.Date = Date.Date;
                                    EMDate.Month = Date.Month;
                                    EMDate.Year = Date.Year;
                                    Fill_Decrement_Date();
                                    Energy_data[10] = EMDate.Date;
                                    Energy_data[11] = EMDate.Month;
                                    Energy_data[12] = EMDate.Year;
                                    Energy_data[13] = 23;
                                    }
                                else
                                    {
                                    Energy_data[13] = Time.Hour - 1;
                                    }
                                Energy_data[14] = 30;
                                Energy_data[15] = Time.Sec;
                                pulseCounter.float_data = kwh;
                                Energy_data[16] = pulseCounter.byte[3];
                                Energy_data[17] = pulseCounter.byte[2];
                                Energy_data[18] = pulseCounter.byte[1];
                                Energy_data[19] = pulseCounter.byte[0];

                                }


                            if (Energy_data[24] != 45)
                                {
                                Energy_data[20] = Date.Date;
                                Energy_data[21] = Date.Month;
                                Energy_data[22] = Date.Year;

                                if (Time.Hour == 0x00)
                                    {
                                    EMDate.Date = Date.Date;
                                    EMDate.Month = Date.Month;
                                    EMDate.Year = Date.Year;
                                    Fill_Decrement_Date();
                                    Energy_data[20] = EMDate.Date;
                                    Energy_data[21] = EMDate.Month;
                                    Energy_data[22] = EMDate.Year;
                                    Energy_data[23] = 23;
                                    }
                                else
                                    {
                                    Energy_data[23] = Time.Hour - 1;
                                    }
                                Energy_data[24] = 45;
                                Energy_data[25] = Time.Sec;
                                pulseCounter.float_data = kwh;
                                Energy_data[26] = pulseCounter.byte[3];
                                Energy_data[27] = pulseCounter.byte[2];
                                Energy_data[28] = pulseCounter.byte[1];
                                Energy_data[29] = pulseCounter.byte[0];
                                }

                            Energy_data[30] = Date.Date;
                            Energy_data[31] = Date.Month;
                            Energy_data[32] = Date.Year;
                            Energy_data[33] = Time.Hour;
                            Energy_data[34] = 0x00;
                            Energy_data[35] = Time.Sec;
                            pulseCounter.float_data = kwh;
                            Energy_data[36] = pulseCounter.byte[3];
                            Energy_data[37] = pulseCounter.byte[2];
                            Energy_data[38] = pulseCounter.byte[1];
                            Energy_data[39] = pulseCounter.byte[0];


                            break;

                        }

                    //										}

                    energy_data_send = 0;
                    Energy_data_cnt++;
                    energy_data_send_cnt = 0;
                    }
                }
            }

        }

    }

void Fill_Energy_data_PowerON(unsigned char start_enrgy_time, unsigned char Fill_enrgy_cnt, unsigned char em_log_cnt_pwn)
    {
    unsigned char Efcount, temp_min = 0, temp_hour; //,PrintStr[50];
    temp_hour = Time.Hour;
    for (Efcount = 0; Efcount < Fill_enrgy_cnt; Efcount++)
        {
        temp_min = temp_min + start_enrgy_time;
        Energy_data[em_log_cnt_pwn++] = Date.Date;
        Energy_data[em_log_cnt_pwn++] = Date.Month;
        Energy_data[em_log_cnt_pwn++] = (unsigned char) Date.Year;
        if (temp_min > 45)
            {
            temp_min = 0x00;
            temp_hour = temp_hour + 1;
            if (temp_hour > 23) temp_hour = 0;
            }

        Energy_data[em_log_cnt_pwn++] = temp_hour;
        Energy_data[em_log_cnt_pwn++] = temp_min;
        Energy_data[em_log_cnt_pwn++] = 0x00;
        pulseCounter.float_data = kwh;
        Energy_data[em_log_cnt_pwn++] = pulseCounter.byte[3];
        Energy_data[em_log_cnt_pwn++] = pulseCounter.byte[2];
        Energy_data[em_log_cnt_pwn++] = pulseCounter.byte[1];
        Energy_data[em_log_cnt_pwn++] = pulseCounter.byte[0];

        //sprintf(buffer1,"\r\nT:%02d:%02d:%02d,%02d,%02d ",Time.Hour,Time.Min,Time.Sec,temp_hour,temp_min);
        //WriteData_UART2(buffer1,strlen(buffer1));

        }
    }

void fill_event_data(void)
    {
    uint8_t dataCounter = 0;
    if (send_event_with_delay == 1) // delay for send data direct delay
        {

        if (feel_event_cnt >= 3)
            {
            send_event_delay_timer = 0;
            send_data_direct = 1;
            event_push_retry_timer = 4;
            send_event_with_delay = 0;


            temp_arr_event[0] = 'S';
            temp_arr_event[1] = 'L';

            pulseCounter.long_data = amr_id;
            temp_arr_event[2] = pulseCounter.byte[3];
            temp_arr_event[3] = pulseCounter.byte[2];
            temp_arr_event[4] = pulseCounter.byte[1];
            temp_arr_event[5] = pulseCounter.byte[0];

            temp_arr_event[6] = Client / 256;
            temp_arr_event[7] = Client % 256;


            temp_arr_event[8] = 'E';
            temp_arr_event[9] = Frm_no;
            Filldata();
            for (dataCounter = 10; dataCounter < 60; dataCounter++)
                {
                temp_arr_event[dataCounter] = temp_arr[dataCounter];
                }
            temp_arr_event[60] = 0;
            track_id++;
            if (track_id >= 65000)track_id = 0;

            temp_arr_event[61] = track_id / 256;
            temp_arr_event[62] = track_id % 256;
            feel_event_cnt = 0;
            }

        }

    }

void last_gasp_detect(void)
    {
    if ((Store_LastGasp_Data == 1)&&(last_gasp_detected == 1))
        {
        Save_BH_kWh();
        Store_LastGasp_Data = 2;
        }
    if ((last_gasp_detected == 1)&&(sendonce == 1)&&(Id_frame_received == 1)&&(lastgasp_retry_timer >= 16))
        {
        if (lastgasp_cnt >= 2)
            {
            sendonce = 0;
            last_gasp_detected = 0;
            }
        lastgasp_cnt++;
        lastgasp_retry_timer = 0;

        LATBbits.LATB7 = 0; //DALIE 
        NOP();
        NOP();
        NOP();
        LATBbits.LATB3 = 1; //DALIT

        DO_1 = 0;
        DO_2 = 0;

        Send_Last_gasp_msg = 1;
        send_fream();

        }

    }

void delay(unsigned int i)
    {
    volatile unsigned int j, k;
    for (j = 0; j < i; j++)
        {
        for (k = 0; k < 53; k++)
            {
            }
        ClrWdt();
        }
    }

void SET_PWM_FREQ(void)
    {
    IFS0bits.T2IF = 0; // Clear Output Compare 1 interrupt flag 	 
    IEC0bits.T2IE = 0; // Enable Output Compare 1 interrupts 	 
    T2CONbits.TON = 0; // Start Timer2 with assumed settings
    T3CONbits.TON = 0; // Start Timer2 with assumed settings
    T2CONbits.T32 = 0;
    OC2R = PWM_CONSTANT; // Initialize Compare Register1 with 0x0026 	 
    OC2RS = PWM_CONSTANT; // Initialize Secondary Compare Register1 with 0x0026 
    OC2CON1 = 0x0006;
    PR2 = PWM_CONSTANT;
    IEC0bits.T2IE = 1; // Enable Output Compare 1 interrupts 	 
    T2CONbits.TON = 1; // Start Timer2 with assumed settings 
    }

void PIN_Initialize(void)
    {
    /****************************************************************************
     * Setting the GPIO Direction SFR(s)
     ***************************************************************************/
    TRISB = 0x0F77;
    TRISC = 0x1000;
    TRISD = 0x0DF5;
    TRISE = 0x002E;
    TRISF = 0x0074;
    TRISG = 0x038C;


    /****************************************************************************
     * Setting the Output Latch SFR(s)
     ***************************************************************************/
    LATB = 0x0000;
    LATC = 0x0000;
    LATD = 0x0000;
    LATE = 0x0000;
    LATF = 0x0000;
    LATG = 0x0000;
    /****************************************************************************
     * Setting the Weak Pull Up and Weak Pull Down SFR(s)
     ***************************************************************************/
    CNPD1 = 0x0000;
    CNPD2 = 0x0000;
    CNPD3 = 0x0000;
    CNPD4 = 0x0000;
    CNPD5 = 0x0000;
    CNPD6 = 0x0000;
    CNPU1 = 0x0000;
    CNPU2 = 0x0000;
    CNPU3 = 0x0000;
    CNPU4 = 0x0000;
    CNPU5 = 0x0000;
    CNPU6 = 0x0000;

    /****************************************************************************
     * Setting the Open Drain SFR(s)
     ***************************************************************************/
    ODCB = 0x0000;
    ODCC = 0x0000;
    ODCD = 0x0000;
    ODCE = 0x0000;
    ODCF = 0x0000;
    ODCG = 0x0000;

    /****************************************************************************
     * Setting the Analog/Digital Configuration SFR(s)
     ***************************************************************************/
    ANSB = 0x0060;
    ANSD = 0x0000;
    ANSE = 0x0000;
    ANSG = 0x0000;

    /****************************************************************************
     * Set the PPS
     ***************************************************************************/
    __builtin_write_OSCCONL(OSCCON & 0xbf); // unlock PPS

    RPINR17bits.U3RXR = 0x0019; //RD4->UART3:U3RX
    RPOR8bits.RP16R = 0x0003; //RF3->UART1:U1TX
    RPOR10bits.RP21R = 0x001E; //RG6->UART4:U4TX
    RPINR27bits.U4RXR = 0x001A; //RG7->UART4:U4RX
    RPOR2bits.RP4R = 0x0005; //RD9->UART2:U2TX
    RPINR19bits.U2RXR = 0x0003; //RD10->UART2:U2RX
    RPINR18bits.U1RXR = 0x001E; //RF2->UART1:U1RX
    RPOR11bits.RP22R = 0x001C; //RD3->UART3:U3TX
    RPOR12bits.RP24R = 0x0013;
    RPINR0bits.INT1R = 0x0002; //RD8->EXT_INT:INT1
    RPINR8bits.IC4R = 0x000C; //RD11->IC4:IC4
    RPINR1bits.INT2R = 0x0025; //RC14->EXT_INT:INT2
    __builtin_write_OSCCONL(OSCCON | 0x40); // lock PPS

    IC4CON1 = 0x0000;
    IEC2bits.IC4IE = 0; // Enable IC1 interrupts
    IFS2bits.IC4IF = 0; // Clear the IC1 interrupt status flag
    NOP();
    TRISD = 0x0DF4;

    }

void CLOCK_Initialize(void)
    {
    // RCDIV FRC/2; DOZE 1:8; DOZEN disabled; ROI disabled; 
    CLKDIV = 0x3100;
    // TUN Center frequency; 
    OSCTUN = 0x00;
    // ROEN disabled; ROSEL FOSC; RODIV 0; ROSSLP disabled; 
    REFOCON = 0x00;
    // ADC1MD enabled; T3MD enabled; T4MD enabled; T1MD enabled; U2MD enabled; T2MD enabled; U1MD enabled; SPI2MD enabled; SPI1MD enabled; T5MD enabled; I2C1MD enabled; 
    PMD1 = 0x00;
    // OC5MD enabled; OC6MD enabled; OC7MD enabled; OC1MD enabled; IC2MD enabled; OC2MD enabled; IC1MD enabled; OC3MD enabled; OC4MD enabled; IC6MD enabled; IC7MD enabled; IC5MD enabled; IC4MD enabled; IC3MD enabled; 
    PMD2 = 0x00;
    // DSMMD enabled; PMPMD enabled; U3MD enabled; RTCCMD enabled; CMPMD enabled; CRCMD enabled; I2C2MD enabled; 
    PMD3 = 0x00;
    // U4MD enabled; UPWMMD enabled; CTMUMD enabled; REFOMD enabled; LVDMD enabled; 
    PMD4 = 0x00;
    // SPI3MD enabled; LCDMD enabled; 
    PMD6 = 0x00;
    // DMA1MD enabled; DMA0MD enabled; 
    PMD7 = 0x00;
    // CF no clock failure; NOSC PRIPLL; SOSCEN disabled; POSCEN disabled; CLKLOCK unlocked; OSWEN Switch is Complete; IOLOCK not-active; 
    __builtin_write_OSCCONH((uint8_t) (0x03));
    __builtin_write_OSCCONL((uint8_t) (0x00));
    }

void INTERRUPT_Initialize(void)
    {

    IPC0bits.T1IP = 1; //TI: T1 - Timer1
    IPC5bits.INT1IP = 1; //    INT1I: INT1 - External Interrupt 1 ZC

    IPC13bits.INT3IP = 2; //    INT3I: INT3 - External Interrupt 3 Kwh
    IPC2bits.T3IP = 2; //TI: T3 - Timer3 DALI//dali enable 


    IPC2bits.U1RXIP = 3; //    URXI: U1RX - UART1 Receiver CISCO RF
    IPC1bits.T2IP = 4; //TI: T2 - Timer2 PWM
    IPC22bits.U4RXIP = 5; //    URXI: U4RX - UART4 Receiver EM
    IPC20bits.U3RXIP = 6; //    URXI: U3RX - UART3 Receiver GPS
    }

void AO_DALI_selection(void)
    {
    RS485_0_Or_DI2_1 = ReadByte_EEPROM(EE_RS485_0_Or_DI2_1);
    if (RS485_0_Or_DI2_1 == 0xFF)
        {
        RS485_0_Or_DI2_1 = 1; //default AI/DI
        WriteByte_EEPROM(EE_RS485_0_Or_DI2_1, RS485_0_Or_DI2_1);
        }

    if (RS485_0_Or_DI2_1 == 1)//AI/DI
        {
        IO_Control1 = 0; //Cirt load at power ON
        IO_Control2 = 1;

        }
    else
        {
        IO_Control1 = 1; //RS485
        IO_Control2 = 0; //Normal

        }
   
    }


///////////////////////

static void DALI_Shift_Bit(BYTE val)
    {
    if (frame & 0x100) // frame full ?
        {
        cn8++;
        frame = 0; // yes, ERROR
        }
    else
        {
        cn9++;
        frame = (frame << 1) | val; // shift bit
        }
    }

static void DALI_Decode(void)
    {
    BYTE action;

    action = previous << 2;
    if ((high_time > MIN_2TE) && (high_time < MAX_2TE))
        {
        action = action | 1; // high_time = long
        }
    else if (!((high_time > MIN_TE) && (high_time < MAX_TE)))
        {
        frame = 0; // DALI ERROR
        return;
        }

    if ((low_time > MIN_2TE) && (low_time < MAX_2TE))
        {
        action = action | 2; // low_time = long
        }
    else if (!((low_time > MIN_TE) && (low_time < MAX_TE)))
        {
        frame = 0; // DALI ERROR
        return;
        }

    switch (action)
        {
        case 0:
            cn1++;
            DALI_Shift_Bit(0); // short low, short high, shift 0
            break;

        case 1:
            cn2++;
            frame = 0; // short low, long high, ERROR
            break;

        case 2:
            cn3++;
            DALI_Shift_Bit(0); // long low, short high, shift 0,1
            DALI_Shift_Bit(1);
            previous = 1; // new half bit is 1
            break;

        case 3:
            cn4++;
            frame = 0; // long low, long high, ERROR
            break;

        case 4:
            cn5++;
            DALI_Shift_Bit(1); // short low, short high, shift 1
            break;

        case 5:
            cn6++;
            DALI_Shift_Bit(0); // short low, long high, shift 0
            previous = 0; // new half bit is 0

            break;

        case 6:
            cn6++;
            frame = 0; // long low, short high, ERROR
            break;

        case 7:
            cn7++;
            DALI_Shift_Bit(0); // long low, long high, shift 0,1
            DALI_Shift_Bit(1);

        default:
            break; // invalid

        }
    }