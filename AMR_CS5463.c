//#include "AMR_CS5463.h"


#include <math.h>
#include <stdio.h>
#include <stdlib.h>

#include "Compiler.h"
#include "generic.h"
#include "AMR_CS5463.h"
#include "protocol.h"
#include "AMR_RTC.h"
#include "Application_logic.h"
#include "serial.h"
#include "MSPI.h"

#define EM_RESET LATEbits.LATE0


float Calculated_Frequency = 0.0;
unsigned long int pulses_r;
unsigned char check_day_burnar_flag = 0;
float newkwh;
float diff;
unsigned char datalow, datahigh, datamid;
unsigned char zempbyte;
unsigned long int zemplong;
unsigned long int reg_data_r[5];
unsigned char tempbyte;
unsigned char intesec;
float base_val;
float zempdoub;
float kbase;
float irdoub;
float vry;
float pf;
float va1;
float w1;
float var1;
float kw1;
float kwh;
float mf1;
float mf2;
float mf3;
float vrdoub;
unsigned char power_on_flag = 0;
unsigned int emt_int_count = 0, Kwh_count = 0;
unsigned int em_reset_counter = 0;
unsigned long int Em_status_reg = 0;
char temperature = 0;
extern unsigned char calibmode;
unsigned long baudrate = 0;
uint32_t Rawvalue = 0;
uint8_t data[10]; //data buffer for read and write
uint8_t EmRxcount = 0;
int selectedPage;
float MCLK = 4.096;
uint32_t hexBR = 0x0;

void init_cs5463()
    {
    hardwareReset();
    setBaudRate(baudRate_9600);
    delay(10);
    Configure_UART4(baudRate_9600);
    delay(10);

    WRITE_CS5480(0, 0, 0xc02020);
    WRITE_CS5480(16, 0, 0x00020A); // config 2, HPF enable on current and voltage channel
    WRITE_CS5480(0, 5, 0x00002f); // PHASE COMPENSATION 
    WRITE_CS5480(18, 28, 0xF35d03); // Page 0 ,pulse rate,  as per Cs5490 datasheet value for 305*20A * 12.5 p /sec
    WRITE_CS5480(0, 9, 0x000300); // Page 0 ,  Kwh
    WRITE_CS5480(0, 8, 0x000005); // Page 0 , pulse width 
    WRITE_CS5480(0, 1, 0x70e402); // Page 0 , config1 Register address 0 ,
    WRITE_CS5480(16, 37, 0x093B00); // Page 16 , AC offset for current,
    // WRITE_CS5480(0, 0, 0xc020a0); // Page 16 , AC offset for current,  added by sandip nandwana
    __delay_ms(100);
    CS_wait_and_clr_DRDY();
    contConv();
    __delay_ms(100);
    CS_wait_and_clr_DRDY();
    }

void calculate_3p4w(void)
    {
    zempdoub = (1.0 / 16777216.0);
    irdoub = get_ieee(reg_data_r[1]);
    vrdoub = get_ieee(reg_data_r[2]);

    zempdoub = (2.0 / 16777216.0);

    zemplong = (reg_data_r[0] & 0x00800000);
    if (zemplong)
        {
        reg_data_r[0] = (16777216 - reg_data_r[0]);
        }
    zempdoub = get_ieee(reg_data_r[0]);
    vry = (vrdoub * irdoub);
    if (vry != 0.000000)
        {
        pf = (zempdoub / vry);
        //pf = pf + 0.02;
        }
    else
        {
        pf = 0.99999;
        }

    if (pf >= 1.0)
        pf = 0.9999;


    //vrdoub=vrdoub*(VRN/0.4456425);
    vrdoub = vrdoub*mf1;

    if (vrdoub < 10.0)
        {
        vrdoub = 0.0;
        pf = 0.9999;
        }
    /////////////////////////////////////
    if ((vrdoub > 85)&&(vrdoub < 160))
        {
        mf3 = (PULSES_120 / (float) KW_Cal_120);
        }
    else
        {
        mf3 = (PULSES / (float) KW_Cal);
        }
    //////////////////////////////////////
    //vrdoub=vrdoub*pt_ratio;

    //irdoub=irdoub*(IR/0.2481445);
    irdoub = irdoub*mf2; //edit on sep 17 as per discussion with sir and sandip

    if (irdoub <= MIN_CREEP_VALUE)
        {
        irdoub = 0.0;
        pf = 0.9999;
        }

    base_val = (vrdoub * irdoub);

    va1 = base_val;

    w1 = (base_val * pf);

    kbase = (base_val / 1000.0);


    kbase = (kbase * pf);


    var1 = (pf * pf);

    var1 = (1.0 - var1);

    var1 = sqrt(var1);

    var1 = (va1 * var1);

    calculate_kwh();
    }

float get_ieee(unsigned long int rawdata)
    {
    float value;
    unsigned int i;

    base_val = zempdoub;
    value = 0.0;
    for (i = 0; i < 24; i++)
        {
        zemplong = 0x00000001;
        if (i)zemplong = zemplong << i;
        zemplong = (zemplong & rawdata);
        if (zemplong)value = value + base_val;
        base_val = (base_val * 2.0);
        }
    return (value);
    }

void calculate_kwh()
    {
    zempbyte = 0;
    if (irdoub > MIN_CREEP_VALUE)
        {
        zempbyte = intesec;
        }
    intesec = 0;
    pulses_r += zempbyte;

    kwh = (((float) pulses_r * mf3));
    //	kwh=(float)pulses_r * (PULSES/701);
    //	kwh=(kwh*(float)ctptgain);

    diff = kwh - newkwh;

    if (diff >= 0.01)
        {
        newkwh = kwh;
        pulseCounter.long_data = pulses_r;
        WriteByte_EEPROM(EE_KWH + 0, pulseCounter.byte[0]);
        WriteByte_EEPROM(EE_KWH + 1, pulseCounter.byte[1]);
        WriteByte_EEPROM(EE_KWH + 2, pulseCounter.byte[2]);
        WriteByte_EEPROM(EE_KWH + 3, pulseCounter.byte[3]);
        }
    }

void Read_EnergyMeter(void)
    {
    uint32_t Rawbuffer = 0;
    unsigned long int frequency = 0;
    if (ReadEM == 1)
        {
        ReadEM = 0;
        Rawbuffer = readReg(0, 23);
        if (Rawbuffer & 0x00800000)
            {
            ////////////////////
            power_on_flag = 1;
            energy_time = 0;
            emt_int_count++;
            energy_meter_ok = 0;
            check_day_burnar_flag = 1; // 1.0.0013
            ////////////////////
            zempdoub = (1.0 / 16777216.0);

            reg_data_r[2] = readReg(16, 7);
            vrdoub = get_ieee(reg_data_r[2]);


            reg_data_r[1] = readReg(16, 6);
            irdoub = get_ieee(reg_data_r[1]);


            temperature = Get_Value_from_Raw_Data(readReg(16, 27), 1, (256.0 / 16777216.0));

            zempdoub = (1.0 / 16777216.0);


            reg_data_r[0] = readReg(16, 5);


            frequency = readReg(16, 49);
            Calculated_Frequency = (frequency * (50.00 / 104831));
            CS_wait_and_clr_DRDY(); //CRDY=0
            if (calibmode == 1)
                {
                if (Voltage_calibration == 1)
                    {
                    Voltage_calibration = 0;
                    Send_Voltage_calibration = 1;
                    Vr_Cal = vrdoub;
                    pulseCounter.float_data = Vr_Cal;
                    WriteEEPROM(EE_Vr_Cal, pulseCounter);
                    }

                if (Current_calibration == 1)
                    {
                    Current_calibration = 0;
                    Send_Current_calibration = 1;
                    Ir_Cal = irdoub;
                    pulseCounter.float_data = Ir_Cal;
                    WriteEEPROM(EE_Ir_Cal, pulseCounter);
                    }

                if (KW_Calibration == 1)
                    {
                    KW_Calibration = 0;
                    Send_Kw_calibration = 1;
                    KW_Cal = (float) pulses_r;
                    //halCommonSetToken(TOKEN_KW_Cal,&KW_Cal);
                    pulseCounter.float_data = KW_Cal;
                    WriteEEPROM(EE_KW_Cal, pulseCounter);
                    pulseCounter.long_data = 0;
                    WriteByte_EEPROM(EE_KWH + 0, pulseCounter.byte[0]);
                    WriteByte_EEPROM(EE_KWH + 1, pulseCounter.byte[1]);
                    WriteByte_EEPROM(EE_KWH + 2, pulseCounter.byte[2]);
                    WriteByte_EEPROM(EE_KWH + 3, pulseCounter.byte[3]);
                    pulses_r = 0;
                    }
                if (KW_Calibration_120 == 1)
                    {
                    KW_Calibration_120 = 0;
                    Send_Kw_calibration_120 = 1;
                    KW_Cal_120 = (float) pulses_r;
                    pulseCounter.float_data = KW_Cal_120;
                    WriteEEPROM(EE_KW_Cal_120, pulseCounter);
                    //WriteByte_EEPROM(EE_KW_Cal_120 + 0, pulseCounter.byte[3]);
                    //WriteByte_EEPROM(EE_KW_Cal_120 + 1, pulseCounter.byte[2]);
                    // WriteByte_EEPROM(EE_KW_Cal_120 + 2, pulseCounter.byte[1]);
                    // WriteByte_EEPROM(EE_KW_Cal_120 + 3, pulseCounter.byte[0]);

                    pulseCounter.long_data = 0;
                    WriteByte_EEPROM(EE_KWH + 0, pulseCounter.byte[0]);
                    WriteByte_EEPROM(EE_KWH + 1, pulseCounter.byte[1]);
                    WriteByte_EEPROM(EE_KWH + 2, pulseCounter.byte[2]);
                    WriteByte_EEPROM(EE_KWH + 3, pulseCounter.byte[3]);
                    }
                }
            if (calibmode == 0)
                {
                calculate_3p4w();

                }
            }
        }
    }

float Get_Value_from_Raw_Data(unsigned long int Rawdata, char Signed_num, float Weight)
    {
    float value;
    if (Signed_num)
        {
        if (Rawdata & 0x00800000)
            {
            {
                Rawdata |= 0xFF000000;
            }
            }
        value = *((signed long int*) &Rawdata) * Weight;
        }
    else
        {
        value = Rawdata * Weight;
        }
    return (value);
    }

void set_mf_energy(void)
    {
    mf1 = VRN / vrdoub;
    mf2 = IR / irdoub;
    mf3 = (PULSES / (float) pulses_r);

    //	mf1 = 506;
    //	mf2 = 12;
    //	mf3 = 0.000140;
    }

///*************************************************************************
//Function Name: calculate_kwh_LampOFF
//input: none.
//Output: none.
//Discription: This function is use to store Kwh at diff of 0.1 at lamp off.
// *************************************************************************/
//void calculate_kwh_LampOFF(void)
//    {
//    zempbyte = 0;
//    if (irdoub > CREEP_LIMIT)
//        {
//        zempbyte = intesec;
//        }
//    intesec = 0;
//    pulses_r += zempbyte;
//
//    kwh = (((float) pulses_r * mf3));
//    sprintf(temp_buf12, "\r\n10.Kwh:%f", kwh);
//    WriteData_UART2(temp_buf12, strlen(temp_buf12));
//    diff = kwh - newkwh;
//    if (diff >= 0.10)
//        {
//        newkwh = kwh;
//        pulseCounter.long_data = pulses_r;
//        WriteByte_EEPROM(EE_KWH + 0, pulseCounter.byte[0]);
//        WriteByte_EEPROM(EE_KWH + 1, pulseCounter.byte[1]);
//        WriteByte_EEPROM(EE_KWH + 2, pulseCounter.byte[2]);
//        WriteByte_EEPROM(EE_KWH + 3, pulseCounter.byte[3]);
//        }
//
//    }

uint32_t readReg(int page, int address)
    {
    READ_CS5480(page, address);
    return concatData();
    }

void WRITE_CS5480(int page, int address, uint32_t value)
    {
    uint8_t buffer, i = 0;
    //Select page and address
    if (selectedPage != page)
        {
        buffer = (pageByte | (uint8_t) page);
        UART4_Write(buffer);
        selectedPage = page;
        }
    buffer = (writeByte | (uint8_t) address);
    UART4_Write(buffer);

    //Send information
    for (i = 0; i < 3; i++)
        {
        buffer = value & 0x000000FF;
        UART4_Write(buffer);
        value >>= 8;
        }
    }

/******* Read a register by the serial communication *******/

/* data bytes pass by data variable from this class */

void READ_CS5480(int page, int address)
    {

    uint8_t buffer, i = 0;

    //clearSerialBuffer();

    for (i = 0; i < 9; i++)
        {
        data[i] = 0x00;
        }
    EM_timeout=0;
    //Select page and address
    if (selectedPage != page)
        {
        buffer = (pageByte | (uint8_t) page);
        UART4_Write(buffer);
        selectedPage = page;
        }
    buffer = (readByte | (uint8_t) address);
    UART4_Write(buffer);
    EmRxcount = 0;
    __delay_ms(10);
    }

/******* Give an instruction by the serial communication *******/

void instruct(int value)
    {
    uint8_t buffer;
    buffer = (instructionByte | (uint8_t) value);
    UART4_Write(buffer);
    }

char CS_wait_and_clr_DRDY(void)
    {
    char temp;
    temp = CS_wait_for_DRDY();
    if (temp)
        return -1;
    CS_clear_DRDY();
    return 0;
    }

void CS_clear_DRDY(void)
    {
    WRITE_CS5480(0, 23, 0x800000); // writing a 1 to DRDY bit clears it.
    }

char CS_wait_for_DRDY(void)
    {
    unsigned long int temp;
    unsigned char loopCntr = 0;
    while (loopCntr < 30)
        {
        READ_CS5480(0, 23);
        temp = concatData();
        if (temp >> 24) // if the read produced an error, ignore it.
            {
            // USB_Write_Strln("Serial timeout");

            }
        else
            {
            if (temp & 0x800000) // if DRDY is set.
                {
                // USB_Write_Strln("Flag set. Success");
                return 0; // exit with success.
                break;
                }
            }
        loopCntr += 1;
        __delay_ms(50);
        }

    return -1;

    }

long getBaudRate()
    {
    uint32_t buffer;
    READ_CS5480(0, 7);
    buffer = concatData();
    buffer -= 0x020000;
    return ( ((buffer / 0.5242880) * MCLK) - 1);
    }

/* SET */
void setBaudRate(uint16_t value)
    {
    //Calculate the correct binary value
    hexBR = ceil(value * 0.5242880 / MCLK);
    if (hexBR > 65535) hexBR = 65535;
    hexBR += 0x020000;

    WRITE_CS5480(0x80, 0x07, hexBR); // 0x80 instead of 0x00 in order to force a page selection command on page 0
    __delay_ms(100);
    return;
    }

void wakeUp()
    {
    instruct(3);
    }

void contConv()
    {
    instruct(21);
    }

void reset()
    {
    instruct(1);
    }

/******* Concatenation of the incomming data from CS5490 *******/
uint32_t concatData()
    {
    uint32_t output = 0;
    output = data[2];
    output <<= 16;
    output |= ((uint32_t) data[1] << 8);
    output |= data[0];
    return output;
    }


/**************************************************************/
/*              PUBLIC METHODS - Instructions                 */

/**************************************************************/
void hardwareReset(void)//#Rht
    {
    EM_RESET = 0;
    __delay_ms(500);
    EM_RESET = 1;
    __delay_ms(500);
    Configure_UART4(baudRate_default);
    __delay_ms(100);
    // SW reset (in case the reset line fails...)
    CS_wait_and_clr_DRDY();
    reset();
    __delay_ms(100);
    CS_wait_and_clr_DRDY();
    __delay_ms(10);
    wakeUp();
    __delay_ms(10);
    }
