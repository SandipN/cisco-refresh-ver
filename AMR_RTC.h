
#ifndef AMR_RTC
#define AMR_RTC

/////////////////////



/***************************** Defines *****************************/
#define ACK 0
#define NACK 1
#define ADDRTC 0xa2 /* I2C slave address */
//	#define DS1339 /* compile directive, modify as required */
/************************* bit definitions *************************/
#define scl LATGbits.LATG2
#define sda LATGbits.LATG3

typedef struct {
    char Sec; /* Second value - [0,59]           RTC_Sec*/
    char Min; /* Minute value - [0,59]           RTC_Min*/
    char Hour; /* Hour value - [0,23]             RTC_Hour*/
} RTCTime;

typedef struct {
    char Date; /* Day of the month value - [1,31] RTC_Mday*/
    char Month; /* Month value - [1,12]            RTC_Mon*/
    unsigned int Year; /* Year value - [0,4095]           RTC_Year*/
    unsigned char Week; /* Day of week value - [0,6]       RTC_Wday*/
    unsigned int DofYear; /* Day of year value - [1,365]     RTC_Yday */
} RTCDate;


extern RTCTime Time, PTime, GPS_Time, SSN_GPS_TIME, Local_Time;
extern RTCDate Date, PDate, GPS_Date, SSN_GPS_DATE, Local_Date, EMDate;
typedef unsigned char uchar;
void I2C_start();
void I2C_stop();
void I2C_write(unsigned char d);
uchar I2C_read(uchar);
void readbyte();
void writebyte();
void initialize();
void init_PCF85363(void);
void RTC_SET_TIME(RTCTime RTC_Time);
void RTC_SET_DATE(RTCDate RTC_Date);
void read_PCF85363_bytes(void);
unsigned char week_day(unsigned int year, unsigned char date, unsigned char month);
void PCF_Force_SW_Reset(void);
unsigned char ReadRTCReg(unsigned char add);
void Find_Previous_date_Month(void);



//********************************************************************//
#define I2C_ADD 0x44 /* I2C slave address */
/************************* bit definitions *************************/
#define I2C_SCL LATFbits.LATF5
#define I2C_SDA LATFbits.LATF4


void UART_I2C_start();
void UART_I2C_stop();
void UART_I2C_write(uchar d);
uchar UART_I2C_read(uchar b);
void I2CRxData(void);
extern unsigned char I2C_data_rec_flag, Rec_count;
extern unsigned char I2C_data_Buff[];
extern volatile unsigned char I2C_Timeout;
#endif
