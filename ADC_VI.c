////////////////////////////only for adc code//////////////////

#include "Compiler.h"
#include "generic.h"
#include "ADC_VI.h"
#include "Application_logic.h"


unsigned int lamp_lumen = 0;
unsigned char Photo_feedback_pin = 0;
unsigned char Photocell_Off_Condition = 0;
volatile unsigned int photocell_off_Cnt = 0;
unsigned int Photocell_Off_Time = 0;
unsigned char Detect_Off_Condition = 0, Make_Lamp_Off = 0;
unsigned char DIGITAL0_ANALOG1_SELECTION = 10; //need to set this value since 0 create problem in analog mode;
float Photosensor_Set_FC, Photosensor_Set_FC_Lamp_OFF;
float photosensor = 3.2, photosensor_average = 0.0; //set 3.2 value default to prevent malfunction since first time it may get 0 unless finish average value
volatile float Photosensor_FootCandle = 0.0, Photosensor_FootCandle_CHeck = 0.0;
void Photocell_ON_OFF_Decision(void);
void Negetive_Foot_Candle_Ratio(void);
unsigned char chr_i = 0, Pin_Detect_Dark = 0;
float Analog_Result = 0;

void ADC_Process()
    {
    unsigned int Convert_adc = 0;
    float temp_sam;
    unsigned int dif = 0; // scaleHi = 50000,scaleLo = 0,
    float Photosensor_On_Off_Decision;
    ////////////////////////////////////////

    //    delay(50);
    //    AD1CHS = 0x0007; //channel 7 for 24v
    //    AD1CSSL = 0x0080;
    //    delay(50);
    //    while (!IFS0bits.AD1IF); //waiting 
    //    IFS0bits.AD1IF = 0;
    //
    //    Convert_adc = ADC1BUF0; //16 bit
    //    AI2_RB7 = (((float) Convert_adc) / 1024)*26.47; //bug 1.0.0009 parvez (((33+4.7)/4.7)*3.3)=26.47
    //    delay(50);
    /////////////////////////////////////////	

    //    if (analog_input_scaling_high_Value != 0)
    //       {
    //        

    AD1CHS = 0x6;
    AD1CON1bits.SAMP = 1;
    delay(50);
    AD1CON1bits.SAMP = 0;
    while (!AD1CON1bits.DONE)
        {
        if (IFS0bits.AD1IF)
            {
            IFS0bits.AD1IF = 0;
            }
        }

    Convert_adc = ADC1BUF0; //16 bit
    Analog_Result = Convert_adc;
    dif = analog_input_scaling_high_Value - analog_input_scaling_low_Value;
    temp_sam = (((float) Convert_adc) / 1024)*(float) dif;
    lamp_lumen = (unsigned int) temp_sam;
    delay(50);
    //    }
    if (DIGITAL0_ANALOG1_SELECTION == 0)
        {
        //    	if(Photo_feedback_pin_DIGITAL == 1)
        //    	{
        //    		Photo_feedback_pin = 1;
        //    	}
        //    	else
        //    	{
        //    		Photo_feedback_pin = 0;
        //    	}		
        }
    else
        {
        ///////////////////////////
        AD1CHS = 0x5;
        AD1CON1bits.SAMP = 1;
        delay(50);
        AD1CON1bits.SAMP = 0;
        while (!AD1CON1bits.DONE)
            {
            if (IFS0bits.AD1IF)
                {
                IFS0bits.AD1IF = 0;
                }
            }

        Convert_adc = ADC1BUF0; //16 bit
        temp_sam = (((float) Convert_adc) / 1024)*3.3;

        photosensor_average = photosensor_average + temp_sam;
        chr_i++;
        if (chr_i > 15)
            {
            photosensor = photosensor_average / chr_i;
            photosensor_average = 0;
            chr_i = 0;
            //}
            ////////////////////////////////////

            Photocell_ON_OFF_Decision();
            Photosensor_FootCandle_CHeck = Photosensor_FootCandle;
            //Photosensor_FootCandle =Photosensor_FootCandle*10.763;

            Photosensor_On_Off_Decision = (float) Photosensor_Set_FC;
            if (Photosensor_FootCandle <= Photosensor_On_Off_Decision)
                {
                Pin_Detect_Dark = 1; //Photo_feedback_pin = 1;
                }

            Photosensor_On_Off_Decision = (float) Photosensor_Set_FC_Lamp_OFF;
            Negetive_Foot_Candle_Ratio();
            if (Photosensor_FootCandle >= Photosensor_On_Off_Decision)
                {
                if (Make_Lamp_Off == 1)
                    {
                    Pin_Detect_Dark = 0; //Photo_feedback_pin = 0;
                    Make_Lamp_Off = 0;
                    }
                }
            if (Pin_Detect_Dark == 1)
                {
                Photo_feedback_pin = 1; //for analog photosensor
                }
            else
                {
                Photo_feedback_pin = 0;
                }
            }
        }

    }

void ADCInit(void)
    {

    AD1CON1 = 0x8004;

    // CSCNA disabled; NVCFG0 AVSS; PVCFG AVDD; ALTS disabled; BUFM disabled; SMPI Generates interrupt after completion of every sample/conversion operation; OFFCAL disabled; BUFREGEN disabled; 

    AD1CON2 = 0x00;

    // SAMC 0; EXTSAM disabled; PUMPEN disabled; ADRC FOSC/2; ADCS 9; 

    AD1CON3 = 0x52F; //64tcy need to do

    // CH0SA AN0; CH0SB AN0; CH0NB AVSS; CH0NA AVSS; 

    AD1CHS = 0x00;

    // CSS26 disabled; CSS25 disabled; CSS24 disabled; CSS30 disabled; CSS29 disabled; CSS28 disabled; CSS27 disabled; 

    AD1CSSH = 0x00;

    // CSS9 disabled; CSS8 disabled; CSS7 disabled; CSS6 disabled; CSS5 disabled; CSS4 disabled; CSS3 disabled; CSS2 disabled; CSS15 disabled; CSS1 disabled; CSS14 disabled; CSS0 disabled; CSS13 disabled; CSS12 disabled; CSS11 disabled; CSS10 disabled; 

    AD1CSSL = 0x00;

    // DMABL Allocates 1 word of buffer to each analog input; 

    AD1CON4 = 0x00;

    // ASEN disabled; WM Legacy operation; ASINT No interrupt; CM Less Than mode; BGREQ disabled; CTMREQ disabled; LPEN disabled; 

    AD1CON5 = 0x00;

    // CHH9 disabled; CHH8 disabled; CHH7 disabled; CHH6 disabled; CHH5 disabled; CHH4 disabled; CHH3 disabled; CHH2 disabled; CHH1 disabled; CHH0 disabled; CHH11 disabled; CHH10 disabled; CHH13 disabled; CHH12 disabled; CHH15 disabled; CHH14 disabled; 

    AD1CHITL = 0x00;

    // CTMEN24 disabled; CTMEN30 disabled; CTMEN29 disabled; CTMEN28 disabled; CTMEN25 disabled; 

    AD1CTMENH = 0x00;

    // CTMEN5 disabled; CTMEN6 disabled; CTMEN7 disabled; CTMEN8 disabled; CTMEN9 disabled; CTMEN12 disabled; CTMEN13 disabled; CTMEN10 disabled; CTMEN0 disabled; CTMEN11 disabled; CTMEN1 disabled; CTMEN2 disabled; CTMEN3 disabled; CTMEN4 disabled; CTMEN14 disabled; CTMEN15 disabled; 

    AD1CTMENL = 0x00;

    // AD1RESDMA 0; 

    AD1DMBUF = 0x00;

    // VBG2EN disabled; VBG6EN disabled; VBGEN disabled; 

    ANCFG = 0x00;

    //AD1CON2bits.BUFREGEN=1;

    }

void ADCStart(void)
    {
    AD1CON1bits.SAMP = 1;
    }


/////////////////////////////////////////////////////////////////

void Photocell_ON_OFF_Decision(void)
    {
    volatile float MV_ARRAY[15] = {0.0514, 0.076, 0.189, 0.247, 0.286, 0.47, 0.74, 1.073, 1.634, 2.245, 2.88};
    volatile float FC_ARRAY[15] = {0.5, 1.2, 2.5, 3.0, 3.75, 5.5, 7.5, 11.0, 15.0, 18.0, 25.0};
    volatile float f_temp = 0.0, FC_temp = 0.0, photosensor_temp = 0.0;
    volatile char v = 0;

    for (v = 0; v < 11; v++)
        {
        if ((photosensor <= MV_ARRAY[v + 1]) && (photosensor >= MV_ARRAY[v + 0]))
            {
            f_temp = (MV_ARRAY[v + 1] - MV_ARRAY[v + 0]);
            FC_temp = FC_ARRAY[v + 1] - FC_ARRAY[v + 0];

            FC_temp = (FC_temp / f_temp);
            photosensor_temp = photosensor - MV_ARRAY[v + 0];
            //photosensor= photosensor*1000;
            Photosensor_FootCandle = (FC_temp * photosensor_temp) + FC_ARRAY[v + 0];
            }
        }
    if (photosensor < 0.0514)
        Photosensor_FootCandle = 0;
    if (photosensor > 2.88)
        Photosensor_FootCandle = 25;

    if (DIGITAL0_ANALOG1_SELECTION == 0) //DIGITAL photosensor ==0
        {
        if (photosensor < 1.5)
            {
            Photosensor_FootCandle = 0;
            }
        }

    }

void Negetive_Foot_Candle_Ratio(void)
    {
    unsigned long f_local = 0;
    f_local = (unsigned long) Photosensor_FootCandle;

    if (Photosensor_Set_FC_Lamp_OFF < Photosensor_Set_FC)
        {
        if (f_local <= Photosensor_Set_FC_Lamp_OFF)
            {
            Make_Lamp_Off = 1;
            Detect_Off_Condition = 1;
            }
        if ((f_local <= Photosensor_Set_FC)&&(f_local > Photosensor_Set_FC_Lamp_OFF)&&(Detect_Off_Condition == 1))
            {
            Make_Lamp_Off = 1;
            }
        if (f_local > Photosensor_Set_FC)
            {
            Make_Lamp_Off = 1;
            Detect_Off_Condition = 0;
            }
        }
    else
        {
        Make_Lamp_Off = 1;
        }
    }
