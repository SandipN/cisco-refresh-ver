#ifndef ADC_VI
#define ADC_VI

extern unsigned int lamp_lumen;
extern unsigned char Photo_feedback_pin;
extern unsigned char Photocell_Off_Condition;
extern volatile unsigned int photocell_off_Cnt;
extern unsigned int Photocell_Off_Time;
extern unsigned char DIGITAL0_ANALOG1_SELECTION;
extern float Photosensor_Set_FC, Photosensor_Set_FC_Lamp_OFF;
extern volatile float Photosensor_FootCandle;

void ADC_Process(void);
void ADCInit(void);
void ADCStart(void);
void delay(unsigned int);
#endif
