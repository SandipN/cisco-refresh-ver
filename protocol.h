/****************************************protocol.h**************************/
/************************ HEADERS **********************************/

#ifndef __PROTOCOL_H_
#define __PROTOCOL_H_

#include "generic.h"

/************************defines for SLC *****************/
#define FREE_JOINING_ENABLE


#define CMD			                  'C'
#define DATA		                  'D'
#define SEND_DIAGNOSIS                'P'
#define SEND_FAULT_PARA	              'R'
#define AMSTATUS	                  'Y'
#define GET_NAK_PARA                  'M'
#define GET_SCH_PARA                  'Q'
#define GET_DIMM_SCH_PARA             'W'							// V6.1.10 // Master sch 15/04/11
#define GET_VERSION_PARA              'V'							// V6.1.10
#define DIM_SCHEDULE 		          'Z'							// SAN
#define GET_ADAPTIVE_DIMMING          'F'
#define GET_MOTION_DIMMING	          'B'						

#define GET_SLC_TEST_1                'd'            // 0x64			// V6.1.10
#define GET_SLC_TEST_2                'e'			   // 0x65			// V6.1.10
#define	GET_SLC_MIX_MODE_SCH_RESPONCE 'G'
#define AMSTATUS	    'Y'

#define RTC_TEST_DONE        0x66
#define EMT_TEST_DONE        0x67
#define LAMP_TEST_DONE       0x68
#define PHOTOCELL_TEST_DONE  0x69
#define CT_TEST_DONE  		 0x6A
#define GPS_CONFIG                0x2E
#define GET_GPS_CONFIG            0x2F


//query type
#define DATA_QUERY	     		0x01			//SAN
//#define DATA_QUERY	     		0x25
#define SET_RTC		     		0x02
#define GET_RTC		     		0x03
#define ERS_EEP          		0x09
#define LL_AMR           		0x06
#define SYNC_RTC         		0x07
#define CLOUDY_FLAG      		0x08
#define ERASE_TRIP_SLC   		0x11
#define EVENT_START      		0x12
#define EVENT_QUERY      		0x13
#define GET_MODE		 		0x14
#define SET_FAULT_PARA   		0x15
#define GET_FAULT_PARA   		0x16
#define SLC_DIAGNOSIS    		0x17
#define SET_NETWORK_PARA 		0x18
#define GET_NETWORK_PARA 		0x19
#define GET_SCH			 		0x20
#define SLC_TEST_QUERY_1 		0x21
#define SLC_TEST_QUERY_1_ACK 	0x22
#define SLC_TEST_QUERY_2 		0x23
#define SLC_TEST_QUERY_2_ACK 	0x24
#define PHOTOCELL_CONFIG          0x25
#define GET_DIMMING_SCH			  0x26			// V6.1.10	// Master sch 15/04/11
#define GET_VERSION_INFO          0x27			// V6.1.10
#define ADAPTIVE_DIMMING_PARA     0x30
#define GET_PHOTOCELL_CONFIG      0x2A	
#define GET_ADAPTIVE_DIMMING_PARA 0x31
#define MOTION_DETECT             0x32

#define SET_RF_NETWORK_PARA       0x33
#define CHANGE_RF_NETWORK_PARA    0x34

#define SLC_CONFIG_PARA           0x35
#define GET_SLC_CONFIG_PARA       0x36
#define SLC_DST_CONFIG_PARA		  0x37
#define GET_SLC_DST_CONFIG_PARA	  0x38
#define SLC_MIX_MODE_SCH          0x39
#define GET_SLC_MIX_MODE_SCH      0x3A
#define SLC_MMOD                  0x3B
#define CHANGE_KEY                0x3C
#define SET_LINK_KEY_SLC          0x3D
#define GET_LINK_KEY_SLC          0x3E
#define TEST_DIMMING              0x3F
#define SET_FOOT_CANDLE           0xC8     //added 21 Feb'2017
#define GET_FOOT_CANDLE           0xC9     //added 21 Feb'2017
#define RESET_CMD                 0xD4
#define EM_RESET_CMD              0xD7
#define CONF_CMD_IN_TEST_MODE     0xD9		//added for manufacturing lib
#define GET_CONF_CMD_IN_TEST_MODE 0xDA		//added for manufacturing lib
#define BROADCAST_RESPONSE        0xDB
#define ID_FRAME_STOP          	  0xD8

#define DIMMING_TEST_DONE    	  0x6B
#define SET_CISCO_CONFIG    	  0xF1
#define Rs485_DI2_CONFIG        0xE7
#define GET_Rs485_DI2_CONFIG    0xE8
#define GET_CISCO_CONFIG    	  0xEB
#define SET_LOG_CONF  	  		  0xEC
#define GET_LOG_CONF  	  		  0xED
#define MIX_SCHEDULE_RESET_CMD    0xD5
#define SET_CMD_DALI_AO_CONFIG     0xE9
#define GET_CMD_DALI_AO_CONFIG     0xEA
#define SET_AUTO_RECOVER_FAULT    0xF2
#define GET_AUTO_RECOVER_FAULT    0xEF
#define CURV_TYPE_SET             0xD2
#define CURV_TYPE_GET             0xD3


#define EE_AMR_ID   	1
#define EE_LOGICMODE	(EE_AMR_ID+2)
#define EE_DO			(EE_LOGICMODE+1)
#define EE_SCH_DAY      (EE_DO+1)			// V6.1.10
#define EE_MASTER_SCH_DAY (EE_SCH_DAY+1)

#define EE_Vr_Cal	(EE_MASTER_SCH_DAY+1)
#define EE_Ir_Cal   (EE_Vr_Cal + 4)      //104
#define EE_KW_Cal (EE_Ir_Cal + 4)      //108
#define EE_KWH		(EE_KW_Cal + 4)    //112
//////////////
#define EE_KW_Threshold		(EE_KWH + 4)    //112

#define EE_LOGRATE                      (EE_KW_Threshold + 4)
#define EE_PUSH_COMP                      (EE_LOGRATE + 1)
#define EE_TIMESLICE                      (EE_PUSH_COMP + 1)
#define EE_IPV6_AD  		(EE_TIMESLICE + 2)    //112

#define EE_PREV_MODE					(EE_IPV6_AD + 2)
#define EE_RTC_New_fault_logic_Enable	(EE_PREV_MODE + 1)
#define EE_FALUT_RECOVER		   (EE_RTC_New_fault_logic_Enable + 1)
#define EE_PUSH_ENERGY_DATA             (EE_FALUT_RECOVER + 1)
#define EE_CLIENT_ID                    (EE_PUSH_ENERGY_DATA + 1)
//////////////////////////
#define EE_LATITUDE (EE_CLIENT_ID + 6)         //116
#define EE_LONGITUDE (EE_LATITUDE + 4)   //120
#define EE_TIMEZONE (EE_LONGITUDE + 4)   //124
#define EE_BURN_HOUR (EE_TIMEZONE +4)    //128
#define EE_LAMP_STADY_CURRENT (EE_BURN_HOUR + 4)                          //132
#define EE_LAMP_STADY_CURRENT_SET (EE_LAMP_STADY_CURRENT + 4)             //136


#define EE_Vol_hi					  (EE_LAMP_STADY_CURRENT_SET + 4)     //140
#define EE_Vol_low					  (EE_Vol_hi + 2)                     //142
#define EE_Curr_Steady_Time			  (EE_Vol_low + 2)                    //144
#define EE_Per_Val_Current			  (EE_Curr_Steady_Time + 2)           //146
#define EE_Lamp_Fault_Time			  (EE_Per_Val_Current + 2)            //148
#define EE_Lamp_off_on_Time			  (EE_Lamp_Fault_Time + 2)            //150
#define EE_Lamp_faulty_retrieve_Count (EE_Lamp_off_on_Time + 2)           //152
#define EE_Sunset_delay				  (EE_Lamp_faulty_retrieve_Count + 2) //154
#define EE_Sunrise_delay              (EE_Sunset_delay + 2)               //156
#define EE_Broadcast_time_delay		  (EE_Sunrise_delay + 2)              //158
#define EE_Unicast_time_delay		  (EE_Broadcast_time_delay + 2)       //160
#define EE_Lamp_lock_condition	      (EE_Unicast_time_delay + 2)         //162

#define EE_analog_input_scaling_high_Value (EE_Lamp_lock_condition + 2)   //164
#define EE_analog_input_scaling_low_Value (EE_analog_input_scaling_high_Value + 2)  //166
#define EE_desir_lamp_lumen (EE_analog_input_scaling_low_Value + 2)       //168
#define EE_lumen_tollarence (EE_desir_lamp_lumen + 2)					  //170
#define EE_ballast_type (EE_lumen_tollarence + 1)						  //172
#define EE_adaptive_light_dimming (EE_ballast_type + 1)					  //173

#define EE_Motion_pulse_rate (EE_adaptive_light_dimming + 1)			  //174
#define EE_Motion_dimming_percentage (EE_Motion_pulse_rate + 1)			  //175
#define EE_Motion_dimming_time (EE_Motion_dimming_percentage + 1)		  //176
#define EE_Motion_group_id (EE_Motion_dimming_time + 2)					  //178
#define EE_dim_applay_time (EE_Motion_group_id + 1)						  //179
#define EE_dim_inc_val (EE_dim_applay_time + 2)							  //180
#define EE_Motion_normal_dimming_percentage (EE_dim_inc_val + 1)		  //181

#define EE_Day_light_harvesting_start_offset (EE_Motion_normal_dimming_percentage + 1)  // 182
#define EE_Day_light_harvesting_stop_offset (EE_Day_light_harvesting_start_offset + 2)  // 184

#define EE_IDIMMER_EN (EE_Day_light_harvesting_stop_offset + 2)           // 186
#define EE_IDIMMER_ID (EE_IDIMMER_EN + 1)								  // 187
#define EE_IDIMMER_MAC (EE_IDIMMER_ID + 2)                                // 189

#define EE_Valid_DCU (EE_IDIMMER_MAC + 8)								  // 197
#define EE_Lamp_type (EE_Valid_DCU + 1)									  // 198
#define EE_SLC_DST_En (EE_Lamp_type + 1)								  // 199
#define EE_SLC_DST_Start_Rule (EE_SLC_DST_En + 1)						// 199
#define EE_SLC_DST_Start_Month (EE_SLC_DST_Start_Rule + 1)						// 199
#define EE_SLC_DST_Start_Time (EE_SLC_DST_Start_Month + 1)				// 200
#define EE_SLC_DST_Stop_Rule (EE_SLC_DST_Start_Time + 1)				// 201
#define EE_SLC_DST_Stop_Month (EE_SLC_DST_Stop_Rule + 1)					// 202
#define EE_SLC_DST_Stop_Time (EE_SLC_DST_Stop_Month + 1)					// 202
#define EE_SLC_DST_Time_Zone_Diff (EE_SLC_DST_Stop_Time + 1)			// 203
#define EE_Time_change_dueto_DST (EE_SLC_DST_Time_Zone_Diff + 4)        // 207
#define EE_SLC_DST_Rule_Enable (EE_Time_change_dueto_DST + 1)           // 208
#define EE_SLC_DST_Start_Date (EE_SLC_DST_Rule_Enable + 1)						// 199
#define EE_SLC_DST_Stop_Date (EE_SLC_DST_Start_Date + 1)				// 201
#define EE_Auto_event (EE_SLC_DST_Stop_Date + 1)                        // 202
#define EE_SLC_New_Manula_Mode_Val (EE_Auto_event + 1)                  // 203
#define EE_Link_Key (EE_SLC_New_Manula_Mode_Val + 1)                 // 204
#define EE_SLC_New_Manual_Mode_Timer (EE_Link_Key + 20)              // 224
#define EE_Photocell_steady_timeout_Val (EE_SLC_New_Manual_Mode_Timer + 2) // 226
#define EE_photo_cell_toggel_counter_Val (EE_Photocell_steady_timeout_Val + 1)   //227 
#define EE_photo_cell_toggel_timer_Val   (EE_photo_cell_toggel_counter_Val + 1)  //228
#define EE_Photo_cell_Frequency           (EE_photo_cell_toggel_timer_Val + 1)   //229
#define EE_Photocell_unsteady_timeout_Val (EE_Photo_cell_Frequency + 1)           //230
#define EE_Schedule_offset                (EE_Photocell_unsteady_timeout_Val + 2) //232
#define EE_Motion_Detect_Timeout          (EE_Schedule_offset + 1)                //233
#define EE_Motion_Broadcast_Timeout       (EE_Motion_Detect_Timeout + 1)          //234
#define EE_Motion_Sensor_Type             (EE_Motion_Broadcast_Timeout + 1)       //235      


#define ASTRO_EE_SCHEDULE	  EE_Motion_Sensor_Type + 1 									// V6.1.10
#define EE_SCHEDULE           ASTRO_EE_SCHEDULE+15									// V6.1.10
#define EE_MASTER_SCHEDULE    EE_SCHEDULE + 50//350						// V6.1.10 (7(days) * 6(no of loc) * 10(no of sch per day) = 350)
#define EE_MIX_SCHEDULE       EE_MASTER_SCHEDULE + 60//450				// V6.1.10
#define EE_FRESH_LOCATION_1	  EE_MIX_SCHEDULE + 980                 //  
#define EE_FRESH_LOCATION_2   EE_FRESH_LOCATION_1 + 1
#define EE_LAST_LOC  EE_FRESH_LOCATION_2 + 1 //1432
////////////////////////////////////
#define EE_GPS_Read_Enable				  (EE_LAST_LOC+ 1)	
#define EE_GPS_Rescan					  (EE_GPS_Read_Enable + 1)
#define EE_GPS_Wake_timer                 (EE_GPS_Rescan +1)
#define EE_No_of_setalite_threshold       (EE_GPS_Wake_timer + 2)
#define EE_Setalite_angle_threshold		   (EE_No_of_setalite_threshold + 1)
#define EE_setalite_Rssi_threshold   (EE_Setalite_angle_threshold  + 1)
#define EE_Maximum_no_of_Setalite_reading  (EE_setalite_Rssi_threshold  + 1)
#define EE_Sbas_setalite_Rssi_threshold  (EE_Maximum_no_of_Setalite_reading +1)
#define EE_Check_Sbas_Enable              (EE_Sbas_setalite_Rssi_threshold  +  1)
#define EE_HDOP_Gps_threshold  				(EE_Check_Sbas_Enable + 1)
#define EE_Auto_Gps_Data_Send       (EE_HDOP_Gps_threshold +4)
#define EE_IDFRAME 					(EE_Auto_Gps_Data_Send +1)
#define EE_POWERONCNT 				(EE_IDFRAME +1)
#define EE_NICIPV6 					(EE_POWERONCNT +4)
#define EE_Photosensor_Set_FC_Lamp_OFF		  (EE_NICIPV6 + 39)                     //parvez
#define EE_Photosensor_Set_FC	      (EE_Photosensor_Set_FC_Lamp_OFF + 4)          //parvez
#define EE_DIGITAL0_ANALOG1_SELECTION       (EE_Photosensor_Set_FC + 4)             //parvez
#define EE_Photocell_Off_Time 				(EE_DIGITAL0_ANALOG1_SELECTION + 1)     //parvez
#define EE_KW_Cal_120					(EE_Photocell_Off_Time + 2)
#define EE_idframe_frequency			(EE_KW_Cal_120 + 4)
#define EE_ID_Frame_reset_time			(EE_idframe_frequency + 1)
#define EE_Normal_reset_time			(EE_ID_Frame_reset_time + 1)
#define EE_ID_frame_timeout				(EE_Normal_reset_time + 1)
#define EE_ENT_BOOT                      (EE_ID_frame_timeout + 2)
#define EE_BHIP_IDFRAME                    (EE_ENT_BOOT + 1)
#define EE_AMR_ID_4byte                    (EE_BHIP_IDFRAME + 1)
#define EE_BACKWARD_COMP					(EE_AMR_ID_4byte + 4)
#define EE_ADDR_DIM_DRIVER_INFO			(EE_BACKWARD_COMP + 1)
#define EE_RS485_0_Or_DI2_1                 (EE_ADDR_DIM_DRIVER_INFO + 1)
#define EE_CURV_TYPE_GET                 (EE_RS485_0_Or_DI2_1 + 1)

//#define EE_RTC_New_fault_logic_Enable	(EE_PREV_MODE + 1)
//#define EE_FALUT_RECOVER		   (EE_RTC_New_fault_logic_Enable + 1)
//#define EE_PUSH_ENERGY_DATA             (EE_FALUT_RECOVER + 1)
#define EE_END_LOC                      (EE_CURV_TYPE_GET + 1)
extern unsigned char DimmerDriverSelectionProcess, DimmerDriver_SELECTION;
extern volatile unsigned char CheckAoDimmerForDetectionTimmer, CheckAoDimmerForDetection;
extern float full_brightPower, full_brightPower_at_30;
extern unsigned char dali_support;
/////////////////////////////////////////
#define NIC_RESET_PIN	LATBbits.LATB12
#define LED_2	LATBbits.LATB13

//#define LED_1						PORTBbits.RB13
extern unsigned char RS485_0_Or_DI2_1;
extern unsigned char Cmd_Rcv_To_Change_Curv_Ype;
extern volatile unsigned char Photo_feedback;
extern unsigned char Rtc_set_Replay, Get_rtc_Replay, Send_Data, Send_Data_push;
extern unsigned int Frm_no;
extern unsigned long int amr_id;
extern unsigned char temp_arr[];
extern ALLTYPES pulseCounter;
extern unsigned char Eep_Ers;
extern unsigned char connection_ok;
extern unsigned char send_id_frame;
extern unsigned char Id_frame_received;
extern volatile float lamp_burn_hour;
extern unsigned char GPS_Rescan;
extern unsigned char GPS_Delay_timer;
extern unsigned char GPS_Data_throw_status;
extern unsigned char Time_set_by_GPS;
extern unsigned char need_Average_value_Gps;
extern unsigned char need_to_send_lat_long;
extern unsigned int Broadcast_time_delay;
extern unsigned int Unicast_time_delay;
extern unsigned char GPS_Read_Enable;
extern unsigned char send_rf_event;
extern unsigned char no_of_pending_event;
extern unsigned char broadcast_read_req;
extern unsigned char stop_event;
extern volatile unsigned int rf_event_send_counter;
extern unsigned char read_event_flag;
extern unsigned char send_all_rf_event;
extern unsigned char send_get_mode_replay;
extern unsigned char send_get_mode_replay_auto;
extern unsigned char Send_Fault_Para_Replay;
extern unsigned char Send_Slc_Diagnosis;
extern unsigned char Diagnosis_DI;
extern volatile unsigned int RF_communication_check_Counter;
extern unsigned int ID_frame_timeout;
extern int Sunset_delay;
extern int Sunrise_delay;
extern volatile unsigned int RF_network_time_out;
extern unsigned char RF_network_faulty;
extern unsigned char Send_Network_Para_Replay;
extern unsigned char Send_Get_Sch_Replay;
extern unsigned char delet_no;
extern unsigned char Slc_test_query_1;
extern volatile unsigned char test_query_1_send_timer;
extern unsigned char send_SLC_test_query_1;
extern unsigned char send_SLC_test_query_2;
extern float test_voltage;
extern float test_current;
extern unsigned int test_emt_int_count;
extern unsigned int test_Kwh_count;
extern unsigned char photocell_test_dicision;
extern unsigned char SLC_Test_Query_2_received;

extern unsigned char Send_Get_Master_Sch_Replay; // V6.1.10			// Master sch 15/04/11
extern unsigned char Sch_week_day; // V6.1.10
extern volatile unsigned char Get_Sch_timer; // V6.1.10
extern volatile unsigned char Get_Master_Sch_timer; // V6.1.10
extern unsigned char Send_Get_Version_info_Replay; // V6.1.10
extern unsigned char get_sch_day; // V6.1.10
extern unsigned char sch_day_send[]; // V6.1.10
extern unsigned char sch_day[]; // V6.1.10
extern unsigned char sch_Master_day[]; // V6.1.10
extern unsigned char sch_Master_day_send[]; // V6.1.10

extern unsigned char Protec_lock;
extern unsigned char send_data_direct;
extern unsigned char Motion_broadcast_miss_counter;
extern unsigned long int Motion_broadcast_Receive_counter, Temp_Motion_broadcast_Receive_counter;

extern int Day_light_harvesting_start_offset;
extern int Day_light_harvesting_stop_offset;

extern long Day_light_harvesting_start_time;
extern long Day_light_harvesting_stop_time;

extern unsigned char idimmer_en;
extern unsigned int idimmer_id;
extern unsigned char send_idimmer_id_frame;
extern volatile unsigned int Send_dimming_cmd;
extern unsigned char idimmer_Id_frame_received;
extern unsigned char send_idimmer_Val_frame;
extern unsigned char dimming_cmd_ack;
extern unsigned char idimmer_trip_erase;
extern unsigned char idimmer_dimval;
extern unsigned char current_dimval;
extern unsigned char dimming_cmd_cnt;
extern volatile unsigned char send_id_frame_time; 
extern unsigned char read_dim_val_timer;
extern volatile unsigned long int read_dim_val_timer1;
extern unsigned char read_dim_val_idimmer;
extern float utcOffset;

////////// free joining ///////
extern unsigned char ID_Frame_Send_counter;
extern unsigned char change_SLC_channel;
extern unsigned char Extended_pan[];
extern unsigned char SLC_channel;
extern unsigned char Test_application_flag;
extern unsigned char Valid_DCU;
extern unsigned char joining_time;
extern unsigned char Network_found;
extern unsigned char SLC_joinig_retry_counter;
extern unsigned char Send_Get_Slc_Config_para_Replay;
extern unsigned char broadcast_received;
extern volatile unsigned char broadcsat_received_timer;
extern unsigned char Network_serch_sec;
extern unsigned char channel_shift;
////////// free joining ///////

extern unsigned char Read_db_val;
extern unsigned char Slc_db;

extern unsigned char SLC_DST_En;
extern unsigned char SLC_DST_Start_Rule;
extern unsigned char SLC_DST_Start_Month;
extern unsigned char SLC_DST_Start_Time;
extern unsigned char SLC_DST_Stop_Rule;
extern unsigned char SLC_DST_Stop_Month;
extern unsigned char SLC_DST_Stop_Time;
extern float SLC_DST_Time_Zone_Diff;
extern unsigned char SLC_DST_Rule_Enable;
extern unsigned char SLC_DST_Start_Date;
extern unsigned char SLC_DST_Stop_Date;

extern unsigned char Time_change_dueto_DST;
extern unsigned char SLC_DST_R_Start_Date;
extern unsigned char SLC_DST_R_Stop_Date;

extern unsigned int old_year;

extern unsigned char SLC_Mix_Sch_Loc_No;
extern unsigned char Send_Get_Mix_Mode_Sch;
extern unsigned char Get_Mix_mode_Sch_Loc;
extern unsigned char L_No;
extern unsigned char Mix_photo_override;
extern unsigned char Lamp_type;
extern unsigned char Auto_event;
extern unsigned char send_data_direct_counter;
extern unsigned char send_event_with_delay;
extern volatile unsigned long int send_event_delay_timer;
extern unsigned char SLC_New_Manula_Mode_En;
extern unsigned char SLC_New_Manula_Mode_Val;
//extern unsigned char Link_Key[];
extern unsigned char Send_Link_key;
extern volatile unsigned char change_Link_key_timer;
extern unsigned char change_Link_key;
extern unsigned int SLC_New_Manual_Mode_Timer;
extern volatile unsigned long int SLC_New_Manual_Mode_counter;
extern unsigned char Module_Type;
extern unsigned char Temp_Slc_Multi_group;
extern BYTE_VAL Group_val1;

extern unsigned char Cali_Cnt;
extern unsigned int vRCON;
////////////////////////////// Calibration //////

extern float Vr_Cal, Ir_Cal, KW_Cal, KW_Cal_120;
extern float Calculated_Frequency;
extern unsigned char Read_KWH_Value;
extern unsigned char Send_Kw_calibration_120;
extern unsigned char KW_Calibration_120;
extern volatile unsigned int SSN_CNT;
extern unsigned char Read_Em_Calibration;
extern unsigned char Send_Kw_calibration;
extern unsigned char KW_Calibration;
extern unsigned char Current_calibration;
extern unsigned char Send_Current_calibration;
extern unsigned char Send_Voltage_calibration;
extern unsigned char Voltage_calibration;
extern unsigned char calibmode;
extern unsigned char DI_GET_LOW, DI_GET_HIGH;
extern float Analog_Result;
extern float ADC_Result_100, ADC_Result_50, ADC_Result_0;
extern unsigned char Start_dimming_test;
extern unsigned char Test_dimming_val;
/////////

extern volatile unsigned int RTC_power_on_Logic_Timer;
extern unsigned char check_RTC_logic_cycle;
extern unsigned char RTC_need_to_varify;
extern unsigned char Lamp_Balast_fault_Remove_Time;
extern volatile unsigned int Lamp_Balast_fault_Remove_Cnt;
////////// idimmer ///////
void send_fream(void);
void checkfream(unsigned char *temp);
void Send_data_RF1(unsigned char *data, unsigned char len, unsigned char only_DCU);
void store_data_in_mem(void);
void get_data_from_mem_and_send(void);
void Write_NI_Para(unsigned int id);
void Check_SLC_Test_query_1(void);
void Send_data_RF_idimmer(unsigned char *data, unsigned char len);
void delet_all_data(void);
void Check_Slc_Test_1(void);
////////// idimmer ///////

void Configure_Slc_Mix_Mode_Sch(unsigned char *temp_str);
void unCompress_Mix_sch(unsigned char loc_no);
unsigned char check_group(unsigned char temp_G1);
unsigned char check_multi_group(unsigned char temp_G1);
void Fill_Slc_Comp_Mix_Mode_schedule_compress(unsigned int i, unsigned int j);

#endif

